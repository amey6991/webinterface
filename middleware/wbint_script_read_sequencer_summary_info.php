<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_sequencer_summary_info.php
  Description : This script will get the information of sequencer which summary csv is created , and we will read the summary csv from the local storage
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script To READ the Summary CSV file of each sequencer Runs From the Local Path at ".$dtCurrentDateTime."\r\n";
fLogger(18,$sLogMsg);

/*
  Define the config values , default values here 
*/
  
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;

$sRemoteCopyPathForCSV = sRemoteCopyPathForSummaryCSV;


/*
  Get the sequencer which need to read the RunCompletionStatus.xml
*/
$aSequencers = fGetSequencerToReadParsedInterOpSummaryCSV();
//echo "<pre>";print_r($aSequencers);exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found to copy the sequencer summary csv into local machine. Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(18,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      We will loop for the sequencer directory and get the CompletedJobInfo.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencersSummaryInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqRemoteServerPath = $aValue['seq_parse_file_path'];
        $iParseStatus = $aValue['seq_parse_status'];
        $sLocalPath = $aValue['seq_local_filepath'];
        $sLocalFileName = $aValue['seq_parsed_filename'];
        

        /*
          we are going to rename the Sequencer CompletedJobInfo.xml filename for our local storage 
        */
        
        $bFileExist = file_exists($sLocalPath);
        
        if($bFileExist==true){
          $aSummaryInfo = fReadSummaryCSVInfo($sLocalPath,$iSeqType);
          if(!empty($aSummaryInfo)){
              $sSummaryJSON = json_encode($aSummaryInfo);
              //!! AVG of ClusterPassingFilter
              $aClusterPF = $aSummaryInfo['clusterPF'];
              $iTotalPFValue = 0;
              $iNumberOfRead=0;
              foreach ($aClusterPF as $key => $value) {
                $iNumberOfRead++;
                $aValueClustar = explode('+/-', $value);
                $iClusterPF = (float)$aValueClustar[0];
                $iTotalPFValue = $iTotalPFValue+$iClusterPF;
              }
              $iClusterPFValue = $iTotalPFValue/$iNumberOfRead;

              //!! AVG of Density
              $aDensity = $aSummaryInfo['aDensity'];
              $iTotalDensityValue = 0;
              $iNumberOfRead=0;
              foreach ($aDensity as $key => $value) {
                $iNumberOfRead++;
                $aValueDensity = explode('+/-', $value);
                $iDensity = (float)$aValueDensity[0];
                $iTotalDensityValue = $iTotalDensityValue+$iDensity;
              }
              $iDensityValue = $iTotalDensityValue/$iNumberOfRead;

              
              $aSequencersSummaryInfo[] = array(
                  'seq_directory_id'=>$iSeqDirectoryID,
                  'seq_summary_json'=>$sSummaryJSON,
                  'avg_q30' => $aSummaryInfo['avg_q30'],
                  'clusterPF' => $iClusterPFValue,
                  'density' => $iDensityValue,
                  'seq_type' => $iSeqType
                );
          }else{
            // Log with a message
            $dtCurrentDateTime = date('Y-m-d H:i:s');
            $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Data found in Summary CSV in Path ({$sLocalPath}) For Sequencer ID $iSeqDirectoryID Runs at ".$dtCurrentDateTime."\r\n";
            fLogger(18,$sLogMsg);
          }
        }
    }
  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Save the info of Summary CSV file for sequencer
  
  */

  if(!empty($aSequencersSummaryInfo)){
      foreach ($aSequencersSummaryInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['seq_directory_id'];
          $iSeqType = $aValue['seq_type'];
          $sSeqSummaryJson = $aValue['seq_summary_json'];
          $sQ30 = $aValue['avg_q30'];
          $sPassingFilter = $aValue['clusterPF'];
          $sdensity = $aValue['density'];

          $iSeqInteropType = 2;
          $iResult = fAddInterOpCSVJSON($iSeqDirectoryid,$iSeqInteropType,$sSeqSummaryJson);
          //!! Update the Q30 , clusterPF and Density for Sequencer
          fUpdateSequencerMasterInfo($iSeqDirectoryid,$sQ30,$sPassingFilter,$sdensity);
          
          $bResult = fCheckSequencerCompleted($iSeqDirectoryid,$iSeqType);
          if($bResult==true){
            fDisableParseInterOpStatus($iSeqDirectoryid);
          }else{
            fDisableParseInterOpStatus($iSeqDirectoryid,3);
            fAddSequencerParseInterOpStatus($iSeqDirectoryid,1);
          }

          if($iResult >0 ){
              $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Summary of Sequence as CSV file Read successfully and it JSON Stored in database.".date('Y-m-d H:i:s')."\r\n";
              fLogger(18,$sLogMsg);
          }else{
              $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Summary of Sequence as CSV file Read successfully and it JSON NOT stored in database.".date('Y-m-d H:i:s')."\r\n";
              fLogger(18,$sLogMsg);
          }         
          
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found to COPY Summary CSV in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(18,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------NoScript for copy Summary CSV file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(18,$sLogMsg);
?>