<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_sequencer_summary_info.php
  Description : This script will get the information of sequencer which summary csv is created , and we will copy the CSV into our machine.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script To get the csv file of each sequencer Runs and copied into local machine at ".$dtCurrentDateTime."\r\n";
fLogger(16,$sLogMsg);

/*
  Define the config values , default values here 
*/
  
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;

$sRemoteCopyPathForCSV = sRemoteCopyPathForSummaryCSV;


/*
  Get the sequencer which need to read the RunCompletionStatus.xml
*/
$aSequencers = fGetSequencerForParsedInterOpCSV();
//echo "<pre>";print_r($aSequencers);exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found to copy the sequencer summary csv into local machine. Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(16,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the summary CSV file from Remote directory to our local directory
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy Summary CSV file for Sequencer. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(16,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy Summary CSV file for Sequencer. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(16,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy Summary CSV file for Sequencer. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(16,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(16,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the CompletedJobInfo.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencersSummaryInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_parse_status'];
        $sRemotePath = $aValue['seq_parse_file_path'];
        $sRemoteFileName = $aValue['seq_parsed_filename'];
        

        /*
          we are going to rename the Sequencer CompletedJobInfo.xml filename for our local storage 
        */
        
        $sRemoteFileName = $sSeqDirectoryName.'_parsesummary_'.date('YmdHis').'.csv';
        $sMainLocalPathSummaryCSV = '/var/www/WebInterface/sequencer_summary/'.$sRemoteFileName;
        
        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sRemotePath);
        if($bFileExists==true){
            $bSCopy = ssh2_scp_recv($connection, $sRemotePath, $sMainLocalPathSummaryCSV);
            

            if($bSCopy==true){
                    // Collect the information into array so that later we can read the xml
                    $iCopyFiles++;
                    $aSequencersSummaryInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'server_path' => $sRemotePath,
                                                  'local_path' => $sMainLocalPathSummaryCSV,
                                                  'new_filename' => $sRemoteFileName,
                                                  'seq_type'=> $iSeqType
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Summary CSV file copied from ( $sRemotePath ) to our local with Path {$sMainLocalPathSummaryCSV} on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(16,$sLogMsg);
            }else{
                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping CompletedJobInfo.xml file from ( $sRemotePath ) to our local PATH ( $sMainLocalPathSummaryCSV ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(16,$sLogMsg);
            }
        }else{
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------CompletedJobInfo.xml file is NOT found in the ( $sRemotePath ) Server Path on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(16,$sLogMsg);
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Save the info of Summary CSV file for sequencer
  
  */
    if(!empty($aSequencersSummaryInfo)){
      foreach ($aSequencersSummaryInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqType = $aValue['seq_type'];
          //$iSeqFileType = 8; // 8 for CSV file of summary
          // $bResult = fCheckSequencerCompleted($iSeqDirectoryid,$iSeqType);
          // if($bResult==true){
          //     fDisableParseInterOpStatus($iSeqDirectoryid);
          // }else{
            
            fDisableParseInterOpStatus($iSeqDirectoryid,2);
            fDisableParseInterOpStatus($iSeqDirectoryid,3);
            fAddSequencerParseInterOpStatus($iSeqDirectoryid,3);
          //}
          
          fUpdateSequencerInterOpMasterInfo($iSeqDirectoryid,1,$sSeqLocalDirectoryPath);
        
      }
    }
}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found to COPY Summary CSV in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(16,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------Script for Summary CSV file Copy from Remote to Local sequencer directory is Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(16,$sLogMsg);
?>