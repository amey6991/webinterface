<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_completionjobstatus_xml.php
  Description : This script is going to read the RunCompletionStatus.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';
require_once __DIR__.'/../'.'funEmails.php';

require_once PATH_TO_ROOT.'sendEmail.php';

$sSenderEmailId = EMAIL_SENDER_ID;
$sSenderName= EMAIL_SENDER_NAME;

$aDefaultList = fGetDefualtEmailList();
$aEmailFeature = getAllEmailFeature();

$bEmailErrorFeature = false;
$bEmailFinishFeature = false;

foreach ($aEmailFeature as $key => $aFeature) {
  if($aFeature['email_feature_name']=='Email on Sequencer Run Error'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailErrorFeature=true;
        break;
      }else{
        $bEmailErrorFeature = false;
        break;
      }
  }
  if($aFeature['email_feature_name']=='Email on Sequencer Run Fiinished'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailFinishFeature=true;
        break;
      }else{
        $bEmailFinishFeature = false;
        break;
      }   
  }
}

$sLogMsg = "---SRT-- Script is started to read RunCompletionStatus.xml file for each sequencer at ".date('Y-m-d H:i:s');
fLogger(21,$sLogMsg);

$iSeqFileType =6;

//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadRunCompletionJobXml();
$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the RunCompletionStatus.xml files. at ".date('Y-m-d H:i:s');
fLogger(21,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
	$aRunCompleteInfo = array();
	$iSequencerDirID = $value['seq_directory_id'];
	$sSeqDirname = $value['seq_dir_name'];
  	$sSeqDirPath = $value['seq_server_path'];
  	$iSeqType = $value['seq_type'];
	
  	$aEmail = array(
  			'dir_name'=>$sSeqDirname ,
  			'dir_path'=>$sSeqDirPath ,
  			'seq_type'=>$iSeqType 
  		);
    $bError=false;
  	$sErrorMesage = "";
  	
  	$bSampleSheetScan = fCheckStatusForSequencer($iSequencerDirID,2);
  	//$bSampleSheetRead = fCheckStatusForSequencer($iSequencerDirID,5);
	$bRunInfoScan = fCheckStatusForSequencer($iSequencerDirID,8);
  	//$bRunInfoRead = fCheckStatusForSequencer($iSequencerDirID,9);
	$bRunParaScan = fCheckStatusForSequencer($iSequencerDirID,3);
  	//$bRunParaRead = fCheckStatusForSequencer($iSequencerDirID,6);
	$bDemultiplexScan = fCheckStatusForSequencer($iSequencerDirID,12);
    
    if($bSampleSheetScan==true){
        $sErrorMesage = 'We Reading RunCompletionStats.xml file but we do not Found the SampleSheet, Please check it manually';
        $bError=true;
    }elseif($bRunInfoScan==true){
        $sErrorMesage = 'We Reading RunCompletionStats.xml file but we do not Found the RunInfo.xml File, Please check it manually';
        $bError=true;
    }elseif($bRunParaScan==true){
        $sErrorMesage = 'We Reading RunCompletionStats.xml file but we do not Found the RunParameters.xml File, Please check it manually';
        $bError=true;
    }

    if($bError==true){
        echo "Error ".$iSequencerDirID."->".$sErrorMesage."</br>";
        $bEntry = fCheckErrorMasterEntryForSequencer($iSequencerDirID);
        if($bEntry==true){
          	$iError = fAddSequencerErrorOccured($iSequencerDirID,$iSeqType,$sErrorMesage);
      	    //!! Email When Error Occured
            $aEmailInfo =array('dir_name'=>$sSeqDirname,
                      'dir_path'=>$sSeqDirPath,
                      'seq_type' => $iSeqType,
                      'error'=>$sErrorMesage);
            if(!empty($aDefaultList) && $bEmailErrorFeature==true){
                foreach ($aDefaultList as $key => $aValue) {
                        $aReciepintName = array($aValue['user_name']);
                        $aReciepintEmail = array($aValue['user_email']);
                       
                        $iEmailTemplateFlag=EMAIL_SEQRUN_ERROR_TO_DEFAULT_LIST;
                        $aAttachement = array();
                        $iResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmailInfo,$aAttachement,$sSenderEmailId,$sSenderName);
              
                    if($iResult != 1){
                      $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN Got an ERROR , Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                       fLogger(21,$sLogMsg);
                    }
                }       
            }
        }
      }else{
			///! Get the local path of file
			$sFileName = $value['seq_local_path'];
			
			///! check if the file is exist or not in local path
			$bFileExist = file_exists($sFileName);

			///! if file exist , then
			if($bFileExist == true){
				///!!  call the function which will read the samplesheet
				$aRunCompleteInfo = ReadAndGetRunCompletionJobXmlInfo($sFileName);
				
				if(!empty($aRunCompleteInfo)){
					///!!! extract the info from array
					$aMasterInfo = $aRunCompleteInfo['master_info'];
					if(!empty($aMasterInfo)){
						if($aMasterInfo[1][1]=='CompletedAsPlanned'){
							$bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aMasterInfo);
						}else{
							//!! do something that will not procceed yo sequencer more.
							$sErrorMesage = $aMasterInfo[14][1];
							$iError = fAddSequencerErrorOccured($iSequencerDirID,$iSeqType,$sErrorMesage);
							if($iError > 0){
								$sLogMsg = "---XX-X-- RunCompletionStatus.xml Master information Found an Error, SEQUENCER is NOT COMPLETED at ".date('Y-m-d H:i:s');
								fLogger(21,$sLogMsg);	
							}

						}
					}
					
					$sRunCompletionInfoJSON = json_encode($aRunCompleteInfo['allXML']);

					///!!! add the information into database

					$iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sRunCompletionInfoJSON);
					if($iResult > 0){
						$iScanStatus = 10;
						$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
		                if($bResult){
		                      // function to change the sequencer directory status to 11 !!
		                      addSequncerMasterStatus($iSequencerDirID,11); //! 11 for we read the generateFastQRuns into database
		                }
						//!! log
						$sLogMsg = "--------- RunCompletionStatus.xml Master information Added in Database at ".date('Y-m-d H:i:s');
						fLogger(21,$sLogMsg);
					}else{
						///!! Error samplesheet master information can not be added
						//!! log
						$sLogMsg = "-xxx----- RunCompletionStatus.xml Master information Not Added in Database , Error occured at ".date('Y-m-d H:i:s');
						fLogger(21,$sLogMsg);
					}
				}		

			}else{
				///!! if File does'not Exist
				//!! log
				$sLogMsg = "--x-x-x-- RunCompletionStatus.xml File Not Fount in Local Storage , as stored in database , at ".date('Y-m-d H:i:s');
				fLogger(21,$sLogMsg);
			}
	}
	//!! Email The Users for Completed Run
	if($bEmailFinishFeature == true){
	  if(!empty($aDefaultList)){
          foreach ($aDefaultList as $key => $aValue) {
                   $aReciepintName = array($aValue['user_name']);
                   $aReciepintEmail = array($aValue['user_email']);
                   
                   $iEmailTemplateFlag=EMAIL_SEQRUN_FINISHED_TO_DEFAULT_LIST;
                   $aAttachement = array();
                  $iResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmail,$aAttachement,$sSenderEmailId,$sSenderName);
        
              if($iResult != 1){
                $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN finished Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                fLogger(5,$sLogMsg);
              }
          }       
      }
	}
}
$sLogMsg = "---OVR-- Script is Finished to reading RunCompletionStatus.xml file for each sequencer at ".date('Y-m-d H:i:s');
fLogger(21,$sLogMsg);
?>