<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_samplesheet_csv.php
  Description : This script is going to read the SampleSheet.csv file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
set_time_limit(120);
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$sLogMsg = "\r\n"."---SRT-- Script is started to read samplesheet.csv file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(23,$sLogMsg);

$iSeqFileType = 1;

// $Array = fGetParametersForSequencer(7);
//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadSamplesheet();
$iTotalSequencer = count($aSequencer);
//echo "<pre>";print_r($aSequencer);exit();
//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the samplesheet files. at ".date('y-m-d H:i:s')."\r\n";
fLogger(23,$sLogMsg);

if(!empty($aSequencer)){
	//! Loop the array of sequencer to get local storage path 
	foreach ($aSequencer as $key => $value) {
		
		$aSampleSheetInfo = array();
		$iSequencerDirID = $value['seq_directory_id'];
		$iSequencerType= $value['seq_type'];
		///! Get the local path of file
		$sFileName = $value['seq_local_path'];
		
		///! check if the file is exist or not in local path
		$bFileExist = file_exists($sFileName);
		
		///! if file exist , then
		if($bFileExist == true){
			///!!  call the function which will read the samplesheet with argument of sequencer type
			$aSampleSheetInfo = getReadSampleSheetInfo($sFileName,$iSequencerType);
			$sSampleSheetInfo = json_encode($aSampleSheetInfo);

			///!! extract the info from array
			
			if(!empty($aSampleSheetInfo)){
				//!! add the whole array (object) into master table
				$iMasterEntrySampleSheetData = addMasterSampleSheetData($iSequencerDirID,$sSampleSheetInfo);
				///!!! extract the Header info from array
				if(!empty($aSampleSheetInfo['Header'])){
					$sWorkflow = $aSampleSheetInfo['Header']['workflow'];
					$sApplication = $aSampleSheetInfo['Header']['application'];
					$sExperimentName = $aSampleSheetInfo['Header']['experiment_name'];
					$sInvestigatorName = $aSampleSheetInfo['Header']['investigator_name'];
					$dDate = $aSampleSheetInfo['Header']['date'];
					$dDate = date('Y-m-d H:i:s',strtotime($dDate));
					$fIEMFileVersion = $aSampleSheetInfo['Header']['fIEMFileVersion'];
					$sAssay = $aSampleSheetInfo['Header']['sAssay'];
					$sDescription = $aSampleSheetInfo['Header']['sDescription'];
					$sChemistry = $aSampleSheetInfo['Header']['sChemistry'];
				}
				///!!! extract the Reads info from array
				if(!empty($aSampleSheetInfo['Reads'])){
					$iReadCount = $aSampleSheetInfo['Reads'][0];
				}
				///!!! extract the Setting info from array
				if(!empty($aSampleSheetInfo['Setting'])){
					$sAdapter = $aSampleSheetInfo['Adapter'];
					$sAdapterRead2 = $aSampleSheetInfo['AdapterRead2'];
				}
				///!!! extract the Sample info from array
				if(!empty($aSampleSheetInfo['sampleinfo'])){
					$aSampleInfo = $aSampleSheetInfo['sampleinfo'];	
				}else{
					$aSampleInfo = array();
				}
			
				///!!! add the information into database


				$iSampleSheetMasterID = addSampleSheetInfoMaster($iMasterEntrySampleSheetData,$iSequencerDirID,$sWorkflow,$sApplication,$sExperimentName,$sInvestigatorName,$fIEMFileVersion,$iReadCount,$dDate,$sAssay,$sDescription,$sChemistry);
				
				if($iSampleSheetMasterID > 0){
					$iScanStatus = 2;
					$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
		            if($bResult==true){
		                
		                if(!empty($aSampleInfo)){
							foreach ($aSampleInfo as $key => $aValue) {
								
								$iSampleID = $aValue['sample_id'];
								$sSampleName = $aValue['sample_name'];
								$s17IndexID = $aValue['17_index_id'];
								$sIndex = $aValue['index'];
								$s15IndexID = $aValue['I5_Index_ID'];
								$sIndex2 = $aValue['index2'];
								$sSampleProject = $aValue['Sample_Project'];
								$sDescription = $aValue['Description'];
								
								// Let's Check The Sample Barcode is Valid or Not
								$ch = curl_init();

								curl_setopt($ch, CURLOPT_URL,"http://52.24.163.117/labc/api.limsdx.case1.php");
								curl_setopt($ch, CURLOPT_POST, 1);
								curl_setopt($ch, CURLOPT_POSTFIELDS,"sample_code=$sSampleName");
								// in real life you should use something like:
								// curl_setopt($ch, CURLOPT_POSTFIELDS, 
								//          http_build_query(array('postvar1' => 'value1')));

								// receive server response ...
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

								$server_output = curl_exec ($ch);
								$aTest = (array)json_decode($server_output);
								$iTestCount = $aTest['count'];
								$aTest = $aTest['tests'];
								if($iTestCount > 0){
									$iIsValid = 1;
								}else{
									$iIsValid = 0;
								}
								///!!! add the sample ifnformatio into database
								$iInserID = addSampleShhetSampleInfo($iSampleSheetMasterID,$iSampleID,$sSampleName,$s17IndexID,$sIndex,$s15IndexID,$sIndex2,$iIsValid);
								// Add Sample Status
								fAddSampleStatus($iInserID,1);
								//!! log
								$sLogMsg = "--------- Sequencer's Samples information stored into database for Sequencer ID $iSequencerDirID and SampleID is $iSampleID , at ".date('y-m-d H:i:s')."\r\n";
								fLogger(23,$sLogMsg);

								$sLogMsg = "--------- We got {$iTestCount} Test for the Samples , at ".date('y-m-d H:i:s')."\r\n";
								fLogger(23,$sLogMsg);

								if($iTestCount > 0){
									foreach ($aTest as $key => $sTestName) {
										$iTestID = fCheckTestAdded($sTestName);
										if($iTestID==0){
											// Add test In master
											$iTestID = fAddTestMaster($sTestName);
											$sLogMsg = "--------- We Added TestName : {$sTestName} Into The Master Table , at ".date('y-m-d H:i:s')."\r\n";
											fLogger(23,$sLogMsg);
											$iWorkflowID =1;
											//!! Mapp Test and Workflow
											fMapWorkflowWithTest($iTestID,$iWorkflowID);
											//!! Mapp Test And Sample
											$iSampleInfoID = $iInserID;
											fMapSampleWithTest($iTestID,$iSampleInfoID);
										}else{
											$iWorkflowID =1;
											//!! Mapp Test and Workflow
											fMapWorkflowWithTest($iTestID,$iWorkflowID);
											//!! Mapp Test And Sample
											$iSampleInfoID = $iInserID;
											fMapSampleWithTest($iTestID,$iSampleInfoID);
										}
									}
								}						
								
							}
							//! call function to change the sequencer directory status to 5 !!
							
							disableSequencerOldStatus($iSequencerDirID,6);
							disableSequencerOldStatus($iSequencerDirID,7);
							disableSequencerOldStatus($iSequencerDirID,9);
							disableSequencerOldStatus($iSequencerDirID,13);
							addSequncerMasterStatus($iSequencerDirID,5); //! 5 for we read the Samplesheet.csv into database
						}else{
							///!!! No Sample information inside the samplesheet.csv
							
							disableSequencerOldStatus($iSequencerDirID,6);
							disableSequencerOldStatus($iSequencerDirID,7);
							disableSequencerOldStatus($iSequencerDirID,9);
							disableSequencerOldStatus($iSequencerDirID,13);
							
							//!! call function to change the sequencer directory status to 5 !!
							addSequncerMasterStatus($iSequencerDirID,5); //! 5 for we read the Samplesheet.csv into database
							//!! log
							$sLogMsg = "x-x-x---- No Samples information Found in samplesheet.csv file of $iSequencerDirID at ".date('y-m-d H:i:s')."\r\n";
							fLogger(23,$sLogMsg);
						}
					}

				}else{
					///!! Error samplesheet master information can not be added
					//!! log
					$sLogMsg = "-xxx----- Sample Sheet Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s')."\r\n";
					fLogger(23,$sLogMsg);
				}
			}		

		}else{
			///!! if File does'not Exist
			//!! log
			$sLogMsg = "--x-x-x-- SampleSheet.csv File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s')."\r\n";
			fLogger(23,$sLogMsg);
		}
		
	}
}else{
	$sLogMsg = "-------- No Sequencer Found for reading the samplesheet.csv at ".date('y-m-d H:i:s')."\r\n";
	fLogger(23,$sLogMsg);	
}



$sLogMsg = "---OVR-- Script is Finished to reading samplesheet.csv file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(23,$sLogMsg);
?>