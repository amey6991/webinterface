<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scan_completionjobstatus.php
  Description : This script is going to scan the RunCompletionStatus.xml file for the sequencer and if file is created then just get it into local. and save the file information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for Copy RunCompletionStatus.xml file from sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(7,$sLogMsg);

/*
  Define the config values , default values here 
*/
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the RunCompletionStatus.xml
*/
$aSequencers = fGetSequencerForRunCompletionJObStatus();
//echo "<pre>";print_r($aSequencers);exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read RunCompletionStatus.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(7,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the RunCompletionStatus.xml file from directory to our local directory
      we renaming the file with , directoryName_RunCompletionStatus.xml
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy RunCompletionStatus.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(7,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy RunCompletionStatus.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(7,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy RunCompletionStatus.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(7,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(7,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the RunCompletionStatus.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aRunCompletionInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          we are going to rename the Sequencer RunCompletionStatus.xml filename for our local storage 
        */
        if($iSeqType==2){
          $sMainRemotePathToRunCompletionJObInfoXML = $sNextSeqDirectoryPath.$sSeqDirectoryName.'/RunCompletionStatus.xml';
          $sDirecotryPath = $sNextSeqDirectoryPath.$sSeqDirectoryName;
        }
        
        $sMainLocalPathToRunCompletionJobInfoXML = '/var/www/WebInterface/sequencer_files/'.$sSeqDirectoryName.'_'.'completionjobstatus.xml';
        $sNewFileName = $sSeqDirectoryName.'_'.'completionjobstatus.xml';

        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sMainRemotePathToRunCompletionJObInfoXML);
            
        if($bFileExists){
            $bSCopy = ssh2_scp_recv($connection, $sMainRemotePathToRunCompletionJObInfoXML, $sMainLocalPathToRunCompletionJobInfoXML);
            
            if($bSCopy){
                    $oResult = ssh2_exec($connection, "du -sh $sDirecotryPath");
                    stream_set_blocking($oResult, true);
                    $stream_out = ssh2_fetch_stream($oResult, SSH2_STREAM_STDIO);
                    $sResult = stream_get_contents($stream_out);
                    $aSize = explode("/", $sResult);
                    $sSize = trim($aSize[0]);
                    fUpdateSequencerSizeMasterInfo($iSeqDirectoryID,$sSize);
                    // Collect the information into array so that later we can read the xml
                    $iCopyFiles++;
                    $aRunCompletionInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'seq_type'=>$iSeqType,
                                                  'sequencer_dir_name'=>$sSeqDirectoryName,
                                                  'seq_dir_path'=>$sDirecotryPath,
                                                  'server_path' => $sMainRemotePathToRunCompletionJObInfoXML,
                                                  'local_path' => $sMainLocalPathToRunCompletionJobInfoXML,
                                                  'new_filename' => $sNewFileName,
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------RunCompletionStatus.xml file copied from ( $sMainRemotePathToRunCompletionJObInfoXML ) to our local with Path {$sMainLocalPathToRunCompletionJobInfoXML} on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(7,$sLogMsg);
            }else{
                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping RunCompletionStatus.xml file from ( $sMainRemotePathToRunCompletionJObInfoXML ) to our local PATH ( $sMainLocalPathToRunCompletionJobInfoXML ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(7,$sLogMsg);
            }
        }else{
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------RunCompletionStatus.xml file is NOT found in the ( $sMainRemotePathToRunCompletionJObInfoXML ) Server Path on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(7,$sLogMsg);
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Read the file
    after reading we will change the status of sequencer directory as 100 !! 100 for RunCompletionStatus.xml reads complete.
  */

  if(!empty($aRunCompletionInfo)){
      foreach ($aRunCompletionInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $iSeqTy = $aValue['seq_type'];
          $sSeqDirName = $aValue['sequencer_dir_name'];
          $sSeqPath = $aValue['seq_dir_path'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqFileType = 6; // 6 for RunCompletionStatus.xml
          
          $bExistResult = fCheckStatusForSequencer($iSeqDirectoryid,10);
          
          if($bExistResult){
            $iResult = addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName);
            if($iResult >0 ){
                // ddisable the isValid to 0 for sequencerID
                $bResult = disableSequencerOldStatus($iSeqDirectoryid);
                if($bResult==true){
                  // function to change the sequencer directory status to 10 !!
                  addSequncerMasterStatus($iSeqDirectoryid,10); 
                  fUpdateFinishTimeSequencerMasterInfo($iSeqDirectoryid);
                }
            }         
          }
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for read RunCompletionStatus.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(7,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------NoScript for Read/copy XML file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(7,$sLogMsg);

?>