<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scanner_master.php
  Description : This script is going to scan the master sequencer directory for newly created directory , if new directory found will get the information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';
require_once __DIR__.'/../'.'funEmails.php';

require_once PATH_TO_ROOT.'sendEmail.php';

$sSenderEmailId = EMAIL_SENDER_ID;
$sSenderName= EMAIL_SENDER_NAME;

$aDefaultList = fGetDefualtEmailList();
$aEmailFeature = getAllEmailFeature();
$bEmailFeature = false;
foreach ($aEmailFeature as $key => $aFeature) {
  if($aFeature['email_feature_name']=='Email on Sequencer Run Start'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailFeature=true;
        break;
      }else{
        $bEmailFeature = false;
        break;
      }
  }
}
// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Sequencer scanner master script Runs at ".$dtCurrentDateTime."\r\n";
//! 1 specifies that this is logged at module → SEQUENCER MASTER SCANNER
fLogger(1,$sLogMsg);

/*
  Define the config values , default values here 
*/
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$iNextSeqType= iNextSeqType;
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;  
$iMiSeqType= iMiSeqType;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  so , here we are going to connect the remote server by given port no ,username and password.
  and also we are making a resource of sftp to look for directories
*/

$connection = ssh2_connect($sIP, $iPort);
if($connection){
  $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
  if(!$autherised){
      $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Sequencer scanner master script , CONNECTION Done but Authentication Refused. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(1,$sLogMsg);
      exit();
  }else{
     $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------Sequencer scanner master script , CONNECTION & Authentication Passed. at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(1,$sLogMsg);
  }
}else{
  $sLogMsg = "---X-X-X---".date('Y-m-d H:i:s')."---------Sequencer scanner master script , CONNECTION refused. ".date('Y-m-d H:i:s')."\r\n";
  fLogger(1,$sLogMsg);
  exit();
}

$sftp = ssh2_sftp($connection);

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
fLogger(1,$sLogMsg);

/*
  we are going to open the NextSeq And MIseq Directory to get new sequencer directories
*/

$oNextSeqhandle = opendir("ssh2.sftp://$sftp".$sNextSeqDirectoryPath);
$oMiSeqhandle = opendir("ssh2.sftp://$sftp".$sMiSeqDirectoryPath);

/*
  We got our handler to look over the directory  
  so will close the connection , because we dont need to use the connection resourse when we already got out directories.
*/
// ssh2_exec($connection, 'exit');
// unset($connection);

// Log with a message
$sLogMsg = "---------".date('Y-m-d H:i:s')."---------Started Looking for new Sequencer Directory in NextSeq on ".date('Y-m-d H:i:s')."\r\n";
fLogger(1,$sLogMsg);

//!! NextSeq Starts Here
// Array to store the directory information from the handler
$aNextSeqDirectoryInfo = array();

while (false != ($sDirName = readdir($oNextSeqhandle))){
      
      $iPreFileName = (int)substr($sDirName,0,3);
      /*
        Continue the loop when the directory is not our cup of tea
      */
      if($sDirName === '.' || $sDirName === '..') 
      {
        continue;
      }
      
      $sFullPath = $sNextSeqDirectoryPath.$sDirName;

      //$sNextSeqSeize = filesize('ssh2.sftp://'.$sftp.$sFullPath);
      $oResult = ssh2_exec($connection, "du -sh $sFullPath");
      stream_set_blocking($oResult, true);
      $stream_out = ssh2_fetch_stream($oResult, SSH2_STREAM_STDIO);
      $sResult = stream_get_contents($stream_out);
      $aSize = explode("/", $sResult);
      $sNextSeqSeize = trim($aSize[0]);
      

      $bIsDir = is_dir('ssh2.sftp://'.$sftp.$sFullPath);
        
      if($bIsDir && $iPreFileName != 0){
          $dtFirstSeenOn = date('Y-m-d H:i:s');
          $sServerPath = $sFullPath;

         /*
            now , i will add the directory information into an array.
            so that , handler connection and database both are separated 
         */
          $aNextSeqDirectoryInfo[]=  array(
                                'dir_name'=>$sDirName,
                                'dir_firstseen_on'=>$dtFirstSeenOn,
                                'dir_server_path'=>$sServerPath,
                                'dir_seq_type'=>$iNextSeqType,
                                'dir_seq_size'=>$sNextSeqSeize
                              );
      }
}
 /*
  we loop the array of directory information and 
  We will check the directory exist in database or not
  if not found then we will add. else continue the loop
*/

if(!empty($aNextSeqDirectoryInfo)){
  $iCountDirectory = count($aNextSeqDirectoryInfo);
  // Log with a error message
  $sLogMsg = "---------".date('Y-m-d H:i:s')."---------$iCountDirectory Directory found in the Database on ".date('Y-m-d H:i:s')."\r\n";
  fLogger(1,$sLogMsg);  

  for($iii=0;$iii < $iCountDirectory;$iii++){
      $sDirectoryName = $aNextSeqDirectoryInfo[$iii]['dir_name'];
      $dtDirectoryFirstSeenOn = $aNextSeqDirectoryInfo[$iii]['dir_firstseen_on'];
      $sDirectoryServerPath = $aNextSeqDirectoryInfo[$iii]['dir_server_path'];
      $iDirectorySequencerType = $aNextSeqDirectoryInfo[$iii]['dir_seq_type'];
      $sSeqDirectorySize = $aNextSeqDirectoryInfo[$iii]['dir_seq_size'];
      $iCycleNumber = 0;
      $sSesQ30 = 0;
      $sSeqPF = 0;
      $sDensity =0;
      $sScannerID = "";
      //  check the sequencer directory is already added or not
      $bSequencerNotExist = bCheckSequencerInMaster($sDirectoryName);

      if($bSequencerNotExist){  
          // Store the sequencer directory information into master table
          $iSequencerMasterID = addSequncerMaster($sDirectoryName,$iDirectorySequencerType,$sDirectoryServerPath,$dtDirectoryFirstSeenOn);
          
          if($iSequencerMasterID > 0){
            // $sMsg = "SequencingRuns Added with ID: ".$iSequencerMasterID;
            // Log with a added sequencer message
            $sLogMsg = "---------".date('Y-m-d H:i:s')."---------New Directory Found {$sDirectoryName} , Added into Database on ".date('Y-m-d H:i:s')."\r\n";
            fLogger(1,$sLogMsg);
            $iMasterResult = addSequencerMasterInfo($iSequencerMasterID,$sSeqDirectorySize,$iCycleNumber,$sSesQ30,$sSeqPF,$sDensity,$sDirectoryName,$sScannerID);
            $iMasterParseInteropID = fAddSequencerParseInterOpStatus($iSequencerMasterID,1);
            $iMasterInteropExtracttionID = fAddSequencerInterOpExtractionStatus($iSequencerMasterID,1);
            $iResult = addSequncerMasterStatus($iSequencerMasterID,1);
            
            $aEmail = array(
                      'dir_name'=> $sDirectoryName,
                      'dir_path'=> $sDirectoryServerPath,
                      'seq_type'=> $iDirectorySequencerType
                      );
            
            if(!empty($aDefaultList) && $bEmailFeature==true){
              foreach ($aDefaultList as $key => $aValue) {
                        $aReciepintName = array($aValue['user_name']);
                        $aReciepintEmail = array($aValue['user_email']);
                        
                        $aEmail['recipient_name'] = $aValue['user_name'];

                        $iEmailTemplateFlag=EMAIL_SEQRUN_START_TO_DEFAULT_LIST;
                        $aAttachement = array();
                        $iEmailResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmail,$aAttachement,$sSenderEmailId,$sSenderName);
              
                    if($iEmailResult != 1){
                      $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN is Started , Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                       fLogger(25,$sLogMsg);
                    }
                }       
            }

            if($iResult>0){
                // Log with a added sequencer message
                $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Directory status as 1 added into database on ".date('Y-m-d H:i:s')."\r\n";
                fLogger(1,$sLogMsg);
            }
          }else{
            // Log with a error message
            $sLogMsg = "-----XXX--".date('Y-m-d H:i:s')."---------Error Occured , can not added the Directory name : {$sDirectoryName} , into Database on ".date('Y-m-d H:i:s')."\r\n";
            fLogger(1,$sLogMsg);
          }
      }else{
          // sequencer is exist so conitue to next ..
          continue;
      }

  }
}else{
      // Log with a error message
      $sLogMsg = "---------".date('Y-m-d H:i:s')."---------No New Directory found in the Database on ".date('Y-m-d H:i:s')."\r\n";
      fLogger(1,$sLogMsg);
}

/*
  Looking for MiSeq Direcories
*/

// Log with a message
$sLogMsg = "---------".date('Y-m-d H:i:s')."---------Started Looking for new Sequencer Directory in MiSeq on ".date('Y-m-d H:i:s')."\r\n";
fLogger(1,$sLogMsg);

// Array to store the directory information from the handler
$aMiSeqDirectoryInfo = array();

while (false != ($sDirName = readdir($oMiSeqhandle))){
      
      $iPreFileName = (int)substr($sDirName,0,3);
      /*
        Continue the loop when the directory is not our cup of tea
      */
      if($sDirName === '.' || $sDirName === '..') 
      {
        continue;
      }
      
      $sFullPath = $sMiSeqDirectoryPath.$sDirName;
      // $sMiSeqSeize = filesize('ssh2.sftp://'.$sftp.$sFullPath);
      $oResult = ssh2_exec($connection, "du -sh $sFullPath");
      stream_set_blocking($oResult, true);
      $stream_out = ssh2_fetch_stream($oResult, SSH2_STREAM_STDIO);
      $sResult = stream_get_contents($stream_out);
      $aSize = explode("/", $sResult);
      $sMiSeqSeize = trim($aSize[0]);

      $bIsDir = is_dir('ssh2.sftp://'.$sftp.$sFullPath);
        
      if($bIsDir && $iPreFileName != 0){
          $dtFirstSeenOn = date('Y-m-d H:i:s');
          $sServerPath = $sFullPath;

         /*
            now , i will add the directory information into an array.
            so that , handler connection and database both are separated 
         */
          $aMiSeqDirectoryInfo[]=  array(
                                'dir_name'=>$sDirName,
                                'dir_firstseen_on'=>$dtFirstSeenOn,
                                'dir_server_path'=>$sServerPath,
                                'dir_seq_type'=>$iMiSeqType,
                                'dir_seq_size'=>$sMiSeqSeize
                              );
      }
}
 /*
  we loop the array of directory information and 
  We will check the directory exist in database or not
  if not found then we will add. else continue the loop
*/

if(!empty($aMiSeqDirectoryInfo)){
  $iCountDirectory = count($aMiSeqDirectoryInfo);
  // Log with a error message
  $sLogMsg = "---------".date('Y-m-d H:i:s')."---------$iCountDirectory Directory found in the Database on ".date('Y-m-d H:i:s')."\r\n";
  fLogger(1,$sLogMsg);  

  for($iii=0;$iii < $iCountDirectory;$iii++){
      $sDirectoryName = $aMiSeqDirectoryInfo[$iii]['dir_name'];
      $dtDirectoryFirstSeenOn = $aMiSeqDirectoryInfo[$iii]['dir_firstseen_on'];
      $sDirectoryServerPath = $aMiSeqDirectoryInfo[$iii]['dir_server_path'];
      $iDirectorySequencerType = $aMiSeqDirectoryInfo[$iii]['dir_seq_type'];
      $iDirectorySequencerSize = $aMiSeqDirectoryInfo[$iii]['dir_seq_size'];
      $iCycleNumber = 0;
      $sSesQ30 = 0;
      $sSeqPF = 0;
      $sSeqDensity = 0;
      $sScannerID = "";
      //  check the sequencer directory is already added or not
      $bSequencerNotExist = bCheckSequencerInMaster($sDirectoryName);

      if($bSequencerNotExist){  
          // Store the sequencer directory information into master table
          $iSequencerMasterID = addSequncerMaster($sDirectoryName,$iDirectorySequencerType,$sDirectoryServerPath,$dtDirectoryFirstSeenOn);
          
          if($iSequencerMasterID > 0){
            // $sMsg = "SequencingRuns Added with ID: ".$iSequencerMasterID;
            // Log with a added sequencer message
            $sLogMsg = "---------".date('Y-m-d H:i:s')."---------New Directory Found {$sDirectoryName} , Added into Database on ".date('Y-m-d H:i:s')."\r\n";
            fLogger(1,$sLogMsg);
            $iMasterResult = addSequencerMasterInfo($iSequencerMasterID,$iDirectorySequencerSize,$iCycleNumber,$sSesQ30,$sSeqPF,$sSeqDensity,$sDirectoryName,$sScannerID);
            $iMasterParseInteropID = fAddSequencerParseInterOpStatus($iSequencerMasterID,1);
            $iMasterInteropExtracttionID = fAddSequencerInterOpExtractionStatus($iSequencerMasterID,1);
            $iResult = addSequncerMasterStatus($iSequencerMasterID,1);
            if($iResult>0){
                // Log with a added sequencer message
                $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Directory status as 1 added into database on ".date('Y-m-d H:i:s')."\r\n";
                fLogger(1,$sLogMsg);
            }
            
            $aEmail = array(
                      'dir_name'=> $sDirectoryName,
                      'dir_path'=> $sDirectoryServerPath,
                      'seq_type'=> $iDirectorySequencerType
                      );

            if(!empty($aDefaultList) && $bEmailFeature==true){
              foreach ($aDefaultList as $key => $aValue) {
                        $aReciepintName = array($aValue['user_name']);
                        $aReciepintEmail = array($aValue['user_email']);
                        
                        $aEmail['recipient_name'] = $aValue['user_name'];

                        $iEmailTemplateFlag=EMAIL_SEQRUN_START_TO_DEFAULT_LIST;
                        $aAttachement = array();
                        $iEmailResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmail,$aAttachement,$sSenderEmailId,$sSenderName);
              
                    if($iEmailResult != 1){
                      $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN is Started , Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                       fLogger(25,$sLogMsg);
                    }
                }       
            }

          }else{
            // Log with a error message
            $sLogMsg = "-----XXX--".date('Y-m-d H:i:s')."---------Error Occured , can not added the Directory name : {$sDirectoryName} , into Database on ".date('Y-m-d H:i:s')."\r\n";
            fLogger(1,$sLogMsg);
          }
      }else{
          // sequencer is exist so conitue to next ..
          continue;
      }

  }
}else{
      // Log with a error message
      $sLogMsg = "---------".date('Y-m-d H:i:s')."---------No New Directory found in the Database on ".date('Y-m-d H:i:s')."\r\n";
      fLogger(1,$sLogMsg);
}





// Log with a finish message
$sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------Sequencer scanner master script is finished at ".date('Y-m-d H:i:s')."\r\n";
fLogger(1,$sLogMsg);
?>