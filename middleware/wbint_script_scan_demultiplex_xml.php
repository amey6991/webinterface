<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_runparameteres.php
  Description : This script is going to scan the DemultiplexingStats.xml file for the sequencer and if file is created then just get it into local. and save the information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for Scan/Copy XML file from sequencer Runs at ".$dtCurrentDateTime."\r\n";
//! 3 specifies that this is logged at module → SEQUENCER DemultiplexingStats.xml
fLogger(8,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sDemultiplexPathInNextSeq = sDemultiplexPathInNextSeq;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;

/*
  Get the sequencer which need to read the DemultiplexingStats.xml
*/
$aSequencers = fGetSequencerForDemultiplexingStats();

if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read DemultiplexingStats.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(8,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the DemultiplexingStats.xml file from directory to our local directory
      we renaming the file with , directoryName_DemultiplexingStats.xml
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy DemultiplexingStats.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(8,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy DemultiplexingStats.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(8,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X---".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy DemultiplexingStats.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(8,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(8,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the DemultiplexingStats.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencerRunParameterXMLInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          we are going to rename the Sequencer DemultiplexingStats.xml filename for our local storage 
        */
        
        $sMainRemotePathToRunParemeterXML = $sNextSeqDirectoryPath.$sSeqDirectoryName.$sDemultiplexPathInNextSeq.'DemultiplexingStats.xml';
        
        
        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sMainRemotePathToRunParemeterXML);
        
        $sMainLocalPathToRunParemeterXML = '/var/www/WebInterface/sequencer_files/'.$sSeqDirectoryName.'_'.'demultiplexingStats.xml';
        $sNewFileName = $sSeqDirectoryName.'_'.'demultiplexingStats.xml';

        if($bFileExists==true){
            $bSCopy = ssh2_scp_recv($connection, $sMainRemotePathToRunParemeterXML, $sMainLocalPathToRunParemeterXML);
            

            if($bSCopy==true){
                    // Collect the information into array so that later we can read the XML
                    $iCopyFiles++;
                    $aSequencerRunParameterXMLInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'server_path' => $sMainRemotePathToRunParemeterXML,
                                                  'local_path' => $sMainLocalPathToRunParemeterXML,
                                                  'new_filename' => $sNewFileName,
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------DemultiplexingStats.xml file copied from ( $sMainRemotePathToRunParemeterXML ) to our local with Path {$sMainLocalPathToRunParemeterXML} on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(8,$sLogMsg);
            }else{
                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping DemultiplexingStats.xml file from ( $sMainRemotePathToRunParemeterXML ) to our local PATH ( $sMainLocalPathToRunParemeterXML ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(8,$sLogMsg);
            }
        }else{
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------The DemultiplexingStats.xml file is NOT found in the ( $sMainRemotePathToRunParemeterXML ) Server Path on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(8,$sLogMsg);
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Read the file
    after reading we will change the status of sequencer directory as 3 !! 3 for samplesheet reads complete.
  */

  if(!empty($aSequencerRunParameterXMLInfo)){
      foreach ($aSequencerRunParameterXMLInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqFileType = 7; // 7 for DemultiplexingStats.xml

          $bExistResult = fCheckStatusForSequencer($iSeqDirectoryid,12);
            if($bExistResult){
                 $iResult = addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName);
            if($iResult >0 ){
                // ddisable the isValid to 0 for sequencerID
                $bResult = disableSequencerOldStatus($iSeqDirectoryid);
                if($bResult){
                  // function to change the sequencer directory status to 12 !!
                  addSequncerMasterStatus($iSeqDirectoryid,12); //! 12 for we get the samplesheet into database
                }
            }
          }         
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for read DemultiplexingStats.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(8,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------NoScript for Read/copy XML file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(8,$sLogMsg);

?>