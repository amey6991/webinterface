<!-- 
Source Code : PHP
Author Name : Amey Damle
Location    : WebInterface::middleware
FileName    : wbint_script_scan_fastq_zip.php
Description : This script is going to scan the .fastq file for the sequencer output after completed the sequencer and save the information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for SCAN the FASTQ file from sequencer Output Directory , at ".$dtCurrentDateTime."\r\n";
fLogger(19,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the samplesheets
*/
$aSequencers = fGetSequencerToScanFastqOutputFiles();

if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for SCAN the FASTQ file in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(19,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and Then we will scan the default Output folder for fast files and scan the files.
      
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for SCAN the FASTQ file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(19,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR SCAN the FASTQ file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(19,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X---".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script SCAN the FASTQ file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(19,$sLogMsg);
      exit();
    }
    // sftp connection established for connection
    $sftp = ssh2_sftp($connection);

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(19,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the samplesheet path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencerOutputInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          Attach the Defualt Output Path with Directory to Scan
        */
        if($iSeqType==1){
          $sMainRemotePathToOutputDir = $sMiSeqDirectoryPath.$sSeqDirectoryName.'/Data/Intensities/BaseCalls/';
        }elseif($iSeqType==2){
          $sMainRemotePathToOutputDir = $sNextSeqDirectoryPath.$sSeqDirectoryName.'/Data/Intensities/BaseCalls/';
        }
        
        $oOutputSeqhandle = opendir("ssh2.sftp://$sftp".$sMainRemotePathToOutputDir);
        
        while (false != ($sDirFileName = readdir($oOutputSeqhandle))){
                /*
            Continue the loop when the directory is not our cup of tea
          */
          if($sDirFileName === '.' || $sDirFileName === '..') 
          {
            continue;
          }

          $sFullPath = $sMainRemotePathToOutputDir.$sDirFileName;
          $bIsDir = is_dir('ssh2.sftp://'.$sftp.$sFullPath);
          if($bIsDir==true){
            continue;
          }else{
            $iPosition = strpos($sDirFileName, '.fastq');
            if($iPosition > 0){
              // Log with a message
              $dtCurrentDateTime = date('Y-m-d H:i:s');
              $sLogMsg = "---------".date('Y-m-d H:i:s')."--------- Fastq File Found for Sequencer ID {$iSeqDirectoryID} .".date('Y-m-d H:i:s')."\r\n";
              fLogger(19,$sLogMsg);             
              $aSequencerOutputInfo[] = array(
                    'full_output_path'=>$sFullPath,
                    'output_filename'=>$sDirFileName,
                    'sequencer_id'=>$iSeqDirectoryID,
                    'seq_type'=>$iSeqType
                );
            }
          }
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Read the file
    after reading we will change the status of sequencer directory as 2 !! 2 for samplesheet reads complete.
  */
  if(!empty($aSequencerOutputInfo)){
      foreach ($aSequencerOutputInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $iSeqType = $aValue['seq_type'];
          $sSeqOutputPath = $aValue['full_output_path'];
          $sOutputFileName = $aValue['output_filename'];
          $bResult = fCheckSequencerFastQScanStatus($iSeqDirectoryid);
          if($bResult==false){
            fAddFastQScanStatus($iSeqDirectoryid);
          }
          
          $iResult = addFastqFileMasterInfo($iSeqDirectoryid,$sSeqOutputPath,$sOutputFileName);
          if($iResult > 0){
                // Log with a message
              $dtCurrentDateTime = date('Y-m-d H:i:s');
              $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SCAN the FASTQ file from sequencer Information is stored at ".$dtCurrentDateTime."\r\n";
              fLogger(19,$sLogMsg);
          }      
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for SCAN the FASTQ file in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(19,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------Script for SCAN the FASTQ file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(19,$sLogMsg);

?>