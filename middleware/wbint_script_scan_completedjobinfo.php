<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scan_completedjobinfo.php
  Description : This script is going to scan the CompletedJobInfo.xml file for the sequencer and if file is created then just get it into local. and save the information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for Copy CompletedJobInfo.xml file from sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(5,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the CompletedJobInfo.xml
*/
$aSequencers = fGetSequencerForCompletedJobInfo();

if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read CompletedJobInfo.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(5,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the CompletedJobInfo.xml file from directory to our local directory
      we renaming the file with , directoryName_CompletedJobInfo.xml
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(5,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy CompletedJobInfo.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(5,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(5,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(5,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the CompletedJobInfo.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencersCompletedJobInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          we are going to rename the Sequencer CompletedJobInfo.xml filename for our local storage 
        */
        if($iSeqType==1){
          $sMainRemotePathToCompletedJobInfoXML = $sMiSeqDirectoryPath.$sSeqDirectoryName.'/CompletedJobInfo.xml';
          $sDirecotryPath = $sMiSeqDirectoryPath.$sSeqDirectoryName;
        }
        
        $sMainLocalPathToCompletedJobInfoXML = '/var/www/WebInterface/sequencer_files/'.$sSeqDirectoryName.'_'.'completedjobinfo.xml';
        $sNewFileName = $sSeqDirectoryName.'_'.'completedjobinfo.xml';

        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sMainRemotePathToCompletedJobInfoXML);
        if($bFileExists){
            $bSCopy = ssh2_scp_recv($connection, $sMainRemotePathToCompletedJobInfoXML, $sMainLocalPathToCompletedJobInfoXML);

            if($bSCopy){
                    $oResult = ssh2_exec($connection, "du -sh $sDirecotryPath");
                    stream_set_blocking($oResult, true);
                    $stream_out = ssh2_fetch_stream($oResult, SSH2_STREAM_STDIO);
                    $sResult = stream_get_contents($stream_out);
                    $aSize = explode("/", $sResult);
                    $sSize = trim($aSize[0]);
                    fUpdateSequencerSizeMasterInfo($iSeqDirectoryID,$sSize);
                    // Collect the information into array so that later we can read the xml
                    $iCopyFiles++;
                    $aSequencersCompletedJobInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'seq_type'=>$iSeqType,
                                                  'sequencer_dir_name'=>$sSeqDirectoryName,
                                                  'seq_dir_path'=>$sDirecotryPath,
                                                  'server_path' => $sMainRemotePathToCompletedJobInfoXML,
                                                  'local_path' => $sMainLocalPathToCompletedJobInfoXML,
                                                  'new_filename' => $sNewFileName,
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------CompletedJobInfo.xml file copied from ( $sMainRemotePathToCompletedJobInfoXML ) to our local with Path {$sMainLocalPathToCompletedJobInfoXML} on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(5,$sLogMsg);
            }else{
                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping CompletedJobInfo.xml file from ( $sMainRemotePathToCompletedJobInfoXML ) to our local PATH ( $sMainLocalPathToCompletedJobInfoXML ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(5,$sLogMsg);
            }
        }else{
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------CompletedJobInfo.xml file is NOT found in the ( $sMainRemotePathToCompletedJobInfoXML ) Server Path on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(5,$sLogMsg);
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Read the file
    after reading we will change the status of sequencer directory as 100 !! 100 for completedjobinfo.xml reads complete.
  */
  
  if(!empty($aSequencersCompletedJobInfo)){
      foreach ($aSequencersCompletedJobInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $iSeqTy = $aValue['seq_type'];
          $sSeqDirName = $aValue['sequencer_dir_name'];
          $sSeqPath = $aValue['seq_dir_path'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqFileType = 4; // 4 for CompletedJobInfo.xml
          
          $bExistResult = fCheckStatusForSequencer($iSeqDirectoryid,100);
          if($bExistResult){
            $iResult = addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName);
            if($iResult >0 ){
                // disable the isValid to 0 for sequencerID
                $bResult = disableSequencerOldStatus($iSeqDirectoryid);
                if($bResult==true){
                  // function to change the sequencer directory status to 100 !!
                  addSequncerMasterStatus($iSeqDirectoryid,100); 
                  fUpdateFinishTimeSequencerMasterInfo($iSeqDirectoryid);
                }
            }         
          }
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for read CompletedJobInfo.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(5,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------NoScript for Read/copy XML file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(5,$sLogMsg);

?>