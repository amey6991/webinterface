<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_generatefastqruns_xml.php
  Description : This script is going to read the generatefastqrun.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$sLogMsg = "\r\n"."---SRT-- Script is started to read GenerateFASTQRunStatistics.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(22,$sLogMsg);

$iSeqFileType = 3; // 3 for GenerateFASTQRunStatistics.xml

//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadFASTQRuns();

$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the GenerateFASTQRunStatistics.xml files. at ".date('y-m-d H:i:s')."\r\n";
fLogger(22,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
	$aGenerateFASTQRunInfo = array();
	$iSequencerDirID = $value['seq_directory_id'];
	///! Get the local path of file
	$sFileName = $value['seq_local_path'];
	
	///! check if the file is exist or not in local path
	$bFileExist = file_exists($sFileName);

	///! if file exist , then
	if($bFileExist == true){
		///!!  call the function which will read the samplesheet
		$aGenerateFASTQRunInfo = ReadAndGetGenerateFASTQRunStatisticsXmlInfo($sFileName);

		if(!empty($aGenerateFASTQRunInfo)){
			///!!! extract the info from array
			$sAnalysisSoftwareVersion = $aGenerateFASTQRunInfo['sAnalysisSoftwareVersion'];
			$iNumberOfClustersPF = $aGenerateFASTQRunInfo['iNumberOfClustersPF'];
			$iNumberOfClustersRaw = $aGenerateFASTQRunInfo['iNumberOfClustersRaw'];
			$iNumberOfDuplicateClusters = $aGenerateFASTQRunInfo['iNumberOfDuplicateClusters'];
			$iNumberOfUnalignedClusters = $aGenerateFASTQRunInfo['iNumberOfUnalignedClusters'];
			$iNumberOfUnalignedClustersPF=$aGenerateFASTQRunInfo['iNumberOfUnalignedClustersPF'];
			$iNumberOfUnindexedClustersPF=$aGenerateFASTQRunInfo['iNumberOfUnindexedClustersPF'];
			$sOverallCoverage = $aGenerateFASTQRunInfo['sOverallCoverage'];
			$sOverallErrorPlotPath = $aGenerateFASTQRunInfo['sOverallErrorPlotPath'];
			$iOverallNoCallPlotPath = $aGenerateFASTQRunInfo['iOverallNoCallPlotPath'];
			$iYieldInBasesPF = $aGenerateFASTQRunInfo['iYieldInBasesPF'];
			$iYieldInBasesRaw = $aGenerateFASTQRunInfo['iYieldInBasesRaw'];

			//!! Sample Summary Info
			$aSampleInfo = $aGenerateFASTQRunInfo['sample_summary'];
			
			// Array for label-value of XML
			$aFASTQRunMaster = array(
						array('AnalysisSoftwareVersion',$sAnalysisSoftwareVersion),
						array('NumberOfClustersPF',$iNumberOfClustersPF),
						array('NumberOfClustersRaw',$iNumberOfClustersRaw),
						array('NumberOfDuplicateClusters',$iNumberOfDuplicateClusters),
						array('NumberOfUnalignedClusters',$iNumberOfUnalignedClusters),
						array('NumberOfUnalignedClustersPF',$NumberOfUnalignedClustersPF),
						array('NumberOfUnindexedClustersPF',$NumberOfUnindexedClustersPF),
						array('OverallCoverage',$sOverallCoverage),
						array('OverallErrorPlotPath',$sOverallErrorPlotPath),
						array('OverallNoCallPlotPath',$iOverallNoCallPlotPath),
						array('YieldInBasesPF',$iYieldInBasesPF),
						array('YieldInBasesRaw',$iYieldInBasesRaw),
				);
			if(!empty($aSampleInfo)){
				$bAddedMaster = addXMLSampleSummaryInfo($iSequencerDirID,$iSeqFileType,$aSampleInfo);
			}

			if(!empty($aFASTQRunMaster)){
				$bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aFASTQRunMaster);
			}
			///!!! JSON for the array of XML info
			$sGenerateFASTQRunInfoJSON = json_encode($aGenerateFASTQRunInfo['allXML']);

			///!!! add the information into database

			$iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sGenerateFASTQRunInfoJSON);
			if($iResult > 0){
				$iScanStatus = 4;
				$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
                  if($bResult){
                      // function to change the sequencer directory status to 7 !!
                      	disableSequencerOldStatus($iSequencerDirID,5);
						disableSequencerOldStatus($iSequencerDirID,9);
						disableSequencerOldStatus($iSequencerDirID,6);
						disableSequencerOldStatus($iSequencerDirID,13);
                      
                      addSequncerMasterStatus($iSequencerDirID,7); //! 7 for we read the generateFastQRuns into database
                  }
				//!! log
				$sLogMsg = "--------- GenerateFASTQRunStatistics.xml Master information Added in Database at ".date('y-m-d H:i:s')."\r\n";
				fLogger(22,$sLogMsg);
			}else{
				///!! Error samplesheet master information can not be added
				//!! log
				$sLogMsg = "-xxx----- GenerateFASTQRunStatistics.xml Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s')."\r\n";
				fLogger(22,$sLogMsg);
			}
		}		

	}else{
		///!! if File does'not Exist
		//!! log
		$sLogMsg = "--x-x-x-- GenerateFASTQRunStatistics.xml File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s')."\r\n";
		fLogger(22,$sLogMsg);
	}
	
}



$sLogMsg = "---OVR-- Script is Finished to reading GenerateFASTQRunStatistics.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(22,$sLogMsg);
?>