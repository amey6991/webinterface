<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_runbcl2fastq_sequencer.php
  Description : This script is for the NextSeq sequencer runs of which status is 11 (means middleware already read the RunCompletionStats.xml file) , for that sequencers we will SSH to galaxy and run the bcl2fast program for the directories one by one.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script to run bcl2fast Program in galaxy server for sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(17,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


$sGalaxyIP = "192.168.35.13";
$sGalaxyuserName = "galaxy";
$sGalaxyPassword = "Galaxy1248";

/*
  Get the sequencer which need to run the bcl2fastq program in galaxy server for NextSeq 
  We will get the sequencer of NextSeq and and the one which status is 11. (Read the RunCompletionStatus.xml)
*/
$aSequencers = fGetSequencerForBCL2FASTQProgram();
echo "<pre>";print_r($aSequencers);
exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read CompletedJobInfo.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(17,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the CompletedJobInfo.xml file from directory to our local directory
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(17,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy CompletedJobInfo.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(17,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(17,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(17,$sLogMsg);
    $sCommand = "nohup bcl2fastq --runfolder-dir $SEQUENCING_RUNS/NS500613/150818_NS500613_0024_AHCKN5BGXX/ --loading-threads 3 --demultiplexing-threads 6 --processing-threads 12 --writing-threads 3 --barcode-mismatches 2 > $SEQUENCING_RUNS/NS500613/150818_NS500613_0024_AHCKN5BGXX/bcl2fastq.log &";

// "sshpass -p 'Galaxy1248' ssh galaxy@192.168.35.13 'source Documents/sequencerconfig.sh ; nohup bcl2fastq --runfolder-dir $SEQUENCING_RUNS/NS500613/150818_NS500613_0024_AHCKN5BGXX/ --loading-threads 3 --demultiplexing-threads 6 --processing-threads 12 --writing-threads 3 --barcode-mismatches 2 > $SEQUENCING_RUNS/NS500613/150818_NS500613_0024_AHCKN5BGXX/bcl2fastq.log &'"

    //!! Connection for Galaxy , so that we can run the command for bcl2fast program

    // Source the sh file to get the ENV variable of PATH
    $sSourceCommand = "source Documents/sequencerconfig.sh";

    $oResult = ssh2_exec($connection, "sshpass -p 'Galaxy1248' ssh galaxy@192.168.35.13");
    
    var_dump($oResult);

        $oResult = ssh2_exec($connection, "sshpass -p 'Galaxy1248' ssh galaxy@192.168.35.13 'source Documents/sequencerconfig.sh ; $SEQUENCING_RUNS' ");
stream_set_blocking($oResult, true);
$stream_out = ssh2_fetch_stream($oResult, SSH2_STREAM_STDIO);
$sResult = stream_get_contents($stream_out);

    var_dump($sResult);
    

    //!! get the sequencer Infomarion
    $aRunBCL2FASTQProgram = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];

        //!! Check the sequencer is need to scan or not

        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        
        
        //!! Run the programm here for the Directory

          //!! if Program Runs , Set scan the log file status as 1
          fAddStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryID,1);
          

    }

?>