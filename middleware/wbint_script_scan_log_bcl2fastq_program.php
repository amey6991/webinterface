<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scan_log_bcl2fastq_program.php
  Description : This script is for the NextSeq sequencer runs , for which the bcl2fast1 program are run in the galaxy through command , will look into the bcl2fastq.log file of that sequencer and find out it successed or Failed.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script to scan log of bcl2fast Program on server for Next Sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(17,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


$sGalaxyIP = "192.168.35.13";
$sGalaxyuserName = "galaxy";
$sGalaxyPassword = "Galaxy1248";

/*
  Get the sequencer which need to scan for LOG file of bcl2fastq program in Sequencer Direcotry for NextSeq 
*/
$aSequencers = fGetSequencerForRunningBCL2FASTQProgram();
echo "<pre>";print_r($aSequencers);
exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read bcl2fastq.log File in Sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(17,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the CompletedJobInfo.xml file from directory to our local directory
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(17,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy CompletedJobInfo.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(17,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy CompletedJobInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(17,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(17,$sLogMsg);

    $sftp = ssh2_sftp($connection);

    // $oResult = ssh2_exec($connection, "sshpass -p 'Galaxy1248' ssh galaxy@192.168.35.13");
    // var_dump($oResult);


    //!! get the sequencer Infomarion
    $aRunBCL2FASTQProgram = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iSeqLogStatus = $aValue['seq_log_status'];

        $sFullLogFilePath = sNextSeqDirectoryPath.$sSeqDirectoryName.'/bcl2fastq.log';
        
        
        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sFullLogFilePath);

        if($bFileExists==true){
            $sResult = file_get_contents('ssh2.sftp://'.$sftp.$sMainBCL2FASTQFilePath);
            if($sResult=""){
              $iTotalSize = strlen($sResult);
              $sSearchString = "Processing completed with 0 errors and 0 warnings.";
              $sSerchError = "ERROR:";
              $iSuccess = strpos($sResult , $sSearchString);
              $iErrorStart = strpos($sResult , $sSerchError);
              if($ibSuccess > 0){
                //!! echo "Programm Run Successfull.";
                $iLogType =1;
                addSequncerMasterStatus($iSeqDirectoryID,20);
                fDisableStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryID);
                fAddLogStatusMessageForBCL2FASTQProgram($iSeqDirectoryid,$iLogType,$sSearchString);
                //!! Update sequencer to not run the bcl2fast program

                //!! update table to not scan the log file // DISABLE the status
              }elseif($iErrorStart > 0){
                  $iLogType =2;
                  $sErrorMessage = substr($sResult,$iErrorStart,$iTotalSize);
                  $iCount = fCheckCountErrorStatusForSequencer($iSeqDirectoryid);
                  if($iCount < 3){
                    fDisableStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryID);
                    fAddStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryid,2);
                    fAddLogStatusMessageForBCL2FASTQProgram($iSeqDirectoryid,$iLogType,$sErrorMessage);
                  }else{
                    fDisableStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryID);
                    fAddLogStatusMessageForBCL2FASTQProgram($iSeqDirectoryid,$iLogType,$sErrorMessage);
                    // Status 2 means Notify User with Error message from Log msg table
                    fAddStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryid,3);
                  }
              }else{
                //!! Continue Scanning

              }
            }else{
               //!! Log file is Empty

            }
        }
    }

?>