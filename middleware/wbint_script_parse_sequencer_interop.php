<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_parse_sequencer_interop.php
  Description : This script will scan all the directories for interop directory and execute the command so that all binary files will parse in summary , will create a csv file of it. that csv will store in a directory. and its information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script To get the csv file of summary of binary files by parsing through command for each sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(15,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;

$sSummaryProgrammPath = sSummaryProgrammPath;
$sRemoteCopyPathForCSV = sRemoteCopyPathForSummaryCSV;



/*
  Get the sequencer which need to read the RunCompletionStatus.xml
*/
$aSequencers = fGetSequencerForParseInterOp();
//echo "<pre>";print_r($aSequencers);exit();
$aParsedInterOpInfo = array();
foreach ($aSequencers as $key => $aValue) {


    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for Parsing the Summary of InterOP in Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(15,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are going to run the command to generate the CSV file of summary into a directory.
      we renaming the csv file with , directoryName_parsesummary.csv
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Parse the InterOp Summary and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(15,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Parse the InterOp Summary and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(15,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Parse the InterOp Summary and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(15,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(15,$sLogMsg);

    //!! Extract the Sequencer Information from the loop
    $SeqDirectroryID = $aValue['seq_directory_id'];
    $SeqType = $aValue['seq_type'];
    $SeqDirName = $aValue['seq_dir_name'];
    $SeqServerPath = $aValue['seq_server_path'];

    if($SeqType==1){
      $sRemoteDirectoryPath = $sMiSeqDirectoryPath.$SeqDirName.'/';
    }elseif($SeqType==2){
      $sRemoteDirectoryPath = $sNextSeqDirectoryPath.$SeqDirName.'/';
    }
    $sCSVFileName = $SeqDirName.'_'.'parsesummary.csv';
    $sRemoteCopyFullPathForCSV = $sRemoteCopyPathForCSV.$sCSVFileName;

    //!! command is written below here
    $sCommand = "{$sSummaryProgrammPath} {$sRemoteDirectoryPath} > {$sRemoteCopyFullPathForCSV}";
    //!! Execute the command
    $stream = ssh2_exec($connection, $sCommand);
    
    //sleep(10);
    //!! if command Execution is TRUE
    if($stream != FALSE){
      $aParsedInterOpInfo[] = array(
                          'seq_directory_id'=>$SeqDirectroryID,
                          'seq_parsed_summary_path'=>$sRemoteCopyFullPathForCSV,
                          'parsed_csv_filename'=>$sCSVFileName,
                          'seq_type'=> $SeqType
                            ); 
    }else{
      $sLogMsg = "--X-XX--".date('Y-m-d H:i:s')."--------- Command Execution is FAILD for generate Sequencer's summary of InterOP for ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(15,$sLogMsg);      
    }
}
// echo "<pre>";print_r($aParsedInterOpInfo);exit();
//!! Loop the array to store the information
if(!empty($aParsedInterOpInfo)){
    foreach ($aParsedInterOpInfo as $key => $aValue) {
        //!! Extract The Array information 
        $SeqDirectroryID = $aValue['seq_directory_id'];
        $sRemoteCopyFullPathForCSV = $aValue['seq_parsed_summary_path'];
        $sCSVFileName = $aValue['parsed_csv_filename'];
        $iSeqParseType =1;
        $iSeqType = $aValue['seq_type'];
        $bExist = fCheckSequencerInInterOpMaster($SeqDirectroryID , $iSeqParseType);
        if($bExist==true){
          $iResult = fAddSequencerParseInterOpMasterInfo($SeqDirectroryID,$iSeqParseType,$sRemoteCopyFullPathForCSV,$sCSVFileName);
        }
        // Add status as 2 for the sequencer that summary CSV get generated
        // $bResult = fCheckSequencerCompleted($SeqDirectroryID,$iSeqType);
        // if($bResult==true){
        //   fDisableParseInterOpStatus($SeqDirectroryID);
        // }else{
          fDisableParseInterOpStatus($SeqDirectroryID,1);
          fDisableParseInterOpStatus($SeqDirectroryID,2);
          fAddSequencerParseInterOpStatus($SeqDirectroryID,2);
        //}

        if($iResult > 0){
          $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
         fLogger(15,$sLogMsg);
        }else{
          $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed But the Information NOT Saved for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
         fLogger(15,$sLogMsg);
        }
    }
}else{
      $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed But the Information NOT Saved for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
     fLogger(15,$sLogMsg);
}
?>