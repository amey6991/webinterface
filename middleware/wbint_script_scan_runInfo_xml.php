<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scan_runInfo.php
  Description : This script is going to scan the RunInfo.xml file for the sequencer and if file is created then just get it into local. and save the file information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for Copy RunInfo.xml file from sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(6,$sLogMsg);

/*
  Define the config values , default values here 
*/
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the RunInfo.xml
*/
$aSequencers = fGetSequencerForRunInfo();

if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read RunInfo.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(6,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the RunInfo.xml file from directory to our local directory
      we renaming the file with , directoryName_RunInfo.xml
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Copy RunInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(6,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Copy RunInfo.xml file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(6,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for copy RunInfo.xml file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(6,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(6,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the RunInfo.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencersRunInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          we are going to rename the Sequencer RunInfo.xml filename for our local storage 
        */
        if($iSeqType==1){
          $sMainRemotePathToRunInfoXML = $sMiSeqDirectoryPath.$sSeqDirectoryName.'/RunInfo.xml';
        }elseif($iSeqType==2){
          $sMainRemotePathToRunInfoXML = $sNextSeqDirectoryPath.$sSeqDirectoryName.'/RunInfo.xml';
        }
        
        $sMainLocalPathToRunInfoXML = '/var/www/WebInterface/sequencer_files/'.$sSeqDirectoryName.'_'.'runinfo.xml';
        $sNewFileName = $sSeqDirectoryName.'_'.'runinfo.xml';
        
        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sMainRemotePathToRunInfoXML);
        
        if($bFileExists){
            $bSCopy = ssh2_scp_recv($connection, $sMainRemotePathToRunInfoXML, $sMainLocalPathToRunInfoXML);
            
            if($bSCopy){
                    // Collect the information into array so that later we can read the xml
                    $iCopyFiles++;
                    $aSequencersRunInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'server_path' => $sMainRemotePathToRunInfoXML,
                                                  'local_path' => $sMainLocalPathToRunInfoXML,
                                                  'new_filename' => $sNewFileName,
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------RunInfo.xml file copied from ( $sMainRemotePathToRunInfoXML ) to our ({$sMainLocalPathToRunInfoXML} ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(6,$sLogMsg);
            }else{
                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping RunInfo.xml file from ( $sMainRemotePathToRunInfoXML ) to our ({$sMainLocalPathToRunInfoXML} ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(6,$sLogMsg);
            }
        }else{
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------RunInfo.xml file is NOT found in the Remote Server Path ( $sMainRemotePathToRunInfoXML ) on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(6,$sLogMsg);
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop 
    after scanning we will change the status of sequencer directory as 8 !! 
    8 for RunInfo.xml scaned and copied complete.
  */

  if(!empty($aSequencersRunInfo)){
      foreach ($aSequencersRunInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqFileType = 5; // 5 for RunInfo.xml
          
          $bExistResult = fCheckStatusForSequencer($iSeqDirectoryid,8);
          
          if($bExistResult){
            $iResult = addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName);
            if($iResult >0 ){
                // ddisable the isValid to 0 for sequencerID
                $bResult = disableSequencerOldStatus($iSeqDirectoryid);
                if($bResult){
                  // function to change the sequencer directory status to 8 !!
                  addSequncerMasterStatus($iSeqDirectoryid,8); //! 8 for we get the RunFastQStatisticsXMLInfo into database
                }
            }         
          }
      }
  }else{
    echo "No file Array found";
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for read RunInfo.xml in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(6,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------NoScript for Read/copy XML file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(6,$sLogMsg);

?>