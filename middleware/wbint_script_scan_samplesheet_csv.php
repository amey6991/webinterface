<!-- 
Source Code : PHP
Author Name : Amey Damle
Location    : WebInterface::middleware
FileName    : wbint_script_scanner_master.php
Description : This script is going to scan the SampleSheet.csv file for the sequencer and if file is created then just get it into local. and save the information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script for Read/copy CSV file from sequencer Runs at ".$dtCurrentDateTime."\r\n";
//! 1 specifies that this is logged at module → SEQUENCER Samplesheet Read
fLogger(2,$sLogMsg);

/*
  Define the config values , default values here 
*/

$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the samplesheets
*/
$aSequencers = fGetSequencerForSamplesheet();

if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found for read samplesheet in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(2,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are using scp rec to copy the samplesheet file from directory to our local directory
      we renaming the file with , directoryName_samplesheet.csv
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Read/copy CSV file. ".date('Y-m-d H:i:s')."\r\n";
          fLogger(2,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Read/copy CSV file. at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(2,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X---".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Script for Read/copy CSV file. ".date('Y-m-d H:i:s')."\r\n";
      fLogger(2,$sLogMsg);
      exit();
    }
    // sftp connection established for connection
    $sftp = ssh2_sftp($connection);

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(2,$sLogMsg);

    /*
      We will loop for the sequencer directory and get the samplesheet path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencerSampleSheetInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $iSeqType = $aValue['seq_type'];
        $sSeqDirectoryName = $aValue['seq_dir_name'];
        $sSeqDirectoryServerPath = $aValue['seq_server_path'];
        $iSeqStatus = $aValue['seq_status'];
        $iIsValid = $aValue['isValid'];
        

        /*
          we willl check the fix exist or NOT then 
          we are going to rename the Sequencer samplesheet filename for our local storage 
          for xml files , use the xml file name
        */
        if($iSeqType==1){
          $sMainRemotePathToSampleSheet = $sMiSeqDirectoryPath.$sSeqDirectoryName.'/SampleSheet.csv';
        }elseif($iSeqType==2){
          $sMainRemotePathToSampleSheet = $sNextSeqDirectoryPath.$sSeqDirectoryName.'/SampleSheet.csv';
        }
        
        $sMainLocalPathToSampleSheet = '/var/www/WebInterface/sequencer_files/'.$sSeqDirectoryName.'_'.'samplesheet.csv';
        $sNewFileName = $sSeqDirectoryName.'_'.'samplesheet.csv';
        
        $bFileExists = file_exists('ssh2.sftp://'.$sftp.$sMainRemotePathToSampleSheet);
        if($bFileExists==true){
            $bSCopy = ssh2_scp_recv($connection, $sMainRemotePathToSampleSheet, $sMainLocalPathToSampleSheet);
            
        
            if($bSCopy==true){
                    // Collect the information into array so that later we can read the csv's
                    $iCopyFiles++;
                    $aSequencerSampleSheetInfo[] = array(
                                                  'sequencer_id' => $iSeqDirectoryID,
                                                  'server_path' => $sMainRemotePathToSampleSheet,
                                                  'local_path' => $sMainLocalPathToSampleSheet,
                                                  'new_filename' => $sNewFileName,
                                                  'seq_type'=>$iSeqType
                                                  );

                    // Log with a message
                    $dtCurrentDateTime = date('Y-m-d H:i:s');
                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SampleSheet.csv file copied from ( $sMainRemotePathToSampleSheet ) to our local with Path {$sMainLocalPathToSampleSheet} on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(2,$sLogMsg);
            }else{
                    // Log with a message
                    $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------Error While coping SampleSheet.csv file from ( $sMainRemotePathToSampleSheet ) to our local ( $sMainLocalPathToSampleSheet ) on ".date('Y-m-d H:i:s')."\r\n";
                    fLogger(2,$sLogMsg);
            }
        }else{
              // Log with a message
              $sLogMsg = "-X-X-X---".date('Y-m-d H:i:s')."---------The SampleSheet.csv file is NOT found in Remote Server PATH ( $sMainRemotePathToSampleSheet ) on ".date('Y-m-d H:i:s')."\r\n";
              fLogger(2,$sLogMsg);          
        }
    }

  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Read the file
    after reading we will change the status of sequencer directory as 2 !! 2 for samplesheet reads complete.
  */
  if(!empty($aSequencerSampleSheetInfo)){
      foreach ($aSequencerSampleSheetInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['sequencer_id'];
          $sSeqRemoteDirectoryPath = $aValue['server_path'];
          $sSeqLocalDirectoryPath = $aValue['local_path'];
          $SNewFileName = $aValue['new_filename'];
          $iSeqFileType = 1;

          $bExistResult = fCheckStatusForSequencer($iSeqDirectoryid,2);
          
          if($bExistResult==true){
              $iResult = addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName);
              if($iResult > 0){
                  // disable the isValid to 0 for sequencerID
                  $bResult = disableSequencerOldStatus($iSeqDirectoryid);
                  // function to change the sequencer directory status to 2 !!
                  if($bResult==true){

                    addSequncerMasterStatus($iSeqDirectoryid,2); //! 2 for we get the samplesheet into database
                  }
              } 
          }        
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found for read samplesheet in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(2,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------Script for Read/copy CSV file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(2,$sLogMsg);

?>