<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_runinfo_xml.php
  Description : This script is going to read the runInfo.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$sLogMsg = "---SRT-- Script is started to read RunINfo.xml file for each sequencer at ".date('y-m-d H:i:s');
fLogger(26,$sLogMsg);

$iSeqFileType =5;

//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadRunInfoXml();
$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the RunINfo.xml files. at ".date('y-m-d H:i:s');
fLogger(26,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
	$aRunInfo = array();
	$iSequencerDirID = $value['seq_directory_id'];
	///! Get the local path of file
	$sFileName = $value['seq_local_path'];
	
	///! check if the file is exist or not in local path
	$bFileExist = file_exists($sFileName);

	///! if file exist , then
	if($bFileExist == true){
		///!!  call the function which will read the samplesheet
		$aRunInfo = ReadAndGetRunInfoXmlInfo($sFileName);
		
		if(!empty($aRunInfo)){
			///!!! extract the info from array
			$sRunID= $aRunInfo['runinfo']['run_id'];
			$sRunNumber= $aRunInfo['runinfo']['run_number'];
			$sInstrument= $aRunInfo['runinfo']['run_instrument'];
			$aRunReadInfo = $aRunInfo['readinfo'];
		
			// Array for label-value of XML
			$aRunMaster = array(
							array('run_id',$sRunID),
							array('run_number',$sRunNumber),
							array('run_instrument',$sInstrument),
				);

			if(!empty($aRunReadInfo)){
				$bAdded = addReadsInfoFromXML($iSequencerDirID,$iSeqFileType,$aRunReadInfo);
			}
			if(!empty($aRunMaster)){
				$bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aRunMaster);
			}

			$sRunInfoJSON = json_encode($aRunInfo['allXML']);

			///!!! add the information into database
			$iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sRunInfoJSON);

			if($iResult > 0){
				$iScanStatus = 8;
				$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
                  if($bResult){
                      // function to change the sequencer directory status to 9 !!
                      disableSequencerOldStatus($iSequencerDirID,5);
						disableSequencerOldStatus($iSequencerDirID,7);
						disableSequencerOldStatus($iSequencerDirID,6);
						disableSequencerOldStatus($iSequencerDirID,13);
                      addSequncerMasterStatus($iSequencerDirID,9); //! 9 for we read the generateFastQRuns into database
                  }
				//!! log
				$sLogMsg = "--------- RunINfo.xml Master information Added in Database at ".date('y-m-d H:i:s');
				fLogger(26,$sLogMsg);
			}else{
				///!! Error samplesheet master information can not be added
				//!! log
				$sLogMsg = "-xxx----- RunINfo.xml Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s');
				fLogger(26,$sLogMsg);
			}
		}		

	}else{
		///!! if File does'not Exist
		//!! log
		$sLogMsg = "--x-x-x-- RunINfo.xml File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s');
		fLogger(26,$sLogMsg);
	}
	
}



$sLogMsg = "---OVR-- Script is Finished to reading RunINfo.xml file for each sequencer at ".date('y-m-d H:i:s');
fLogger(26,$sLogMsg);
?>