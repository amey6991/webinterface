<?php

require_once __DIR__.'/../'.'funSequencers.php';
require_once __DIR__.'/../'.'funEmails.php';

require_once PATH_TO_ROOT.'sendEmail.php';

$sSenderEmailId = EMAIL_SENDER_ID;
$sSenderName= EMAIL_SENDER_NAME;


$aSeq = getAllSequencerActive();
$aDefaultList = fGetDefualtEmailList();
$aEmailFeature = getAllEmailFeature();
$bEmailFeature = false;
foreach ($aEmailFeature as $key => $aFeature) {
  if($aFeature['email_feature_name']=='Email on Sequencer Run Error'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailFeature=true;
        break;
      }else{
        $bEmailFeature = false;
        break;
      }
  }
}
// echo "<pre>";print_r($aSeq);
// exit();
foreach ($aSeq as $key => $aValue) {
  $iSeqDirectoryID = $aValue['seq_directory_id'];
  $sSeqDirname = $aValue['seq_dir_name'];
  $sSeqDirPath = $aValue['seq_server_path'];
  $iSeqType = $aValue['seq_type'];
  $dStartedOn = $aValue['seq_first_seen_on'];
  $dtSequencerAfter36Hour = date('Y-m-d H:i:s',strtotime($dStartedOn.'+36 hours'));
  $dtToday = date('Y-m-d H:i:s');
  if($dtToday > $dtSequencerAfter36Hour){
        
          $bError=false;
          $sErrorMesage = "";
          $bSampleSheetScan = fCheckStatusForSequencer($iSeqDirectoryID,2);
          
          $bSampleSheetRead = fCheckStatusForSequencer($iSeqDirectoryID,5);

          $bRunInfoScan = fCheckStatusForSequencer($iSeqDirectoryID,8);
          //$bRunInfoRead = fCheckStatusForSequencer($iSeqDirectoryID,9);

          $bRunParaScan = fCheckStatusForSequencer($iSeqDirectoryID,3);
          //$bRunParaRead = fCheckStatusForSequencer($iSeqDirectoryID,6);

          if($iSeqType==1){
            $bFastQScan = fCheckStatusForSequencer($iSeqDirectoryID,4);
            $bCompletedScan = fCheckStatusForSequencer($iSeqDirectoryID,100);
            //$bFastQRead = fCheckStatusForSequencer($iSeqDirectoryID,7);  

          }elseif($iSeqType==2){
            $bDemultiplexScan = fCheckStatusForSequencer($iSeqDirectoryID,12);
            $bCompletionScan = fCheckStatusForSequencer($iSeqDirectoryID,10);
          }

          if($bSampleSheetScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the SampleSheet, Please check it manually';
            $bError=true;
          }elseif($bRunInfoScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the RunInfo.xml File, Please check it manually';
            $bError=true;
          }elseif($bRunParaScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the RunParameters.xml File, Please check it manually';
            $bError=true;
          }

          if($iSeqType==1 AND $bFastQScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the GenerateFastQStatistics.xml File, Please check it manually';
            $bError=true;
          }elseif($iSeqType==1 AND $bCompletedScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the CompletedJobInfo.xml File, Please check it manually';
            $bError=true;
          }
          
          if($iSeqType==2 AND $bCompletionScan==true){
            $sErrorMesage = 'Its Been 36 Hour the Sequencer is Not Found the RunCompletion.xml File, Please check it manually';
            $bError=true;
          }

          if($bError==true){
            echo "Error ".$iSeqDirectoryID."->".$sErrorMesage."</br>";
            $bEntry = fCheckErrorMasterEntryForSequencer($iSeqDirectoryID);
            if($bEntry==true){
                $iError = fAddSequencerErrorOccured($iSeqDirectoryID,$iSeqType,$sErrorMesage);

                //!! Email When Error Occured
                $aEmailInfo =array('dir_name'=>$sSeqDirname,
                          'dir_path'=>$sSeqDirPath,
                          'seq_type' => $iSeqType,
                          'error'=>$sErrorMesage);
                if(!empty($aDefaultList) && $bEmailFeature==true){
                    foreach ($aDefaultList as $key => $aValue) {
                            $aReciepintName = array($aValue['user_name']);
                            $aReciepintEmail = array($aValue['user_email']);
                           
                            $iEmailTemplateFlag=EMAIL_SEQRUN_ERROR_TO_DEFAULT_LIST;
                            $aAttachement = array();
                            $iResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmailInfo,$aAttachement,$sSenderEmailId,$sSenderName);
                  
                        if($iResult != 1){
                          // $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN finished Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                          // fLogger(5,$sLogMsg);
                        }
                    }       
                }
            }
          }else{
              // Do something for Sequencer is done , dont check it next time.
            echo "Perfect ".$iSeqDirectoryID."</br>";
          }
      
  }else{
    continue;
  }
}
?>