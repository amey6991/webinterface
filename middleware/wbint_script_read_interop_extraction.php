<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_interop_extraction.php
  Description : This script will get the information of sequencer which extraction csv is created , and we will read the extraction csv from the local storage
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once __DIR__.'/../'.'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script To get the csv file of each sequencer Runs and copied into local machine at ".$dtCurrentDateTime."\r\n";
fLogger(33,$sLogMsg);

/*
  Define the config values , default values here 
*/
  
$sMiSeqDirectoryPath = sMiSeqDirectoryPath;
$sNextSeqDirectoryPath = sNextSeqDirectoryPath;
$sIP = sIP;
$iPort = iPort;
$sUserName = sUserName;
$sPassword = sPassword;


/*
  Get the sequencer which need to read the ExtractionMetricsOut.bin
*/
$aSequencers = fGetSequencerToReadParsedInterOpExtractionCSV();
//echo "<pre>";print_r($aSequencers);exit();
if(!empty($aSequencers)){
    $iTotalSequencer = count($aSequencers);
      // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------{$iTotalSequencer} Sequencer found to Read the sequencer extraction csv From local machine. Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(33,$sLogMsg);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      We will loop for the sequencer directory and get the CompletedJobInfo.xml path
      then we will rename the file with our naming convention
      and then securley copy the file 
    */
    $iCopyFiles =0;
    $aSequencerExtractionInfo = array();
    foreach ($aSequencers as $key => $aValue) {
        $iSeqDirectoryID = $aValue['seq_directory_id'];
        $sSeqRemoteServerPath = $aValue['seq_parse_file_path'];
        $sLocalPath = $aValue['seq_local_filepath'];
        $sLocalFileName = $aValue['seq_parsed_filename'];
        $iSeqType = $aValue['seq_type'];

        /*
          we are going to rename the Sequencer CompletedJobInfo.xml filename for our local storage 
        */
        
        $bFileExist = file_exists($sLocalPath);
        
        if($bFileExist==true){
          $aExtractionInfo = fReadExtractionCSVInfo($sLocalPath);
          
          if(!empty($aExtractionInfo)){
              $sExtractionJSON = json_encode($aExtractionInfo);
              
              $iCycleNumber = $aExtractionInfo['cycle_number'];

              $aSequencerExtractionInfo[] = array(
                  'seq_directory_id'=>$iSeqDirectoryID,
                  'seq_extraction_json'=>$sExtractionJSON,
                  'cycle_number'=>$iCycleNumber,
                  'seq_type'=>$iSeqType
                );
          }else{
              // Log with a message
              $dtCurrentDateTime = date('Y-m-d H:i:s');
              $sLogMsg = "---------No Info found in while Read the sequencer extraction csv From ( $sLocalPath ). Runs at ".$dtCurrentDateTime."\r\n";
              fLogger(33,$sLogMsg);
          }
        }
    }
  /*
    We got our information in an array so now ,
    we will close the connection , because we dont need to use the connection resourse.
  */
  ssh2_exec($connection, 'exit');
  unset($connection);

  /*
    Copied file of sequencer will loop and Save the info of Summary CSV file for sequencer
  
  */

  if(!empty($aSequencerExtractionInfo)){
      foreach ($aSequencerExtractionInfo as $key => $aValue) {
          $iSeqDirectoryid = $aValue['seq_directory_id'];
          $sSeqExtractionJson = $aValue['seq_extraction_json'];
          $iCycleNumber = $aValue['cycle_number'];
          $iSeqType = $aValue['seq_type'];
          $iSeqInteropType = 1;
          $iResult = fAddInterOpCSVJSON($iSeqDirectoryid,$iSeqInteropType,$sSeqExtractionJson);
          // To track the cycle number of sequencer
          fAddSequencerExtraInfo($iSeqDirectoryid,$iCycleNumber); // Testing Table 
          fUpdateSequencerCycleNumberMasterInfo($iSeqDirectoryid,$iCycleNumber); // Master Info Tvble
          $bResult = fCheckSequencerCompleted($iSeqDirectoryid,$iSeqType);
          if($bResult==true){
            fDisableInterOpExtractionStatus($iSeqDirectoryid);
          }else{
            fDisableInterOpExtractionStatus($iSeqDirectoryid,3);
          }

          if($iResult >0 ){
              $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Extraction of Sequence as CSV file Read successfully and it JSON Stored in database.".date('Y-m-d H:i:s')."\r\n";
              fLogger(33,$sLogMsg);
          }else{
              $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Extraction of Sequence as CSV file Read successfully and it JSON NOT stored in database.".date('Y-m-d H:i:s')."\r\n";
              fLogger(33,$sLogMsg);
          }         
          
      }
  }

}else{
    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "XX-X-----".date('Y-m-d H:i:s')."---------No Sequencer found to READ Extraction CSV in sequencer Runs at ".$dtCurrentDateTime."\r\n";
    fLogger(33,$sLogMsg);
}

    // Log with a message
    $dtCurrentDateTime = date('Y-m-d H:i:s');
    $sLogMsg = "---OVR---".date('Y-m-d H:i:s')."---------Script finished to READ Extraction CSV file from sequencer Finished at ".$dtCurrentDateTime."\r\n";
    fLogger(33,$sLogMsg);
?>