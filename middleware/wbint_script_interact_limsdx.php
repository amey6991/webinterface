<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_interact_limsdx.php
  Description : This script is going to Interact with LimsDx to get the TestName for sample barcode as input and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$iWorkflowID =1;

$sLogMsg = "---SRT-- Script is started to Interact LimsDx using its API for each sequencer at ".date('Y-m-d H:i:s');
fLogger(10,$sLogMsg);

//! function called to get seuqncer for read their samplesheet
$aSequencer = fGetSequencerReadSampleSheet();
$iTotalSequencer = count($aSequencer);

$sLogMsg = "-------- {$iTotalSequencer} Sequencer Found to Interact LimsDx using its API for Samples at ".date('Y-m-d H:i:s');
fLogger(10,$sLogMsg);

foreach ($aSequencer as $key => $aValue) {
	$iSeqDirectoryID = $aValue['seq_directory_id'];
	$iSeqType = $aValue['seq_type'];
	$sSeqDirectoryName = $aValue['seq_dir_name'];
	$sSeqDirectoryPath = $aValue['seq_server_path'];
	$iSeqDirectoryStatus = $aValue['seq_status'];
	

	$aSampleInfo = array();

	$aSampleSheetInfo = fGetSampleSheetInfoForSequencer($iSeqDirectoryID);
	if(!empty($aSampleSheetInfo)){
	    $iSampleSheetInfoMasterID = $aSampleSheetInfo['seq_samplesheet_master_id'];
	    $aSampleInfo = fGetSampleInformationForSequencer($iSampleSheetInfoMasterID);
	}

	if(!empty($aSampleInfo)){
		$iSampleInfoTableID = $aSampleInfo['seq_sample_info_id'];
		$iSamplemasterID = $aSampleInfo['seq_samplesheet_master_id'];
		$iSampleID = $aSampleInfo['sample_ID'];
		$sSampleNameBarcode = $aSampleInfo['sample_name_barcode'];
		$sSampleIndex17 = $aSampleInfo['index_17_id'];
		$sSampleIndex = $aSampleInfo['index'];
		$sSampleIndex15 = $aSampleInfo['index_15_id'];
		$sSampleIndex2 = $aSampleInfo['index2'];

		//!! Pass the barcode to LimsDx API and get the API


		//!! Process the TestName for sample of Samplesheet

		$iTestAdded = fCheckTestAdded($sTestName);
		if($iTestAdded == 0){
			$iTestID = fAddTestMaster($sTestName);
			if($iTestID > 0){
				$sLogMsg = "--------{$sTestName} is Added into Master with TestID = {$iTestID} at ".date('Y-m-d H:i:s');
				fLogger(10,$sLogMsg);	

				///!!! map the Sample with Test
				$iMapID = fMapSampleWithTest($iTestID,$iSampleInfoTableID);
				if($iMapID > 0){
					$sLogMsg = "--------{$sTestName} is Mapped With Sample->{$sSampleNameBarcode}  MapID = {$iMapID} at ".date('Y-m-d H:i:s');
					fLogger(10,$sLogMsg);					
				}
				$iMappID = fMapWorkflowWithTest($iTestID,$iWorkflowID);
				if($iMappID > 0){
					$sLogMsg = "--------{$sTestName} is Mapped With WorkflowID->{$iWorkflowID}  MapID = {$iMappID} at ".date('Y-m-d H:i:s');
					fLogger(10,$sLogMsg);					
				}
			}else{
				$sLogMsg = "--------{$sTestName} is Added into Master with TestID = {$iTestID} at ".date('Y-m-d H:i:s');
				fLogger(10,$sLogMsg);
			}
		}else{
			$sLogMsg = "--------{$sTestName} is Already Added into Master with TestID = {$iTestAdded} at ".date('Y-m-d H:i:s');
			fLogger(10,$sLogMsg);
		}

	}else{
		$sLogMsg = "-------- No Sample Info Found for Sequencer Found to Interact LimsDx using its API for Samples at ".date('Y-m-d H:i:s');
		fLogger(10,$sLogMsg);
	}
}
?>