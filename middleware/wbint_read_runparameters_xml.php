<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_runparameters_xml.php
  Description : This script is going to read the RunParameters.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$sLogMsg = "\r\n"."---SRT-- Script is started to read RunParameters.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(24,$sLogMsg);

// 2 for runParameter.xml
$iSeqFileType = 2; 

//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadRunParameters();

$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the RunParameters.xml files. at ".date('y-m-d H:i:s')."\r\n";
fLogger(24,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
	$aRunParameterInfo = array();
	$iSequencerDirID = $value['seq_directory_id'];
	$iSeqType = $value['seq_type'];
	
	///! Get the local path of file
	$sFileName = $value['seq_local_path'];
	
	///! check if the file is exist or not in local path
	$bFileExist = file_exists($sFileName);

	///! if file exist , then
	if($bFileExist == true){
		///!!  call the function which will read the samplesheet
		$aRunParameterInfo = ReadAndGetRunParametersXmlInfo($sFileName,$iSeqType);
		
		if(!empty($aRunParameterInfo)){
			///!!! extract the info from array
			
			$dRunStartDate = $aRunParameterInfo['dRunStartDate'];
			$iRunNumber = $aRunParameterInfo['iRunNumber'];
			$iScannerID = $aRunParameterInfo['iScannerID'];
			$iRunID = $aRunParameterInfo['iRunID'];
			$sExperimentName = $aRunParameterInfo['sExperimentName'];
			if($iSeqType==1){
				$aReadInfo = $aRunParameterInfo['aReadInfo'];
				if(!empty($aReadInfo)){
					$bAdded = addReadsInfoFromXML($iSequencerDirID,$iSeqFileType,$aReadInfo);
				}
			}
			fUpdateScannerID($iSequencerDirID,$iScannerID);
			//Extra Information to store
			$sFPGAVersion = $aRunParameterInfo['sFPGAVersion'];
			$sMCSVersion = $aRunParameterInfo['sMCSVersion'];
			$sRTAVersion = $aRunParameterInfo['sRTAVersion'];
			$sPR2BottleBarcode = $aRunParameterInfo['sPR2BottleBarcode'];
			$iReagentKitPartNumberEntered = $aRunParameterInfo['iReagentKitPartNumberEntered'];
			$sReagentKitVersion = $aRunParameterInfo['sReagentKitVersion'];
			$sReagentKitBarcode = $aRunParameterInfo['sReagentKitBarcode'];


	        // Array for label-value of XML
			$aRunParameterMaster = array(
						array('run_start_date',$dRunStartDate),
						array('run_number',$iRunNumber),
						array('run_scanner_id',$iScannerID),
						array('run_id',$iRunID),
						array('run_sExperimentName',$sExperimentName),
						array('run_sFPGAVersion',$sFPGAVersion),
						array('run_sMCSVersion',$sMCSVersion),
						array('run_sRTAVersion',$sRTAVersion),
						array('run_sPR2BottleBarcode',$sPR2BottleBarcode),
						array('run_iReagentKitPartNumberEntered',$iReagentKitPartNumberEntered),
						array('run_sReagentKitVersion',$sReagentKitVersion),
						array('run_sReagentKitBarcode',$sReagentKitBarcode),
				);

			if(!empty($aRunParameterMaster)){
				$bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aRunParameterMaster);
			}
			///!!! JSON for the array of XML info
			$sRunParameterInfoJSON = json_encode($aRunParameterInfo['allXML']);

			///!!! add the information into database

			$iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sRunParameterInfoJSON);
			if($iResult > 0){
				$iScanStatus = 3;
				$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
                  if($bResult){
                      // function to change the sequencer directory status to 6 !!
	                    disableSequencerOldStatus($iSequencerDirID,5);
						disableSequencerOldStatus($iSequencerDirID,7);
						disableSequencerOldStatus($iSequencerDirID,9);
						disableSequencerOldStatus($iSequencerDirID,13);
							
                      addSequncerMasterStatus($iSequencerDirID,6); //! 6 for we read the RunParameters into database
                  }
				//!! log
				$sLogMsg = "--------- RunParameters.xml Master information Added in Database at ".date('y-m-d H:i:s')."\r\n";
				fLogger(24,$sLogMsg);
			}else{
				///!! Error samplesheet master information can not be added
				//!! log
				$sLogMsg = "-xxx----- RunParameters.xml Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s')."\r\n";
				fLogger(24,$sLogMsg);
			}
		}		

	}else{
		///!! if File does'not Exist
		//!! log
		$sLogMsg = "--x-x-x-- RunParameters.xml File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s')."\r\n";
		fLogger(24,$sLogMsg);
	}
	
}



$sLogMsg = "---OVR-- Script is Finished to reading RunParameters.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(24,$sLogMsg);
?>