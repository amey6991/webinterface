<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_completedjobinfo_xml.php
  Description : This script is going to read the CompletedJobInfo.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';
require_once __DIR__.'/../'.'funEmails.php';

require_once PATH_TO_ROOT.'sendEmail.php';

$sSenderEmailId = EMAIL_SENDER_ID;
$sSenderName= EMAIL_SENDER_NAME;

$aDefaultList = fGetDefualtEmailList();
$aEmailFeature = getAllEmailFeature();
$bEmailErrorFeature = false;
$bEmailFinishFeature = false;
foreach ($aEmailFeature as $key => $aFeature) {
  if($aFeature['email_feature_name']=='Email on Sequencer Run Error'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailErrorFeature=true;
        break;
      }else{
        $bEmailErrorFeature = false;
        break;
      }
  }
  if($aFeature['email_feature_name']=='Email on Sequencer Run Fiinished'){
      $iActive = $aFeature['activate'];
      if($iActive==1){
        $bEmailFinishFeature=true;
        break;
      }else{
        $bEmailFinishFeature = false;
        break;
      }  	
  }
}

$sLogMsg = "\r\n"."---SRT-- Script is started to read CompletedJobInfo.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(25,$sLogMsg);

// 4 for CompletedJobInfo.xml
$iSeqFileType = 4; 

//! function called to get seuqncer for read their samplesheet
$aSequencer = fGetSequencerToReadCompletedJobInfo();
$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the CompletedJobInfo.xml files. at ".date('y-m-d H:i:s')."\r\n";
fLogger(25,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
	$aCompletedJobInfo = array();
	$iSequencerDirID = $value['seq_directory_id'];
	$sSeqDirname = $value['seq_dir_name'];
  	$sSeqDirPath = $value['seq_server_path'];
  	$iSeqType = $value['seq_type'];

  	$aEmail = array(
  			'dir_name'=>$sSeqDirname ,
  			'dir_path'=>$sSeqDirPath ,
  			'seq_type'=>$iSeqType 
  		);
	//!! Check the sequencer is already had the files and values
	      $bError=false;
          $sErrorMesage = "";
          $bSampleSheetScan = fCheckStatusForSequencer($iSequencerDirID,2);
          
          //$bSampleSheetRead = fCheckStatusForSequencer($iSequencerDirID,5);

          $bRunInfoScan = fCheckStatusForSequencer($iSequencerDirID,8);
          //$bRunInfoRead = fCheckStatusForSequencer($iSequencerDirID,9);

          $bRunParaScan = fCheckStatusForSequencer($iSequencerDirID,3);
          //$bRunParaRead = fCheckStatusForSequencer($iSequencerDirID,6);

	        //$bFastQScan = fCheckStatusForSequencer($iSequencerDirID,4);
	        //$bCompletedScan = fCheckStatusForSequencer($iSequencerDirID,100);
	        //$bFastQRead = fCheckStatusForSequencer($iSequencerDirID,7);  

          if($bSampleSheetScan==true){
            $sErrorMesage = 'We Reading CompletedJobInfo.xml file but we do not Found the SampleSheet, Please check it manually';
            $bError=true;
          }elseif($bRunInfoScan==true){
            $sErrorMesage = 'We Reading CompletedJobInfo.xml file but we do not Found the RunInfo.xml File, Please check it manually';
            $bError=true;
          }elseif($bRunParaScan==true){
            $sErrorMesage = 'We Reading CompletedJobInfo.xml file but we do not Found the RunParameters.xml File, Please check it manually';
            $bError=true;
          }


        if($bError==true){
            echo "Error ".$iSequencerDirID."->".$sErrorMesage."</br>";
            $bEntry = fCheckErrorMasterEntryForSequencer($iSequencerDirID);
            if($bEntry==true){
              $iError = fAddSequencerErrorOccured($iSequencerDirID,$iSeqType,$sErrorMesage);
                //!! Email When Error Occured
                $aEmail['error']=$sErrorMesage;
                
                          
                if(!empty($aDefaultList) && $bEmailErrorFeature==true){
                	foreach ($aDefaultList as $key => $aValue) {
                            $aReciepintName = array($aValue['user_name']);
                            $aReciepintEmail = array($aValue['user_email']);
                           
                            $iEmailTemplateFlag=EMAIL_SEQRUN_ERROR_TO_DEFAULT_LIST;
                            $aAttachement = array();
                            $iResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmail,$aAttachement,$sSenderEmailId,$sSenderName);
                  
                        if($iResult != 1){
                          $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN Got an ERROR , Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
                           fLogger(25,$sLogMsg);
                        }
                    }       
                }
            }
          }else{
            ///! Get the local path of file
			$sFileName = $value['seq_local_path'];
			
			///! check if the file is exist or not in local path
			$bFileExist = file_exists($sFileName);

			///! if file exist , then
			if($bFileExist == true){
				///!!  call the function which will read the samplesheet
				$aCompletedJobInfo = ReadAndGetCompleteJobXmlInfo($sFileName);

				if(!empty($aCompletedJobInfo)){
					///!!! extract the info from array
					
					$dtStartDateTime = $aCompletedJobInfo['dtStart'];
					$dtCompleted = $aCompletedJobInfo['dtCompleted'];
					$sSheetType = $aCompletedJobInfo['sSheetType'];
					$sWorkflow = $aCompletedJobInfo['sWorkflow'];
					$sRTAOutputFolder = $aCompletedJobInfo['sRTAOutputFolder'];
					
					// Array for label-value of XML
					$aCompleteJobInfoMaster = array(
									array('run_start_date',$dtStartDateTime),
									array('run_finish_date',$dtCompleted),
									array('run_sheet_type',$sSheetType),
									array('run_workflow',$sWorkflow),
									array('run_rtaoutputfolder',$sRTAOutputFolder),
						);

					if(!empty($aCompleteJobInfoMaster)){
						$bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aCompleteJobInfoMaster);
					}
					///!!! JSON for the array of XML info
					$sCompletedJobInfoJSON = json_encode($aCompletedJobInfo['allXML']);

					///!!! add the information into database

					$iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sCompletedJobInfoJSON);
					if($iResult > 0){
							$iScanStatus = 100;
							$bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
		                  	if($bResult){
		                      // function to change the sequencer directory status to 101 !!
		                      addSequncerMasterStatus($iSequencerDirID,101); //! 101 for we read the completedJobInfo into database
		                  	}
						//!! log
						$sLogMsg = "--------- CompletedJobInfo.xml Master information Added in Database at ".date('y-m-d H:i:s')."\r\n";
						fLogger(25,$sLogMsg);
					}else{
						///!! Error samplesheet master information can not be added
						//!! log
						$sLogMsg = "-xxx----- CompletedJobInfo.xml Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s')."\r\n";
						fLogger(25,$sLogMsg);
					}
				}
					//!! Email The Users for Completed Run
					if($bEmailFinishFeature == true){
					  if(!empty($aDefaultList)){
			              foreach ($aDefaultList as $key => $aValue) {
			                       $aReciepintName = array($aValue['user_name']);
			                       $aReciepintEmail = array($aValue['user_email']);
			                       
			                       $iEmailTemplateFlag=EMAIL_SEQRUN_FINISHED_TO_DEFAULT_LIST;
			                       $aAttachement = array();
			                      $iResult = fSendEmail($aReciepintEmail,$aReciepintName,$iEmailTemplateFlag,$aEmail,$aAttachement,$sSenderEmailId,$sSenderName);
			            
			                  if($iResult != 1){
			                    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------Sequencer RUN finished Email Send to {$sReciepintName} On {$sReciepintEmail} at ".date('Y-m-d H:i:s')."\r\n";
			                    fLogger(5,$sLogMsg);
			                  }
			              }       
			          }
					}			
			}else{
				///!! if File does'not Exist
				//!! log
				$sLogMsg = "--x-x-x-- CompletedJobInfo.xml File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s')."\r\n";
				fLogger(25,$sLogMsg);
			}
		}
}

$sLogMsg = "---OVR-- Script is Finished to reading CompletedJobInfo.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(25,$sLogMsg);
?>