<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_read_demultiplex_xml.php
  Description : This script is going to read the DemultiplexingStats.xml file from the sequencer local storage. We will get the important data for the sequencer information and save the information into database.
-->
<?php
//! Include the function pages , any classes if required for the script

require_once __DIR__.'/../'.'funSequencers.php';

$sLogMsg = "\r\n"."---SRT-- Script is started to read DemultiplexingStats.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(27,$sLogMsg);

// 7 for DemultiplexStats.xml
$iSeqFileType = 7; 

//! function called to get seuqncer for read their samplesheet
$aSequencer = fetSequencerToReadDemultiplexStatsXml();
// $aMaster =ReadAndGetDemultiplexStatsXmlInfo('/var/www/WebData/data/SequencingRuns/150403_NS500613_0013_AH73YJBGXX/DemultiplexingStats.xml');
// $aInfo = $aMaster['imp_info'];

// echo "<pre>";//print_r($aInfo);

// exit();
$iTotalSequencer = count($aSequencer);

//!! log
$sLogMsg = "--------- $iTotalSequencer , Sequencer found to read the DemultiplexingStats.xml files. at ".date('y-m-d H:i:s')."\r\n";
fLogger(27,$sLogMsg);

//! Loop the array of sequencer to get local storage path 
foreach ($aSequencer as $key => $value) {
  $aDemultiplexStatsInfo = array();
  $iSequencerDirID = $value['seq_directory_id'];
  $iSeqType = $value['seq_type'];
  
  ///! Get the local path of file
  $sFileName = $value['seq_local_path'];
  
  ///! check if the file is exist or not in local path
  $bFileExist = file_exists($sFileName);

  ///! if file exist , then
  if($bFileExist == true){
    ///!!  call the function which will read the samplesheet
    $aDemultiplexStatsInfo = ReadAndGetDemultiplexStatsXmlInfo($sFileName);
    
    if(!empty($aDemultiplexStatsInfo)){
      ///!!! extract the info from array
      $aInfo = $aDemultiplexStatsInfo['imp_info'];
      $iTotalBarcodeCount = $aDemultiplexStatsInfo['iTotalBarcodeCount'];
      $aSampelCount = array();
      foreach ($aInfo as $key => $aValue) {
        if($key=='all'){
          continue;
        }
        $sSampleName = $key;
        foreach ($aValue as $key1 => $aMainValue) {
          if($key1=='all'){
            continue;
          }
          foreach ($aMainValue as $key => $aData) {
            $sBarcodeCount = $aData['sBarcodeCount'];

          }
        }
      }
      // Array for label-value of XML
      $aRunMaster = array(
              array('iTotalBarcodeCount',$iTotalBarcodeCount),
        );

      // Array for label-value of XML's master info
      if(!empty($aRunMaster)){
        $bAddedMaster = addImportantXMLInfo($iSequencerDirID,$iSeqFileType,$aRunMaster);
      }

  
      ///!!! JSON for the array of XML info
      $sDemultiplexStatsInfoJSON = json_encode($aDemultiplexStatsInfo['allXML']);

      ///!!! add the information into database

      $iResult = addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sDemultiplexStatsInfoJSON);

      if($iResult > 0){
        $iScanStatus = 12;
        $bResult = disableSequencerOldStatus($iSequencerDirID,$iScanStatus);
                  if($bResult){
                      // function to change the sequencer directory status to 6 !!
                      disableSequencerOldStatus($iSequencerDirID,5);
                      disableSequencerOldStatus($iSequencerDirID,9);
                      disableSequencerOldStatus($iSequencerDirID,6);
                      disableSequencerOldStatus($iSequencerDirID,7);
                      addSequncerMasterStatus($iSequencerDirID,13); //! 6 for we read the RunParameters into database
                  }
        //!! log
        $sLogMsg = "--------- DemultiplexingStats.xml Master information Added in Database at ".date('y-m-d H:i:s')."\r\n";
        fLogger(27,$sLogMsg);
      }else{
        ///!! Error samplesheet master information can not be added
        //!! log
        $sLogMsg = "-xxx----- DemultiplexingStats.xml Master information Not Added in Database , Error occured at ".date('y-m-d H:i:s')."\r\n";
        fLogger(27,$sLogMsg);
      }
    }   

  }else{
    ///!! if File does'not Exist
    //!! log
    $sLogMsg = "--x-x-x-- DemultiplexingStats.xml File Not Fount in Local Storage , as stored in database , at ".date('y-m-d H:i:s')."\r\n";
    fLogger(27,$sLogMsg);
  }
  
}



$sLogMsg = "---OVR-- Script is Finished to reading DemultiplexingStats.xml file for each sequencer at ".date('y-m-d H:i:s')."\r\n";
fLogger(27,$sLogMsg);
?>