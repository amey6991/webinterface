<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";

	function GetAllFileDataForUserID($iUserID){
        $aFiles = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sFileTable = DATABASE_TABLE_PREFIX.'_files';
        $sFileUserTable = DATABASE_TABLE_PREFIX.'_user_files';
        $sIQuery = "SELECT * FROM `{$sFileTable}` as a LEFT JOIN `{$sFileUserTable}` as b on a.`file_id`=b.`file_id` and b.`user_id`={$iUserID} WHERE a.`deleted` =0 AND b.`deleted`=0";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            while($aRow = $sResult->fetch_assoc()){
                $aFiles[]=$aRow;    
            }
        }
        return $aFiles;
    }
    
    function GetAllFileDataForUserTypeID($iType){

    }

    function mapUserWithFIle($iUserID,$iFileID){
            $iInsertID=0;
            $DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sUserFileTable = DATABASE_TABLE_PREFIX.'_user_files';
            $dUploadedOn=date(DB_DATETIME_FORMATE);
            $sIQuery = "INSERT INTO `{$sUserFileTable}`(`map_id`,`user_id`,`file_id`,`deleted`)
                        VALUES (NULL,'{$iUserID}','{$iFileID}','0')";
            
            $sResult = $conn->query($sIQuery);        
            if($sResult){
                $iInsertID = $conn->insert_id;
            }
            return $iInsertID;
    }

    function getTotalFiles($iUserType,$iUserClientID){
        $iTotalFile=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sFileTable = DATABASE_TABLE_PREFIX.'_files';
        $sUserFileTable= DATABASE_TABLE_PREFIX.'_user_files';
        $sUseTable= DATABASE_TABLE_PREFIX.'_users';
        $sWhere="";
        if($iUserType==2){
           $sSQuery="SELECT count(*) as `total` FROM  `{$sFileTable}` as a LEFT JOIN `{$sUserFileTable}` as b on b.`file_id`=a.`file_id` 
                    LEFT JOIN `{$sUseTable}` as c on c.`user_id`=b.`user_id` 
                    WHERE a.`deleted`=0 and c.`client_id`={$iUserClientID} and c.`user_type_id`=3";
        }else{
            $sSQuery = "SELECT count(*) as `total` FROM  `{$sFileTable}` as f LEFT JOIN `{$sUserFileTable}` as uf on uf.`file_id`=f.`file_id`
                    LEFT JOIN `{$sUseTable}` as u ON u.`client_id`={$iUserClientID}
                    AND uf.`user_id`= u.`user_id`
                    WHERE f.`deleted`=0";
        }
        
        $sResult = $conn->query($sSQuery);        
        if($sResult){
            $aRow = $sResult->fetch_assoc();
            $iTotalFile = $aRow['total'];
        }
        return $iTotalFile;
    }
?>