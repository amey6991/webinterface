<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";

	$sFirstName = $_POST['firstName'];
	$sLastName = $_POST['lastName'];
	$sFullName =$sFirstName.' '.$sLastName;
	$iUserType = $_POST['user_type'];
	$iClientID = $_POST['client_id'];
	$sStateName = $_POST['stateName'];
	$sCityName = $_POST['cityName'];
	$sAddress = $_POST['address'];
	$iMobileNumber = $_POST['mobile'];
	$sEmail = $_POST['emailAddress'];
	$sUserName = $_POST['userName'];
	$sPassword = $_POST['password'];
	$iInsertID=0;

	$oUsers = new User();
	$iInsertID = $oUsers->fAddUsers($sFirstName,$sLastName,$sFullName,$iUserType,$iClientID,$sStateName,$sCityName,$sAddress,$iMobileNumber,$sEmail,$sUserName,$sPassword);
	
	if($iInsertID>0)
	{
		$sMsg = array();
		$sMsg[] = "S2";
	    redirectWithAlert("viewUsers.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E2";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("addUser.php", $sMsg);
		}
?>