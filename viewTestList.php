<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "viewList.php";
$iPageID=12;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "View List";
$aTestList = getTestList();
$aWorkflowList = getWorkflowList();


include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"View Test List","link"=>"viewTestList.php","isActive"=>true)
            );
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <ol class="breadcrumb navbar-breadcrumb">
                                            <?php echo parseBreadcrumb($aBreadcrumb); ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                         <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable" style="margin-left:40px">
                                            <thead>
                                                <tr>
                                                    <th>Sr No</th>
                                                    <th>Test Name</th>
                                                    <th>Action</th>
                                                    <th>Mapped Workflow</th>
                                                    <th>Mapp Samples</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <?php
                                                    for($iii=0;$iii<count($aTestList);$iii++){
                                                        $iTestID = $aTestList[$iii]['test_id'];
                                                        $iWorkflowID=getMappedWorkflowIDForTestID($iTestID);

                                                        $sTestName = $aTestList[$iii]['test_name'];
                                                        $iStatus = $aTestList[$iii]['status'];
                                                        $sMappButton ="<button type='button' class='btn btn-info classMappButton' id='{$iTestID}'>Map Samples</button>";
                                                        if($iStatus==1){
                                                            $sFreezeButton = "<button type='button' class='btn btn-info classFreezeButton' id='{$iTestID}'>Freeze</button>";
                                                        }else{
                                                            $sFreezeButton = "<button type='button' class='btn btn-danger classUnFreezeButton' id='{$iTestID}'>UnFreeze</button>";
                                                        }
                                                ?>
                                                <tr>
                                                    <td><?php echo $iii+1;?></td>
                                                    <td>
                                                        <?php echo $sTestName;?>
                                                    </td>
                                                    
                                                    <td><?php echo $sFreezeButton;?></td>
                                                    <td>   
                                                        <select id="idMappedSample" class="form-control" style="width:150px;" onchange="setSample(this.value,<?php echo $iTestID;?>)">
                                                            <option value="">Select Any</option>
                                                            
                                                            <?php
                                                                if(!empty($aWorkflowList)){
                                                                    foreach ($aWorkflowList as $key => $aList) {
                                                                            if($aList['wf_id']==$iWorkflowID){
                                                                    ?>
                                                                            <option value="<?php echo $aList['wf_id'];?>" selected><?php echo $aList['workflow_name']; ?></option>
                                                                    <?php
                                                                            }else{
                                                                        ?>
                                                                            <option value="<?php echo $aList['wf_id'];?>"><?php echo $aList['workflow_name']; ?></option>
                                                                        <?php
                                                                            }
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                    <?php echo $sMappButton;?>
                                                    </td>
                                                </tr>
                                                <?php  }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        
        $('#idDataTable').DataTable();

        $(".classMappButton").click(function(){
            var iTestID = this.id;
            window.open('viewSampleTestMapping.php?iTestID='+iTestID,'_blank');
        });

    	$(".classFreezeButton").click(function(){
    		var iTestID = this.id;
    		if(confirm('Are you sure want to Freeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'FreezeTest',iTestID:iTestID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                                window.location='viewTestList.php';    
                            }
                });
            }
        });

        $(".classUnFreezeButton").click(function(){
            var iTestID = this.id;
            if(confirm('Are you sure want to UnFreeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'UnFreezeTest',iTestID:iTestID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                           window.location='viewTestList.php';
                        }
                });
            }
        });

    	// $("#idRenameWorkflow").click(function(){
    	// 	var iWorkflowID = $("#idWorkflowID").val();
    	// 	var sWorkflowName = $("#idNewWorkflowName").val();
    		
    	// 	if(sWorkflowName!=''){
     //            if(confirm('Are Your Sure to Update Workflow Name ?')){
     //                $.ajax({
     //                    url:"ajaxRequest.php",
     //                    data:{sFlag:'UpdateWorkflow',iWorkflowID:iWorkflowID,sWorkflowName:sWorkflowName},
     //                    async:true,
     //                    method:"GET",
     //                    success:function(bResult){
     //                        if(bResult){
     //                            window.location="viewWorkflowList.php";    
     //                        }
     //                    }
     //                });
     //            }else{
     //                $("#idWorkflowID").val('');
     //                $("#idUpdateWorkflowModal").modal('hide');
     //            }
     //        }else{
     //            $("#idWorkflowID").val('');
     //            $("#idUpdateWorkflowModal").modal('hide');
     //        }
    	// });
    });
    function setSample(iWorkflowID,iTestID){
        if(confirm('Are you sure to Map this workflow to Test ?')){
            $.ajax({
                    url:"ajaxRequest.php",
                    data:{sFlag:'MapTestWorkflow',iTestID:iTestID,iWorkflowID:iWorkflowID},
                    async:true,
                    method:"GET",
                    success:function(iResult){
                       window.location='viewTestList.php';
                    } 
                });
        }
    }
    </script>
        <div class="modal fade" id="idMapSampleModal" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update File Name</h4>
              </div>
              <div class="modal-body">
                    <input type="text" name="new_workflow_name" id="idNewWorkflowName" class="form-control">
                    <input type="hidden" name="file_id" id="idWorkflowID">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="idRenameWorkflow">Update</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>