<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";

	$sClientName = $_POST['client_name'];
	$sClientEmail = $_POST['client_email'];
	$sClientMobile = $_POST['client_mobile'];
	$sClientCountry = $_POST['client_country'];
	$sClientState = $_POST['client_state'];
	$sClientCity = $_POST['client_city'];
	$sClientAddress = $_POST['client_address'];
	$sClientUrl = $_POST['client_url'];
	
	$sPersonName = $_POST['person_name'];
	$sPersonEmail = $_POST['person_email'];
	$sPersonContact = $_POST['person_mobile'];

	$iInsertID=0;

	$oUsers = new User();
	$iInsertID = $oUsers->fAddClient($sClientName,$sClientEmail,$sClientMobile,$sClientCountry,$sClientState,$sClientCity,$sClientAddress,$sClientUrl,$sPersonName,$sPersonEmail,$sPersonContact);
	
	if($iInsertID>0)
	{
		$sMsg = array();
		$sMsg[] = "S4";
	    redirectWithAlert("viewClients.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E4";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("addClient.php", $sMsg);
	}
?>