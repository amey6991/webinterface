<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";


$sactionTitle = "View Action Listing";

$aActionData = fGetAllActions();
$iCountAction = count($aActionData);

$oSession = new SessionManager();
$iType = $oSession->iType;

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Action </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Feature Name</th>
                                                <th>Action Code</th>
                                                <th>Action Name</th>
                                                <th>Action Notes</th>
                                                <th>Action Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountAction;$iii++){
                                                   $iActionID = $aActionData[$iii]['action_id'];
                                                   $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iActionID}'>Update</button>";
                                                   $aFeatureData = fGetFeatureDetail($aActionData[$iii]['feature_id']);
                                                   $sFeatureName = $aFeatureData[0]['feature_name'];
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td><?php echo $sFeatureName;?></td>
                                                <td><?php echo $aActionData[$iii]['action_code'];?></td>
                                                <td><?php echo $aActionData[$iii]['action_name'];?></td>
                                                <td><?php echo $aActionData[$iii]['notes'];?></td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".classUpdateButton").click(function(){
            var iActionID = this.id;
            window.location="updateAction.php?iActionID="+iActionID;
        });
        $('#idDataTable').DataTable();
    });

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>