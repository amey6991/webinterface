<?php

include_once dirname(__FILE__)."/config.app.php";
include_once dirname(__FILE__)."/config.database.php";
include_once dirname(__FILE__)."/config.message_constants.php";
include_once dirname(__FILE__)."/config.email_alerts.php";
include_once dirname(__FILE__)."/config.Email_SMS.php";
include_once dirname(__FILE__)."/config.feature.php";
include_once dirname(__FILE__)."/config.workflow.php";
include_once dirname(__FILE__)."/config.sequencer.php";
include_once dirname(__FILE__)."/config.EmailTemplate.php";

date_default_timezone_set(DATE_DEFAULT_TIMEZONE);
?>