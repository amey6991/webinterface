<?php
/*
	This Config file is for the Define contant value for the Sequencer 	
*/
	
	// Sequencer Path for NextSeq
	define('sNextSeqDirectoryPath', "/home/plus91/data/Sequencers/192.168.35.32/");
	define('iNextSeqType', 2);
	
	// Sequencer Path for MiSeq
	define('sMiSeqDirectoryPath', "/home/plus91/data/Sequencers/192.168.35.101/"); // Sequencers/192.168.35.101/
	define('iMiSeqType', 1);
	

	// Sequencer Host name
	define('sIP', 'localhost');
	define('iPort', 22);
	
	// Sequencer Server Username Password
	define('sUserName', 'plus91');
	define('sPassword', 'Plus91.13912');
	
	// Sequencer Demultiplex.xml file Path
	define('sDemultiplexPathInNextSeq', "/Data/Intensities/BaseCalls/Stats/");

	// Sequencer Program Path
	define('sExtractionProgrammPath', "/home/plus91/data/SetupInterop/setup/bin/interop2csv");
	define('sSummaryProgrammPath', "/home/plus91/data/SetupInterop/setup/bin/summary");
	// Sequencer CSV Storage Path
	define('sRemoteCopyPathForExtractionCSV', "/home/plus91/data/sequencer_extraction/");
	define('sRemoteCopyPathForSummaryCSV', "/home/plus91/data/sequencer_summary/");


	//!! galaxy Config
	// API Key
	define('galaxy_api', '6f45333adda9b0a6a56c2376eb6b31e4');
	// URL
	define('galaxy_url', '68.106.75.78:35202');
	// Default Workflow ID
	define('galaxy_workflow_id', '1cd8e2f6b131e891');
	// Defualt Library ID
	define('galaxy_library_id', 'c9468fdb6dc5c5f1'); // 03501d7626bd192f Test , using PHPLibrart
	// Defualt Reference Library ID
	define('galaxy_reference_library_id', '0a248a1f62a0cc04');
	// Defualt Reference Library-Folder ID
	define('galaxy_reference_library_folder_id', 'Fba751ee0539fff04');
	// Default DB key
	define('sDBKey', 'hg19')
?>