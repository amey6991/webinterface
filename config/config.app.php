<?php
//! Set organization name
define('ORG_NAME', 'Web Interface');

//! Set organization name
define('CONFIG_BRAND_NAME', 'Research Dx');

//! Set organization logo
define('MEDIXCEL_LOGO', 'medixcel_logo.png');

//! Set organization logo
define('ORG_LOGO', 'org_logo.jpg');

//! Sessio  Timeout
define('SESSION_TIME','30');

//! define site url path
define('SITE_URL', 'http://68.106.75.78:35200/WebInterface/');

//! Set Organisation Logo URL
define('ORG_LOGO_URL', 'http://68.106.75.78:35200/WebInterface/images/ResearchDx.jpg');

//! Set PLUS91 logo url
define('PLUS91_LOGO_URL', 'http://68.106.75.78:35200/WebInterface/images/plus91_logo.jpg');

//! A unique Medixcel key for multiple installation on same domain
define('CONFIG_APP_UNIQUE_KEY', '231asd5465456wq456q4w56e565a4s56d');

//! Set Default Timezone
define('DATE_DEFAULT_TIMEZONE', 'Asia/Calcutta');


//! Set UI date format
define('UI_DATE_FORMATE', 'd-m-Y');

//! Set UI Time format
define('UI_TIME_FORMATE', 'h:i A');

//! Date format to be Used
define('DB_DATE_FORMATE', 'Y-m-d');

//! Time format to be Used
define('DB_TIME_FORMATE', 'H:i:s');

//! Date Time format to be Used
define('DB_DATETIME_FORMATE', 'Y-m-d H:i:s');

define('CURRENCY_TEXT', 'Rs. ');

define('DEFAULT_FILE_PATH','/var/www/WebInterface/Files/');

define('PATH_TO_ROOT','/var/www/WebInterface/');
?>