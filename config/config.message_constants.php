<?php

//! @file 
/*! This file contains all alert message constants. 
*	Constants starting with S prefix are the Success Messages
*	Constants starting with W prefix are the Warning Messages
*	Constants starting with E prefix are the Error Messages
*
*/

//! Sucess Messages
define("S1", "Login Successful");
define("S2", "User Added Successfully");
define("S3", "User Updated Successfully");
define("S4", "Client Added Successfully");
define("S5", "Client Updated Successfully");
define("S6", "Page Added Successfully");
define("S7", "Page Updated Successfully");
define("S8", "Feature Added Successfully");
define("S9", "Feature Updated Successfully");
define("S10", "Action Added Successfully");
define("S11", "Action Updated Successfully");
define("S12", "Permission Added Successfully");
define("S13", "Permission Updated Successfully");
define("S14", "File Uploaded Successfully");
define("S15", "File OverWrite Successfully");

//! Error Messages
define("E1", "Username or password is wrong. Please try again.");
define("E2", "Some error occures while Adding User. Please try again.");
define("E3", "Some error occures while Updating User. Please try again.");
define("E4", "Some error occures while Adding Client. Please try again.");
define("E5", "Some error occures while Updating Client. Please try again.");
define("E6", "Some error occures while Adding Page. Please try again.");
define("E7", "Some error occures while Updating Page. Please try again.");
define("E8", "Some error occures while Adding Feature. Please try again.");
define("E9", "Some error occures while Updating Feature. Please try again.");
define("E10", "Some error occures while Adding Action. Please try again.");
define("E11", "Some error occures while Updating Action. Please try again.");
define("E12", "Some error occures while Adding Permission. Please try again.");
define("E13", "Some error occures while Updating Permission. Please try again.");
define("E14", "Some error occures while Uploading Files. Please check the File Type Or File Size You allowed. Please try again.");
define("E15", "Some error occures while Uploading Files. Please check the File Destination path you Given. Please try again.");
//! Warning Messages

?>