<?php
/*
* File contains define variables for Email templates
* Created On : 20th April 2016
* Created By : Amey
*/

define('EMAIL_SEQRUN_FINISHED_TO_DEFAULT_LIST', 1);

define('EMAIL_SEQRUN_ERROR_TO_DEFAULT_LIST', 2);

define('EMAIL_SEQRUN_START_TO_DEFAULT_LIST', 3);
?>