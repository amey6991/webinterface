<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
set_time_limit(120);

$sPageTitle = "View Users";
$aPermission = fGetAllPermissionData();
$iCountPermission = count($aPermission);

$oSession = new SessionManager();
$iType = $oSession->iType;                             
include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Permissions</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>User Entity Type</th>
                                                <th>User Entity</th>
                                                <th>Permission Entity Type</th>
                                                <th>Permission Entity</th>
                                                <th>Permission Level</th>
                                                <th>Update</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountPermission;$iii++){
                                                    $iPermissionID = $aPermission[$iii]['id'];
                                                    $iUserEntity = $aPermission[$iii]['user_entity_type'];
                                                    $iUserEntityID = $aPermission[$iii]['user_entity_id'];
                                                    $iPermEntityType = $aPermission[$iii]['perm_entity_type'];
                                                    $iPermEntityID = $aPermission[$iii]['perm_entity_id'];

                                                    $sOptionEntity="";
                                                    if($iPermEntityType==0){
                                                        $aPermEntityData = fGetAllPages();
                                                         for($jjj=0;$jjj<count($aPermEntityData);$jjj++){
                                                            if((int)$iPermEntityID == (int)$aPermEntityData[$jjj]['page_id']){
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$jjj]['page_id']}' selected>{$aPermEntityData[$jjj]['page_code']}</option>";
                                                            }else{
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$jjj]['page_id']}'>{$aPermEntityData[$jjj]['page_code']}</option>";
                                                            }
                                                        }
                                                     }else if($iPermEntityType==1){
                                                        $aPermEntityData = fGetAllFeatures();
                                                         for($kkk=0;$kkk<count($aPermEntityData);$kkk++){
                                                            if((int)$iPermEntityID == (int)$aPermEntityData[$kkk]['feature_id']){
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$kkk]['feature_id']}' selected>{$aPermEntityData[$kkk]['feature_code']}</option>";
                                                            }else{
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$kkk]['feature_id']}'>{$aPermEntityData[$kkk]['feature_code']}</option>";
                                                            }
                                                        }
                                                     }else if($iPermEntityType==2){
                                                        $aPermEntityData = fGetAllActions();
                                                         for($mmm=0;$mmm<count($aPermEntityData);$mmm++){
                                                            if((int)$iPermEntityID == (int)$aPermEntityData[$mmm]['action__id']){
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$mmm]['action__id']}' selected>{$aPermEntityData[$mmm]['action_code']}</option>";
                                                            }else{
                                                                $sOptionEntity.="<option value='{$aPermEntityData[$mmm]['action__id']}'>{$aPermEntityData[$mmm]['action_code']}</option>";
                                                            }
                                                        }
                                                    }
                                                    
                                                    $iPermLevel = $aPermission[$iii]['perm_level'];
                                                    $aUserEntityData=array();
                                                    $sOptionUserEntity="";
                                                    if($iUserEntity==0){
                                                        $aUserEntityData=fGetAllUsers();
                                                        for($kkk=0;$kkk<count($aUserEntityData);$kkk++){
                                                            if($iUserEntityID==$aUserEntityData[$iii]['user_id']){
                                                                $sOptionUserEntity.="<option value='$aUserEntityData[$kkk]['user_id']' selected>$aUserEntityData[$kkk]['full_name']</option>";
                                                            }else{
                                                                $sOptionUserEntity.="<option value='$aUserEntityData[$kkk]['user_id']'>$aUserEntityData[$kkk]['full_name']</option>";
                                                            }
                                                        }
                                                    }else{
                                                        $aUserEntityData=fGetUserTypes();
                                                        for($jjj=0;$jjj<count($aUserEntityData);$jjj++){
                                                            if((int)$iUserEntityID == (int)$aUserEntityData[$jjj]['user_type_id']){
                                                                $sOptionUserEntity.="<option value='{$aUserEntityData[$jjj]['user_type_id']}' selected>{$aUserEntityData[$jjj]['user_type']}</option>";
                                                            }else{
                                                                $sOptionUserEntity.="<option value='{$aUserEntityData[$jjj]['user_type_id']}'>{$aUserEntityData[$jjj]['user_type']}</option>";
                                                            }
                                                        }
                                                    }
                                                    $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iPermissionID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td>
                                                    <select class="form-control" id="idUserEntity" name="user_entity">
                                                        <?php 
                                                            if($iUserEntity==0){
                                                        ?>
                                                            <option value='0' selected>User</option>
                                                            <option value='1'>user Type</option>
                                                        <?php
                                                            }else{
                                                        ?>
                                                            <option value='0'>User</option>
                                                            <option value='1' selected>user Type</option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="idUserEntityID" name="user_entity_id" class="form-control">
                                                <?php 
                                                        echo $sOptionUserEntity;
                                                ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="perm_entity_type" id="idPermEntitytype" class="form-control">
                                                <?php
                                                    if($iPermEntityType==0){
                                                ?>
                                                        <option value='0' selected>Page</option>
                                                        <option value='1'>Feature</option>
                                                        <option value='2'>Action</option>
                                                <?php     
                                                    }else if($iPermEntityType==1){
                                                ?>
                                                        <option value='0'>Page</option>
                                                        <option value='1' selected>Feature</option>
                                                        <option value='2'>Action</option>
                                                <?php
                                                    }else if($iPermEntityType==2){
                                                ?>
                                                        <option value='0'>Page</option>
                                                        <option value='1'>Feature</option>
                                                        <option value='2' selected>Action</option>
                                                <?php
                                                    }
                                                ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="entity_type_id" id="idEntityType" class="form-control">
                                                        <?php echo $sOptionEntity; ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="perm_level" id="idPermLevel" class="form-control">
                                                <?php
                                                    if($iPermEntityType==0 || $iPermEntityType==1) {
                                                        if($iPermLevel==0){
                                                    ?>
                                                            <option value='0' selected>No Permission</option>
                                                            <option value='1'>Partial Permission</option>
                                                            <option value='2' >Full Permission</option> 
                                                    <?php
                                                        }else if($iPermLevel==1){
                                                    ?>
                                                            <option value='0'>No Permission</option>
                                                            <option value='1' selected>Partial Permission</option>
                                                            <option value='2'>Full Permission</option>  
                                                    <?php
                                                        }else if($iPermLevel==2){
                                                    ?>
                                                            <option value='0'>No Permission</option>
                                                            <option value='1'>Partial Permission</option>
                                                            <option value='2' selected>Full Permission</option>  
                                                    <?php
                                                        }
                                                    }else{
                                                        if($iPermLevel==0){
                                                    ?>
                                                            <option value='0' selected>True</option>
                                                            <option value='1'>False</option>
                                                    <?php
                                                        }else if($iPermLevel==1){
                                                    ?>
                                                            <option value='0'>True</option>
                                                            <option value='1' selected>False</option>
                                                    <?php
                                                        }
                                                    }
                                                ?>
                                                    </select>
                                                </td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        var aPermissionData = '<?php echo json_encode($aPermission);?>';
        // for(var iii=0;iii<aPermissionData.length;iii++){
        //     console.log(aPermissionData);
        // }
        console.log(aPermissionData);
        $(".classUpdateButton").click(function(){
            var iUserID = this.id;
            //window.location="updateUser.php?iUserID="+iUserID;
        });
        $('#idDataTable').DataTable();
    });

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>