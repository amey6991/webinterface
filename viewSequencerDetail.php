<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funWebinterfaceWorkflow.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funSequencers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";


$sPageTitle = "View Sequencer Detail";
$iSeqTableID = $_GET['iSeqTableID'];

$oSession = new SessionManager();
$iUserID = $oSession->iUserID;


$aSequencer =fGetSequencerDetailForSequencerID($iSeqTableID);
$aXMLFileInfo = fGetParametersForSequencer($iSeqTableID);
$aCSV = $aXMLFileInfo['csv_info'];
$aCSVSample = $aXMLFileInfo['csv_sample_info'];

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"View Sequencer Detail","link"=>"viewSequencerDetail.php?iSeqTableID={$iSeqTableID}","isActive"=>true)
            );
?>

<!-- Main Content -->
<div class="container-fluid">
    <div class="side-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <ol class="breadcrumb navbar-breadcrumb">
                                <?php echo parseBreadcrumb($aBreadcrumb); ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-body">
                            <table class="datatable table table-hover table-bordered" cellspacing="0" width="100%" id="idDataTable">
                                <tbody>
                                    <tr>
                                        <td>Sequencer ID</td>
                                        <td><?php echo $aSequencer['seq_scanner_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Run ID</td>
                                        <td><?php echo $aSequencer['run_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>SequencerRun Type</td>
                                        <td>
                                            <?php 
                                                if($aSequencer['seq_type']==1){
                                                    echo "MiSeq";
                                                }elseif($aSequencer['seq_type']==2){
                                                    echo "NextSeq";
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sequencer Cycle Number</td>
                                        <td><?php echo $aSequencer['seq_cycle_number']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Started On</td>
                                        <td><?php echo date('m-d-Y H:i:s',strtotime($aSequencer['started_on'])); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div>
                                <strong>Other Information </strong> <hr></br>
                                <?php
                                $aXML = $aXMLFileInfo['xml_info'];
                                if(!empty($aXML)){
                                ?>
                                <table class="datatable table table-hover table-bordered" cellspacing="0" width="100%" id="idDataTable">
                                    <tbody>
                                        <tr>
                                            <td>FPGA Version</td>
                                            <td><?php echo $aXML[5]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>MCS Version</td>
                                            <td><?php echo $aXML[6]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>RTA Version</td>
                                            <td><?php echo $aXML[7]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>PR2BOTTLE Barcode</td>
                                            <td><?php echo $aXML[8]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Reagent Kit Part Number Entered</td>
                                            <td><?php echo $aXML[9]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Reagent Kit Version</td>
                                            <td><?php echo $aXML[10]['xml_tag_value']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Reagent Kit Barcode</td>
                                            <td><?php echo $aXML[11]['xml_tag_value']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                                    }else{
                                        echo "No XML Data Available. </br>";
                                    }
                                ?>
                            </div>
                            <div>
                                <strong>CSV Information</strong> <hr></br>
                                <?php
                                if(!empty($aCSV)){
                                    $sWorkflow = $aCSV['workflow'];
                                    $sApplication = $aCSV['application'];
                                    $sExperimentName = $aCSV['experiment_name'];
                                    $sInvestigatorName = $aCSV['investigator_name'];
                                    $sIEMFileVersion = $aCSV['IEMFileVersion'];
                                    $sAssay = $aCSV['assay'];
                                ?>
                                <table class="datatable table table-hover table-bordered" cellspacing="0" width="100%" id="idDataTable">
                                    <thead>
                                        <tr>
                                            <td>Workflow</td>
                                            <td>Application</td>
                                            <td>experiment_name</td>
                                            <td>Investigator Name</td>
                                            <td>IEMFileVersion</td>
                                            <td>Assay</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $sWorkflow;?></td>
                                            <td><?php echo $sApplication;?></td>
                                            <td><?php echo $sExperimentName;?></td>
                                            <td><?php echo $sInvestigatorName;?></td>
                                            <td><?php echo $sIEMFileVersion;?></td>
                                            <td><?php echo $sAssay;?></td>
                                        </tr>
                                   </tbody>
                                </table>
                                <?php
                                }else{
                                    echo "No CSV Data Available. </br>";
                                }

                                if(!empty($aCSVSample)){
                                    echo "</br><strong>Sample Information:</strong> <hr></br>";
                            ?>
                                <table class="datatable table table-hover table-bordered" cellspacing="0" width="100%" id="idDataTable">
                                    <thead>
                                        <tr>
                                            <td>Sample ID</td>
                                            <td>Sample Name</td>
                                            <td>Index_17 ID</td>
                                            <td>Index</td>
                                            <td>Index_15 ID</td>
                                            <td>Index2</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($aCSVSample as $key => $aSample) {
                                    ?>
                                        <tr>
                                            <td><?php echo $aSample['sample_ID'];?></td>
                                            <td><?php echo $aSample['sample_name_barcode'];?></td>
                                            <td><?php echo $aSample['index_17_id'];?></td>
                                            <td><?php echo $aSample['index'];?></td>
                                            <td><?php echo $aSample['index_15_id'];?></td>
                                            <td><?php echo $aSample['index2'];?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                   </tbody>
                                </table>
                            <?php
                                }else{
                                    echo "No Sample Data Available. </br>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chart">
        
    </div>
</div>
        
<script type="text/javascript">
    $(document).ready(function(){
       
    });
</script>

<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>