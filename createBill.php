<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$sPageTitle = "Dashboard";

$aBreadcrumb = array(
    array(
        "title"=>"Dashboard",
        "link"=>"dashboard.php"
        ),
    array(
        "title"=>"POS",
        "link"=>""
        ),
    array(
        "title"=>"Create Bill",
        "link"=>"createBill.php",
        "isActive"=> true
        )
    );

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";

?>
<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Create Bill</span>
            <div class="description">Create Bills </div>
        </div>
        <div class="row">
            <div class="col-xs-6"></div>
            <div class="col-xs-6">
                <div class="row" data-bind="with: oPatient">
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail">
                            <img data-bind="attr: { src: sProfileImage }">
                        </a>
                    </div>
                    <div class="col-xs-8 classPatientControlCard" >
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="form-group classBlockTwitterTypeaheadContainer">
                                    <label>Patient Name</label>
                                    <input type="text" id="idPatientName" name="patient_name" data-bind="value: sPatientName" class="form-control input-sm classInput classInputText classInputPatientName" placeholder="Patient Name">
                                    <input type="hidden" name="medixcel_patient_id" data-bind="value: iMedixcelPatientID">
                                    <input type="hidden" name="patient_id" data-bind="value: iPatientID">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender" data-bind="value: sGender" class="form-control input-sm classInput classInputSelect classInputGender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <input type="text" name="date_of_birth" data-bind="value: dDOB" class="form-control input-sm classInput classInputText classInputGender" placeholder="Date Of Birth">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Age</label>
                                    <input type="text" name="age" data-bind="value: iAge" class="form-control input-sm classInput classInputText classInputAge" placeholder="Age">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Blood Group</label>
                                    <select name="blood_group" data-bind="value: sBloodGroup" class="form-control input-sm classInput classInputSelect classInputBloodGroup">
                                        <option value="">Select</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="form-group">
                                            <label>Mobile No.</label>
                                            <input type="text" name="mobile_no" data-bind="value: sMobile" class="form-control input-sm classInput classInputText classInputMobileNo" placeholder="Mobile No.">
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" data-bind="value: sEmail" class="form-control input-sm classInput classInputText classInputEmail" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Bill Detail</div>
                            <div class="sub-title">Bill No:<span data-bind="text: oBill.iBillID" class="description"></span>
                              <div>Bill Date:<span data-bind="text: oBill.dBillDate" class="description"></span>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 classBlockTwitterTypeaheadContainer">
                                <input type="text" id="idItemSuggestionbox" class="form-control input-sm classInput classInputText classInputItemName" placeholder="Add Item">
                                <input type="hidden" id="idItemSuggestionID" data-bind="value: oBill.iSuggestionItemID">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary btn-sm classNoTopSpacing" data-bind="attr : {'disabled': oBill.iSuggestionItemID == 0 ? true:false}">Add Item</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-hover" data-bind="visible: aItemParticulars().length > 0">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Item No</th>
                                            <th>Item Code</th>
                                            <th>Item Name</th>
                                            <th>Item Qty</th>
                                            <th>Item rate</th>
                                            <th>Item Tax Rate</th>
                                            <th>Tax Amount</th>
                                            <th>Item Dicount </th>
                                            <th>Discount Amount</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody  data-bind="foreach: aItemParticulars">
                                        <tr>
                                            <td data-bind="text: iSrNo"></td>
                                            <td data-bind="text: oItem.iItemNo"></td>
                                            <td data-bind="text: oItem.sItemCode"></td>
                                            <td data-bind="text: oItem.sItemName"></td>
                                            <td data-bind="text: iQty"></td>
                                            <td data-bind="text: oItem.iItemRate"></td>
                                            <td data-bind="text: iItemTax"></td>
                                            <td data-bind="text: iTaxAmount"></td>
                                            <td data-bind="text: iItemDiscount"></td>
                                            <td data-bind="text: iDiscountAmount"></td>
                                            <td data-bind="text: iTotal"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <label>Grand total is : <strong><span data-bind="text: iGrandTotal"></span></strong></label>
                <label>Tax Amount total is : <strong><span data-bind="text: iTaxAmount"></span></strong></label>
                <label>Discount Amount total is : <strong><span data-bind="text: iDiscountAmount"></span></strong></label>
                <label>Net Amount total is : <strong><span data-bind="text: iNetAmount"></span></strong></label>
                <label>Pending Amount total is : <strong><span data-bind="text: iNetAmount"></span></strong></label>
            </div>
        </div>
        <div class="card">
           <table class="table" data-bind="visible: aPaymentPerticular().length > 0">
            <thead>
                <tr>
                    <th>Paid Amount</th>
                    <th>Date of Payment</th>
                    <th>Payment Mode</th>
                    <th>Pending Amount</th>
                </tr>
            </thead>
            <tbody  data-bind="foreach: aPaymentPerticular">
                <tr>
                    <td data-bind="text: oPayment.iPaidAmount"></td>
                    <td data-bind="text: oPayment.dPaymentDate"></td>
                    <td data-bind="text: oPayment.sPaymentMode"></td>
                    <td data-bind="text: oPayment.iPending"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="js/bill.js"></script>
<script type="text/javascript" src="assets/typeahead.js/dist/typeahead.jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#idPatientName').typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {
                $.get("ajaxHandler.php", {keyword: query, func: 'getPatient'}, function (data) {
                    var patients = [];

                    $.map(data, function(patient){
                        var group;
                        group = {
                            id: patient.iPatientID,
                            name: patient.sPatientName,
                            bIsExternalPatient: patient.bIsExternalPatient,
                            toString: function () {
                                return JSON.stringify(this);
                            },
                            toLowerCase: function () {
                                return this.name.toLowerCase();
                            },
                            indexOf: function (string) {
                                return String.prototype.indexOf.apply(this.name, arguments);
                            },
                            replace: function (string) {
                                return String.prototype.replace.apply(this.name, arguments);
                            }
                        };

                        patients.push(group);
                    });

                    return async(patients);
                });
            },
            display: 'name',
            templates: {
                suggestion: function (data) {
                    if(data.bIsExternalPatient){
                        return '<div><strong>' + data.name + '&nbsp;&nbsp;&nbsp;<span class="label label-info">Extrenal Patient</span></div>';
                    } else {
                        return '<div><strong>' + data.name + '</div>';
                    }
                }
            }
        })
        .on('typeahead:selected', function (obj, datum) {
            oPOS.oPatient.iPatientID(datum.id);
        });

        $('#idItemSuggestionbox').typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {
                $.get("ajaxHandler.php", {keyword: query, func: 'getAvailableItems'}, function (data) {
                    var items = [];

                    $.map(data, function(item){
                        var group;
                        group = {
                            id: item.item_id,
                            name: item.item_name,
                            toString: function () {
                                return JSON.stringify(this);
                            },
                            toLowerCase: function () {
                                return this.name.toLowerCase();
                            },
                            indexOf: function (string) {
                                return String.prototype.indexOf.apply(this.name, arguments);
                            },
                            replace: function (string) {
                                return String.prototype.replace.apply(this.name, arguments);
                            }
                        };

                        items.push(group);
                    });

                    return async(items);
                });
            },
            display: 'name'
        })
        .on('typeahead:selected', function (obj, datum) {
            oPOS.oBill.iSuggestionItemID(datum.id);
        });

        $('#idItemSuggestionID').change(function(event) {
            if($(this).val() == ""){
                oPOS.oBill.iSuggestionItemID(datum.id);
            }
        });

        var oPOS = new POS(null,"20-07-2015", "BL-1");
        ko.applyBindings(oPOS);
    });

    $('.classPatientControlCard [name="patient_name"]')

</script>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>
