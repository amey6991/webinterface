<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."config/config.app.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.PermissionHandler.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funEmails.php";

//! Specified the Page ID for the current page
$iPageID  = 7;

$sScreenURL = "setup.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$sPageTitle = "Setup";

$aEmailFeature = getAllEmailFeature();
$aFQDNSetting = fGetEmailFQDNSetting();
$aDefaultList = fGetDefualtEmailList();
//print_r($aDefaultList);exit();
$iType = $sessionManager->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: logout.php');
    }
}
include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"DashBoard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"Setup","link"=>"setup.php","isActive"=>true)
            );
?>
<header>
    <script type="text/javascript">
    $(document).ready(function(){
        $("[name='my-checkbox']").bootstrapSwitch();
        ko.applyBindings(new ListTestForQcPipelineViewModal());
        $(".classEmailFqdn").click(function(){
            //var sEmail = $("#idEmailAddress").val();
            var sEmailSMTPSecure = $("#idEmailSMTPSecure").val();
            var sEmailHost = $("#idEmailHost").val();
            var sEmailPort = $("#idEmailPort").val();
            var sEmailUserName = $("#idEmailUserName").val();
            var sEmailPassword = $("#idEmailPassword").val();
            var iResult;
            $.ajax({
                url:"ajaxRequest.php",
                data:{
                    sFlag:'addEmailFqdnSetting',
                    sFQDNSMTPSecure:sEmailSMTPSecure,
                    sFQDNHost:sEmailHost,
                    sFQDNPort:sEmailPort,
                    sFQDNUserName:sEmailUserName,
                    sFQDNPassword:sEmailPassword
                  },
                async:false,
                method:"GET",
                success:function(data){
                    iResult = data;
                    if(iResult > 0){
                        displayAlert('FQDN Email Setting Saved.','success');
                        $("#idEmailSMTPSecure").val(sEmailSMTPSecure);$("#idEmailSMTPSecure").attr('readonly',true);

                        $("#idEmailHost").val(sEmailHost);$("#idEmailHost").attr('readonly',true);
                        $("#idEmailPort").val(sEmailPort);$("#idEmailPort").attr('readonly',true);
                        $("#idEmailUserName").val(sEmailUserName);$("#idEmailUserName").attr('readonly',true);
                        $("#idEmailPassword").val(sEmailPassword);$("#idEmailPassword").attr('readonly',true);
                        $(".classEmailFqdn").attr('disabled','disabled');

                    }else{
                      displayAlert('FQDN Email Setting Not Saved.','error');
                    }
                }
            });
        });

        $("#idUpdateEmailFQDN").click(function(){
            var sEmailSMTPSecure = $("#idChangeEmailSMTPSecure").val();
            var sEmailHost = $("#idChangeEmailHost").val();
            var sEmailPort = $("#idChangeEmailPort").val();
            var sEmailUserName = $("#idChangeEmailUserName").val();
            var sEmailPassword = $("#idChangeEmailPassword").val();
            $.ajax({
                url:"ajaxRequest.php",
                data:{
                    sFlag:'updateEmailFqdnSetting',
                    sFQDNSMTPSecure:sEmailSMTPSecure,
                    sFQDNHost:sEmailHost,
                    sFQDNPort:sEmailPort,
                    sFQDNUserName:sEmailUserName,
                    sFQDNPassword:sEmailPassword
                  },
                async:false,
                method:"GET",
                success:function(data){
                    iResult = data;
                    if(iResult > 0){
                      $("#idModalChangeEmailConfig").modal('hide');
                        displayAlert('FQDN Email Setting Updated.','success');
                        $("#idEmailSMTPSecure").val(sEmailSMTPSecure);$("#idEmailSMTPSecure").attr('readonly',true);

                        $("#idEmailHost").val(sEmailHost);$("#idEmailHost").attr('readonly',true);
                        $("#idEmailPort").val(sEmailPort);$("#idEmailPort").attr('readonly',true);
                        $("#idEmailUserName").val(sEmailUserName);$("#idEmailUserName").attr('readonly',true);
                        $("#idEmailPassword").val(sEmailPassword);$("#idEmailPassword").attr('readonly',true);
                        $(".classEmailFqdn").attr('disabled','disabled');

                    }else{
                      displayAlert('FQDN Email Setting Not Saved.','error');
                    }
                }
            });


        });

        $(".classChangeEmailFqdn").click(function(){
            $("#idModalChangeEmailConfig").modal('show');
        });

        $("#idAddEmailList").click(function(){
            $("#idModalDefaultEmailList").modal('show');
        });

        $("#idAddEmailListSave").click(function(){
          var sName = $("#idDefaltName").val();
          var sEmail = $("#idDefaltEmail").val();

          if(sName == ''){
            displayAlert('Name can not be empty.','error');
          }else if(fValidateEmail(sEmail)==false){
            displayAlert('Please Check Your Email.','error');
          }else{
              $.ajax({
                url:"ajaxRequest.php",
                data:{sFlag:'DefaultEmailList',sName:sName,sEmail:sEmail},
                async:false,
                method:"GET",
                success:function(result){
                    if(result > 0){
                      $("#idModalDefaultEmailList").modal('hide');
                       location.reload();
                       //displayAlert('This Email is ADDED.','success');
                     }else{
                       displayAlert('This Email is Not ADDED.','error');
                     }
                  }
              });
          }

        });

        $(".classDisableEmail").click(function(){
          sID = $(this)[0].id;
          iID = sID.split('_')[1];
            $.ajax({
              url:"ajaxRequest.php",
              data:{sFlag:'DisableDefaultEmail',sID:iID},
              async:false,
              method:"GET",
              success:function(result){
                  console.log(result);
                  if(result==1){
                    displayAlert('This Email is Disable.','success');
                    $('#'+sID).attr('disabled','disabled');
                  }else{
                    displayAlert('This Email is Not Disable.','error');
                  }
               }
            });          
        });

        $('[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {

            var bChecked = $(this)[0].checked;
            var sID = $(this)[0].id;
            iID = sID.split('_')[1];

            $.ajax({
                  url:"ajaxRequest.php",
                  data:{sFlag:'EmailStateChange',bChecked:bChecked,iFeatureID:iID},
                  async:false,
                  method:"GET",
                  success:function(result){
                      if(result == 1){
                          if(bChecked==true){
                            displayAlert('This Email Feature is Enable.','success');
                          }else{
                            displayAlert('This Email Feature is Disable.','success');
                          }
                      }else{
                        displayAlert('Changes Not Saved','error');
                      }
                  }
              });

        });


    });
  
    function fValidateEmail(sEmail){
        var email = /^$|^[a-zA-Z0-9]{1}([a-zA-Z0-9]?[\.\-\_]{0,1}[a-zA-Z0-9]+)*[@]{1}(([a-zA-Z0-9]+[\.\-\_]{1})+([a-zA-Z]{2,3})+)$/;
        if(!sEmail.match(email)){
            return false;
        }else{
            return true;
        }
    }
    </script>
</header>
<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <ol class="breadcrumb navbar-breadcrumb">
                <?php echo parseBreadcrumb($aBreadcrumb); ?>
            </ol>
            <!-- <span class="title">Test List And Setup</span>
            <div class="description"></div> -->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title" style="font-size:25px;font-family:oswald;">Test List And Email Setup Management</div>
                            <div class="card-subtitle" style="font-size:20px;">
                            Listing of Test QC and Sequencer <br/> Manage the Subscriber and the list.     
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12" style="font-size:20px;font-family:oswald;">
                               LimsDx FQDN:
                               <br/>
                               Galaxy FQDN:
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 " id="idDivDataTable">
                                <strong style="font-size:25px;font-family:oswald;">List of Tests and Associated Pipelines</strong>
                                <br/><br/>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>TestID</th>
                                            <th>QC Pipeline</th>
                                            <th>Sample Pipeline</th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: TestDataQCAnalysis">
                                           <tr>
                                               <td data-bind="text: testID"></td>
                                               <td data-bind="text: QcPipeline"></td>
                                               <td data-bind="text: samplePipeLine"></td>
                                           </tr>
                                    </tbody>
                                </table>
                            <br/><br/>
                            </div>
                            
                            <div class="col-md-12">
                            <strong style="font-size:25px;font-family:oswald;">Email Configuration</strong><br/><br/>
                                <div class="col-md-2">
                                    Email server FQDN    
                                </div>
                                <div class="col-md-10">
                                <?php
                                  if(!empty($aFQDNSetting)){
                                ?>
                                    <div class="col-md-4">
                                        <input type="text" id="idEmailSMTPSecure" class="form-control" readonly value="<?php echo $aFQDNSetting['fqdn_smtp_secure']; ?>">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="idEmailHost" class="form-control" readonly value="<?php echo $aFQDNSetting['fqdn_smtp_host']; ?>">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="idEmailPort" class="form-control" readonly value="<?php echo $aFQDNSetting['fqdn_smtp_port']; ?>">
                                    </div>
                                    <div class="col-md-4" style="margin-top:5px;">
                                      <input type="text" id="idEmailUserName" class="form-control" readonly value="<?php echo '************';//$aFQDNSetting['fqdn_smtp_username']; ?>">
                                    </div>
                                    <div class="col-md-4" style="margin-top:5px;">
                                      <input type="text" id="idEmailPassword" class="form-control" readonly value="<?php echo '************';//$aFQDNSetting['fqdn_smtp_password']; ?>">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="button" class="btn btn-primary classEmailFqdn" id="idAddEmailFQDN" readonly value="ADD" disabled="disabled">
                                      <input type="button" class="btn btn-primary classChangeEmailFqdn" id="idChangeEmailFQDN" value="Change">
                                    </div>
                                <?php
                                  }else{
                                ?>
                                    <div class="col-md-4">
                                        <input class="hide" type="text" id="idEmailAddress" class="form-control">
                                        <input type="text" id="idEmailSMTPSecure" class="form-control" placeholder="Please enter SMTP_Secure">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="idEmailHost" class="form-control" placeholder="Please enter Email Host">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="idEmailPort" class="form-control" placeholder="Please enter Email Host Port">
                                    </div>
                                    <div class="col-md-4" style="margin-top:5px;">
                                      <input type="text" id="idEmailUserName" class="form-control" placeholder="Please enter Host Username">
                                    </div>
                                    <div class="col-md-4" style="margin-top:5px;">
                                      <input type="text" id="idEmailPassword" class="form-control" placeholder="Please enter Host Password">
                                    </div>
                                    <div class="col-md-4">
                                      <input type="button" class="btn btn-primary classEmailFqdn" id="idAddEmailFQDN" value="ADD">
                                      <input type="button" class="btn btn-primary classChangeEmailFqdn" id="idChangeEmailFQDN" value="Change" disabled="disabled">
                                    </div>
                                <?php
                                  }
                                ?>
                                  </div>
                                    
                                </div>
                                <br/><br/><br/><br/>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="col-md-3">
                                  <strong style="font-size:25px;font-family:oswald;">Default email List</strong>
                                </div>
                                <div class="col-md-9">
                                  <input type="button" class="btn btn-success classDefaultEmailList" id="idAddEmailList" style="margin-top:0px" value="ADD">
                                </div>
                                <br/><br/>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                        if(!empty($aDefaultList)){
                                          foreach ($aDefaultList as $key => $avalue) {
                                            ?>
                                              <tr>
                                                 <td><?php echo $avalue['user_name'];?></td>
                                                 <td><?php echo $avalue['user_email'];?></td>
                                                 <td> 
                                                  <input type="button" class="btn btn-primary btn-sm classDisableEmail" value="Disable" id="idDisableEmail_<?php echo $avalue['default_id'];?>">
                                                 </td>
                                              </tr>
                                            <?php
                                          }
                                        }else{
                                          ?>
                                            <tr>
                                              <td> <strong>No Default Email List Added.</strong></td>
                                              <td></td>
                                              <td></td>
                                            </tr>
                                          <?php
                                      }
                                      ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-md-12">
                                <strong style="font-size:25px;font-family:oswald;">Email Triggered</strong>
                                <br/><br/><br/>
                                <div class="col-md-5">
                                        <table class="table table-striped table-bordered table-hover">
                                           <tbody>
                                              <?php
                                                if(!empty($aEmailFeature)){
                                                    foreach ($aEmailFeature as $key => $aFeatureEmail) {
                                              ?>
                                                <tr>
                                                  <td>
                                                        <?php 
                                                            echo $aFeatureEmail['email_feature_name'];
                                                        ?>
                                                  </td>
                                                   <td>
                                                      <?php 
                                                            if($aFeatureEmail['activate']==1){
                                                        ?>
                                                            <input type="checkbox" id="idEmailFeature_<?php echo $aFeatureEmail['id'];?>" class="classEmailFeature" name="my-checkbox" checked>   
                                                        <?php
                                                            }else{
                                                        ?>
                                                              <input type="checkbox" id="idEmailFeature_<?php echo $aFeatureEmail['id'];?>" class="classEmailFeature" name="my-checkbox">
                                                        <?php
                                                            }
                                                        ?>
                                                       
                                                   </td>
                                                </tr>
                                       
                                              <?php
                                                    }
                                                }
                                              ?>
                                           </tbody>

                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="idModalDefaultEmailList">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Default Email List</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal">
              <div class="form-group">
                  <label class="col-sm-2 control-label">Name</label>
                  <div class="col-md-4">
                      <input type="text" name="inputname" class="form-control" id="idDefaltName" placeholder="Enter Name Please">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Email</label>
                  <div class="col-md-4">
                      <input type="text" name="inputemail" class="form-control" id="idDefaltEmail" placeholder="Enter Email Please">
                  </div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="idAddEmailListSave" class="btn btn-primary">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="idModalChangeEmailConfig">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Email Config</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal">
              <div class="form-group">
                  <label class="col-sm-2 control-label">SMTP Secure</label>
                  <div class="col-md-4">
                      <input type="text" name="inputname" class="form-control" id="idChangeEmailSMTPSecure" placeholder="Enter SMTP Secure for Email Config">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Host</label>
                  <div class="col-md-4">
                      <input type="text" name="inputname" class="form-control" id="idChangeEmailHost" placeholder="Enter Host for Email">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Port</label>
                  <div class="col-md-4">
                      <input type="text" name="inputemail" class="form-control" id="idChangeEmailPort" placeholder="Enter Port for Email Config">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">UserName</label>
                  <div class="col-md-4">
                      <input type="text" name="inputemail" class="form-control" id="idChangeEmailUserName" placeholder="Enter Username for Email Config">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Password</label>
                  <div class="col-md-4">
                      <input type="text" name="inputemail" class="form-control" id="idChangeEmailPassword" placeholder="Enter Password for Email Config">
                  </div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="idUpdateEmailFQDN" class="btn btn-primary">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>
