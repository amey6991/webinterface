<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";

$sessionManager = new SessionManager();



//! Redirect user to dashboard if he is already logged in
if($sessionManager->isLoggedIn()){
    header("Location: dashboard.php");
    exit;
}

$sPageTitle = "Login";

include_once "mxcelHeaderBase.php";

?>
<body class="flat-blue login-page">
    <div class="container">
        <div class="login-box">
            <div>
                <div class="login-form row">
                    <div class="col-sm-12 text-center login-header">
                        <div class="pull-left">
                            <img src="images/ResearchDx.jpg" height="40px" width="80px" style="margin-top:8px;">
                        </div>
                        <h3 class="pull-right login-title"><?php echo ORG_NAME; ?></h3>
                    </div>
                    <div class="col-sm-12">
                        <div class="login-body">
                            <div class="text-center">
                                <img class="login-logo" src=""/>
                            </div>
                            <div class="progress hidden" id="login-progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    Log In...
                                </div>
                            </div>
                            <form class="form classForm classFormLogin" method="post" action="doLogin.php">
                                <div class="control">
                                    <input type="text" name="username" class="form-control classInput classInputText" value="" placeholder="Username&hellip;" />
                                </div>
                                <div class="control">
                                    <input type="password" name="password" class="form-control classInput classInputPassword" value="" placeholder="Password&hellip;" />
                                </div>
                                <div class="login-button text-center">
                                    <input type="submit" class="btn btn-primary classButton classButtonLogin" value="Login">
                                </div>
                            </form>
                        </div>
                        <div class="login-footer">
                            <span class="text-right"><a href="#.php" class="color-white" style="color:#342828">Forgot password?</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php
//! Include footer files
include_once "mxcelFooterBase.php";
?>
