<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.FileManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.EmailList.php";

//!! AddEmail for Listing
	function addEmailFQDN($sEmail){
	    $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = 'email_fqdn';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sTable}`(`email_id`,`email_addr`,`added_on`,`status`) VALUES (NULL,'{$sEmail}','{$dAddedOn}','1')";
        $sResult = $conn->query($sIQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }else{
        	return $conn->insert_id;
        }
	}
//!! Function to get All Email Feature
	function getAllEmailFeature(){
		$aEmailFeatures = array();
		$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = 'email_feature';
        $sQuery ="SELECT * FROM {$sTable} WHERE `status`=1";
        $sResult = $conn->query($sQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aEmailFeatures[]=$aRow;
                }
            }
    	}
    	return $aEmailFeatures;
	}
//!! Function to enable the EMAIL feature
    function fEnableEmailFeature($iFeatureID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = 'email_feature';
        
        $sUQuery = "UPDATE {$sTable} SET `activate`= 1 WHERE `id`='{$iFeatureID}'";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Function to disable the EMAIL feature
    function fDisableEmailFeature($iFeatureID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = 'email_feature';
        
        $sUQuery = "UPDATE {$sTable} SET `activate`= 0 WHERE `id`='{$iFeatureID}'";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

// Function to add The FQDN Configuration Setting
    function fAddFQDNConfiguration($sFQDNSMTPSecure,$sFQDNHost,$sFQDNPort,$sFQDNUserName,$sFQDNPassword){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_email_fqdb_config_master';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sTable}`(`id_fqdN_master`,`fqdn_smtp_secure`,`fqdn_smtp_host`,`fqdn_smtp_port`,`fqdn_smtp_username`,`fqdn_smtp_password`,`added_on`,`status`) VALUES (NULL,'{$sFQDNSMTPSecure}','{$sFQDNHost}','{$sFQDNPort}','{$sFQDNUserName}','{$sFQDNPassword}','{$dAddedOn}','1')";
        $sResult = $conn->query($sIQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }else{
            return $conn->insert_id;
        }        
    }
//!! Function to Update the EMAIL FQDN config
    function fUpdateFQDNConfiguration($sFQDNSMTPSecure,$sFQDNHost,$sFQDNPort,$sFQDNUserName,$sFQDNPassword){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_email_fqdb_config_master';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sUQuery = "UPDATE `{$sTable}` SET `fqdn_smtp_secure` = '{$sFQDNSMTPSecure}' ,`fqdn_smtp_host` ='{$sFQDNHost}',`fqdn_smtp_port` = '{$sFQDNPort}',`fqdn_smtp_username` = '{$sFQDNUserName}',`fqdn_smtp_password` = '{$sFQDNPassword}' WHERE `id_fqdN_master`=1";
        $sResult = $conn->query($sUQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }else{
            return True;
        }        
    }
//!! Function to get Email Configuration Setting
    function fGetEmailFQDNSetting(){
        $aFQDNSetting = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_email_fqdb_config_master';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `status`=1";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $aFQDNSetting=$aRow;
            }
        }
        return $aFQDNSetting;                
    }
//!! Function to add Default List
    function fAddDefualtEmailList($sName,$sEmail){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_default_email_master';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sTable}`(`default_id`,`user_name`,`user_email`,`added_on`,`status`) VALUES (NULL,'{$sName}','{$sEmail}','{$dAddedOn}','1')";
        $sResult = $conn->query($sIQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }else{
            return $conn->insert_id;
        }        
    }

//!! Function to get the Email List
    function fGetDefualtEmailList(){
        $aDefaultEmail = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_default_email_master';
        
        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `status`=1";        
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aDefaultEmail[]=$aRow;
                }
            }
        }
        return $aDefaultEmail;        
    }
//!! Function to disable the email list
    function fDisableDefaultEmail($iID){
        $aDefaultEmail = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_default_email_master';
        
        $sUQuery = "UPDATE `{$sTable}` SET `status`=0 WHERE `default_id`='{$iID}'";         
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
?>