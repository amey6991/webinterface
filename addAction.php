<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
$sPageTitle = "Add Action";
$aFeatureData = fGetAllFeatures();
$iCountfeature = count($aFeatureData);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Add Action</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormAddAction" action="em_add_action.php">
                                         <div class="form-group">
                                            <label for="id_feature_select" class="col-sm-2 control-label">Feature</label>
                                            <div class="col-md-4">
                                                <select name="feature_id" id="idFeatureID" class="form-control" onchange="fSetFeatureName()">
                                                    <option value="0">Select</option>
                                                <?php
                                                for($iii=0;$iii<$iCountfeature;$iii++){
                                                    echo "<option value='{$aFeatureData[$iii]['feature_id']}'>{$aFeatureData[$iii]['feature_code']}</option>";
                                                }
                                                ?>  
                                                </select>
                                                <input type="hidden" id="idFeatureName" name="feature_name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_action_code" class="col-sm-2 control-label">Action Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="action_code" class="form-control" id="id_action_code" placeholder="Enter Unique action Code">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_action_name" class="col-sm-2 control-label">Action Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="action_name" class="form-control" id="id_action_name" placeholder="Enter action Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_action_notes" class="col-sm-2 control-label">Action Note</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="action_notes" class="form-control" id="id_action_notes"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddAction" class="btn btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddAction").click(function(){
                var sActionCode = $("#id_action_code").val();
                var sActionName = $("#id_action_name").val();
                var iFeatureID = $("#idFeatureID").val();
                
                if(sActionCode ==''){
                    alert('Action Code Must to enter.');
                }else if(sActionName==''){
                    alert('Action Name Must to enter.');
                }else if(iFeatureID==0){
                    alert('Please Select feature for Action.');
                }else{
                    $("#idFormAddAction").submit();
                }
         });
    });
    function fSetFeatureName(){
        var iFeatureID = $("#idFeatureID").val();
        console.log(iFeatureID);
         $.ajax({
            url:"ajaxRequest.php",
            data:{sFlag:'GetFeatureName',iFeatureID:iFeatureID},
            async:false,
            method:"GET",
            success:function(bResult){
                $("#idFeatureName").val(bResult);
            }
        });
     }
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>