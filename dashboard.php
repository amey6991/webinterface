<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."config/config.app.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.PermissionHandler.php";

//! Specified the Page ID for the current page
$iPageID  = 1;

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$sPageTitle = "Dashboard";

$iType = $sessionManager->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: logout.php');
    }
}
include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";

$aDashboardStats = getDashboardStats();
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>true),
            );
?>
<head>
  <script type="text/javascript">
    
  $(document).ready(function(){
        window.oViewModelDashboard = new DashboardDataViewModal();
        
        oDate = getFirstDateOfWeek(new Date());
        
        var iMonth = ("0" + (oDate.getMonth() + 1)).slice(-2);
        var iDate = ("0" + (oDate.getDate())).slice(-2);
        
        var sFirstDate = iMonth+'-'+iDate+'-'+oDate.getFullYear();
        var dEndDate = getLastDateOfWeek();
        
        $("#idDashboardStartDate").val(sFirstDate);
        $("#idDashboardFinishDate").val(dEndDate);

        $("#idSampleStartDate").val(sFirstDate);
        $("#idSampleFinishDate").val(dEndDate);

        oViewModelDashboard.applyDateFilter(sFirstDate,dEndDate);
        oViewModelDashboard.applySampleDateFilter(sFirstDate,dEndDate);
        ko.applyBindings(oViewModelDashboard);

        // console.log(dEndDate);
        // dStartDate = '2016-07-13';
        // dEndDate = '2016-07-22';
        

        $('[data-toggle="tooltip"]').tooltip();
        
        $.fn.datepicker.defaults.format = "mm-dd-yyyy";
        $.fn.datepicker.defaults.clearBtn = "true";
        $.fn.datepicker.defaults.autoclose = "true";
        $.fn.datepicker.defaults.orientation = "bottom";

        function getFirstDateOfWeek(oDate) {
            var day = oDate.getDay(),
            diff = oDate.getDate() - day + (day == 0 ? -6 : 0); 
            return new Date(oDate.setDate(diff));
        }
        function getLastDateOfWeek(){
            var today = new Date();
            var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + (6-today.getDay()));
            var lastWeekMonth = ("0" + (lastWeek.getMonth() + 1)).slice(-2);
            var lastWeekDay = ("0" + (lastWeek.getDate() + 1)).slice(-2); // +1 for Next day so it will filter till last day like last day is 20 then it will query for 21st.
            var lastWeekYear = lastWeek.getFullYear();

            var lastWeekDisplay = lastWeekMonth+"-"+lastWeekDay+"-"+lastWeekYear;
            return lastWeekDisplay ;
        }
    });
  </script>
</head>
<div class="container-fluid">
<div class="side-body padding-top">
        
        <div class="row" id="idSequencerInfo">
        <!-- <div id="idSequencerInfo"> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                
                    <div class='classDisplayLegendBlock'>
                            <h2 id="idCaptionHeader">Sequencers</h2>
                                <div class='classDisplayLegend'>
                                    <div class="classNumericInfo">
                                        <span data-bind="text: iActiveRuns" class="classTableTRHalfWidth"></span>
                                    </div>
                                    <div class="classLabelBlue">
                                        <span  class="classTableTRFullWidth">Run</span>
                                    </div>
                                </div>
                                <div class='classDisplayLegend'>
                                    <div class="classNumericInfo">
                                        <span data-bind="text: iRunningSequencer" class="classTableTRHalfWidth">66</span>
                                    </div>
                                    <div class="classWidgetLabelSuccess">
                                        <span  class="classTableTRFullWidth">Running</span>
                                    </div>
                                </div>
                                <div class='classDisplayLegend'>
                                    <div class="classNumericInfo">
                                        <span data-bind="text: iSequencerWarning" class="classTableTRHalfWidth">7</span>
                                    </div>
                                    <div class="classWidgetLabelWarning">
                                        <span class="classTableTRFullWidth">Warning</span>
                                    </div>
                                </div>
                                <div class='classDisplayLegend'>
                                    <div class="classNumericInfo">
                                    <div class="classNumericInfoHS">
                                        <span data-bind="text: iSequencerCompletedWeek" class="classTableTRHalfWidth">4</span>
                                        <span class=" classWidgetLabelInfo">This Week</span>
                                    </div>
                                    <div id="classNumericInfoHS">
                                    <span data-bind="text: iSequencerCompletedMonth" class="classTableTRHalfWidth">6</span>
                                        
                                        <span class=" classWidgetLabelInfo">This Month</span>
                                    </div>
                                    </div>
                                    <div class="classWidgetLabelSuccess">
                                        <span colspan="2" class="classTableTRFullWidth">Completed</span>
                                    </div>
                                </div>
                                </div>
                            
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                        $iFeatureID=2;
                        $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                        if($iFeaturePermLevel>0){
                    ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <a href="viewClients.php">
                                <div class="card white summary-inline classCardBorder">
                                    <div class="card-body" style="color:#C34127;height:113px;">
                                        <i class="icon fa fa-users fa-2x"></i>
                                        <div class="content">
                                            <div class="title"><?php echo $aDashboardStats['aTotalClient']; ?></div>
                                            <div class="sub-title">Total Client</div>
                                        </div>
                                        <div class="clear-both"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php
                        }
                        $iFeatureID=3;
                        $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                        if($iFeaturePermLevel>0){
                    ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 classDisplayLegendBlockRight">
                            
                            <h2 id="idCaptionHeader">Users</h2>
                            <div class='classDisplayLegend'>
                                <div class="classNumericInfo">
                                    <span class="classTableTRHalfWidth"><?php echo $aDashboardStats['aTotalUsers']; ?></span>
                                </div>
                                <div class="classLabelBlue">
                                    <span  class="classTableTRFullWidth">Total Users</span>
                                </div>
                            </div>
                        
                        </div>

                        <div class="hide col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <a href="viewUsers.php">
                            <div class="card white summary-inline classCardBorder">
                                <div class="card-body" style="color:#C34127;height:72px;">
                                    <i class="icon fa fa-users fa-2x"></i>
                                    <div class="content">
                                        <div class="title"><?php echo $aDashboardStats['aTotalUsers']; ?></div>
                                        
                                    </div>
                                    <div class="clear-both"></div>
                                </div>
                                <div style="margin-left:25px;font-size:27px;">Total Users</div>
                            </div>
                        </a>
                    </div>
                <?php
                    }
                    $iFeatureID=4;
                    $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                    if($iFeaturePermLevel>0){
                ?>
                    <div class="hide col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#">
                            <div class="card white summary-inline classCardBorder">
                                <div class="card-body" style="color:#1ABC9C">
                                    <i class="icon fa fa-file fa-2x"></i>
                                    <div class="content">
                                        <div class="title"><?php echo $aDashboardStats['aTotalFilePending']; ?></div>
                                        <div class="sub-title">Total Files Pending</div>
                                    </div>
                                    <div class="clear-both"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php
                    }
                    $iFeatureID=5;
                    $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                    if($iFeaturePermLevel>0){
                ?>
                    <div class="hide col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#">
                            <div class="card white summary-inline classCardBorder">
                                <div class="card-body" style="color:#22A7F0">
                                    <i class="icon fa fa-files-o fa-4x"></i>
                                    <div class="content">
                                        <div class="title"><?php echo $aDashboardStats['aTotalFileProcessed']; ?></div>
                                        <div class="sub-title">Total Files Processed</div>
                                    </div>
                                    <div class="clear-both"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php
                    }
                ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row classContainerDashboard" id="idContainerDiv">
          <?php
            $iFeatureID=8;
            $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
            if($iFeaturePermLevel>0){
          ?>
            <div class="col-md-12" id="idSequencerList">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <caption class="classCaptionColor">
                        <div class="col-md-6" style="font-family:oswald;">
                            <h2 id="idCaptionHeaderTable">Sequencer List</h2>
                        </div>
                        <div class="col-md-2">
                            Start Date<div class="input-group date" data-provide="datepicker">
                              <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idDashboardStartDate" name="start_date">
                                <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                        Finish Date
                            <div class="input-group date" data-provide="datepicker">
                              <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idDashboardFinishDate" name="finish_date">
                              <span class="input-group-addon" id="basic-addon2">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <input type="button" class="btn btn-primary" value="Apply" id="idDateFilter" style="margin-top:15px;">
                        </div>
                            <span id="idExpandSequencerList" title="Make Full Screen"><i class="fa fa-expand" aria-hidden="true"></i></span>
                            <span id="idCompressSequencerList" title="Exit Full Screen">
                                <i class="fa fa-compress" aria-hidden="true"></i>
                            </span>
                        

                    </caption>
                    <thead>
                        <tr>
                            <th>Sequencer ID</th>
                            <th>Sequencer Type</th>
                            <th id="idSeqStatus">
                                <span data-bind="visible: oViewModelDashboard.iTotalProcessDataRows() == 0 && oViewModelDashboard.aFilerSeqStatus().length == 0">Status</span>
                                <div class="dropdown" id="idSeqFilterDropdown" data-bind="visible: oViewModelDashboard.iTotalProcessDataRows() != 0 || oViewModelDashboard.aFilerSeqStatus().length > 0">
                                    <label id="idTitleStatus" style="margin-bottom:0px;">Status</label>
                                    <button class="btn btn-default dropdown-toggle pull-right" type="button" id="idSeqStatusdropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin:0px;padding:0px;border:0px solid!important">
                                        <img src="images/filter.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerSeqStatus().length==0">
                                        <img src="images/filter_process.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerSeqStatus().length > 0">
                                    </button>
                                    <ul class="pull-right dropdown-menu classDropdownElement" aria-labelledby="idSeqStatusdropdownMenu">

                                        <li style="margin-left:15px">
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classSeqStatusCheckbox" id="idCheckbox_1" value="1"> Sequencer Found
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classSeqStatusCheckbox" id="idCheckbox_101" value="101"> Sequencer Completed (MiSeq)
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classSeqStatusCheckbox" id="idCheckbox_11" value="11"> Sequencer Completed (NextSeq)
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classSeqStatusCheckbox" id="idCheckbox_50" value="50"> Sequencer Running
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classSeqStatusCheckbox" id="idCheckbox_60" value="60"> Sequencer Warning
                                                </label>    
                                            </div>
                                            <div>
                                                <input type="button" class="btn btn-primary" id="idSeqStatusFilter" value="Ok">
                                                <input type="button" class="btn btn-primary" id="idSeqStatusFilterClear" value="Clear">
                                                <input type="button" class="btn btn-primary" id="idSeqStatusFilterCancel" value="Cancel">
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </th>
                            <th>Start Date/Time</th>
                            <th>Cycle Number</th>
                            <th>Estimated Data</th>
                            <th>Q30</th>
                            <th>% Passing Filter</th>
                            <th>Finish Date/Time</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: aViewableProcessRows">
                        <tr data-bind="style: { color: (sErrorStatus==111) ? 'red' : 'green' }">
                           <td>
                               <a data-bind="attr:{href:'viewSequencerDetail.php?iSeqTableID='+iSeqTableID,title:'RunID: '+sRunID},style: { color: (sErrorStatus == 111) ? 'red' : 'green' }" data-toggle="tooltip">
                                   <i class="fa fa-external-link"></i><span data-bind="text: sequenceID" style="margin-left:10px"></span>
                               </a>
                                <i class="hide fa fa-info-circle classSequncerInfo" style="font-size:16px;margin-left:5px" data-bind="event: {mouseover: oViewModelDashboard.onSequncerMouseOver, mouseleave: oViewModelDashboard.onSequncerMouseLeave},attr:{id:'idSequencerInfo_'+iSeqTableID}"></i> 
                           </td>
                           <td>
                               <span data-bind="if: iSeqType==1">Mi-Seq</span>
                               <span data-bind="if: iSeqType==2">Next-Seq</span>
                           </td> 
                           <td>
                                <span data-bind="if: cstatus==1">Sequencer Found.</span>
                                
                                <span data-bind="if: cstatus==2">SampleSheet.CSV Found</span>
                                <span data-bind="if: cstatus==3">RunParameter.xml File Found</span>
                                <span data-bind="if: cstatus==4">GenerateFastqStatistics.xml File Found</span>
                                <span data-bind="if: cstatus==5">SampleSheet.csv File Read</span>
                                <span data-bind="if: cstatus==6">RunParameter.xml File Read</span>
                                <span data-bind="if: cstatus==7">GenerateFastqStatistics.xml File Read</span>

                                <span data-bind="if: cstatus==8">RunInfo.xml File Found</span>
                                <span data-bind="if: cstatus==9">RunParameter.xml File Read</span>
                                <span data-bind="if: cstatus==12">Demultiplex.xml File Found</span>
                                <span data-bind="if: cstatus==13">Demultiplex.xml File Read</span>

                                <!--<span data-bind="if: cstatus==10">RunCompletionStats.xml File Found / Sequencer Run Completed</span> -->
                                <span data-bind="if: sErrorStatus==111">Warning (Files are Missed) Check manually</span>
                                <span data-bind="if: cstatus==11">Sequencer Run Completed</span>
                                <span data-bind="if: cstatus==100">CompletedJobInfo.xml File Found / Sequencer Run Completed</span>
                                <span data-bind="if: cstatus==101">Sequencer Run Completed</span>
                           </td> 
                           <td data-bind="text: dStartDate"></td>
                           <td data-bind="text: cyclenumber"></td> 
                           <td data-bind="text: datasize"></td> 
                           <td data-bind="text: q30"></td> 
                           <td data-bind="text: passingfilter"> </td>
                           <td data-bind="text: dFinishDate"> </td>
                        </tr>
                    </tbody>
                </table>
                <div class="pull-right">
                    <ul class="pagination" data-bind="html: sProcessPagination">
                    </ul>
                </div>
                <span class="classMarginRight" data-bind="visible: oViewModelDashboard.iTotalProcessDataRows() ==0">
                    <h5><strong>No Sequencer Information Found.</strong></h5>
                </span>
                <span class="hide" data-bind="text: iTotalProcessDataRows"></span>
            </div>
            <div class="col-md-12">
                <hr style="border-top:1px solid #ACA6A6;">
            </div>
            <?php
              }
              $iFeatureID=9;
              $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
              if($iFeaturePermLevel>0){
            ?>
            <div class="col-md-7 hide">
                <div class="col-md-3">
                    <span>Filters</span>
                </div>
                <div class="col-md-4">
                    <select id="" class="form-control" name="">
                        <option>Select</option>
                        <option>Sequencer ID</option>
                        <option>Run ID</option>
                        <option>Analysis Pipeline</option>
                        <option>Status</option>
                        <option>Sample QC</option>
                        <option>Pipeline output</option>
                        <option>Process order</option>
                    </select>
                </div>
            </div>
            <?php
              }
              $iFeatureID=10;
              $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
              if($iFeaturePermLevel>0){
            ?>
            <div class="col-md-12" id="idSampleSequencerList">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <caption class="classCaptionColor">
                        <div class="col-md-6" style="font-family:oswald;">
                            <h2 id="idCaptionHeaderTable">Sample Sequencer Details</h2>
                        </div>
                        <div class="col-md-2">
                            Start Date<div class="input-group date" data-provide="datepicker">
                              <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idSampleStartDate" name="start_date">
                                <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                        Finish Date
                            <div class="input-group date" data-provide="datepicker">
                              <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idSampleFinishDate" name="finish_date">
                              <span class="input-group-addon" id="basic-addon2">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <input type="button" class="btn btn-primary" value="Apply" id="idSampleDateFilter" style="margin-top:15px;">
                        </div>
                        <span id="idExpandSampleSequencerList" title="Make Full Screen"><i class="fa fa-expand" aria-hidden="true"></i></span>
                        <span id="idCompressSampleSequencerList" title="Exit Full Screen"><i class="fa fa-compress" aria-hidden="true"></i></span>
                    
                    </caption>
                    <thead>
                        <tr>
                            <th>Sample List</th>
                            <th>Sequencer ID</th>
                            <th>Run ID</th>
                            <th id="idPipeline">
                                <span data-bind="visible: oViewModelDashboard.iTotalSampleDataRows() == 0 && oViewModelDashboard.aFilerPipeline().length == 0">Analysis Pipeline</span>
                                <div class="dropdown" id="idPepilineDropdown" data-bind="visible: oViewModelDashboard.iTotalSampleDataRows() != 0 || oViewModelDashboard.aFilerPipeline().length > 0">
                                    <label id="idTitlePipeline" style="margin-bottom:0px;">Analysis Pipeline</label>
                                    <button class="btn btn-default dropdown-toggle pull-right" type="button" id="idPipelinedropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin:0px;padding:0px;border:0px solid!important">
                                        <img src="images/filter.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerPipeline().length==0">
                                        <img src="images/filter_process.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerPipeline().length > 0">
                                        <!--<i class="fa fa-filter fa-1x" aria-hidden="true"></i> -->
                                    </button>
                                    <ul class="pull-right dropdown-menu classDropdownElement" id="idDropFilter" aria-labelledby="idPipelinedropdownMenu">
                                        <input type="text" id="idInputPipeline" class="form-control" onkeyup="getFilter(this.value)">
                                        <div data-bind="foreach: aFilterPipelineAvail" class="classUnselectedFilter">
                                            <div data-bind="if: $index() < (oViewModelDashboard.aFilerPipeline().length)">
                                                <li>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" class="classPipelineCheckbox" data-bind="value: iPipelineID ,attr: { id:'idPipelineCheckbox_'+iPipelineID}">
                                                          <span data-bind="text: text"></span>
                                                        </label>    
                                                    </div>
                                                </li>
                                            </div>
                                            <li role="separator" class="divider" data-bind="visible: (oViewModelDashboard.aFilerPipeline().length-1)== $index()"></li>
                                        </div>
                                        <div data-bind="foreach: aPipelineAvailable" class="classPipelineFilter">
                                    <!-- data-bind="if: $index() > (oViewModelDashboard.aFilerPipeline().length-1) -->
                                            <div>
                                                <li>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" class="classPipelineCheckbox" data-bind="value: iPipelineID ,attr: { id:'idPipelineCheckbox_'+iPipelineID}">
                                                          <span data-bind="text: text"></span>
                                                        </label>    
                                                    </div>
                                                </li>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                         <div style="margin-left:15px">
                                                <li role="separator" class="divider"></li>
                                                <input type="button" class="btn btn-primary" id="idPipelineFilter" value="Ok">
                                                <input type="button" class="btn btn-primary" id="idPipelineFilterClear" value="Clear">
                                                <input type="button" class="btn btn-primary" id="idPipelineFilterCancel" value="Cancel">
                                            </div>
                                    </ul>
                                </div>
                            </th>
                            <th id="idTHStatus">
                                <span data-bind="visible: oViewModelDashboard.iTotalSampleDataRows() == 0 && oViewModelDashboard.aFilerStatus().length == 0">Status</span>
                                <div class="dropdown" id="idFilterDropdown" data-bind="visible: oViewModelDashboard.iTotalSampleDataRows() != 0  || oViewModelDashboard.aFilerStatus().length > 0">
                                    <label id="idTitleStatus" style="margin-bottom:0px;">Status</label>
                                    <button class="btn btn-default dropdown-toggle pull-right" type="button" id="idStatusdropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin:0px;padding:0px;border:0px solid!important">
                                        <img src="images/filter.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerStatus().length==0">
                                        <img src="images/filter_process.png" width="14px" height="14px" data-bind="visible: oViewModelDashboard.aFilerStatus().length > 0">
                                    </button>
                                    <ul class="pull-right dropdown-menu classDropdownElement" aria-labelledby="idStatusdropdownMenu">
                                        <li style="margin-left:15px">
                                         <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classStatusCheckbox" id="idCheckbox_1" value="1"> On Sequencer
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classStatusCheckbox" id="idCheckbox_2" value="2"> QC Analysis
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classStatusCheckbox" id="idCheckbox_3" value="3"> Sample Analsyis
                                                </label>    
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="classStatusCheckbox" id="idCheckbox_4" value="4"> Analysis Complete
                                                </label>    
                                            </div>
                                            <div class="hide checkbox">
                                                <label>
                                                  <input type="checkbox" class="classStatusCheckbox" id="idCheckbox_5" value="5"> 
                                                </label>    
                                            </div>
                                        

                                            <div>
                                                <input type="button" class="btn btn-primary" id="idStatusFilter" value="Ok">
                                                <input type="button" class="btn btn-primary" id="idStatusFilterClear" value="Clear">
                                                <input type="button" class="btn btn-primary" id="idStatusFilterCancel" value="Cancel">
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </th>
                            <th>Sample QC</th>
                            <th>Pipeline output</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: availableSampleDataQCAnalysis">
                        <tr>
                          <td data-bind="text: sSampleName"></td> 
                          <td> 
                            <a class="btn" data-bind="attr:{href:'viewSequencerDetail.php?iSeqTableID='+iSeqTableID}">
                                <i class="fa fa-external-link"></i>
                                    <span data-bind="text: iSequenceID" style="margin-left:10px"></span>
                                
                            </a>
                            <i class="hide fa fa-info-circle classSequncerInfo" style="font-size:16px;" data-bind="event: {mouseover: oViewModelDashboard.onSequncerMouseOver, mouseleave: oViewModelDashboard.onSequncerMouseLeave},attr:{id:'idSequencerInfo_'+iSeqTableID}"></i> 
                          </td> 
                          <td><a href="" class="btn " data-bind="text: iRunID"><i class="fa fa-external-link"></i></a> </td> 
                          <td data-bind="text: sPipeLineName"></td> 
                          <td>
                            <label data-bind="if: iStatus==1">
                                <span>On Sequencer</span>
                            </label>
                            <label data-bind="if: iStatus==2">
                                <span>QC Analysis</span>
                            </label>
                            <label data-bind="if: iStatus==3">
                                <span>Sample Analsyis</span>
                            </label>
                            <label data-bind="if: iStatus==4">
                                <span>Analysis Complete</span>
                            </label>
                          </td> 
                          <td><a href="" class="btn "><i class="fa fa-external-link classMarginRight"></i>View</a></td>
                          <td><a href="" class="btn "><i class="fa fa-external-link classMarginRight"></i>View</a></td> 
                      </tr>
                    </tbody>
                    
                </table>
                <div class="pull-right">
                    <ul class="pagination" data-bind="html: sSampleProcessPagination">
                    </ul>
                </div>
                <span class="classMarginRight" data-bind="visible: oViewModelDashboard.iTotalSampleDataRows() ==0">
                    <h5><strong>No Data Found.</strong></h5>
                </span>
            </div>
            <?php
              }
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        
        $("#idDateFilter").click(function(){
            var dStartDate = $("#idDashboardStartDate").val();
            var dEndDate = $("#idDashboardFinishDate").val();
            oViewModelDashboard.applyDateFilter(dStartDate,dEndDate);
        });
        
        $("#idSampleDateFilter").click(function(){
            var dStartDate = $("#idSampleStartDate").val();
            var dEndDate = $("#idSampleFinishDate").val();
            oViewModelDashboard.applySampleDateFilter(dStartDate,dEndDate);
        });

        $("#idExpandSequencerList").click(function(){
            $("#idSequencerInfo").hide();
            $("#idSampleSequencerList").hide();
            $("#idContainerDiv").removeClass('classContainerDashboard');
            $("#idCompressSequencerList").show();
            $("#idExpandSequencerList").hide();
            $("#idCaptionHeader").hide();
        });

        $("#idCompressSequencerList").click(function(){
            $("#idSequencerInfo").show();
            $("#idSampleSequencerList").show();
            $("#idContainerDiv").addClass('classContainerDashboard');
            $("#idCompressSequencerList").hide();
            $("#idExpandSequencerList").show();
            $("#idCaptionHeader").show();
        });

        $("#idExpandSampleSequencerList").click(function(){
            $("#idSequencerInfo").hide();
            $("#idSequencerList").hide();
            $("#idContainerDiv").removeClass('classContainerDashboard');
            $("#idCompressSampleSequencerList").show();
            $("#idExpandSampleSequencerList").hide();
            $("#idCaptionHeader").hide();
        });
        $("#idCompressSampleSequencerList").click(function(){
            $("#idSequencerInfo").show();
            $("#idSequencerList").show();
            $("#idContainerDiv").addClass('classContainerDashboard');
            $("#idCompressSampleSequencerList").hide();
            $("#idExpandSampleSequencerList").show();
            $("#idCaptionHeader").show();
        });

        $('body .dropdown-menu').on('click', function (e) {
            event.stopPropagation();
        });

        $('body').on('click', function (e) {
            var bFilterStatus = $("#idFilterDropdown").hasClass('open');
            var bFilterPipeline = $("#idPepilineDropdown").hasClass('open');
            var bFilterSeqStatus = $("#idSeqFilterDropdown").hasClass('open');
            // pipeline filter
             if(bFilterPipeline == true){
                if(oViewModelDashboard.aFilerPipeline().length == 0){
                    $(".classPipelineCheckbox").each(function(){
                        if($(this).is(':checked')){
                            $(this).prop('checked',false);
                        }
                    });           
                }else{
                    $(".classPipelineCheckbox").each(function(){
                        var sID = $(this)[0].id;
                        var iID = sID.split('_')[1];
                        var sTempID = iID.toString();
                        var sElement = "#idPipelineCheckbox_"+iID;

                        if(oViewModelDashboard.aFilerPipeline().indexOf(sTempID) == -1){
                            $(sElement).prop('checked',false);
                        }else{
                            $(sElement).prop('checked',true);
                        }
                    });
                }           
            }           
            // status filter
            if(bFilterStatus == true){
                if(oViewModelDashboard.aFilerStatus().length == 0){
                    $(".classStatusCheckbox").each(function(){
                        if($(this).is(':checked')){
                            $(this).prop('checked',false);
                        }
                    });           
                }else{
                    $(".classStatusCheckbox").each(function(){
                        var sID = $(this)[0].id;
                        var iID = sID.split('_')[1];
                        var sTempID = iID.toString();
                        var sElement = "#idCheckbox_"+iID;

                        if(oViewModelDashboard.aFilerStatus().indexOf(sTempID) == -1){
                            $(sElement).prop('checked',false);
                        }else{
                            $(sElement).prop('checked',true);
                        }
                    });
                }              
            }

            // Sequencer Status Filter
            if(bFilterSeqStatus == true){
                if(oViewModelDashboard.aFilerSeqStatus().length == 0){
                    $(".classSeqStatusCheckbox").each(function(){
                        if($(this).is(':checked')){
                            $(this).prop('checked',false);
                        }
                    });           
                }else{
                    $(".classSeqStatusCheckbox").each(function(){
                        var sID = $(this)[0].id;
                        var iID = sID.split('_')[1];
                        var sTempID = iID.toString();
                        var sElement = "#idCheckbox_"+iID;

                        if(oViewModelDashboard.aFilerSeqStatus().indexOf(sTempID) == -1){
                            $(sElement).prop('checked',false);
                        }else{
                            $(sElement).prop('checked',true);
                        }
                    });
                }              
            }
        });
        // apply status filter to list

        $("#idStatusFilter").click(function(){
            var aStatus = [];
            $(".classStatusCheckbox").each(function(){
                if($(this).is(':checked')){
                    aStatus.push(this.value);
                }
            });
            oViewModelDashboard.aFilerStatus(aStatus);
            oViewModelDashboard.setSampleCurrentPage(1);
            $("#idStatusdropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });

        $("#idStatusFilterCancel").click(function(){
            $("#idStatusdropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });

        $("#idStatusFilterClear").click(function(){
            $(".classStatusCheckbox").each(function(){
                if($(this).is(':checked')){
                    $(this).prop('checked',false);
                }
            });
        });
        // apply pipeline filter
        $("#idPipelineFilter").click(function(){
            var aPipeline = [];
            $(".classPipelineCheckbox").each(function(){
                if($(this).is(':checked')){
                    aPipeline.push(this.value);
                }
            });
            oViewModelDashboard.aFilerPipeline(aPipeline);
            oViewModelDashboard.setSampleCurrentPage(1);
            $("#idPipelinedropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });

        $("#idPipelineFilterCancel").click(function(){
            $("#idPipelinedropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });
        
        $("#idPipelineFilterClear").click(function(){
            $(".classPipelineCheckbox").each(function(){
                if($(this).is(':checked')){
                    $(this).prop('checked',false);
                }
            });
        });
        
        // apply Sequencer status filter
        $("#idSeqStatusFilter").click(function(){
            var aStatus = [];
            $(".classSeqStatusCheckbox").each(function(){
                if($(this).is(':checked')){
                    aStatus.push(this.value);
                }
            });
            oViewModelDashboard.aFilerSeqStatus(aStatus);
            oViewModelDashboard.setCurrentPage(1);
            $("#idSeqStatusdropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });        
        $("#idSeqStatusFilterCancel").click(function(){
            $("#idSeqStatusdropdownMenu").attr('aria-expanded',false);
            $("body").click();
        });

        $("#idSeqStatusFilterClear").click(function(){
            $(".classSeqStatusCheckbox").each(function(){
                if($(this).is(':checked')){
                    $(this).prop('checked',false);
                }
            });
        });

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].name === nameKey) {
                    return myArray[i];
                }
            }
        }
    });
    function getFilter(sText){
        oViewModelDashboard.setPipelineText(sText);
        $(".classPipelineCheckbox").each(function(){
            var sID = $(this)[0].id;
            var iID = sID.split('_')[1];
            var sTempID = iID.toString();
            var sElement = "#idPipelineCheckbox_"+iID;

            if(oViewModelDashboard.aFilerPipeline().indexOf(sTempID) == -1){
                $(sElement).prop('checked',false);
            }else{
                $(sElement).prop('checked',true);
            }
        });
        
    }
</script>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>