<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "addSample.php";
$iPageID=13;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "Add Sample";

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"Add Sample","link"=>"addSample.php","isActive"=>true)
            );
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <ol class="breadcrumb navbar-breadcrumb">
                                        <?php echo parseBreadcrumb($aBreadcrumb); ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormTest" action="#">
                                        <div class="form-group">
                                            <label for="id_feature_code" class="col-sm-2 control-label">Sample Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="test_name" class="form-control" id="id_sample_name" placeholder="Enter Test Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddSample" class="btn btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddSample").click(function(){
                var sSampleName = $("#id_sample_name").val();
                if(sSampleName ==''){
                    alert('Workflow Name Must to enter.');
                }else{
                    $.ajax({
                        url:"ajaxRequest.php",
                        data:{sFlag:'addSampleMaster',sSampleName:sSampleName},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                            if(iResult > 0){
                                displayAlert('Sample Added Successfully','success');
                                $("#id_sample_name").val('');
                            }else{
                                displayAlert('Some Error Occured','warning');
                                $("#id_sample_name").val('');
                            }
                        }
                    });
                }
         });
    });
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>