<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funFileManager.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.FileManager.php";

$iPageID=6;

$sPageTitle = "View Uploaded Files";
$aUserTypes = fGetUserTypes();
// $sAlert = $_GET['alert'];
// $iAlertType = $_GET['alertType'];

$oSession = new SessionManager();
$iType = $oSession->iType;
$iUserID = $oSession->iUserID;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
if($iType==1 || $iType==2){
    $aFileData = GetAllFileDataForUserTypeID($iType);
}else{
    $aFileData = GetAllFileDataForUserID($iUserID);
}
//print_r($aFileData);
$iCountFiles = count($aFileData);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"View Uploaded File","link"=>"viewUploadFile.php","isActive"=>true)
            );
?>

<!-- Main Content -->
<div class="container-fluid">
    <div class="side-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <ol class="breadcrumb navbar-breadcrumb">
                                <?php echo parseBreadcrumb($aBreadcrumb); ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                       <div class="card-body">
                        <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>File Name</th>
                                    <th>File Size</th>
                                    <th>File Type</th>
                                    <th>Uploaded On</th>
                                    <th>Download</th>
                                    <th>Rename</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php
                                    for($iii=0;$iii<$iCountFiles;$iii++){
                                        $iFileID = $aFileData[$iii]['file_id'];
                                        $sDownloadButton = "<button type='button' class='btn btn-info btn-sm classDownloadButton' id='{$iFileID}'>Download</button>";
                                        $sDeleteButton = "<button type='button' class='btn btn-danger btn-sm classDeleteButton' id='{$iFileID}'>Delete</button>";
                                        $sRenameButton = "<button type='button' class='btn btn-warning btn-sm classRenameButton' id='{$iFileID}'>Rename</button>";
                                ?>
                                <tr>
                                    <td><?php echo $iii+1;?></td>
                                    <td><?php echo $aFileData[$iii]['file_name_upload'];?></td>
                                    <td><?php echo $aFileData[$iii]['file_size'];?></td>
                                    <td><?php echo $aFileData[$iii]['file_type'];?></td>
                                    <td><?php echo $aFileData[$iii]['uploaded_on'];?></td>
                                    <td><?php echo $sDownloadButton;?></td>
                                    <td><?php echo $sRenameButton;?></td>
                                    <td><?php echo $sDeleteButton;?></td>
                                </tr>
                                <?php  }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<script type="text/javascript">
    $(document).ready(function(){
        $(".classDownloadButton").click(function(){
            var iFileID = this.id;
            $.ajax({
                url:"ajaxRequest.php",
                data:{sFlag:'GetFilePath',iFileID:iFileID},
                async:true,
                method:"GET",
                success:function(sFilePath){
                    var sFilePath = "downloadFile.php?sFilePath="+sFilePath;
                    window.open(sFilePath,'_blank');
                }
            });
        });
        $(".classDeleteButton").click(function(){
            var iFileID = this.id;
            $.ajax({
                url:"ajaxRequest.php",
                data:{sFlag:'DeleteFile',iFileID:iFileID},
                async:true,
                method:"GET",
                success:function(bResult){
                    if(bResult){
                        window.location="viewUploadedFile.php?alert=Deleted successfully.&alertType=1";
                    }
                }
            });
        });
        
        $(".classRenameButton").click(function(){
            var iFileID = this.id;
            $("#idFileID").val(iFileID);
            $("#idModalRenameFile").modal('show');
        });

        $("#idRenameFile").click(function(){
            var iFileID = $("#idFileID").val();
            var sFileName = $("#idNewFileName").val();
            if(sFileName!=''){
                if(confirm('Are Your Sure to Update File Name ?')){
                    $.ajax({
                        url:"ajaxRequest.php",
                        data:{sFlag:'RenameFile',iFileID:iFileID,sFileName:sFileName},
                        async:true,
                        method:"GET",
                        success:function(bResult){
                            if(bResult){
                                window.location="viewUploadedFile.php";    
                            }
                        }
                    });
                }else{
                    $("#idFileID").val('');
                    $("#idModalRenameFile").modal('hide');
                }
            }else{
                $("#idFileID").val('');
                $("#idModalRenameFile").modal('hide');
            }
        });

        $('#idDataTable').DataTable({
            columnDefs: [ { "targets": 5, "orderable": false } ],
            columnDefs: [ { "targets": 6, "orderable": false } ],
            columnDefs: [ { "targets": 7, "orderable": false } ],
        });
    });
</script>
<div class="modal fade" id="idModalRenameFile" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update File Name</h4>
      </div>
      <div class="modal-body">
            <input type="text" name="new_file_name" id="idNewFileName" class="form-control">
            <input type="hidden" name="file_id" id="idFileID">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="idRenameFile">Update</button>
      </div>
    </div>
  </div>
</div>

<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>