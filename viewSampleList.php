<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "viewSampleList.php";
$iPageID=14;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "View Sample";

$aSampleData = getSampleList();

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Sample</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                	 <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable" style="margin-left:40px">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Sample Name</th>
                                                <th>Update</th>
                                                <th>Freeze</th>
                                                <th>Mapp</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<count($aSampleData);$iii++){
                                                	$iSampleID = $aSampleData[$iii]['sample_id'];
                                                	$sSampleName = $aSampleData[$iii]['sample_name'];
                                                    $iStatus = $aSampleData[$iii]['status'];
                                                	$sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iSampleID}'>Update</button>";
                                                    if($iStatus ==1){
                                                        $sFreezeButton = "<button type='button' class='btn btn-info classFreezeButton' id='{$iSampleID}'>Freeze</button>";
                                                    }else{
                                                        $sFreezeButton = "<button type='button' class='btn btn-danger classUnFreezeButton' id='{$iSampleID}'>UnFreeze</button>";
                                                    }
                                                    $sMappButton = "<button type='button' class='btn btn-info classMappButton' id='{$iSampleID}'>Mapp Test</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td>
                                               		<?php echo $sSampleName;?>
                                                </td>
                                                
                                                <td><?php echo $sUpdateButton;?></td>
                                                <td><?php echo $sFreezeButton;?></td>
                                                <td><?php echo $sMappButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
    	$(".classUpdateButton").click(function(){
    		iID = this.id;
    		$("#idSampleID").val(iID);
    		$("#idUpdateSampleModal").modal('show');
    	});
        
        $(".classMappButton").click(function(){
            iID = this.id;
            window.open('viewSampleTestMapping.php?iSampleID='+iID, '_blank');
        });

    	$("#idRenameSample").click(function(){
    		var iSampleID = $("#idSampleID").val();
    		var sSampleName = $("#idNewSampleName").val();
    		
    		if(sSampleName!=''){
                if(confirm('Are Your Sure to Update Sample Name ?')){
                    $.ajax({
                        url:"ajaxRequest.php",
                        data:{sFlag:'UpdateSample',iSampleID:iSampleID,sSampleName:sSampleName},
                        async:true,
                        method:"GET",
                        success:function(bResult){
                            if(bResult){
                                window.location="viewSampleList.php";    
                            }
                        }
                    });
                }else{
                    $("#idSampleID").val('');
                    $("#idUpdateSampleModal").modal('hide');
                }
            }else{
                $("#idSampleID").val('');
                $("#idUpdateSampleModal").modal('hide');
            }
    	});

        $(".classFreezeButton").click(function(){
            var iSampleID = this.id;
            if(confirm('Are you sure want to Freeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'FreezeSample',iSampleID:iSampleID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                                window.location='viewSampleList.php';    
                            }
                });
            }
        });

        $(".classUnFreezeButton").click(function(){
            var iSampleID = this.id;
            if(confirm('Are you sure want to UnFreeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'UnFreezeSample',iSampleID:iSampleID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                           window.location='viewSampleList.php';
                        }
                });
            }
        });

    });
    </script>
        <div class="modal fade" id="idUpdateSampleModal" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update File Name</h4>
              </div>
              <div class="modal-body">
                    <input type="text" name="new_sample_name" id="idNewSampleName" class="form-control">
                    <input type="hidden" name="sample_id" id="idSampleID">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="idRenameSample">Update</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>