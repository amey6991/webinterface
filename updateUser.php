<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";

$iUserID = $_REQUEST['iUserID'];
$oUser = new User($iUserID);

$sPageTitle = "Update User";
$aClients = fGetAllClients();
$aStates = getAllStates();

$sOptionClient="<option value=''>Select</option>";
for($iii=0;$iii<count($aClients);$iii++){
    $iCID = $aClients[$iii]['client_id'];
    $sCName = $aClients[$iii]['client_name'];
    $sOptionClient .= "<option value='{$iCID}'>{$sCName}</option>";
}
$aUserType = fGetUserTypes();

$sOptionUserType="<option value=''>Select</option>";
for($iii=0;$iii<count($aUserType);$iii++){
    $iUTypeID = $aUserType[$iii]['user_type_id'];
    $sUserType = $aUserType[$iii]['user_type'];
    if($iType==1){
        if($iUTypeID==1){
            continue;
        }else{
            $sOptionUserType .= "<option value='{$iUTypeID}'>{$sUserType}</option>";        
        }
    }
    if($iType==2){
        if($iUTypeID==2){
            continue;
        }else{
            $sOptionUserType .= "<option value='{$iUTypeID}'>{$sUserType}</option>";        
        }
    }  
}

if(!empty($aStates)){
    $sStateOption = "<option value=''>Select</option>";
    foreach ($aStates as $key => $aValue) {
        $sStateName = $aValue['name'];
        if($oUser->sState==$sStateName){
            $sStateOption .= "<option value='{$sStateName}' selected>{$sStateName}</option>";
        }else{
            $sStateOption .= "<option value='{$sStateName}'>{$sStateName}</option>";
        }
    }
}else{
    $sStateOption = "<option value=''>Select</option>";
}

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"ResearchDx","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"Update Users","link"=>"updateUser.php?iUserID={$iUserID}","isActive"=>true)
            );
?>

    <!-- Main Content -->
    <div class="container-fluid">
        <div class="side-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <ol class="breadcrumb navbar-breadcrumb">
                                    <?php echo parseBreadcrumb($aBreadcrumb); ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormAdduser" action="em_update_user.php">
                                        <input type="hidden" name="user_type" value="<?php echo $oUser->iType ;?>">
                                        <input type="hidden" name="client_id" value="<?php echo $oUser->iUserClientID ;?>">
                                        <div class="form-group">
                                            <label for="idInputFirstName" class="col-sm-2 control-label">First Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="firstName" class="form-control" id="idInputFirstName" placeholder="first name" value="<?php echo $oUser->sFirstName;?>">
                                                <input type="hidden" name="iuserID" value="<?php echo $iUserID ;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="idInputLastName" class="col-sm-2 control-label">Last Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="lastName" class="form-control" id="inputLastName" placeholder="last name" value="<?php echo $oUser->sLastName;?>">
                                            </div>
                                        </div>
                                        <div class="form-group hide">
                                            <label for="idInputLastName" class="col-sm-2 control-label">User Type</label>
                                            <div class="col-md-4">
                                                <select id="idUserType" name="user_type1">
                                                    <?php echo $sOptionUserType;?>        
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group hide">
                                            <label for="idInputLastName" class="col-sm-2 control-label">User Client</label>
                                            <div class="col-md-4">
                                                <select id="idClient" name="client_id1">
                                                    <?php echo $sOptionClient;?>        
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputStateName" class="col-sm-2 control-label">State</label>
                                            <div class="col-md-4">
                                                <select id="idInputStateName" name="stateName" class="form-control">
                                                    <?php echo $sStateOption;?>
                                                </select>
                                                <!-- <input type="text" name="stateName" class="form-control" id="inputStateName" value="<?php //echo $oUser->sState;?>"> -->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputCityName" class="col-sm-2 control-label">City</label>
                                            <div class="col-md-4">
                                                <input type="text" name="cityName" class="form-control" id="inputCityName" value="<?php echo $oUser->sCity;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="address" class="form-control" id="idInputAddress"><?php echo $oUser->sAddress;?></textarea> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputMobileNumber" class="col-sm-2 control-label">Mobile no</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile" class="form-control" id="idInputMobileNumber" value="<?php echo $oUser->sMobile;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                            <div class="col-md-4">
                                                <input type="email" name="emailAddress" class="form-control" id="idInputEmail" placeholder="Email" value="<?php echo $oUser->sEmail;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-2 control-label">User Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="userName" class="form-control" id="idInputUserName" placeholder="user name" value="<?php echo $oUser->sUsername;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idUpdateUser" class="btn btn-default">Update</button>
                                                <a  class="btn btn-warning" href="viewUsers.php">Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idUpdateUser").click(function(){
                var sFirstName = $("#idInputFirstName").val();
                var sLastName = $("#idInputLastName").val();
                var sStateName = $("#idInputStateName").val();
                var sCityName = $("#idInputCityName").val();
                var sAddresstName = $("#idInputAddress").val();
                var iUserType = $("#idUserType").val();
                var sUserName = $("#idInputUserName").val();
                var sEmail = $("#idInputEmail").val();
                var sMobileNumber = $("#idInputMobileNumber").val();
                

                if(sFirstName ==''){
                    displayAlert('<strong>Alert!</strong><br />First Name Must to enter.','error');
                }else if(sEmail=='' || !fValidateEmail(sEmail)){
                    displayAlert('<strong>Alert!</strong><br />Please Checke Your EMAIL.','error');
                }else if(sMobileNumber=='' || !fValidateMobileNumber(sMobileNumber)){
                    displayAlert('<strong>Alert!</strong><br />Mobile Number is Not Valid.','error')
                }else if(sUserName==''){
                    displayAlert('<strong>Alert!</strong><br />User Name is Can Not be Empty.','error');
                }else if(sStateName ==""){
                    displayAlert('<strong>Alert!</strong><br />Please Select a STATE.','error')
                }else{
                    $("#idFormAdduser").submit();
                }
         });
    });
    
    function fValidateMobileNumber(sMobileNumber){
        var sRegularExp =  /^\s*(?:\+?(\d{1,3}))?[-(]*(\d{3})[-)]*(\d{3})[-]*(\d{4})(?: *x(\d+))?\s*$/;

        if(!sMobileNumber.match(sRegularExp)){
            return false;
        }else{
            return true;
        }
    }

    function fValidateEmail(sEmail){
            var email = /^$|^[a-zA-Z0-9]{1}([a-zA-Z0-9]?[\.\-\_]{0,1}[a-zA-Z0-9]+)*[@]{1}(([a-zA-Z0-9]+[\.\-\_]{1})+([a-zA-Z]+))$/;
            if(!sEmail.match(email)){
                return false;
            }else{
                return true;
            }

    }

    function fValidateUserName(sUserName){
        var iResult;
        $.ajax({
                url:"ajaxRequest.php",
                data:{sFlag:'GetuserNameAvailablility',sUserName:sUserName},
                async:false,
                method:"GET",
                success:function(bResult){
                   if(bResult > 0){
                        iResult=1;
                        
                   }else{
                        iResult=0;
                   }
                }
            });
        return iResult;
    }

    </script>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>