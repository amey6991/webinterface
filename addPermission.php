<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";
$sPageTitle = "Add Permission";

$oSession = new SessionManager();
$iUserTypeID = $oSession->iType;
$iUserClientID = $oSession->iUserClientID;

$aClients = fGetAllClients();
$aUserType = fGetUserTypes();
$aPages = fGetAllPages();
$aFeatures = fGetAllFeatures();
$aActions = fGetAllActions();

$sOptionUserType="<option value=''>Select</option>";
for($iii=0;$iii<count($aUserType);$iii++){
    $iUTypeID = $aUserType[$iii]['user_type_id'];
    $sUserType = $aUserType[$iii]['user_type'];
    $sOptionUserType .= "<option value='{$iUTypeID}'>{$sUserType}</option>";
}
$sOptionPages="<option value=''>Select</option>";
for($iii=0;$iii<count($aPages);$iii++){
    $iPageID = $aPages[$iii]['page_id'];
    $sPageCode = $aPages[$iii]['page_code'];
    $sOptionPages .= "<option value='{$iPageID}'>{$sPageCode}</option>";
}
$sOptionFeatures="<option value=''>Select</option>";
for($iii=0;$iii<count($aFeatures);$iii++){
    $iFeatureID = $aFeatures[$iii]['feature_id'];
    $sFeatureCode = $aFeatures[$iii]['feature_code'];
    $sOptionFeatures .= "<option value='{$iFeatureID}'>{$sFeatureCode}</option>";
}
$sOptionActions="<option value=''>Select</option>";
for($iii=0;$iii<count($aActions);$iii++){
    $iActionID = $aActions[$iii]['action_id'];
    $sActionCode = $aActions[$iii]['action_code'];
    $sOptionActions .= "<option value='{$iActionID}'>{$sActionCode}</option>";
}

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Add Permission</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormAddPermission" action="em_add_permission.php">
                                        <div class="form-group">
                                            <label for="idInputLastName" class="col-sm-2 control-label">User Type</label>
                                            <div class="col-md-4">
                                                <select id="idUserPermission" name="user_permission" class="form-control" onchange="fShowNextResult()">
                                                    <option value="">Select</option>
                                                    <option value="0">User</option>
                                                    <option value="1">User Type</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group hide" id="idDivUserType">
                                            <label for="idInputLastName" class="col-sm-2 control-label">User Type</label>
                                            <div class="col-md-4">
                                                <select id="idUserType" name="user_type" class="form-control" onchange="fShowFinalFiled()">
                                                    <?php echo $sOptionUserType;?>        
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group hide" id="idDivUserName">
                                                <label for="idInputLastName" class="col-sm-2 control-label">User Name</label>
                                                <div class="col-md-4">
                                                   <input type="text" id="idUserName" onkeyup="fGetUserName()" name="user_name" class="form-control" placeholder="Enter minimum 4 character ">
                                                   <input type="hidden" id="idUserNameID" name="user_id">
                                                </div>
                                        </div>
                                        <div class="hide" id="idDivRestFields">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Permission Entity</label>
                                                <div class="col-md-4">
                                                    <select id="idPermissionType" onchange="fShowNextField(this.value)" name="permission_entity_type" class="form-control">
                                                          <option value="3">Select</option>
                                                          <option value="0">Page</option>
                                                          <option value="1">Feature</option>
                                                          <option value="2">Action</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group hide" id="idDivEntity_0">
                                                <label class="col-sm-2 control-label">Page Entities</label>
                                                <div class="col-md-4">
                                                    <select id="idPermissionPageEntity" onChange="fSetPageID(this.value)" name="permission_page_entity" class="form-control">
                                                        <?php echo $sOptionPages;?>      
                                                    </select>
                                                    <input type="hidden" name="page_id" id="idPageID">
                                                </div>
                                            </div>
                                            <div class="form-group hide" id="idDivEntity_1">
                                                <label class="col-sm-2 control-label">Feature Entities</label>
                                                    <div class="col-md-4">
                                                        <select id="idPermissionFeatureEntity" onChange="fSetFeatureID(this.value)" name="permission_feature_entity" class="form-control">
                                                            <?php echo $sOptionFeatures;?>      
                                                        </select>
                                                        <input type="hidden" name="feature_id" id="idFeatureID">
                                                    </div>
                                            </div>
                                            <div class="form-group hide" id="idDivEntity_2">
                                                <label class="col-sm-2 control-label">Action Entities</label>
                                                    <div class="col-md-4">
                                                        <select id="idPermissionActionEntity" onChange="fSetFeatureID(this.value)" name="permission_action_entity" class="form-control">
                                                            <?php echo $sOptionActions;?>      
                                                        </select>
                                                        <input type="hidden" name="action_id" id="idActionID">
                                                    </div>
                                            </div>
                                            <div class="form-group hide" id="idDivPermLevel">
                                                <label class="col-sm-2 control-label">Permission Level</label>
                                                    <div class="col-md-4">
                                                        <select id="idPermissionLevelAll" name="permission_level" class="form-control hide">
                                                            <option value=''>Select</option>
                                                            <option value='0'>No Permission</option>
                                                            <option value='1'>Partial Permission</option>
                                                            <option value='2'>Full Permission</option>    
                                                        </select>
                                                        <select id="idPermissionLevelAction" name="permission_level_action" class="form-control hide">
                                                            <option value=''>Select</option>
                                                            <option value='0'>True</option>
                                                            <option value='1'>False</option>
                                                        </select>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddPermission" class="btn btn-success">Save</button>
                                                <a href="dashboard.php" class="btn btn-info">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddPermission").click(function(){
                $("#idFormAddPermission").submit();
         });
    });
    function fGetUserName(){
        var sUserName=$("#idUserName").val();
        console.log(sUserName);
        $.ajax({
            url:'ajaxRequest.php',
            dataType:'Json',
            data:{
                sFlag:'GetAllUserNames',
                sUserName:sUserName
            },
            success:function(result){
                console.log(result);
                var aUserData = [];
                $.each(result, function (id,aUserName) {
                    aUserData.push({
                        label: aUserName[1],
                        value: aUserName[1],
                        id:aUserName[0]
                     })
                });
                console.log(aUserData);
                $("#idUserName").autocomplete({
                    source:aUserData,
                    minLength:3,
                    select: function(event, ui) {
                        $('#idUserName').val(ui.item.label);
                        $('#idUserNameID').val(ui.item.id);
                        sUserName=ui.item.label;
                    }

                });
            }
         });
    }

    function fShowFinalFiled(){
        var iuserType = $("#idUserType").val();
        if(iuserType != ''){
            $("#idDivRestFields").removeClass("hide");
        }else{
            $("#idDivRestFields").addClass("hide");
        }
    }
    function fShowNextField(iEntity){
        if(iEntity==0){
            var sID="#idDivEntity_"+iEntity;
            $(sID).removeClass("hide");
            $("#idPageID").val('');
            $("#idFeatureID").val('');
            $("#idActionID").val('');
            $("#idDivPermLevel").removeClass("hide");
            $("#idPermissionLevelAll").removeClass("hide");
            $("#idPermissionLevelAction").addClass("hide");
            $("#idDivEntity_1").addClass("hide");
            $("#idDivEntity_2").addClass("hide");
        }else if(iEntity==1){
            var sID="#idDivEntity_"+iEntity;
            $(sID).removeClass("hide");
            $("#idPageID").val('');
            $("#idFeatureID").val('');
            $("#idActionID").val('');
            $("#idDivPermLevel").removeClass("hide");
            $("#idPermissionLevelAll").removeClass("hide");
            $("#idPermissionLevelAction").addClass("hide");
            $("#idDivEntity_0").addClass("hide");
            $("#idDivEntity_2").addClass("hide");
        }else if(iEntity==2){
            var sID="#idDivEntity_"+iEntity;
            $(sID).removeClass("hide");
            $("#idPageID").val('');
            $("#idFeatureID").val('');
            $("#idActionID").val('');
            $("#idDivPermLevel").removeClass("hide");
            $("#idPermissionLevelAction").removeClass("hide");
            $("#idPermissionLevelAll").addClass("hide");
            $("#idDivEntity_0").addClass("hide");
            $("#idDivEntity_1").addClass("hide");
        }else if(iEntity==3){
            $("#idDivPermLevel").addClass("hide");
            $("#idPermissionLevelAction").addClass("hide");
            $("#idPermissionLevelAll").addClass("hide");
            $("#idDivEntity_0").addClass("hide");
            $("#idDivEntity_1").addClass("hide");
            $("#idDivEntity_2").addClass("hide");
            $("#idPageID").val('');
            $("#idFeatureID").val('');
            $("#idActionID").val('');
        }
    }
    function fShowNextResult(){
       var iUserPermission = $("#idUserPermission").val();
       if(iUserPermission==0){
            $("#idDivUserName").removeClass("hide");
            $("#idDivUserType").addClass("hide");
       }else{
            $("#idDivUserType").removeClass("hide");
            $("#idDivUserName").addClass("hide");
       }
    }
    function fSetPageID(iPageID){
        $("#idPageID").val(iPageID);
    }
    function fSetFeatureID(iFeatureID){
        $("#idFeatureID").val(iFeatureID);
    }
    function fSetActionID(iActionID){
        $("#idActionID").val(iActionID);
    }

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>