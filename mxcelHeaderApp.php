<?php
    require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
    require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
    require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
    require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
    require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";

    $iType = $sessionManager->iType;
    $aBreadcrumb = array(array(
                "title"=>"ResearchDx",
                "link"=>"dashboard.php",
                "isActive"=>true
            ));
?>
<body class="flat-blue" style="font-family:lato;">
    <div class="app-container">
        <div class="row content-container">
            <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                <div class="container-fluid" style="height:60px">
                    <div class="navbar-header">
                        <ol class="breadcrumb navbar-breadcrumb">
                            <?php //echo parseBreadcrumb($aBreadcrumb); ?>
                            <img src="images/ResearchDx.jpg" width="120px" height="50px" style="margin-top:5px;">
                        </ol>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <ul class="nav navbar-nav navbar-right hide">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <li class="dropdown profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $sessionManager->sName; ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="profile-img">
                                    <img src="images/webuser.png" class="profile-img">
                                </li>
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username"><?php echo $sessionManager->sName; ?> <small class="text-muted"><?php echo $sessionManager->sUsername; ?></small></h4>
                                        <p><?php echo $sessionManager->sEmail; ?></p>
                                        <div class="btn-group margin-bottom-2x" role="group">
                                            <a href="viewProfile.php?iUserID=<?php echo $sessionManager->iUserID;?>" class="btn btn-default"><i class="fa fa-user"></i> Profile</button>
                                            <a href="logout.php" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="side-menu" style="font-family:lato;">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="side-menu-container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <button type="button" class="navbar-expand-toggle">
                                    <i class="fa fa-bars icon"></i>
                                </button>
                                <div class="hide title classHeaderTitle"><?php echo CONFIG_BRAND_NAME; ?></div>
                            </a>
                            <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav" style="height: 400px;">
                            <li>
                                <a href="dashboard.php">
                                    <i class="fa fa-tachometer fa-1x classIcontFont"></i><span class="title classDropdownTitle">Dashboard</span>
                                </a>
                            </li>
                               <?php
                                $iFirstFeatureID = 11;
                                $iFirstFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFirstFeatureID);
                                if($iFirstFeaturePermLevel>0) {
                            ?>
                                <li class="hide panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#idFileDropDown" class="classExpandSideBar">
                                        <i class="fa fa-file-o fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manager Files</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idFileDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="uploadFile.php">Upload File</a>
                                                </li>
                                                <li><a href="viewUploadedFile.php">Download File</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                             <?php
                                }
                                $iFirstFeatureID = 1;
                                $iFirstFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFirstFeatureID);
                                if($iFirstFeaturePermLevel>0) {
                            ?>
                                <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#idClientDropDown" class="classExpandSideBar">
                                        <i class="fa fa-users fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manager Client</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idClientDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addClient.php">Add Client</a>
                                                </li>
                                                <li><a href="viewClients.php">View Client</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                    }
                                ?>
                            
                            <?php
                                $iFeatureID = 6;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                                <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#idUserDropDown" class="classExpandSideBar">
                                        <i class="fa fa-user fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manager Users</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idUserDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addUser.php">Add User</a>
                                                </li>
                                                <li><a href="viewUsers.php">View Users</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php 
                                }
                                $iFeatureID = 7;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                         
                                 <li class="">
                                    <a  href="setup.php">
                                        <i class="fa fa-cog fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manage Email</span>
                                    </a>
                                </li>
                            <?php 
                                }
                                $iFeatureID = 15;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                                 <li class="">
                                    <a  href="Reports.php">
                                        <i class="fa fa-file-text fa-1x classIcontFont"></i><span class="title classDropdownTitle">Reports</span>
                                    </a>
                                </li>
                            <?php 
                                }
                                $iFeatureID = 12;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                                    <li class="hide panel panel-default dropdown">
                                        <a data-toggle="collapse" href="#idWorkflowDropDown" class="classExpandSideBar">
                                            <i class="fa fa-bell fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manager Workflow</span>
                                        </a>
                                        <!-- Dropdown level 1 -->
                                        <div id="idWorkflowDropDown" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="nav navbar-nav">
                                                    <li><a href="addWorkflow.php">Add Workflow</a>
                                                    </li>
                                                    <li><a href="viewWorkflowList.php">View Permission</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                            <?php 
                                }
                                $iFeatureID = 13;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                                    <li class="hide panel panel-default dropdown">
                                        <a data-toggle="collapse" href="#idTestDropDown" class="classExpandSideBar">
                                            <i class="fa fa-bell fa-1x classIcontFont"></i><span class="title classDropdownTitle" >Manager Test</span>
                                        </a>
                                        <!-- Dropdown level 1 -->
                                        <div id="idTestDropDown" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="nav navbar-nav">
                                                    <li><a href="addTest.php">Add Test</a>
                                                    </li>
                                                    <li><a href="viewTestList.php">View Test</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                            <?php 
                                }$iFeatureID = 14;
                                $iFeaturePermLevel = PermissionHandler::getFeaturePermissionLevel($sessionManager, $iFeatureID);
                                if($iFeaturePermLevel>0){
                            ?>
                                    <li class="hide panel panel-default dropdown">
                                        <a data-toggle="collapse" href="#idSampleDropDown" class="classExpandSideBar">
                                            <i class="fa fa-bell fa-1x classIcontFont"></i><span class="title classDropdownTitle">Manager Sample</span>
                                        </a>
                                        <!-- Dropdown level 1 -->
                                        <div id="idSampleDropDown" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="nav navbar-nav">
                                                    <li><a href="addSample.php">Add Sample</a>
                                                    </li>
                                                    <li><a href="viewSampleList.php">View Sample List</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                            <?php 
                                }
                            ?>
                                <li class="panel panel-default dropdown hide">
                                    <a data-toggle="collapse" href="#idPageDropDown" class="classExpandSideBar">
                                        <i class="fa fa-file fa-1x classIcontFont"></i><span class="title">Manager Pages</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idPageDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addPage.php">Add Pages</a>
                                                </li>
                                                <li><a href="viewPages.php">View Pages</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel panel-default dropdown hide">
                                    <a data-toggle="collapse" href="#idFeatureDropDown" class="classExpandSideBar">
                                        <i class="fa fa-file fa-1x classIcontFont"></i><span class="title">Manager Feature</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idFeatureDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addFeature.php">Add Feature</a>
                                                </li>
                                                <li><a href="viewFeatures.php">View Features</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel panel-default dropdown hide">
                                    <a data-toggle="collapse" href="#idActionDropDown" class="classExpandSideBar">
                                        <i class="fa fa-file fa-1x classIcontFont"></i><span class="title">Manager Action</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idActionDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addAction.php">Add Action</a>
                                                </li>
                                                <li><a href="viewActions.php">View Action</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel panel-default dropdown hide">
                                    <a data-toggle="collapse" href="#idPermissionDropDown" class="classExpandSideBar">
                                        <i class="fa fa-bell fa-1x classIcontFont"></i><span class="title">Manager Permision</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="idPermissionDropDown" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="addPermission.php">Add Permission</a>
                                                </li>
                                                <li><a href="viewPermissions.php">Update Permission</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
                <div class="classUserInfo classDropdownTitle">
                    Logged In as <strong><?php echo $sessionManager->sUsername." </strong> "." "; ?>
                    <?php 
                        if($sessionManager->iType==1){
                            echo "<img src='images/superadmin.png' class='img-responsive img-rounded' alt='Super Admin' style='width:50px;height:50px;'>"."<br/>";
                            }elseif($sessionManager->iType==2){
                                echo "<img src='images/admin.png' class='img-responsive img-rounded' alt='Admin' style='width:50px;height:50px;'>"."<br/>";
                            }else{
                                echo "<i class='fa fa-user fa-1x' style='font-size:22px' tooltip='User'></i>"."<br/>";
                            }
                    ?>
                    Last Login on <?php echo date('m-d-Y',strtotime($sessionManager->aLastActivity['login_date'])); ?> at  <?php echo date('H:i A',strtotime($sessionManager->aLastActivity['login_time']))." (".date_default_timezone_get().")<br/>" ?>
                    Logged in Since: <?php echo date('H:i A',strtotime($sessionManager->sLoginTime))." (".date_default_timezone_get().") <br/>" ?>
                    <div class="btn-group margin-bottom-2x" role="group">
                        <a href="viewProfile.php?iUserID=<?php echo $sessionManager->iUserID;?>" class="btn btn-default"><i class="fa fa-user"></i> Profile</button>
                        <a href="logout.php" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".classExpandSideBar").click(function(){
                        if($(this).attr('aria-expanded') == undefined || $(this).attr('aria-expanded') == 'false'){
                            $( "body div" ).first().addClass('expanded');
                            $('.navbar-expand-toggle').addClass('fa-rotate-90');
                        }else{
                            $( "body div" ).first().removeClass('expanded');
                            $('.navbar-expand-toggle').removeClass('fa-rotate-90');
                        }
                    });
                });
            </script>