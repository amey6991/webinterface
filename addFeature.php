<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
$sPageTitle = "Add Feature";


include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Add Feature</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormFeature" action="em_add_feature.php">
                                        <div class="form-group">
                                            <label for="id_feature_code" class="col-sm-2 control-label">Feature Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="feature_code" class="form-control" id="id_feature_code" placeholder="Enter Unique feature Code">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_feature_name" class="col-sm-2 control-label">Feature Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="feature_name" class="form-control" id="id_feature_name" placeholder="Enter feature Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_feature_notes" class="col-sm-2 control-label">Feature Note</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="feature_notes" class="form-control" id="id_feature_notes"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddFeature" class="btn btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddFeature").click(function(){
                var sFeatureCode = $("#id_feature_code").val();
                var sFeatureName = $("#id_feature_name").val();
                
                
                
                if(sFeatureCode ==''){
                    alert('feature Code Must to enter.');
                }else if(sFeatureName==''){
                    alert('feature Name Must to enter.');
                }else{
                    $("#idFormFeature").submit();
                }
         });
    });
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>