<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";

	$iuserID = $_POST['iuserID'];
	$sFirstName = $_POST['firstName'];
	$sLastName = $_POST['lastName'];
	$sFullName =$sFirstName.' '.$sLastName;
	$iUserType = $_POST['user_type'];
	$iClientID = $_POST['client_id'];
	$sStateName = $_POST['stateName'];
	$sCityName = $_POST['cityName'];
	$sAddress = $_POST['address'];
	$iMobileNumber = $_POST['mobile'];
	$sEmail = $_POST['emailAddress'];
	$sUserName = $_POST['userName'];
	
	$oUsers = new User();
	$bUpdatedResult = $oUsers->fUpdateUsers($iuserID,$sFirstName,$sLastName,$sFullName,$iUserType,$iClientID,$sStateName,$sCityName,$sAddress,$iMobileNumber,$sEmail,$sUserName);
	
	if($bUpdatedResult)
	{
		$sMsg = array();
		$sMsg[] = "S3";
	    redirectWithAlert("viewUsers.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E3";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("updateUser.php?iUserID=".$iuserID, $sMsg);
		}
?>