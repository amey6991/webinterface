<?php 
    displayAlerts();
?>
<script type="text/javascript">
    function updateMoments() {
        $('.classMomentTimestamp').each(function(){
            var sTime = $(this).attr("data-timestamp");
            var sDate = moment(sTime, 'YYYY-MM-DD H:mm:ss').fromNow();
            $(this).text(sDate);
        });
        setTimeout(updateMoments, 30000);
    }

    updateMoments();

    $(function() {
        $(".navbar-expand-toggle").click(function() {
            $(".app-container").toggleClass("expanded");
            return $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
        });
        return $(".navbar-right-expand-toggle").click(function() {
            $(".navbar-right").toggleClass("expanded");
            return $(".navbar-right-expand-toggle").toggleClass("fa-rotate-90");
        });
    });
    function displayAlert(sDisplayMsg,sDisplayType){
        noty({
                text        : sDisplayMsg,
                type        : sDisplayType,
                theme       : 'relax',
                dismissQueue: true,
                layout      : "topRight",
                closeWith: ['click']
            });
    }
</script>
</body>
</html>