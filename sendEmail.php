<?php
/*
* File contains the Email creating and sending function
* Created On : 20th July 2016
* Created By : Amey
*/

//! It contains the Email template constant variable
//! Include configEhr.php file for constant variable values.

require_once PATH_TO_ROOT."config/config.EmailTemplate.php";
require_once PATH_TO_ROOT."config/config.Email_SMS.php";
require_once PATH_TO_ROOT."classes/class.Email.php";
require_once PATH_TO_ROOT."classes/class.EmailTemplateBuilder.php";

//! brief function to send Email using Email class
//! Parameter : Recipient details, Email template flag, Email info
//! $aRecipientEID - Array - It contains all the recipient numbers
//! $aRecipientName - Array - It contains all the recipient names
//! $iEmailTemplateFlag - Integer - It contains Email template flag value
//! $aEmailInfo - Array - It contains the Email info which is used toe create Email template with dynamic values.
//! $sSenderEID - Array - It contains sender email id and it is not mandatory
//! $sSenderEName - Array - It contains sender name and it is not mandatory
//! Return : TRUE -> Send Email successfully, FALSE -> Not send and gives the error message

function fSendEmail($aRecipientEID,$aRecipientName,$iEmailTemplateFlag,$aEmailInfo,$aAttachement,$sSenderEID,$sSenderEName){

	$iResult = 0;

	if(EMAIL_ON == FALSE){
		return TRUE;
	}

	if(TEST_EMAIL == TRUE){
		$aRecipientEID = array(TEST_EMAIL_ID);
	}

	if($iEmailTemplateFlag == 0){
		return TRUE;
	}

	//! Create Email template
	$aEmailInfo['recipient_name']=$aRecipientName[0];
	$aEmailContent = fCreateEmailContent($iEmailTemplateFlag,$aEmailInfo);
	// print_r($aEmailContent);
	// exit();

	if(!empty($aEmailContent)){

		$iRecipientCount = count($aRecipientEID);
		$sSubject = $aEmailContent['sSubject'];
		$sBody = $aEmailContent['sBody'];

		$oEmailBuilder = new EmailTemplateBuilder();
		$oEmailBuilder->fCreateOneColumnTemplate($sBody);
		$oEmailBuilder->fClose();
		$sBody = $oEmailBuilder->sEmailTemplate;

		$oEmail = new Email($aRecipientEID[0],$aRecipientName[0],$sSubject,$sBody,$sSenderEID,$sSenderEName);

		//! Check when Email created successfully
		if($oEmail->iInsertEId > 0){

			if($iRecipientCount > 1){

				//! Added Recipient if it is more that one
				for($iii=1;$iii<$iRecipientCount;$iii++){
					$oEmail->addRecipient($aRecipientEID[$iii],$aRecipientName[$iii]);
				}
			}

			//! add attachment id any
			if(!empty($aAttachement)){

				for($kkk=0;$kkk<count($aAttachement);$kkk++){
					$oEmail->addAttachment($aAttachement[$kkk],''); //! Set Filename to null menas it will gose with real file name.
				}
			}
		}

		//! Send Email
		$iResult = $oEmail->sendNow();
		
	}

	return $iResult;
}



//! brief function to create a Email template
//! Parameter - Email template flag, Email dynamic info
//! Return - string - Email Template for successful result, 0 for unsuccessful
function fCreateEmailContent($iEmailTemplateFlag,$aEmailInfo){
  	
  	$aEmailContent = array();

  	$sSiteURL = SITE_URL;
  	$sLogoURL = ORG_LOGO_URL;
  	$sORGName = ORG_NAME;
  	

  	if($iEmailTemplateFlag == EMAIL_SEQRUN_FINISHED_TO_DEFAULT_LIST){

  		$sDIRName = $aEmailInfo['dir_name'];
  		$sDIRPath = $aEmailInfo['dir_path'];
  		$iSeqType = $aEmailInfo['seq_type'];
  		$sReciepentName = $aEmailInfo['recipient_name'];

  		if($iSeqType==1){
  			$sSeqType = 'MiSeq';
  		}else{
  			$sSeqType = 'NextSeq';
  		}

  			$aEmailContent['sSubject'] = "Sequencer RUN Finished";
		  	$aEmailContent['sBody'] = <<<EOB
		<span style='font-weight: bold;font-size: 18px'> Dear {$sReciepentName}, </span>
		<br/><br/>
		The Running Sequencer is Finished of Following Details: <br/><br/>
		
		<ol>
			<li>Sequencer Type: <strong>{$sSeqType}</strong></li>
			<li>Sequencer Name: <strong>{$sDIRName}</strong></li>
			<li>Sequencer Path: <strong>{$sDIRPath}</strong></li>
		</ol>
		<br /><br />
		Please visit: <strong><a href="{$sSiteURL}">{$sSiteURL}</a></strong>
		<br /><br />
		Thank You.
		
EOB;
  	}

  	if($iEmailTemplateFlag == EMAIL_SEQRUN_ERROR_TO_DEFAULT_LIST){
  		$sDIRName = $aEmailInfo['dir_name'];
  		$sDIRPath = $aEmailInfo['dir_path'];
  		$iSeqType = $aEmailInfo['seq_type'];
  		$sError = $aEmailInfo['error'];

  		$sReciepentName = $aEmailInfo['recipient_name'];

  		if($iSeqType==1){
  			$sSeqType = 'MiSeq';
  		}else{
  			$sSeqType = 'NextSeq';
  		}

		$aEmailContent['sSubject'] = "Error On Sequencer RUN" ;
 	  	$aEmailContent['sBody'] = "	<span style='font-weight: bold;font-size: 18px'> Dear {$sReciepentName}, </span>
		<br/><br/>
		The Running Sequencer is Not Finished Because of <strong>Error Occured </strong>, Following Details are: <br/><br/>
		
		<ol>
			<li>Sequencer Type: <strong>{$sSeqType}</strong></li>
			<li>Sequencer Name: <strong>{$sDIRName}</strong></li>
			<li>Sequencer Path: <strong>{$sDIRPath}</strong></li>
			<li>Error Message : <strong>{$sError}</strong></li>
		</ol>
		<br /><br />
		Kindly Request to you to Go and Check Manually and Re-Run the Sequencer if Required.<br/>
		Please visit: <strong><a href=".$sSiteURL.">{$sSiteURL}</a></strong>
		<br /><br />
		Thank You.";	

  	}

  	if($iEmailTemplateFlag == EMAIL_SEQRUN_START_TO_DEFAULT_LIST){

  		$sDIRName = $aEmailInfo['dir_name'];
  		$sDIRPath = $aEmailInfo['dir_path'];
  		$iSeqType = $aEmailInfo['seq_type'];
  		$sReciepentName = $aEmailInfo['recipient_name'];

  		if($iSeqType==1){
  			$sSeqType = 'MiSeq';
  		}else{
  			$sSeqType = 'NextSeq';
  		}

  			$aEmailContent['sSubject'] = "Sequencer RUN Started";
		  	$aEmailContent['sBody'] = "<span style='font-weight: bold;font-size: 18px'> Dear {$sReciepentName}, </span>
				<br/><br/>
				The Running Sequencer is Started of Following Details: <br/><br/>
				
				<ol>
					<li>Sequencer Type: <strong>{$sSeqType}</strong></li>
					<li>Sequencer Name: <strong>{$sDIRName}</strong></li>
					<li>Sequencer Path: <strong>{$sDIRPath}</strong></li>
				</ol>
				<br /><br />
				Please visit: <strong><a href=".$sSiteURL.">{$sSiteURL}</a></strong>
				<br /><br />
				Thank You.";
  	}
	return $aEmailContent;
}

?>