<?php

include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
include_once __DIR__.DIRECTORY_SEPARATOR."class.SessionManager.php";

/*!
 * @class User
 * @brief This class represents a user and it's behavior. 
 * @author Kishan Gor
 */

class Client
{
    //! user_id of User
    public $iID; 
    //! Name of the User
    public $sClientName;
    
    //! Email of the User
    public $sClientEmail; 
    //! users contact info
    public $sClientState;
    public $sClientCity;
    public $sClientAddress;
    public $sClientMobile;
    
    public $sUsername;
    public $iLoginID;
    public $iUserTypeID=2;
    public $dAddedOn;
    private $sPassword;
    public $sLastError;

    /*! @brief initialize the User class
    *  This class will instantiate the User class according to passed \a $iID representing the User ID.
    * @param $iID User ID
    * @note If no @a $iID is passed, it will not load the values and work as an skeleton for new User.
    *
    */
    function __construct($iID = NULL)
    {
        if($iID !== NULL){
          
        }
    }
}

?>