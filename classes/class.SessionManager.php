<?php
/*!
 * @class SessionManager
 * This class manages the user session.
 * @author Kishan Gor <me@kishan.co>
 */

class SessionManager {
    public $isLoggedIn;
    public $iUserID;
    public $sUsername;
    public $sName;
    public $sEmail;
    public $sError;
    public $iType;
    public $iUserClientID;
    public $sLoginDate;
    public $sLoginTime;
    public $sIP;
    public $sBrowser;
    public $sDeviceType;
    public $lastActivity;
    public $iLoginInstanceID;
    public $autoLoginSession;
    public $bForcePasswordUpdate;
    public $aPermMatrix;
    public $aLastActivity = array();


    //! constructor
    function __construct()
    {
        session_start();
        if($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_lastActivity'] < (time() - SESSION_TIME*60)){
            if ($this->autoLogin()) {
            }
            else {
                $this->logOut();
            }
            return;
        }
        if($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_isLoggedIn'] === TRUE){
            $this->isLoggedIn = TRUE;
            $this->iUserID=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iUserID'];
            $this->sUsername=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sUsername'];
            $this->sName=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sName'];
            $this->sEmail=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sEmail'];
            $this->sLoginDate=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime'];
            $this->iType=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iType'];
            $this->iUserClientID=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iUserClientID'];
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_lastActivity'] = time();
            $this->lastActivity = time();
            $this->sRememberMe = $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_RememberMe'];
            $this->sError = NULL;        
            $this->autoLoginSession = $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_autoLoginSession'];
            
            $oUser = $this->getLoggedInUser();
            $this->aPermMatrix = unserialize(urldecode($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_permMatrix']));
            
            // $sNextPWReset = strtotime($oUser->sNextPWReset);

            // if($sNextPWReset<time() || $sNextPWReset===false) {
            //     $this->bForcePasswordUpdate = TRUE;
            // }
            // else {
            //     $this->bForcePasswordUpdate = FALSE;   
            // }
            $this->aLastActivity = $this->getLastLoginDateTimeUserID($this->iUserID);
            
        }
        else {
            $this->isLoggedIn = FALSE;
        }
        session_write_close();
    }
    
    //! @return boolean Boolean value stating whether current user is logged in or not
    function isLoggedIn()
    {
        return $this->isLoggedIn;

    }

    /*!
    * @brief Redirects User to login page if not logged in 
    * This function should be called on the top of the pages which require user to be authenticated.
    * If user is not authenticated, it will be redirected to login page. On successful login, it will be
    * redirected to url specificed in argument
    */
    function requireAuth($sURL = FALSE)
    {
        if($sURL !==false && $sURL != "") {
            $sLoginURL = "login.php?redirect=".urlencode($sURL);    
        }
        else {
            $sLoginURL = "login.php";
        }
        
        if(!$this->isLoggedIn()){
            header("Location: {$sLoginURL}");
            exit;
        }
    }

    /*!
    * @brief Does not allow user to view page if he doesn't have required level
    * This function should be called on the top of the pages which require particular level.
    * If user does not have required level, he will be redirected to dashboard with error message.
    * @param $iLevel integer the maximum level integer that can access this page.
    */
    function requireLevel($iLevel){
        if(!$this->isLoggedIn()){
            $sMsg = array();
            $sMsg[] = "E13";
            //! Redirect User with appropriate alert message
		redirectWithAlert("dashboard.php", $sMsg);
            return;
        }
        
        if($this->iType > $iLevel){
            $sMsg = array();
            $sMsg[] = "E12";
            //! Redirect User with appropriate alert message
		redirectWithAlert("dashboard.php", $sMsg);
            return;
        }

    }


    //! Authenticates the users 
    //! @return boolean TRUE if login was successful, FALSE if failed.
    function authenticateUser($sUsername, $sPassword, $bRemember = FALSE)
    {
        session_start();
        //! trim username to remove leading and trailing whitespaces
        $sUsername = trim($sUsername);
        $sPassword = $sPassword;
        if ($sUsername == '' || $sPassword == '')
        {
            $this->sError = INVALID_DATA;
            return FALSE;
        }
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        //! Senitize the username & password to prevent SQL Injections
        $sUsername = $conn->real_escape_string($sUsername);
        $sPassword = $conn->real_escape_string($sPassword);
        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sQuery = "SELECT user_id,password FROM {$sLoginTable} WHERE username='{$sUsername}'";
        $result = $conn->query($sQuery);
        if(!$result){
            $this->sError = DATABASE_ERROR;
            return FALSE;
        }
        $aTemp = $result->fetch_array();
        $sRealPass = $aTemp[1];
       $iUID = $aTemp[0];

        $verified = $this->verifyPassword($sPassword,$sRealPass);

        //! If verified, login. Set required session fields
        if($verified){

            $this->iUserID=$iUID;
            $oUser = new User($this->iUserID);

            $this->isLoggedIn = TRUE;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_isLoggedIn']=TRUE;
            
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sUsername'] = $this->sUsername;
            
            $this->iType = $oUser->iType;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iType'] = $this->iType;
            
            $this->iUserClientID = $oUser->iUserClientID;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iUserClientID'] = $this->iUserClientID;
            
            $this->sName=$oUser->sName;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sName']=$oUser->sName;
            
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sEmail']=$oUser->sEmail;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iUserID'] = $this->iUserID;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
            
            $this->aPermMatrix = PermissionHandler::getPermissionModel($oUser);
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_permMatrix'] = urlencode(serialize($this->aPermMatrix));

            $this->sLoginDate=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime'];
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_lastActivity'] = time();
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $this->saveLoginInstance();


            // echo $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_permMatrix'];exit();
            if($bRemember){
                $this->storeRememberMeCookie($iUID);
            }
            session_write_close();
            return TRUE;
        }
        else {
            $this->sError = WRONG_CREDENTIAL;
            session_write_close();
            return FALSE;
        }
    }

    //! Match @param $sPlainPassword and @sPasswordHash using proprietary algorighm that was used to generate the hash
    function verifyPassword($sPlainPassword, $sPasswordHash)
    {
        //! retrieve the salt
        $salt = substr($sPasswordHash,0,32);
        //! retrive the actual hash.
        $hash = substr($sPasswordHash,32);
        //! generate the actual salt for plainpassword for given salt
        $sSHAHash = hash("sha256", $salt.$sPlainPassword);
        if($sPasswordHash == $salt.$sSHAHash){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    //! logs out the user
    function logOut(){
        session_start();
        //! Unset the Session variables
        $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_isLoggedIn']=FALSE;
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sUsername']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iType']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sName']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sEmail']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_autoLoginSession']);
        unset($_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_trialExpired']);

        //! Destroy cookies
        setcookie("uid", $iUID, time()-3600);
        setcookie("remembermehash", $remembermehash, time()-3600);

        if(strlen($this->iUserID)>0) {
            $auditMsg = "User [user]{$this->iUserID}[/user] was logged out.";
        }

        $iInstanceId= $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iLoginInstanceID'];
        $this->updateLoginInstanceUsingInstanceId($iInstanceId);
        session_write_close();
    }

    //! Function that saves the login instance in database
    function saveLoginInstance(){
        session_start();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_login_instance';
        $sQuery = "INSERT INTO `{$sLoginInstanceTable}` (`instance_id`, `user_id`, `login_date`, `login_time`, `browser`, `ip`, `device_type`) 
            VALUES (NULL, {$this->iUserID}, '{$this->sLoginDate}', '{$this->sLoginTime}', '{$this->sBrowser}', '{$this->sIP}', '{$this->sDeviceType}')";
        
        $conn->query($sQuery);
        $this->iLoginInstanceID = $conn->insert_id;
        $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iLoginInstanceID']= $this->iLoginInstanceID;
        session_write_close();

    }

    /*! @fn function getLoggedInUser()
    * @brief Gives the logged in user
    * This function will return the object of User who is currently logged In.
    * @return object It will return the object of User class
    */
    function getLoggedInUser(){
        return new User($this->iUserID);
    }

    //! @brief Stores the Remember Me Cookie
    private function storeRememberMeCookie($iUID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sQuery = "SELECT password FROM {$sLoginTable} WHERE user_id='{$iUID}'";
        $result = $conn->query($sQuery);
        $aResult = $result->fetch_array();
        
        //! Hash the password hash again!
        $salt = md5(uniqid('', true));
        $sSHAHash = hash("sha256", $salt.$aResult[0]);
        $remembermehash = $salt.$sSHAHash;
        

        //! set cookie for user_id 
        setcookie("uid", $iUID, time()+3600*24*REMEMBER_ME_EXPIRE_TIME);

        //! set cookie for hash
        setcookie("remembermehash", $remembermehash, time()+3600*24*REMEMBER_ME_EXPIRE_TIME);
    }

    //! @brief Auto-logins user if valid Remember Me cookies
    private function autoLogin(){

        

        session_start();

        if(!(isset($_COOKIE['uid']) && isset($_COOKIE['remembermehash']))){
            return FALSE;
        }


        $uid = $_COOKIE['uid'];
        $remembermehash = $_COOKIE['remembermehash'];
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sQuery = "SELECT password FROM {$sLoginTable} WHERE user_id='{$uid}'";
        $result = $conn->query($sQuery);
        $aResult = $result->fetch_array();

        $verified = $this->verifyPassword($aResult[0],$remembermehash);
        if($verified){
            
            $this->iUserID=$uid;
            $oUser = new User($this->iUserID);

            $this->isLoggedIn = TRUE;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_isLoggedIn']=TRUE;
            
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sUsername'] = $this->sUsername;
            $this->iType = $oUser->iType;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iType'] = $oUser->iType;
            $this->sName=$oUser->sName;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sName']=$oUser->sName;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_iUserID'] = $this->iUserID;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
            $this->sLoginDate=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_sLoginTime'];
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_lastActivity'] = time();
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $this->saveLoginInstance();

            $this->autoLoginSession = TRUE;
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_autoLoginSession'] = TRUE;

            $this->aPermMatrix = PermissionHandler::getPermissionModel($oUser);
            $_SESSION['_'.CONFIG_APP_UNIQUE_KEY.'_permMatrix'] = urlencode(serialize($this->aPermMatrix));
            
            //! check if password has expired
            // $sNextPWReset = strtotime($oUser->sNextPWReset);

            // if($sNextPWReset<time()) {
            //     $this->bForcePasswordUpdate = TRUE;
            // }

            session_write_close();
            return TRUE;
        }
        else {

            //! Delete Cookies inorder to avoid redirect loop
            setcookie("uid", $iUID, time()-3600*24);
            setcookie("remembermehash", $remembermehash, time()-3600*24);
            session_write_close();
            return FALSE;
        }
    }

    function isValidPassword($sPlainPassword) {
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sQuery = "SELECT password FROM {$sLoginTable} WHERE user_id='{$this->iUserID}'";
        $result = $conn->query($sQuery);
        $aResult = $result->fetch_array();

        $verified = $this->verifyPassword($sPlainPassword,$aResult[0]);

        if($verified) {
            return true;
        }
        else {
            return false;
        }
    }
    // function to get Last login Date Time
    function getLastLoginDateTimeUserID($iUserID){
        
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_login_instance';
        $sQuery ="SELECT * FROM `{$sLoginInstanceTable}` WHERE `user_id`={$iUserID} ORDER BY `instance_id` DESC LIMIT 2";
        $result = $conn->query($sQuery);
        if($result!==FALSE){
            while($aRow = $result->fetch_array()){
                $aResult = $aRow;
            }
        }
        return $aResult;
    }
    //! @fn function updateLoginInstanceUsingInstanceId()
       /* @brief Update the log out time using instance id 
       * @parameter instance id is passed
       */

   function updateLoginInstanceUsingInstanceId($iInstanceId){

       $DBMan = new DBConnManager();
       $conn =  $DBMan->getConnInstance();
       $logoutTime = date('H:i:s',time());
       $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_login_instance';
       // var_dump($logoutTime);
       // var_dump($iInstanceId);
       // var_dump($sLoginInstanceTable);exit;
       $sQuery = "UPDATE `{$sLoginInstanceTable}` SET `logout_time`='{$logoutTime}' WHERE  `instance_id`='{$iInstanceId}'";
          //var_dump($sQuery);exit;
       $conn->query($sQuery);
     

   }
}

?>