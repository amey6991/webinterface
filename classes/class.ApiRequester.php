<?php

/**
* Class to send api request
* It will generate signature and will send request
*/
class ApiRequester {
	private $sKey = null; // Key to generate signature
	private $sSignature = null; // Signature
	public $sBaseUrl = null;
	public $sEndPoint = null;
	public $aParameters = array();
	public $sAuthParam = 'signature';
	public $sCurlError = null;
	public $sCurlErrorNo = null;
	public $sResponseType = 'json';


	function __construct($sBaseUrl = null, $sKey = null) {
		if(! is_null($sBaseUrl)){
			$this->sBaseUrl = $sBaseUrl;
		}

		if(! is_null($sKey)){
			$this->sKey = $sKey;
		}
	}

	public function setBaseUrl($sBaseUrl){
		$this->sBaseUrl = $sBaseUrl;

		return $this;
	}

	public function setEndPoint($sEndPoint){
		$this->sEndPoint = $sEndPoint;

		return $this;
	}

	public function setKey($sKey){
		$this->sKey = $sKey;

		return $this;
	}

	public function setQueryParameter($sParameter, $sValue){

		if($sParameter == $this->sAuthParam){
			throw new Exception($this->sAuthParam." is not allowed to be set as parameter for security reasons.", 1);
		} else {
			$this->aParameters[$sParameter] = $sValue;
			ksort($this->aParameters);

			return $this;
		}
	}

	public function setQueryParameters($addParameters){
		if( in_array($this->sAuthParam, array_keys($addParameters)) ){
			throw new Exception($this->sAuthParam." is not allowed to be set as parameter for security reasons.", 1);
		} else {
			$this->aParameters = array_merge($this->aParameters, $addParameters);
			ksort($this->aParameters);

			return $this;
		}
	}

	public function post($sEndPoint, $aParam) {
		$this->setQueryParameters($aParam);
		$this->setEndPoint($sEndPoint);
		return $this->send("POST");
	}

	public function get($sEndPoint, $aParam) {
		$this->setQueryParameters($aParam);
		$this->setEndPoint($sEndPoint);
		return $this->send("GET");
	}

	// It will send request
	public function send($sHTTPRequestType="GET"){

		// Get Signature with signature
		$curl = curl_init($this->getQueryString());

		if($sHTTPRequestType=="POST") {
			curl_setopt($curl, CURLOPT_POST, 1);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->aParameters));
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);			


		$result = curl_exec($curl);

		if($result === FALSE){
			$this->sCurlError = curl_error($curl);
			$this->sCurlErrorNo = curl_errno($curl);

			throw new Exception('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
		} else {
			$this->sCurlError = null;
			$this->sCurlErrorNo = null;
			
		}

		return $this->getResponseByType($result, $this->sResponseType);
	}

	// It will return signature and will set it in Object
	public function generateSignature($aParameters)	{
		return hash_hmac('sha256', http_build_query($aParameters), $this->sKey);
	}

	public function getQueryString(){
		//  Sort Parameters
		ksort($this->aParameters);

		//  Get Signature
		$this->sSignature = $this->generateSignature($this->aParameters);

		// Merge signature with Parameters
		$this->aParameters[$this->sAuthParam] = $this->sSignature;

		return $this->sBaseUrl.'/'.$this->sEndPoint.'?'.http_build_query($this->aParameters);
	}


	public function getEndPointUrl(){
		return $this->sBaseUrl.'/'.$this->sEndPoint;
	}


	public function getResponseByType($result, $sResponseType = null){
		if(is_null($sResponseType)){
			$sResponseType = $this->sResponseType;
		}

		$sMethod = "formatResponseAs".ucfirst($sResponseType);

		if(method_exists($this, $sMethod)){
			return $this->$sMethod($result);
		} else {
			return $result;
		}
	}


	public function formatResponseAsJson($result){
		return json_decode($result, true);
	}
}