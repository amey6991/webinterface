<?php

/*!
 * @class Filemanager
 * @brief This class represents a Filemanager and it's behavior. 
 */

class FileManager
{
    public $iMinFileSize=0;
    public $iMaxFileSize=131072;
    public $aFileType = array();
    public $bOverWritten=true;

    // Set minimum filezise
    public function setMinimumFileSize($iMinimumFileSize){
        $this->iMinFileSize = $iMinimumFileSize;
    }
    public function getMinimumFileSize(){
        return $this->iMinFileSize;
    }
    // set maximum filesize
    public function setMaximumFileSize($iMaximumFileZise){
        // $sMaxSize= ini_get('post_max_size');
        // $iMaxSize = substr($sMaxSize,0,1);
        // if($iMaximumFileZise <= $iMaxSize){
            $this->iMaxFileSize = $iMaximumFileZise;
        // }else{
        //     echo "Error while setting the Max size for File.";
        // }
    }
    public function getMaximumFileSize(){
        return $this->iMaxFileSize;
    }
    // extension of files will be accpeted here , please pass file extension
    public function setFileTypeToAllow($aFIleTypes){
        for($iii=0;$iii<count($aFIleTypes);$iii++){
            $this->aFileType[]=$aFIleTypes[$iii];    
        }
    }
    public function getFileTypesAllowed(){
        return $this->aFileType;   
    }

    public function setOverWrittenFile($bOverWrite){
        $this->bOverWritten=$bOverWrite;
    }

    public function uploadFile($sSourcePath, $sFilePath=''){
        
        $aFileID = array();
        $S_TargetPath = DEFAULT_FILE_PATH;
        $aFileDetail = pathinfo($sSourcePath);

        $sFullFileName = $aFileDetail['basename'];
        $sFileExtension = strtolower($aFileDetail['extension']);
        $sFileName = $aFileDetail['filename'];
        $sDirName = $aFileDetail['dirname'];
        $sFileType=$sFileExtension;
        $iFileSize = filesize($sSourcePath);
        $sFileTempName = $sSourcePath;
        $sUploadFileName=rand().date('_d_m_Y_h_i_s');

        $iInsertID=0;
        if($this->iMinFileSize <= $iFileSize && $this->iMaxFileSize >= $iFileSize && $this->allowedFileType($sFileExtension)){
            
            if($sFilePath!=''){
                    $bFilePath = realpath($sFilePath);
                    if($bFilePath){
                        $S_TargetPath = $sFilePath;
                        $sFullFileName = basename($sFullFileName);
                        $sTargetPath = $S_TargetPath.$sUploadFileName;
                        $bFileExist = file_exists($sTargetPath);
                        if($bFileExist){
                            if(!$this->bOverWritten){
                                $sFileName=$sFileName.'_'.date('his');
                                $sFullFileName = $sFileName.'.'.$sFileExtension;
                                $sTargetPath = $S_TargetPath.$sUploadFileName.'.'.$sFileExtension;
                                $bMoved = copy($sFileTempName,$sTargetPath);    
                            }else{
                                $bMoved=false;
                                $iInsertID = -1;
                            }
                        }else{
                            $bMoved = copy($sFileTempName,$sTargetPath);    
                        }  
                        if($bMoved){
                            $DBMan = new DBConnManager();
                            $conn =  $DBMan->getConnInstance();
                            $sFileTable = DATABASE_TABLE_PREFIX.'_files';
                            $dUploadedOn=date(DB_DATETIME_FORMATE);
                            $sIQuery = "INSERT INTO `{$sFileTable}`(`file_id`,`file_name_orignal`,`file_name_upload`,`file_extension`,`file_type`,`file_size`,`file_temp_path`,`file_path`,`processed`,`uploaded_on`,`modified_on`,`deleted`)
                                        VALUES (NULL,'{$sFileName}','{$sUploadFileName}','{$sFileExtension}','{$sFileType}','{$iFileSize}','{$sFileTempName}','{$sTargetPath}','0','{$dUploadedOn}','{$dUploadedOn}','0')";
                            
                            $sResult = $conn->query($sIQuery);        
                            if($sResult){
                                $iInsertID = $conn->insert_id;
                                $aFileID[]=$iInsertID;
                            }
                        }
                        return $iInsertID;
                    }else{
                        $iInsertID= -2;
                        return $iInsertID;
                    }
                }else{

                        $sFullFileName = basename($sFullFileName);
                        $sTargetPath = $S_TargetPath.$sUploadFileName.'.'.$sFileExtension;
                        $bFileExist = file_exists($sTargetPath);
                        if($bFileExist){
                            if(!$this->bOverWritten){
                                $sFileName=$sFileName.date('_h_i_s');
                                $sFullFileName = $sFileName.'.'.$sFileExtension;
                                $sTargetPath = $S_TargetPath.$sUploadFileName.'.'.$sFileExtension;
                                $bMoved = copy($sFileTempName,$sTargetPath);    
                            }else{
                                $bMoved=false;
                                $iInsertID = -1;
                            }
                        }else{
                            $bMoved = copy($sSourcePath,$sTargetPath);
                        }
                        
                        if($bMoved){
                            $DBMan = new DBConnManager();
                            $conn =  $DBMan->getConnInstance();
                            $sFileTable = DATABASE_TABLE_PREFIX.'_files';
                            $dUploadedOn=date(DB_DATETIME_FORMATE);
                            $sIQuery = "INSERT INTO `{$sFileTable}`(`file_id`,`file_name_orignal`,`file_name_upload`,`file_extension`,`file_type`,`file_size`,`file_temp_path`,`file_path`,`processed`,`uploaded_on`,`modified_on`,`deleted`)
                                        VALUES (NULL,'{$sFileName}','{$sUploadFileName}','{$sFileExtension}','{$sFileType}','{$iFileSize}','{$sFileTempName}','{$sTargetPath}','0','{$dUploadedOn}','{$dUploadedOn}','0')";
                            
                            $sResult = $conn->query($sIQuery);        
                            if($sResult){
                                $iInsertID = $conn->insert_id;
                                $aFileID[]=$iInsertID;
                            }
                        }
                    return $iInsertID;
                }
        }else{
            return $iInsertID;
        }
    }

    function allowedFileType($sFileExtension){
        if(in_array($sFileExtension,$this->aFileType)){
            return true;
        }else{
            return false;
        }
    }
    //! function to get the filepath by file ID and also by user ID
    public function getFilePath($iFileID){
        $sFilePath = "";
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sFileTable = DATABASE_TABLE_PREFIX.'_files';
        $sSQuery = "SELECT `file_path` FROM `{$sFileTable}` WHERE `file_id`='{$iFileID}' AND `deleted`=0";      
        
        $sResult = $conn->query($sSQuery);        
        if($sResult){
            $aRow = $sResult->fetch_assoc();
            $sFilePath=$aRow['file_path'];
        }
        return $sFilePath;
    }

    public function deleteFile($iFileID){
        $sFilePath = "";
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $conn1 =  $DBMan->getConnInstance();
        $conn2 =  $DBMan->getConnInstance();
        $sFileTable = DATABASE_TABLE_PREFIX.'_files';
        $sUserFileTable = DATABASE_TABLE_PREFIX.'_user_files';
        $sSQuery = "SELECT `file_path`,`file_name_upload`,`file_extension` FROM `{$sFileTable}` WHERE `file_id`='{$iFileID}' AND `deleted`=0";      
        
        $sResult = $conn->query($sSQuery);        
        if($sResult){
            $aRow = $sResult->fetch_assoc();
            $sFilePath=$aRow['file_path'];
            $sFilename=$aRow['file_name_upload'].'.'.$aRow['file_extension'];
            $sPath=str_replace($sFilename,'', $sFilePath);
            $sPath=$sPath.'Recycle/'.$sFilename;
            copy($sFilePath, $sPath);
            
            $bDelete = unlink($sFilePath);
            if($bDelete){
                $sDQuery1 = "UPDATE `{$sFileTable}` SET `deleted`='1' WHERE `file_id`='{$iFileID}' ";
                $sDQuery2= "UPDATE `{$sUserFileTable}` SET `status`='1' WHERE `file_id`='{$iFileID}' ";
                $sResult1 = $conn1->query($sDQuery1);
                $sResult2 = $conn2->query($sDQuery2);
                if($sResult1 AND $sResult2){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

    public function renameFile($iFileID,$sNewFileName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $conn1 =  $DBMan->getConnInstance();
        $sFileTable = DATABASE_TABLE_PREFIX.'_files';

        $sSQuery = "SELECT * FROM `{$sFileTable}` WHERE `file_id`='{$iFileID}' AND `deleted`=0";      
        
        $sResult = $conn->query($sSQuery);        
        if($sResult){
            $aRow = $sResult->fetch_assoc();
            $sFilePath=$aRow['file_path'];
            $sFileName=$aRow['file_name'];
            $sFileExtens=$aRow['file_extension'];
            $sSearch =$sFileName.'.'.$sFileExtens;
            $sNewFile= $sNewFileName.'.'.$sFileExtens;
            
            $sNewFilePath = str_replace($sSearch, $sNewFile, $sFilePath);
            $bRename = rename($sFilePath,$sNewFilePath);
            
            if($bRename){
                $sUQuery = "UPDATE `{$sFileTable}` SET `file_name`='{$sNewFileName}',`file_path`='{$sNewFilePath}' WHERE `file_id`='{$iFileID}' AND `deleted`=0";
                $sResult = $conn1->query($sUQuery);
                if($sResult){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
}
?>