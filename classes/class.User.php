<?php

include_once __DIR__.DIRECTORY_SEPARATOR."class.DBConnManager.php";
include_once __DIR__.DIRECTORY_SEPARATOR."class.SessionManager.php";

/*!
 * @class User
 * @brief This class represents a user and it's behavior. 
 * @author Kishan Gor
 */

class User
{
    //! user_id of User
    public $iID; 
    //! Name of the User
    public $sFirstName;
    public $sLastName;
    public $sFullName; 
    //! Email of the User
    public $sEmail; 
    //! users contact info
    public $sState;
    public $sCity;
    public $sAddress;
    public $sMobile;

    public $iStaffID; 
    public $sUsername;
    public $iLoginID;
    public $iType;
    public $iUserClientID;
    public $sUserType;
    public $dAddedOn;
    private $sPassword;
    public $sLastError;

    /*! @brief initialize the User class
    *  This class will instantiate the User class according to passed \a $iID representing the User ID.
    * @param $iID User ID
    * @note If no @a $iID is passed, it will not load the values and work as an skeleton for new User.
    *
    */
    function __construct($iID = NULL)
    {
        if($iID !== NULL){
            $this->iID = $iID;
            $DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sUserTable = DATABASE_TABLE_PREFIX.'_users';
            $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
            $sUserTypeTable = DATABASE_TABLE_PREFIX.'_user_type';

            $query = "SELECT * FROM `{$sUserTable}` a, `{$sLoginTable}` b, `{$sUserTypeTable}` c WHERE a.user_id={$this->iID} AND b.user_id=a.user_id AND a.user_type_id=c.user_type_id";
            $result = $conn->query($query);
            if($result!==FALSE){
                $row = $result->fetch_array();
            }
            $this->sFirstName = $row['first_name'];
            $this->sLastName = $row['last_name'];
            $this->sFullName = $row['full_name'];
            $this->sState = $row['state'];
            $this->sCity = $row['city'];
            $this->sAddress= $row['address'];
            $this->sMobile= $row['mobile_no'];

            $this->sEmail = $row['email'];
            $this->sUsername = $row['username'];
            $this->iLoginID = $row['login_id'];
            $this->iType = $row['user_type_id'];
            $this->iUserClientID = $row['client_id'];    
            
            $this->sUserType = $row['user_type'];
            $this->dAddedOn = $row['added_on'];
            $this->sPassword = $row['password'];
            $this->sLastError = NULL;

            //! Destruct the DBConnManager Instance;
            $DBMan = NULL;
        }
    }

    /*! @brief adds the user as new user
    *  Calling this function will create a new user. Once we have set the user details, we can call this function and add its entry in the database.
    * @param $passoword Password for the new user.
    * @return boolean It will return true if user is added successfully. On failure, it will return error.
    * @warning If user id is set, it will not do anything, but return the false. 
    */
    //! function to add users
    function fAddUsers($sFirstName,$sLastName,$sFullName,$iUserType,$iClientID,$sStateName,$sCityName,$sAddress,$iMobileNumber,$sEmail,$sUserName,$sPassword){
        $DBMan = new DBConnManager();
        
        $sUserTable = DATABASE_TABLE_PREFIX.'_users';
        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $oSession = new SessionManager();
        $iUserID = $oSession->iUserID;

        $sQuery = "INSERT INTO `{$sUserTable}` (`user_id`,`first_name`,`last_name`,`full_name`,`user_type_id`,`client_id`,`state`,`city`,`address`,`mobile_no`,`email`,`added_on`,`added_by`) 
            VALUES (NULL,'{$sFirstName}', '{$sLastName}', '{$sFullName}','{$iUserType}','{$iClientID}', '{$sStateName}','{$sCityName}','{$sAddress}','{$iMobileNumber}','{$sEmail}','{$dAddedOn}','{$iUserID}');";
        
        $conn1 =  $DBMan->getConnInstance();
        $sResult = $conn1->query($sQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $iInsertID = $conn1->insert_id;
            
        if($iInsertID>0){
            $sPassword = $this->fGetPasswordHash($sPassword);
            $conn2 =  $DBMan->getConnInstance(); 
            $sIQuery = "INSERT INTO `{$sLoginTable}` (`login_id`, `username`, `password`, `user_id`)
                        VALUES (NULL, '{$sUserName}', '{$sPassword}', '{$iInsertID}');";
            $sIResult = $conn2->query($sIQuery);
            if(!$sIResult){
                $this->sLastError = DATABASE_ERROR;
                return FALSE;
            }
            $iLoginID = $conn2->insert_id;
            if($iLoginID>0){
                return $iInsertID;
            }
        }else{
            return $iInsertID;
        }
        
    }
    //! function to update the users details
    function fUpdateUsers($iuserID,$sFirstName,$sLastName,$sFullName,$iUserType,$iClientID,$sStateName,$sCityName,$sAddress,$iMobileNumber,$sEmail,$sUserName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sUserTable = DATABASE_TABLE_PREFIX.'_users';
        $sUQuery ="UPDATE `{$sUserTable}` SET `first_name`='{$sFirstName}',`last_name`='{$sLastName}',`full_name`='{$sFullName}',`user_type_id`='{$iUserType}',`client_id`='{$iClientID}',`state`='{$sStateName}',`city`='{$sCityName}',`address`='{$sAddress}',`mobile_no`='{$iMobileNumber}',`email`='{$sEmail}' WHERE `user_id`='{$iuserID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }        
    }

    /*! #brief generates the password hash
    * @arg $plainPassword 
    * @return string returns the hash of @a $plainPassword with 92 characters
    */
    private function fGetPasswordHash($plainPassword){
        $salt = md5(uniqid('', true));
        $sSHAHash = hash("sha256", $salt.$plainPassword);
        $sEncryptedPassword = $salt.$sSHAHash;
        return $sEncryptedPassword;
    }

    //! brief function to get all userd list
    function fGetAllUserList(){

        $aUserList = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sProfileTable = DATABASE_TABLE_PREFIX.'_user_profiles';
        
        $sQuery = "SELECT `user_id`, `name` FROM `{$sProfileTable}`";
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aUserList[] = $aRow;
            }
        }

        return $aUserList;
    }

    //! brief function to get all users for user type
    function fGetUsersForUserType($iUserTypeID){

        $aUserList = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sProfileTable = DATABASE_TABLE_PREFIX.'_user_profiles';
        
        $sQuery = "SELECT `user_id`, `name` FROM `{$sProfileTable}` WHERE `user_type_id` = '{$iUserTypeID}'";
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aUserList[] = $aRow;
            }
        }

        return $aUserList;
    }

    //! function to add clients
    function fAddClient($sClientName,$sClientEmail,$sClientMobile,$sClientCountry,$sClientState,$sClientCity,$sClientAddress,$sClientUrl,$sPersonName,$sPersonEmail,$sPersonContact){
        $DBMan = new DBConnManager();
        
        $sClientTable = DATABASE_TABLE_PREFIX.'_client';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $oSession = new SessionManager();
        $iUserID = $oSession->iUserID;

        $sQuery = "INSERT INTO `{$sClientTable}` (`client_id`,`client_name`,`client_email`,`client_mobile`,`client_country`,`client_state`,`client_city`,`client_url`,`client_address`,`person_name`,`person_email`,`person_mobile`,`client_added_by`,`client_added_on`) 
            VALUES (NULL,'{$sClientName}', '{$sClientEmail}', '{$sClientMobile}', '{$sClientCountry}', '{$sClientState}','{$sClientCity}','{$sClientUrl}','{$sClientAddress}','{$sPersonName}','{$sPersonEmail}','{$sPersonContact}','{$iUserID}','{$dAddedOn}');";
        
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $iInsertID = $conn->insert_id;
            
        if($iInsertID>0){
            return $iInsertID;
        }else{
            return $iInsertID;
        }
    }
    //! function to update client detail
    function fUpdateClient($iClientID,$sClientName,$sClientEmail,$sClientMobile,$sClientCountry,$sClientState,$sClientCity,$sClientAddress,$sClientUrl,$sPersonName,$sPersonEmail,$sPersonContact){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sClientTable = DATABASE_TABLE_PREFIX.'_client';
        $sUQuery ="UPDATE `{$sClientTable}` SET `client_name`='{$sClientName}',`client_email`='{$sClientEmail}',`client_mobile`='{$sClientMobile}',`client_country`='{$sClientCountry}',`client_state`='{$sClientState}',`client_city`='{$sClientCity}',`client_url`='{$sClientUrl}',`client_address`='{$sClientAddress}',`person_name`='{$sPersonName}',`person_email`='{$sPersonEmail}',`person_mobile`='{$sPersonContact}' WHERE `client_id`='{$iClientID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
}

?>