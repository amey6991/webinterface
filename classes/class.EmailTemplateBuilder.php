<?php

//! @brief - Class EmailTemplate
//! It is used to create sigle and two column email template.
//! It also include the emial header and footer functionality.

class EmailTemplateBuilder{

	public $sEmailTemplate;
	public $sEmailContent;
	private $sHeader;
	private $sFooter;
	private $sLogoURL;
	private $sORGName;
	private $sPlus91Logo;
	private $sMedixcelLogo;
	private $sTwitterLogo;
	private $sFacebookLogo;
	private $dCurrentYear;
	private $sCopy;

	function __construct(){

		$this->sLogoURL = ORG_LOGO_URL;
		$this->sORGName = ORG_NAME;
		$this->sPlus91Logo = PLUS91_LOGO_URL;
		$this->sTwitterLogo = SITE_URL."images/twitter.png";
		$this->sFacebookLogo = SITE_URL."images/facebook.png";
		$this->dCurrentYear = date("Y");
		$this->sCopy = "&copy;";

		$this->sEmailTemplate = $this->fHeader();
	}

	public function fClose(){

		$this->sEmailTemplate .= $this->fFooter();
	}

	private function fHeader(){

		$sHeader = <<<EOB
		<div style="padding: 10px;background-color:#f2f2f2;color:#333;font-family:'Helvetica Neue';">
			<div style='border-radius:10px 0px 0px 0px;background-color:#FFF;border:1px solid #FFF;'>
				<div style='padding: 20px 50px 0px 50px;'>
					<img src='{$this->sMedixcelLogo}' style='height:50px;display:none;' />
					<img src='{$this->sLogoURL}' style='float:right;max-height:50px !important;' />
				</div>
			</div>
EOB;


		return $sHeader;
	}

	private function fFooter(){

		$sFooter = <<<EOB
			<div style='text-align:center;font-size:14px;padding-top:15px;display:none;'>
				<a href='https://twitter.com/plus91' target='_blank'><img src='{$this->sTwitterLogo}' alt='twitter' width='30' style='cursor:pointer;padding-right:5px;' /></a>
				<a href='https://www.facebook.com/plus91.in' target='_blank'><img src='{$this->sFacebookLogo}' alt='facebook' width='30' style='cursor:pointer;' /></a>
				<br />
				<a style='text-decoration:none;line-height:2;color:#97a3b1;' target='_blank' href='http://www.plus91.in'>{$this->sCopy} 2006- {$this->dCurrentYear} Plus91 Technologies PVT. LTD. </a> 
				<br />
				<span style='padding-left:0px;color:#97a3b1;'>Powered By WebInterface</span>
			</div>
		</div>
EOB;


		return $sFooter;
	}

	public function fCreateOneColumnTemplate($sContent){

		$this->sEmailTemplate .= <<<EOB
		<div style='border-radius:10px;background-color:#FFF;border:1px solid #FFF;margin: -60px 0px 0px 0px;font-size:14px;'>
			<div style='padding: 20px 50px 50px 50px;'>
				{$sContent}	
			</div>
		</div>
EOB;

		return $sEmailTemplate;
	}

	public function fCreateTwoColumnTemplate($sContentOne,$sContentTwo){

		$this->sEmailTemplate .= <<<EOB
		<div style='border-radius:10px;background-color:#FFF;border:1px solid #FFF;margin: -60px 0px 0px 0px;font-size:14px;'>
			<div style='padding: 20px 50px 50px 50px;'>
				<div style='width:48%;float:left;'>
					{$sContentOne}	
				</div>

				<div style='width=48%;float:right;'>
					{$sContentTwo}	
				</div>
			</div>
		</div>
EOB;


		return $sEmailTemplate;
	}
}
?>