<?php
	class EmailList
	{
		public $iListID=0;
		public $sListName='';
		public $aSubscribers = array();
		public $iCountSubsriber=0;

		function __construct($iID=0){
			if($iID>0){
				$this->iListID=$iID;
			}
		}

		public function addEmailListName($sEmailListName){
				$DBMan = new DBConnManager();
                $conn =  $DBMan->getConnInstance();
                $sEmailListTable= 'email_list';
                $dtAddedOn = date(DB_DATETIME_FORMATE);
                $sIQuery = "INSERT INTO `{$sEmailListTable}`(`list_id`,`list_name`,`added_on`,`status`)
                            VALUES (NULL,'{$sEmailListName}','{$dtAddedOn}','1')";
                
                $sResult = $conn->query($sIQuery);        
                if($sResult){
                    $this->iListID = $conn->insert_id;
                    $this->sListName=$sEmailListName;
                }
                return $this->iListID;
		}
		public function getAllEmailList(){
			$aList = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sEmailListTable= 'email_list';
            
            $sQuery = "SELECT * FROM `{$sEmailListTable}` WHERE `status`=1";
            
         	$sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow=$sQueryR->fetch_assoc()){
	            	$aList[]=$aRow;
	            }
	        }
            return $aList;
		}

		public function addEmailListSubscriber($iLisID , $sSubName , $sSubEmail){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sEmailListTable= 'email_subscriber';
            $dtAddedOn = date(DB_DATETIME_FORMATE);
            $sIQuery = "INSERT INTO `{$sEmailListTable}`(`subs_id`,`list_id`,`subs_name`,`subs_email`,`status`)
                        VALUES (NULL,'{$iLisID}','{$sSubName}','{$sSubEmail}','1')";
            
            $sResult = $conn->query($sIQuery);        
            if($sResult){
                $this->iListID = $conn->insert_id;
                $this->sListName=$sEmailListName;
            }
            return $this->iListID;
		}

		public function getEmailListSubscriber($iLisID){
			$aSubsList = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sEmailListTable= 'email_subscriber';
            $sQuery = "SELECT * FROM `{$sEmailListTable}` WHERE `list_id`='{$iLisID}' AND `status`=1";
            
            $sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow=$sQueryR->fetch_assoc()){
	            	$aSubsList[]=$aRow;
	            }
	        }
            return $aSubsList;
            
		}

		public function getEmailListDetail($iEmailListID){
			$aList = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sEmailListTable= 'email_list';
            
            $sQuery = "SELECT * FROM `{$sEmailListTable}` WHERE `status`=1 AND `list_id`='{$iEmailListID}'";
            
         	$sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow=$sQueryR->fetch_assoc()){
	            	$aList=$aRow;
	            }
	        }
            return $aList;
		}

		public function updateFeatureEmail($iListID,$iFlag){
			$DBMan = new DBConnManager();
            $conn2 =  $DBMan->getConnInstance();
            $sEmailListTable= 'email_list';
            
            $sUQuery = "UPDATE `{$sEmailListTable}` SET `is_active`='{$iFlag}' WHERE `list_id`='{$iListID}' ";
            $sResult = $conn2->query($sUQuery);        
            if($sResult){
                return true;
            }else{
            	return false;
            } 
		}
	}
?>