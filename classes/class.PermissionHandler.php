<?php
/*!
 * @class User
 * @brief This class does Permission Handling for your web application. 
 * @author Kishan Gor <me@kishan.co>
 */

class PermissionHandler {

	static public function getPagePermissionLevel($sessionManger, $iPageID) {
		
		if(isset($sessionManger->aPermMatrix["0_{$iPageID}"])) {
			return $sessionManger->aPermMatrix["0_{$iPageID}"];
		}
		else {
			return 0;
		}
	}

	static public function getFeaturePermissionLevel($sessionManger, $iFeatureID) {
		if(isset($sessionManger->aPermMatrix["1_{$iFeatureID}"])) {
			return $sessionManger->aPermMatrix["1_{$iFeatureID}"];
		}
		else {
			return 0;
		}
	}

	static public function canPerformAction($sessionManger, $iActionID) {
		if(isset($sessionManger->aPermMatrix["2_{$iActionID}"])) {
			if($sessionManger->aPermMatrix["2_{$iActionID}"]==1) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	static public function getPermissionModel($oUser) {

		$aPermMatrix = array();

		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
        $sAuthPermMatrix = 'auth_perm_matrix';
        
        //! Fetch permissions for the user type
        $sQuery = "SELECT * FROM {$sAuthPermMatrix} WHERE user_entity_type=1 AND deleted=0 AND (user_entity_id={$oUser->iType} OR user_entity_id=0) ORDER BY user_entity_id ASC";

        $rResult = $conn->query($sQuery);

        if($rResult) {
        	while($row= $rResult->fetch_array()) {

        		$aPermMatrix[$row['perm_entity_type'].'_'.$row['perm_entity_id']] = $row['perm_level'];

        	}
        }
        else {
        	$oLogger = Logger::getInstance();

        	$oLogger->error("Failed to fetch permission matrix.", array("Query"=>$sQuery, "Error"=>$conn->error));
        }

        //! Fetch permissions for the user
        $sQuery = "SELECT * FROM {$sAuthPermMatrix} WHERE user_entity_type=0 AND user_entity_id={$oUser->iID} AND deleted=0";

        $rResult = $conn->query($sQuery);

        if($rResult) {
        	while($row= $rResult->fetch_array()) {
        		$aPermMatrix[$row['perm_entity_type'].'_'.$row['perm_entity_id']] = $row['perm_level'];
        	}
        }
        else {
        	$oLogger = Logger::getInstance();

        	$oLogger->error("Failed to fetch permission matrix.", array("Query"=>$sQuery, "Error"=>$conn->error));
        }

        return $aPermMatrix;
	}
}

?>