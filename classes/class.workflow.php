<?php
	/*
		class workflow to handle all the stped under the workflow & process for complete the task.
	*/
	class WorkFlow
	{
		public $iFlowID=0;
		public $sFlowName;
		public $aSteps = array();
		public $aProcess = array();

		function __construct($iWorkFlowID=0){
			$aWorkFlow = array();
			$aWorkflowSteps=array();
			if($iWorkFlowID>0){
				$DBMan = new DBConnManager();
	            $conn =  $DBMan->getConnInstance();
	            $sWoorkflowTable= DATABASE_TABLE_PREFIX.'_workflow';
	            $sStepsTable= DATABASE_TABLE_PREFIX.'_steps';
	            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';

	           	$sQuery = "SELECT * FROM {$sWoorkflowTable} WHERE `wf_id`='{$iWorkFlowID}'";
	            $sQueryR = $conn->query($sQuery);
		        if($sQueryR!==FALSE){
		            $aRow=$sQueryR->fetch_assoc();
		            if(!empty($aRow)){
		            	$this->sFlowName= $aRow['workflow_name'];
		            	$this->iFlowID=$iWorkFlowID;
		            	$this->aSteps = $this->getWorkflowSteps($iWorkFlowID);
		            	if(!empty($this->aSteps)){
		            		foreach ($this->aSteps as $key => $aValue) {
		            			$aWorkflowSteps[] = $aValue['step_id'];
							}

							$this->aProcess = $this->getWorkflowProcess($aWorkflowSteps);

						}
		            	
		            }
		        }
			}
	    }

	    public function setWorkflowID($iWorkID){
	    	$this->iFlowID=$iWorkID;
	    }
	    public function getStepsWorkflow(){
	    	if($this->iFlowID>0){
	    		$iWorkFlowID=$this->iFlowID;
	    		$this->aSteps = $this->getWorkflowSteps($iWorkFlowID);	
	    	}
	    	return $this->aSteps;
		}
		public function getProcessWorkflow(){
			if(!empty($this->aSteps)){
				foreach ($this->aSteps as $key => $aValue) {
	            	$aWorkflowSteps[] = $aValue['step_id'];
				}
				$this->aProcess = $this->getWorkflowProcess($aWorkflowSteps);
			}
			return $this->aProcess;
		}

		public function setWorkflow($aWorkFlow,$aFlowSteps=array()){
			if(!empty($aWorkFlow)){
				$DBMan = new DBConnManager();
                $conn =  $DBMan->getConnInstance();
                $sWoorkflowTable= DATABASE_TABLE_PREFIX.'_workflow';
                $sWorkflowName = $aWorkFlow['flowname'];
                $dtAddedOn = date(DB_DATETIME_FORMATE);
                $sIQuery = "INSERT INTO `{$sWoorkflowTable}`(`wf_id`,`workflow_name`,`added_on`,`status`)
                            VALUES (NULL,'{$sWorkflowName}','{$dtAddedOn}','1')";
                            
                $sResult = $conn->query($sIQuery);        
                if($sResult){
                    $this->iFlowID = $conn->insert_id;
                    $this->sFlowName=$sWorkflowName;
                }
			}
			return $this->iFlowID;
			
			if(!empty($aFlowSteps)){
				$this->setSteps($aFlowSteps);
			}
		}
		// array('0'=>array('wf_id','step_name'))
		public function setSteps($aFlowSteps){
			$aStepID = array();
			foreach ($aFlowSteps as $key => $aValue) {
				$iFlowID = $aValue[0];
				$sStepname = $aValue[1];
				$iStepID = $this->addStep($iFlowID,$sStepname);	
				if($iStepID>0){
					$aStepID[$iFlowID][]=$iStepID;
				}
			}
			return $aStepID;
		}

		public function addStepStatus($iFlowID,$iStepID,$iProcessID){
			$DBMan = new DBConnManager();
            $conn2 =  $DBMan->getConnInstance();
            $sStepsTrackTable= DATABASE_TABLE_PREFIX.'_track_work';
            $dtAddedOn = date(DB_DATETIME_FORMATE);
           	$sIQuery = "INSERT INTO `{$sStepsTrackTable}`(`track_id`,`work_id`,`step_id`,`process_id`,`added_on`,`is_complete`,`status`)
                            VALUES (NULL,'{$iFlowID}','{$iStepID}','{$iProcessID}','{$dtAddedOn}','0','1')";
            $sResult = $conn2->query($sIQuery);        
            if($sResult){
                return $conn2->insert_id;
            }else{
            	return 0;
            }               
		}

		public function updateStepProcessStatus($iFlowID,$iStepID,$iProcessID,$iStatus){
			$DBMan = new DBConnManager();
            $conn2 =  $DBMan->getConnInstance();
            $sStepsTrackTable= DATABASE_TABLE_PREFIX.'_track_work';
            
            $sUQuery = "UPDATE `{$sStepsTrackTable}` SET `is_complete`='{$iStatus}' WHERE `work_id`='{$iFlowID}' AND `step_id`='{$iStepID}' AND `process_id`='{$iProcessID}' ";
            $sResult = $conn2->query($sUQuery);        
            if($sResult){
                return true;
            }else{
            	return false;
            }               
		}

		public function addStep($iFlowID,$sStepname){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepsTable= DATABASE_TABLE_PREFIX.'_steps';
            $oSession = new SessionManager();
        	$iAddedBy = 1;//$oSession->iUserID;
            $dtAddedOn = date(DB_DATETIME_FORMATE);
            
          	$sIQuery = "INSERT INTO `{$sStepsTable}`(`step_id`,`wf_id`,`step_name`,`added_on`,`added_by`,`status`)
                            VALUES (NULL,'{$iFlowID}','{$sStepname}','{$dtAddedOn}','{$iAddedBy}','1')";
                  
            $sResult = $conn->query($sIQuery);        
            if($sResult){
                return $conn->insert_id;
            }else{
            	return 0;
            }
		}

		public function getJobID($iStepID){
			$iWorkFlowID=0;
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepsTable= DATABASE_TABLE_PREFIX.'_steps';
            $sQuery = "SELECT `wf_id` FROM `{$sStepsTable}` WHERE `step_id`='{$iStepID}'";
            $sQueryR = $conn->query($sQuery);
            if($sQueryR != false){
            	$aRow=$sQueryR->fetch_assoc();
            	if(!empty($aRow)){
            		$iWorkFlowID = $aRow['wf_id'];
            	}
            	return $iWorkFlowID;
            }
            
		}
		// set the process by values
		public function setStepProcess($aStepProcess){
			$aProcessID = array();
			foreach ($aStepProcess as $key => $aValue) {
				$iStepID = $aValue[0];
				$sProcessName = $aValue[1];
				$sProcessFunction = $aValue[2];
				$iProcessID = $this->addProcess($iStepID,$sProcessName,$sProcessFunction);
				if($iProcessID>0){
					$iFlowID = $this->getJobID($iStepID);
					$this->addStepStatus($iFlowID,$iStepID,$iProcessID);
					$aProcessID[$iStepID][]=$iProcessID;
				}
			}
			return $aProcessID;
		}
		public function addProcess($iStepID,$sProcessName,$sProcessFunction){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';
            $oSession = new SessionManager();
        	$iAddedBy = 1;//$oSession->iUserID;
            $dtAddedOn = date(DB_DATETIME_FORMATE);
            
         	$sIQuery = "INSERT INTO `{$sProcessTable}`(`process_id`,`step_id`,`process_name`,`process_function`,`added_on`,`added_by`,`status`)
                      VALUES (NULL,'{$iStepID}','{$sProcessName}','{$sProcessFunction}','{$dtAddedOn}','{$iAddedBy}','1')";
                     
            $sResult = $conn->query($sIQuery);        
            if($sResult){
                return $conn->insert_id;
            }else{
            	return 0;
            }
		}

		public function getStepProcess($iStepID){
			$aProcess = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';
            $sQuery = "SELECT * FROM {$sProcessTable} WHERE `step_id`='{$iStepID}'";
            $sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow = $sQueryR->fetch_assoc()){
	                $aProcess[] = $aRow;
	            }
	        }
	        return $aProcess;
		}

		private function getWorkflowSteps($iWorkflow){
			$aSteps = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepsTable= DATABASE_TABLE_PREFIX.'_steps';
            $sQuery = "SELECT * FROM {$sStepsTable} WHERE `wf_id`='{$iWorkflow}'";
            $sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow = $sQueryR->fetch_assoc()){
	                $aSteps[] = $aRow;
	            }
	        }
	        return $aSteps;
		}

		private function getWorkflowProcess($aWorkflowSteps){
			$aWorkflowProcess=array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';
            foreach ($aWorkflowSteps as $key => $iValue) {
            	$iStepID = $iValue;
            	$sQuery = "SELECT * FROM {$sProcessTable} WHERE `step_id`='{$iStepID}'";
            	$sQueryR = $conn->query($sQuery);
            	if($sQueryR!==FALSE){
		            while($aRow = $sQueryR->fetch_assoc()){
		                $aWorkflowProcess[$iStepID][] = $aRow;
			        }
			    }
            }
            return $aWorkflowProcess;
		}

		private function getWorkflowProcessStepID($iStep){
			$aProcessData=array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';
            
        	$sQuery = "SELECT * FROM {$sProcessTable} WHERE `step_id`='{$iStep}'";
        	$sQueryR = $conn->query($sQuery);
        	if($sQueryR!==FALSE){
        		while ($aRow = $sQueryR->fetch_assoc()){
	            	$aProcessData[] =$aRow;
        		}
	        }
            
            return $aProcessData;
		}

		public function getWorkFlowStepAndProcess($iWorkFlow){
			$aWorkFlow = array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sWoorkflowTable= DATABASE_TABLE_PREFIX.'_workflow';
            $sStepsTable= DATABASE_TABLE_PREFIX.'_steps';
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process';

            $sQuery = "	SELECT a.wf_id,a.workflow_name,a.added_on,b.step_id,b.step_name,b.added_on,b.added_by,c.process_id,c.process_name,c.status 
            			FROM {$sWoorkflowTable} as a 
            			LEFT JOIN {$sStepsTable} as b ON b.wf_id=a.wf_id AND b.status=1
            			LEFT JOIN {$sProcessTable} as c ON c.step_id=b.step_id AND c.status=1
            			WHERE a.wf_id='{$iWorkFlow}' AND a.status=1";
            $sQueryR = $conn->query($sQuery);
	        if($sQueryR!==FALSE){
	            while($aRow = $sQueryR->fetch_assoc()){
	                $aWorkFlow[] = $aRow;
	            }
	        }
	       	return $aWorkFlow;
		}

		public function getStatusProcessStep($iWorkID,$iStepID,$iProcessID){
			$iStatus=0;
			$DBMan = new DBConnManager();
            $conn2 =  $DBMan->getConnInstance();
            $sStepsTrackTable= DATABASE_TABLE_PREFIX.'_track_work';
            
            $sSQuery = "SELECT * FROM `{$sStepsTrackTable}` WHERE `work_id`='{$iWorkID}' AND `step_id`='{$iStepID}' AND `process_id`='{$iProcessID}' ";
            $sResult = $conn2->query($sSQuery);        
          	if($sResult!==FALSE){
	            $aRow = $sResult->fetch_assoc();
	            $iStatus = $aRow['is_complete'];
	        }
	        return $iStatus;
		}

		// Process Execution start from here

		public function getStartProcessing($iWorkID , $aShareDataa=array()){
			if(!empty($aShareDataa)){
				$sSharedData=json_encode($aShareDataa);
			}else{
				$sSharedData="";
			}
			$iJobID=0;
			$DBMan = new DBConnManager();
            $conn2 =  $DBMan->getConnInstance();
            $sWorkJobTable= 'workflow_job';
            $dtStartedOn = date(DB_DATETIME_FORMATE);
            $sIQuery = "INSERT INTO `{$sWorkJobTable}`(`job_id`,`workflow_id`,`executed_on`,`input_data`,`completed`)
                  VALUES (NULL,'{$iWorkID}','{$dtStartedOn}','{$sSharedData}','0')";
                 
            $sResult1 = $conn2->query($sIQuery);        
            if($sResult1){
            	$iJobID=$conn2->insert_id;
            }else{
            	$iJobID= 0;
            }
			
			if($iJobID > 0){
				$aProcessData = array();
				$sSharedInputData ="";
				
				//fetch all steps and process
				$aSteps	 = $this->getWorkflowSteps($iWorkID);
				foreach ($aSteps as $key => $aValues) {
					$iStepid= $aValues['step_id'];
					echo "Execution start for STEP : ".$iStepid."<br/>";
					// log for entry 

					$this->addLogJobStep($iJobID,$iStepid,$sSharedData);
					$aProcessData = $this->getWorkflowProcessStepID($iStepid);
					//$sInputData=json_encode(array('1','amey'));
					$this->executeProcessing($iJobID,$aProcessData,$sSharedData);
					
					$bResult = $this->getStatusJobStep($iJobID,$iStepid);

					if($bResult){
						echo "Step ".$iStepid." is completed here.<br/><br/>";
						$aOutputData = $this->getOutputStepProcess($iJobID,$iStepid);
						$sSharedData = json_encode($aOutputData);
						$this->updateLogJobStep($iJobID,$iStepid,$sSharedData);
						continue;
					}else{
						$sString= "<b>Further Steps will not execute.</b>";
						return $sString;
					}
				}
			}
		}
		public function getOutputStepProcess($iJobID,$iStepid){
			$aProcessid=array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sJobProcessTable= 'workflow_process';
            
            $sSQuery = "SELECT `process_id` FROM `{$sJobProcessTable}` WHERE `job_id`='{$iJobID}' ";
            $sResult = $conn->query($sSQuery);        
          	if($sResult!==FALSE){
	            while($aRow = $sResult->fetch_assoc()){
	            	$aProcessid[] =$aRow['process_id'];
	            }
	        }

	        if(!empty($aProcessid)){
	        	$aOutputData=array();
	        	foreach ($aProcessid as $key => $value) {
	        		$DBMan = new DBConnManager();
		            $conn =  $DBMan->getConnInstance();
		            $sJobProcessTable= 'workflow_process';
		            
		           	$sSQuery1 = "SELECT `output_data` FROM `{$sJobProcessTable}` WHERE `job_id`='{$iJobID}' AND `process_id`='{$value}' ";
		            $sResult1 = $conn->query($sSQuery1);
		            if($sResult1!==FALSE){
			            while($aRow = $sResult1->fetch_assoc()){
			            	$sOutput= $aRow['output_data'];
							$aOutputData[] =$sOutput;
			            }
			        }
			    }
			    return $aOutputData;
	        }
		}

		public function getStatusJobStep($iJobID,$iStepid){
			$aProcessid=array();
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
             $sJobProcessTable= 'mxcel_process';
            
            $sSQuery = "SELECT `process_id` FROM `{$sJobProcessTable}` WHERE `step_id`='{$iStepid}' ";
            $sResult = $conn->query($sSQuery);        
          	if($sResult!==FALSE){
	            while($aRow = $sResult->fetch_assoc()){
	            	$aProcessid[] =$aRow['process_id'];
	            }
	        }
	        if(!empty($aProcessid)){
	        	foreach ($aProcessid as $key => $value) {
	        		$bResult = $this->getCompletedStatusProcessJob($iJobID,$value);
	        		if(!$bResult){
	        			$this->getStatusJobStep($iJobID,$iStepid);
	        		}
	        	}
	        	return true;
	        }else{

	        }

		}
		public function getCompletedStatusProcessJob($iJobID,$iProcessID){
			$iResult=0;
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sJobProcessTable= 'workflow_process';
            
            $sSQuery = "SELECT `completed` FROM `{$sJobProcessTable}` WHERE `job_id`='{$iJobID}' AND `process_id`='{$iProcessID}' ";
            $sResult = $conn->query($sSQuery);        
          	if($sResult!==FALSE){
	            $aRow = $sResult->fetch_assoc();
	            $iResult = $aRow['completed'];
	        }
	        if($iResult > 0){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function executeProcessing($iJobID,$aProcessData,$sInputData){
			$sProcessInput="";
			$iCount = count($aProcessData);
			foreach ($aProcessData as $key => $aValue) {
				$iProcessID =  $aValue['process_id'];

				$sProcessName =  $aValue['process_name'];
				$sProcessFunction =  $aValue['process_function'];
				$iProcessStep =  $aValue['step_id'];
				echo "executing Process : ".$iProcessID."<br />";
				// add the log with input
				$iProcessInstanceID= $this->addLogJobProcess($iJobID,$iProcessID,$sInputData);
				$iIsertID = $this->addLogProcessing($iProcessStep , $iProcessID,$sProcessInput);
				// process Exection
				$sOutPutJson = $this->executeProcess($iProcessInstanceID,$sProcessFunction,$iJobID,$iProcessID,$sInputData);
				
				// $this->updateLogProcessing($iProcessStep , $iProcessID,$sOutPutJson);
				// if(strlen($sOutPutJson) != 2){
				// 	$aOutput[]=json_decode($sOutPutJson);	
				// }
			}
			// if($iCount == count($aOutput)){
			// 	return true;
			// }else{
			// 	return false;
			// }
		}

		public function executeProcess($iTokenID,$sProcessFunction,$iJobID,$iProcessID,$sInputData){
			
			$aSharedData= json_decode($sInputData,true);
			echo "Here The Process will execute for Process :".$iProcessID."<br/>";
			$aResult =  $sProcessFunction($iTokenID, $aSharedData);
			return json_encode($aResult);
		}	

		public static function processingComplete($iTokenID,$aSharedData){
			$aProcessJob= self::getProcessJObID($iTokenID);
			if(!empty($aProcessJob)){
				$iProcessid= $aProcessJob[0];
				$iJobid=$aProcessJob[1];
				self::markAsCompleteProcess($iJobid,$iProcessid,$aSharedData);
			}

		}
		public static function getProcessJObID($iTokenID){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sJobProcessTable= 'workflow_process';
            
            $sSQuery = "SELECT `process_id`,`job_id` FROM `{$sJobProcessTable}` WHERE `process_instance_id`='{$iTokenID}' ";
            $sResult = $conn->query($sSQuery);        
          	if($sResult!==FALSE){
	            $aRow = $sResult->fetch_assoc();
	            $aProcessJob=array($aRow['process_id'],$aRow['job_id']);
	            return $aProcessJob;
	        }else{
				return array();
	        }
		}

		public function addLogJobStep($iJobID,$iStepID,$sInputData){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepTable= 'workflow_steps';
            $oSession = new SessionManager();
        	$dtStartedOn = date(DB_DATETIME_FORMATE);
            
         	$sIQuery = "INSERT INTO `{$sStepTable}`(`step_instance_id`,`step_id`,`job_id`,`executed_on`,`input_data`,`completed`)
                      VALUES (NULL,'{$iStepID}','{$iJobID}','{$dtStartedOn}','{$sInputData}','0')";
            
            $sResult = $conn->query($sIQuery);        
            if($sResult){
            	echo "Log is Saved for Job ".$iJobID." & Step :".$iStepID.".<br/>";
                return $conn->insert_id;
            }else{
            	return 0;
            
			}
		}
		public function updateLogJobStep($iJobID,$iStepid,$sOutPutdData){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepTable= 'workflow_steps';
            $oSession = new SessionManager();
        	$dtFinishedOn = date(DB_DATETIME_FORMATE);
            
         	$sUQuery = "UPDATE `{$sStepTable}` SET `executed_finished_on`='{$dtFinishedOn}',`output_data`='{$sOutPutdData}',`completed`=1 
         					WHERE `job_id`='{$iJobID}' AND `step_id`='{$iStepid}'";
            
            $sResult = $conn->query($sUQuery);        
            if($sResult){
            	return true;
            }else{
            	return false;
            
			}
		}

		public function addLogJobProcess($iJobID,$iProcessID,$sInputData){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepTable= 'workflow_process';
            $oSession = new SessionManager();
        	$dtStartedOn = date(DB_DATETIME_FORMATE);
            
         	$sIQuery = "INSERT INTO `{$sStepTable}`(`process_instance_id`,`job_id`,`process_id`,`executed_on`,`input_data`,`completed`)
                      VALUES (NULL,'{$iJobID}','{$iProcessID}','{$dtStartedOn}','{$sInputData}','0')";
              
            $sResult = $conn->query($sIQuery);        
            if($sResult){
            	echo "Log is Saved for Job ".$iJobID." & Process :".$iProcessID.".<br/>";
                return $conn->insert_id;
            }else{
            	return 0;
            
			}
		}

		public function addLogProcessing($iStepID , $iProcessID,$sProcessInput){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process_log';
            $oSession = new SessionManager();
        	$dtStartedOn = date(DB_DATETIME_FORMATE);
            
         	$sIQuery = "INSERT INTO `{$sProcessTable}`(`log_id`,`step_id`,`process_id`,`process_input`,`initial_time`,`status`)
                      VALUES (NULL,'{$iStepID}','{$iProcessID}','{$sProcessInput}','{$dtStartedOn}','1')";
                     
            $sResult = $conn->query($sIQuery);        
            if($sResult){
            	echo "Log is Recored while adding for Process :".$iProcessID."<br/>";
                return $conn->insert_id;
            }else{
            	return 0;
            }
		}

		public static function markAsCompleteProcess($iJobID,$iProcessID,$aSharedData){
			$sSharedData = json_encode($aSharedData);
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sStepTable= 'workflow_process';
            $oSession = new SessionManager();
        	$dtFinishedOn = date(DB_DATETIME_FORMATE);
            
         	$sIQuery = "UPDATE `{$sStepTable}` SET `executed_finished_on`='{$dtFinishedOn}',`output_data`='{$sSharedData}',`completed`=1
         				WHERE `job_id`='{$iJobID}' AND `process_id`='{$iProcessID}'";
              
            $sResult = $conn->query($sIQuery);        
            if($sResult){
            	echo "<b>mark as completed for Job ".$iJobID." & Process :".$iProcessID.".</b><br/>";
                return $conn->insert_id;
            }else{
            	return 0;
            }
		}

		public function updateLogProcessing($iStepID , $iProcessID,$sProcessOutput){
			$DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProcessTable= DATABASE_TABLE_PREFIX.'_process_log';
            $oSession = new SessionManager();
        	$dtFinishedOn = date(DB_DATETIME_FORMATE);
            
         	$sUQuery = "UPDATE `{$sProcessTable}` SET `end_time`='{$dtFinishedOn}',`process_output`='{$sProcessOutput}',`status`=2 WHERE
                      	 `step_id`='{$iStepID}' AND `process_id`='{$iProcessID}'";
                     
            $sResult = $conn->query($sUQuery);        
            if($sResult){
                return true;
            }else{
            	return false;
            }
		}
	}
?>