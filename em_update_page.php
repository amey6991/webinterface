<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$iPageId = $_POST['page_id'];
	$sPageCode = $_POST['page_code'];
	$sPageName = $_POST['page_name'];
	$sPageNote = $_POST['page_notes'];
	
	$bResult = fUpdatePage($iPageId,$sPageCode,$sPageName,$sPageNote);
	
	if($bResult)
	{
		$sMsg = array();
		$sMsg[] = "S7";
	    redirectWithAlert("viewPages.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E7";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("updatePage.php?iPageID=".$iPageId, $sMsg);
		}
?>