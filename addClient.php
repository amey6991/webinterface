<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
$sPageTitle = "Add Client";


include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Add Client</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormAddClient" action="em_add_client.php">
                                        <div class="col-xs-offset-1 col-md-10">
                                            <h3>Client Information<hr></h3>
                                        </div>
                                        <div class="form-group">
                                            <label for="idInputFirstName" class="col-sm-2 control-label">Client Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_name" class="form-control" id="id_client_name" placeholder="Enater name please">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="idInputLastName" class="col-sm-2 control-label">Client Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_email" class="form-control" id="id_client_email" placeholder="Enter Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_client_mobile" class="col-sm-2 control-label">Client Contact No</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_mobile" class="form-control" id="id_client_mobile">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_client_url" class="col-sm-2 control-label">Client URL (website)</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_url" class="form-control" id="id_client_url" /> 
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_client_country" class="col-sm-2 control-label">Country</label>
                                            <div class="col-md-4">
                                                <input type="email" name="client_country" class="form-control" id="id_client_country" placeholder="Country">
                                            </div>
                                        </div>
                                        <div class="form-group hide">
                                            <label for="id_client_state" class="col-sm-2 control-label">State</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_state" class="form-control" id="id_client_state">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_client_city" class="col-sm-2 control-label">City</label>
                                            <div class="col-md-4">
                                                <input type="text" name="client_city" class="form-control" id="id_client_city">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_client_address" class="col-sm-2 control-label">Address(Office)</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="client_address" class="form-control" id="id_client_address"></textarea> 
                                            </div>
                                        </div>
                                         <div class="col-xs-offset-1 col-md-10">
                                            <h3>Contact Person<hr></h3>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_person_name" class="col-sm-2 control-label">Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="person_name" class="form-control" id="id_person_name" placeholder="Enater name please">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_person_email" class="col-sm-2 control-label">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="person_email" class="form-control" id="id_person_email" placeholder="Enter Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_person_mobile" class="col-sm-2 control-label">Contact No</label>
                                            <div class="col-md-4">
                                                <input type="text" name="person_mobile" class="form-control" id="id_person_mobile">
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddClient" class="btn btn-success">Save</button>
                                                <a href="dashboard.php" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddClient").click(function(){
                var sClientName = $("#id_client_name").val();
                var sClientEmail = $("#id_client_email").val();
                var sClientMobile = $("#id_client_mobile").val();
                var sPersonMobile = $("#id_person_mobile").val();
                
                
                if(sClientName ==''){
                    displayAlert('<strong>Alert!</strong><br />Client Name Must to enter.','error');
                }else if(sClientEmail=='' || !fValidateEmail(sClientEmail)){
                    alert('Check your Entered Client Email.');
                    displayAlert('<strong>Alert!</strong><br />Client Name Must to enter.','error');
                }else if(sClientMobile=='' || !fValidateMobileNumber(sClientMobile)){
                    displayAlert("<strong>Alert!</strong><br />Check Client's Contact Number","error");
                }else if(sPersonMobile=='' || !fValidateMobileNumber(sPersonMobile)){
                    displayAlert("<strong>Alert!</strong><br />Client's Contact Person Mobile No","error");
                }else{
                    $("#idFormAddClient").submit();
                }
         });
    });
    
    function fValidateMobileNumber(sMobileNumber){
        var sRegularExp = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[123456789]\d{9}$/;

        if(!sMobileNumber.match(sRegularExp)){
            return false;
        }else{
            return true;
        }
    }

    function fValidateEmail(sEmail){
            var email = /^$|^[a-zA-Z0-9]{1}([a-zA-Z0-9]?[\.\-\_]{0,1}[a-zA-Z0-9]+)*[@]{1}(([a-zA-Z0-9]+[\.\-\_]{1})+([a-zA-Z]+))$/;
            if(!sEmail.match(email)){
                return false;
            }else{
                return true;
            }

    }

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>