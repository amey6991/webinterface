<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
$sPageTitle = "Add Page";


include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Add Page</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormAddPage" action="em_add_page.php">
                                        <div class="form-group">
                                            <label for="id_page_code" class="col-sm-2 control-label">Page Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="page_code" class="form-control" id="id_page_code" placeholder="Enter Unique Page Code">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_page_name" class="col-sm-2 control-label">Page Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="page_name" class="form-control" id="id_page_name" placeholder="Enter Page Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_page_notes" class="col-sm-2 control-label">Page Note</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="page_notes" class="form-control" id="id_page_notes"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddPage" class="btn btn-default">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddPage").click(function(){
                var sPageCode = $("#id_page_code").val();
                var sPageName = $("#id_page_name").val();
                
                
                
                if(sPageCode ==''){
                    alert('Page Code Must to enter.');
                }else if(sPageName==''){
                    alert('Page Name Must to enter.');
                }else{
                    $("#idFormAddPage").submit();
                }
         });
    });
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>