<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_galaxy_history.php
  Description : This script is going to get the status for history just run. and the basis of status , we will do something
-->
<?php

//! Include the function pages , any classes if required for the script
require_once __DIR__.'/'.'funSequencers.php';

$sGalaxyUrl = galaxy_url;
$sGalaxyAPIKey = galaxy_api;
$sGalaxyWorkflowID = galaxy_workflow_id;
$sGalaxyLibraryID = galaxy_library_id;
$sDBKey = sDBKey;
$sReferenceLibraryID = galaxy_reference_library_id;
$sReferenceLibraryFolderID = galaxy_reference_library_folder_id;

$aHistory = fGetHistoryActive();

if(!empty($aHistory)){
	foreach ($aHistory as $key => $aValue) {
		$iTableID = $aValue['id_galaxy_history'];
		$sGalaxyHistoryID = $aValue['galaxy_history_id'];
		$sGalaxyHistoryName = $aValue['galaxy_history_name'];

		//!! Get the SampleName(Barcode) for History
		$sSampleNameBarcode = fGetSampleNameForHistory($sGalaxyHistoryID);
		$iSampleInfoID = fGetSampleInfoIDForValidSampleNameBarcode($sSampleNameBarcode);
		

		$sCommand = "python lib/bioblend/wbint_history_status.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyHistoryID} 1";
		$sOutput  = exec($sCommand);
		$aOutput = explode(',', $sOutput);
		
		$sPercentComplete = $aOutput[0];
		if($sPercentComplete==='100'){
			//!! History Job is Done
			echo "Completed </br>";
			// Sample is in Analysis Complete
			fUpdateSampleStatus($iSampleInfoID,4);
			//!! Update History Status as 2 , --> Its for check the output.
			$iStatus=0; // History Job is Completed
			fUpdateHistoryStatus($sGalaxyHistoryID,$iStatus);
		}else{
			$sCommand = "python lib/bioblend/wbint_history_status.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyHistoryID} 2";
			$sOutput  = exec($sCommand);
			$aOutput = explode(',', $sOutput);
			$sState = $aOutput[0];
			if($sState=='running'){
				echo "Running </br>";
				echo $iPercentComplete = (int)$sPercentComplete;
				if($iPercentComplete < 60){
					// Sample is in QC Analysis
					fUpdateSampleStatus($iSampleInfoID,2);
				}else{
					// Sample is in Sample Analsyis
					fUpdateSampleStatus($iSampleInfoID,3);
				}
				continue;
			}elseif($sState=='ok'){
				//!! Job is Completed
				$iStatus=0; // History Job is Completed
				fUpdateHistoryStatus($sGalaxyHistoryID,$iStatus);
				// Sample is in Analysis Complete
				fUpdateSampleStatus($iSampleInfoID,4);
			}elseif($sState=='queued'){
				continue;
				//$iStatus=2; // History Job is Error Occured
				//fUpdateHistoryStatus($sGalaxyHistoryID,$iStatus);
			}
		}

	}
}else{
	
	//!! No Active History In galaxy
	echo "No Active History in Galaxy";
}

?>