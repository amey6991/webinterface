<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_galaxy_workflow.php
  Description : This script is going to get the sequencer which fastq files are read by middleware , will create library , folder , datasets , history , mapp dataset with history and save the information into database.
-->
<?php

//! Include the function pages , any classes if required for the script
require_once __DIR__.'/'.'funSequencers.php';
$sLogMsg = "\r\n"."---SRT-- Script is started to Work with Galaxy using python Library Bioblend and create library , folder , Datasets at ".date('y-m-d H:i:s')."\r\n";
fLogger(11,$sLogMsg);

$aSequencer = fGetSequencerForgalaxyWorkflow();

$iTotalSequencer = count($aSequencer);
//echo "<pre>";print_r($aSequencer);exit();
//$aSequencerFastq = fGetSequencerForgalaxyWorkflow();

$sLogMsg = "\r\n"."---SRT-- {$iTotalSequencer} Sequencer found for use the fastq files and run the workflow at ".date('y-m-d H:i:s')."\r\n";
fLogger(11,$sLogMsg);

$sGalaxyUrl = galaxy_url;
$sGalaxyAPIKey = galaxy_api;
$sGalaxyWorkflowID = galaxy_workflow_id;
$sGalaxyLibraryID = galaxy_library_id;
$sDBKey = sDBKey;
$sReferenceLibraryID = galaxy_reference_library_id;
$sReferenceLibraryFolderID = galaxy_reference_library_folder_id;

$aSampleNameMaster = array();
$aSampleInfo = array();
$aSamplePairInfo = array();
$aSamplePairMaster = array();

foreach ($aSequencer as $key => $aValue) {
	$iSeqDirectoryID = $aValue['seq_directory_id'];
	$sSeqDirectoryName = $aValue['seq_dir_name'];
	$iSeqType = $aValue['seq_type'];
	$sSeqServerPath = $aValue['seq_server_path'];
	$sFastQFilePath = $aValue['seq_output_path'];
	$sFastqFilename = $aValue['seq_output_filename'];


	// Get the pair files in array
	$aSample = explode('_', $sFastqFilename);	
	$sSampleName = $aSample[0];
	$sSampleReverseType = $aSample[3];

	$iLengthSampleName = strlen($sSampleName);

	//!! Check the sample Name is valid by LimsDx or NOT
	$ivalid = (int)fCheckSampleNameValidForLimsDx($sSampleName);

	if($ivalid == 1){
		$bExist = array_key_exists($sSampleName,$aSampleInfo);
		
		if($bExist==true){

			$aInfo = $aSampleInfo[$sSampleName];

			$sFastQFileFullPath = $aInfo['sFastQFilePath'];
			$iSeqDirID = $aInfo['iSeqDirectoryID'];
			$sSeqDirName = $aInfo['sSeqDirectoryName'];

			if($iSeqDirID==$iSeqDirectoryID && $sSeqDirName==$sSeqDirectoryName){
				$aFastq = explode('_', $sFastQFileFullPath);	
				$sReverseType = $aFastq[6];
				if($sReverseType=='R1'){
					$aSamplePairInfo[0] = array(
							'farword_fastq_path'=>$sFastQFileFullPath
							);
					$aSamplePairInfo[1] = array(
							'reverse_fastq_path'=>$sFastQFilePath
							);
				}elseif($sReverseType=='R2'){
					$aSamplePairInfo[1] = array(
							'reverse_fastq_path'=>$sFastQFileFullPath
							);
					$aSamplePairInfo[0] = array(
							'farword_fastq_path'=>$sFastQFilePath
							);
				}
				$aSamplePairInfo[2] = array(
							'seq_dir_name'=>$sSeqDirectoryName
							);
				$aSamplePairInfo[3] = array(
							'seq_dir_id'=>$iSeqDirectoryID
							);
				$aSamplePairInfo[4] = array(
							'seq_sample_name'=>$sSampleName
							);
			}
			$aSamplePairMaster[]= $aSamplePairInfo;
			//exit();
		}else{
			
			$aSampleInfo[$sSampleName]= array(
					'sFastQFilePath'=> $sFastQFilePath,
					'sSeqDirectoryName' => $sSeqDirectoryName,
					'iSeqDirectoryID' => $iSeqDirectoryID
				);

		}
	}
}
//echo "<pre>";print_r($aSamplePairMaster);exit();
foreach ($aSamplePairMaster as $key => $aValue) {

	$sForwordFastqFile = $aValue[0]['farword_fastq_path'];
	$sReverseFastqFile = $aValue[1]['reverse_fastq_path'];
	$sSeqDirectoryName = $aValue[2]['seq_dir_name'];
	$iSeqDirectoryID = $aValue[3]['seq_dir_id'];
	$sSampleName = $aValue[4]['seq_sample_name'];
	//!! Check folder is created or Not
	$sFolderName = $sSeqDirectoryName;
	$sFolderDesc = 'Folder Created to store all Datasets';
	$bFolderExist = fCheckFolderNameExist($sFolderName);
	//var_dump($bFolderExist);exit();
	if($bFolderExist != true){
		
		//! Create Folder
		$sCreateFolderCommand = "python lib/bioblend/wbint_library_folder.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyLibraryID} {$sFolderName}";
		
		$sFolderID = exec($sCreateFolderCommand);
		$iGalaxyLibFolderTableID = fAddGalaxyDataLibraryFolder($sGalaxyLibraryID ,$sFolderID,$sFolderName , $sFolderDesc);

		
		// Create History
		$aFolder = explode('_',$sFolderName);
		$sHistoryName = 'History_'.$aFolder[0].'_'.date('YmdHis');

		$sCreateHistoryCommand = "python lib/bioblend/wbint_history.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sHistoryName}";
		

		$sHistoryID = exec($sCreateHistoryCommand);	
		
		$iHistroyTableID = fAddGalaxyHistory($sHistoryID,$sHistoryName);

		//!! Add Fastq File to the folder , become Dataset Input for Workflow
		$sFileType = 'fastq';

		// Add Forward Fast File R1
		$sAddForwardDataSetCommand = "python lib/bioblend/wbint_add_dataset.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyLibraryID} {$sForwordFastqFile} {$sFolderID} {$sFileType} {$sDBKey}";
		$sForwardDataSetID = exec($sAddForwardDataSetCommand);
		$iDatasetType =2;
		$iResultForword = fAddGalaxyDataSet($sGalaxyLibraryID ,$iDatasetType,$sFolderID , $sForwordFastqFile ,$sForwardDataSetID );

		// Add Reverse Fast File R2

		$sAddReverseDataSetCommand = "python lib/bioblend/wbint_add_dataset.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyLibraryID} {$sReverseFastqFile} {$sFolderID} {$sFileType} {$sDBKey}";
		$sReverseDataSetID = exec($sAddReverseDataSetCommand);
		$iResultReverse = fAddGalaxyDataSet($sGalaxyLibraryID ,$iDatasetType,$sFolderID , $sReverseFastqFile ,$sReverseDataSetID );


		//!! Add Sample Fastq with History
		
		fAddGalaxyHistorySample($sHistoryID,$sSampleName,$sForwardDataSetID,$sReverseDataSetID);

		// Inputs Values to run the workflow
		$aReferDataSet = fGetReferenceDataSetIDForLibraryAndFolder($sReferenceLibraryID,$sReferenceLibraryFolderID);

		$sPileUpDataSetID = $aReferDataSet[0];
		$sTargetDataSetID = $aReferDataSet[1];
		$sBaitDataSetID = $aReferDataSet[2];

		$bImportInputsToHistory = True;

		$aDataSetMap = array(
		          '0'=> array('src'=>'ld','id'=>$sForwardDataSetID),
		          '1'=> array('src'=>'ld','id'=>$sReverseDataSetID),
		          '2'=> array('src'=>'ld','id'=>$sPileUpDataSetID),
		          '3'=> array('src'=>'ld','id'=>$sTargetDataSetID),
		          '4'=> array('src'=>'ld','id'=>$sBaitDataSetID),
		        );
		//$sDataSetMap = json_encode($aDataSetMap,JSON_FORCE_OBJECT);
		$sDataSetMap = " '{\"1\": {\"src\": \"ld\", \"id\": \"{$sReverseDataSetID}\"}, \"0\": {\"src\": \"ld\", \"id\": \"{$sForwardDataSetID}\"}, \"3\": {\"src\": \"ld\", \"id\": \"{$sTargetDataSetID}\"}, \"2\": {\"src\": \"ld\", \"id\": \"{$sPileUpDataSetID}\"}, \"4\": {\"src\": \"ld\", \"id\": \"{$sBaitDataSetID}\"}} '";

		$sRunName = 'MGCDv2.1';
		$sSampleName = 'Spikein1';
		$dRunDate = '2016-07-12';
		$sRGLB = 'Clinical';

		$aParameterMap = array(
				'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0'=> array(
						'rgid'=>$sRunName,
						'rgsm'=>$sSampleName,
						'rglb'=>$sRGLB,
						'rgdt'=>$dRunDate
					),
				'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65'=> array(
						'contaminants'=>None,
						'limits'=>None
					)
				);

		//$sParameterMap = escapeshellarg(json_encode($aParameterMap,JSON_UNESCAPED_SLASHES));
		$sParameterMap = " '{\"toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65\": {\"contaminants\": \"None\", \"limits\": \"None\"}, \"toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0\": {\"rgsm\": \"{$sSampleName}\", \"rglb\": \"{$sRGLB}\", \"rgdt\": \"{$dRunDate}\", \"rgid\": \"{$sRunName}\"}}' ";
		echo "<br>";
		// Run The workflow
		echo $sRunWorkflowCommand = "python lib/bioblend/wbint_run_workflow.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyWorkflowID} {$sDataSetMap} {$sParameterMap} {$sHistoryID} {$bImportInputsToHistory}";
		
		$sWorkflowOutput = exec($sRunWorkflowCommand);

		//!! Get the Output of History and store it

		echo $sOutputCommand = "python lib/bioblend/wbint_history_output_status.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sHistoryID}";
		$sOutputHistory = exec($sOutputCommand);
		$sOutputHistory = str_replace("u'",'',$sOutputHistory);
		$sOutputHistory = str_replace("'",'',$sOutputHistory);
		$sOutputHistory = str_replace("[",'',$sOutputHistory);
		$sOutputHistory = str_replace("]",'',$sOutputHistory);
		$aOutput = explode(',', $sOutputHistory);

		$bAdded = fCheckHistoryOutputEntry($iHistroyTableID);
		if($bAdded==false){
			if(!empty($aOutput)){
				foreach ($aOutput as $key => $sOutputID) {
					if($key < 5){
						continue;
					}else{
						//!! Store the Output ID
						$iIndex = $key+1;
						fAddHistoryOutputID($iHistroyTableID,$iIndex,$sOutputID);
					}
				}
			}
		}
		echo "<br>";
		//exit();
	}else{
		
		$sLogMsg = "\r\n"."--------{$sFolderName} Named Folder is already created at ".date('y-m-d H:i:s')."\r\n";
		fLogger(11,$sLogMsg);

		// Get The Folder ID
		
		$sFolderID = fGetFolderIDByFolderName($sFolderName);
		// Create History
		$aFolder = explode('_',$sFolderName);
		$sHistoryName = 'History_'.$aFolder[0].'_'.date('YmdHis');
		
		$sCreateHistoryCommand = "python lib/bioblend/wbint_history.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sHistoryName}";
		
		$sHistoryID = exec($sCreateHistoryCommand);	

		$iHistroyTableID = fAddGalaxyHistory($sHistoryID,$sHistoryName);
		

		//!! Add Fastq File to the folder , become Dataset Input for Workflow
		$sFileType = 'fastq';
		
		// Add Forward Fast File R1
		$iDatasetType=2;
		$sAddForwardDataSetCommand = "python lib/bioblend/wbint_add_dataset.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyLibraryID} {$sForwordFastqFile} {$sFolderID} {$sFileType} {$sDBKey}";
		$sForwardDataSetID = exec($sAddForwardDataSetCommand);
		
		$iResultForword = fAddGalaxyDataSet($sGalaxyLibraryID ,$iDatasetType,$sFolderID , $sForwordFastqFile ,$sForwardDataSetID );
		
		// Add Reverse Fast File R2

		$sAddReverseDataSetCommand = "python lib/bioblend/wbint_add_dataset.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyLibraryID} {$sReverseFastqFile} {$sFolderID} {$sFileType} {$sDBKey}";
		$sReverseDataSetID = exec($sAddReverseDataSetCommand);
		
		$iResultReverse = fAddGalaxyDataSet($sGalaxyLibraryID ,$iDatasetType,$sFolderID , $sReverseFastqFile ,$sReverseDataSetID );
		
		//!! Add Sample Fastq with History
		
		fAddGalaxyHistorySample($sHistoryID,$sSampleName,$sForwardDataSetID,$sReverseDataSetID);

		// Get the Reference DatasetID
		$aReferDataSet = fGetReferenceDataSetIDForLibraryAndFolder($sReferenceLibraryID,$sReferenceLibraryFolderID);

		$sPileUpDataSetID = $aReferDataSet[0];
		$sTargetDataSetID = $aReferDataSet[1];
		$sBaitDataSetID = $aReferDataSet[2];
		// Run The workflow
		// Inputs Values to run the workflow

		$bImportInputsToHistory = True;

		$aDataSetMap = array(
		          '1'=> array('src'=>'ld','id'=>$sReverseDataSetID),
		          '0'=> array('src'=>'ld','id'=>$sForwardDataSetID),
		          '3'=> array('src'=>'ld','id'=>$sTargetDataSetID),
		          '2'=> array('src'=>'ld','id'=>$sPileUpDataSetID),
		          '4'=> array('src'=>'ld','id'=>$sBaitDataSetID),
		        );
		
		
		//echo $sDataSetMap = escapeshellarg(json_encode($aDataSetMap,JSON_FORCE_OBJECT));
		$sDataSetMap = " '{\"1\": {\"src\": \"ld\", \"id\": \"{$sReverseDataSetID}\"}, \"0\": {\"src\": \"ld\", \"id\": \"{$sForwardDataSetID}\"}, \"3\": {\"src\": \"ld\", \"id\": \"{$sTargetDataSetID}\"}, \"2\": {\"src\": \"ld\", \"id\": \"{$sPileUpDataSetID}\"}, \"4\": {\"src\": \"ld\", \"id\": \"{$sBaitDataSetID}\"}} '";

		
		
		$sRunName = 'MGCDv2.1';
		$sSampleName = 'Spikein1';
		$dRunDate = '2016-07-12';
		$sRGLB = 'Clinical';

		$aParameterMap = array(
				'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0'=> array(
						'rgid'=>$sRunName,
						'rgsm'=>$sSampleName,
						'rglb'=>$sRGLB,
						'rgdt'=>$dRunDate
					),
				'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65'=> array(
						'contaminants'=>None,
						'limits'=>None
					)
				);

		//echo $sParameterMap = escapeshellarg(json_encode($aParameterMap,JSON_UNESCAPED_SLASHES));
		$sParameterMap = " '{\"toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65\": {\"contaminants\": \"None\", \"limits\": \"None\"}, \"toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0\": {\"rgsm\": \"{$sSampleName}\", \"rglb\": \"{$sRGLB}\", \"rgdt\": \"{$dRunDate}\", \"rgid\": \"{$sRunName}\"}}' ";
		
		echo "<br>";
		echo $sRunWorkflowCommand = "python lib/bioblend/wbint_run_workflow.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sGalaxyWorkflowID} {$sDataSetMap} {$sParameterMap} {$sHistoryID}";//   
		$sWorkflowOutput = exec($sRunWorkflowCommand);
		
		
		//!! get the Output ID and store it 
		$sOutputCommand = "python lib/bioblend/wbint_history_output_status.py {$sGalaxyUrl} {$sGalaxyAPIKey} {$sHistoryID}";
		$sOutputHistory = exec($sOutputCommand);
		$sOutputHistory = str_replace("u'",'',$sOutputHistory);
		$sOutputHistory = str_replace("'",'',$sOutputHistory);
		$sOutputHistory = str_replace("[",'',$sOutputHistory);
		$sOutputHistory = str_replace("]",'',$sOutputHistory);
		$aOutput = explode(',', $sOutputHistory);

		$bAdded = fCheckHistoryOutputEntry($iHistroyTableID);
		if($bAdded==false){
			if(!empty($aOutput)){
				foreach ($aOutput as $key => $sOutputID) {
					if($key < 5){
						continue;
					}else{
						//!! Store the Output ID
						$iIndex = $key+1;
						fAddHistoryOutputID($iHistroyTableID,$iIndex,$sOutputID);
					}
				}
			}
		}

	}
}
?>