<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";

$sessionManager = new SessionManager();

if(isset($_GET['redirect']) && $_GET['redirect']!="") {
    $sRedirect = $_GET['redirect'];
}
else {
    $sRedirect = "index.php";
}


$sessionManager = new SessionManager();

$username = addslashes($_POST['username']);
$password = addslashes($_POST['password']);
$rememberMe = addslashes($_POST['remember']);
if(isset($rememberMe) && $rememberMe == "yes"){
    $rememberMe = TRUE;
}
else {
    $rememberMe = FALSE;    
}
//! Redirect to dashboard if login successful
if($sessionManager->authenticateUser($username, $password, $rememberMe))
{
    header("Location: {$sRedirect}");
}
else {
    $sMsg = array();
    $sMsg[] = "E1";
    //! Redirect User with appropriate alert message
    redirectWithAlert("login.php?redirect=".urlencode($sRedirect), $sMsg);
}

?>