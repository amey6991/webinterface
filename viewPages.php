<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";


$sPageTitle = "View Page Listing";

$aPageData = fGetAllPages();
$iCountPage = count($aPageData);

$oSession = new SessionManager();
$iType = $oSession->iType;

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Page </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Page Code</th>
                                                <th>Page Name</th>
                                                <th>Page Notes</th>
                                                <th>Page Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountPage;$iii++){
                                                   $iPageID = $aPageData[$iii]['page_id'];
                                                   $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iPageID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td><?php echo $aPageData[$iii]['page_code'];?></td>
                                                <td><?php echo $aPageData[$iii]['page_name'];?></td>
                                                <td><?php echo $aPageData[$iii]['notes'];?></td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".classUpdateButton").click(function(){
            var iPageID = this.id;
            window.location="updatePage.php?iPageID="+iPageID;
        });
        $('#idDataTable').DataTable();
    });

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>