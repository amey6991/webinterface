<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$iPageID=5;

$sScreenURL = "dashboard.php";
$sPageTitle = "Upload Files";

$oSession = new SessionManager();
$iUserTypeID = $oSession->iType;
$iUserID = $oSession->iUserID;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"Upload File","link"=>"uploadFile.php","isActive"=>true)
            );
?>

<!-- Main Content -->
<div class="container-fluid">
    <div class="side-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <ol class="breadcrumb navbar-breadcrumb">
                                <?php echo parseBreadcrumb($aBreadcrumb); ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" method="POST" id="idFormUploadFile" action="em_upload_file.php" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="idInputFirstName" class="col-sm-2 control-label">Select File</label>
                            <div class="col-md-4">
                                <input type="file" name="file_detail[]" class="form-control" id="idFile" multiple>
                                <input type="hidden" name="user_id" value="<?php echo $iUserID;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="idInputFirstName" class="col-sm-2 control-label">Rename File</label>
                            <div class="col-md-4">
                                <input type="text" name="file_name" class="form-control" id="idFileName" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-md-4">
                                <input type="submit" id="idUploadFile" class="btn btn-success" value="Upload">
                                <a href="dashboard.php" class="btn btn-info">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
        
    <script type="text/javascript">
    $(document).ready(function(){

         // $("#idUploadFile").click(function(){
         //        var sFirstName = $("#idInputFirstName").val();
         //        var sLastName = $("#idInputLastName").val();
         //        var sStateName = $("#idInputStateName").val();
         //        var sCityName = $("#idInputCityName").val();
         //        var sAddresstName = $("#idInputAddress").val();
         //        var iUserType = $("#idUserType").val();
         //        var sUserName = $("#idInputUserName").val();
         //        var sEmail = $("#idInputEmail").val();
         //        var sMobileNumber = $("#idInputMobileNumber").val();
                

         //        if(sFirstName ==''){
         //            alert('First Name Must to enter.');
         //        }else if(iUserType ==""){
         //            alert('User Type Must to Select.');
         //        }else if(sEmail=='' || !fValidateEmail(sEmail)){
         //            alert('Check your Entered Email.');
         //        }else if(sMobileNumber=='' || !fValidateMobileNumber(sMobileNumber)){
         //            alert('Check your Mobile No');
         //        }else if(fValidateUserName(sUserName)){
         //            alert('User Name is Already Exist');
         //        }else{
         //           // $("#idFormAdduser").submit();
         //        }
         // });
    });
    


    </script>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>