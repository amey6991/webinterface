CREATE TABLE `webiface_sequencer_samplesheet_info_master` (
  `seq_samplesheet_master_id` INT(11) NOT NULL AUTO_INCREMENT,
  `workflow` VARCHAR(255) NULL,
  `application` VARCHAR(255) NULL,
  `experiment_name` VARCHAR(255) NULL,
  `investigator_name` VARCHAR(255) NULL,
  `IEMFileVersion` VARCHAR(255) NULL,
  `cycle_no` INT(11) NULL,
  `created_date` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`seq_samplesheet_master_id`));
ALTER TABLE `webiface_sequencer_samplesheet_info_master` 
ADD COLUMN `seq_directory_id` INT(11) NULL AFTER `seq_samplesheet_master_id`;

CREATE TABLE `webiface_sequencer_sample_info_master` (
  `seq_sample_info_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_samplesheet_master_id` INT(11) NULL,
  `sample_ID` INT(11) NULL,
  `index_17_id` INT(11) NULL,
  `index_barcode` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`seq_sample_info_id`));
