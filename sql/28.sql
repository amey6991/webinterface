CREATE TABLE `webiface_sequencer_email_fqdb_config_master` (
  `id_fqdN_master` INT(11) NOT NULL AUTO_INCREMENT,
  `fqdn_smtp_secure` VARCHAR(255) NULL,
  `fqdn_smtp_host` VARCHAR(255) NULL,
  `fqdn_smtp_port` VARCHAR(255) NULL,
  `fqdn_smtp_username` VARCHAR(255) NULL,
  `fqdn_smtp_password` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_fqdN_master`));

CREATE TABLE `webiface_sequencer_default_email_master` (
  `default_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(255) NULL,
  `user_email` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`default_id`));
