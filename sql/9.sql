CREATE TABLE `webiface_sequencer_master` (
  `seq_directory_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_dir_name` VARCHAR(255) NOT NULL,
  `seq_server_path` VARCHAR(255) NOT NULL,
  `seq_first_seen_on` VARCHAR(45) NOT NULL,
  `seq_status` INT(11) NOT NULL,
  PRIMARY KEY (`seq_directory_id`));

CREATE TABLE `webiface_sequencer_master_status` (
  `status_id` INT(11) NOT NULL,
  `seq_directory_id` INT(11) NULL,
  `seq_timestamp` DATETIME NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`status_id`));

ALTER TABLE `webiface_sequencer_master_status` 
ADD COLUMN `seq_status` INT(11) NULL AFTER `seq_directory_id`;

ALTER TABLE `webiface_sequencer_master` 
DROP COLUMN `seq_status`;
ALTER TABLE `webiface_sequencer_master_status` 
CHANGE COLUMN `status_id` `status_id` INT(11) NOT NULL AUTO_INCREMENT ;
