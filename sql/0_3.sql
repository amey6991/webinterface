CREATE TABLE `webiface_files` (
  `file_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NULL,
  `file_name` VARCHAR(500) NULL,
  `file_type` VARCHAR(100) NULL,
  `file_size` VARCHAR(100) NULL,
  `file_path` VARCHAR(500) NULL,
  `processed` INT(11) NULL,
  `uploaded_on` DATETIME NULL,
  `modified_on` DATETIME NULL,
  `deleted` INT(11) NULL,
  PRIMARY KEY (`file_id`));
ALTER TABLE `webiface_files` 
ADD COLUMN `file_extension` VARCHAR(45) NULL AFTER `file_name`;
ALTER TABLE `webiface_files` 
ADD COLUMN `file_temp_path` VARCHAR(500) NULL AFTER `file_size`;

ALTER TABLE `webiface_files` 
CHANGE COLUMN `file_name` `file_name_orignal` VARCHAR(500) NULL DEFAULT NULL ,
ADD COLUMN `file_name_upload` VARCHAR(500) NULL AFTER `file_name_orignal`;
