CREATE TABLE `webiface_sequencer_sample_status` (
  `id_sample_status` INT(11) NOT NULL,
  `seq_sample_info_id` INT(11) NULL,
  `seq_sample_status` INT(11) NULL,
  `added_on` DATETIME NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`id_sample_status`));

ALTER TABLE `webiface_sequencer_sample_status` 
CHANGE COLUMN `id_sample_status` `id_sample_status` INT(11) NOT NULL AUTO_INCREMENT ;
