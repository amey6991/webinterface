CREATE TABLE `webiface_sequencer_galaxy_history_info` (
  `id_history_info` INT(11) NOT NULL AUTO_INCREMENT,
  `galaxy_history_id` VARCHAR(255) NULL,
  `galaxy_forward_dataset_id` VARCHAR(255) NULL,
  `galaxy_reverse_dataset_id` VARCHAR(255) NULL,
  `sample_name_barocde` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_history_info`));
