CREATE TABLE .`webiface_workflow` (
  `wf_id` INT NOT NULL AUTO_INCREMENT,
  `workflow_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`wf_id`));

CREATE TABLE .`webiface_steps` (
  `step_id` INT NOT NULL AUTO_INCREMENT,
  `wf_id` INT NOT NULL,
  `step_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `added_by` INT NULL,
  `status` INT NULL DEFAULT 1,
  PRIMARY KEY (`step_id`));

CREATE TABLE .`webiface_process` (
  `process_id` INT NOT NULL AUTO_INCREMENT,
  `step_id` INT NOT NULL,
  `process_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `added_by` INT NOT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT '1',
  PRIMARY KEY (`process_id`));
ALTER TABLE `webiface_process` 
ADD COLUMN `process_function` VARCHAR(255) NULL AFTER `process_name`;


CREATE TABLE .`webiface_track_work` (
  `track_id` INT(11) NOT NULL AUTO_INCREMENT,
  `work_id` INT(11) NOT NULL,
  `step_id` INT(11) NOT NULL,
  `is_complete` INT(11) NOT NULL DEFAULT 0,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`track_id`));

ALTER TABLE .`webiface_track_work` 
ADD COLUMN `process_id` INT(11) NOT NULL AFTER `step_id`;

ALTER TABLE `webiface_track_work` 
ADD COLUMN `added_on` DATETIME NULL AFTER `process_id`;


CREATE TABLE `webiface_process_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `process_input` varchar(1000) DEFAULT NULL,
  `process_output` varchar(1000) DEFAULT NULL,
  `initial_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `workflow_job` (
  `workflow_job_id` INT(11) NOT NULL AUTO_INCREMENT,
  `job_id` INT(11) NOT NULL,
  `workflow_id` INT(11) NOT NULL,
  `executed_on` DATETIME NULL,
  `execution_finished_on` DATETIME NULL,
  `completed` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`workflow_job_id`));

ALTER TABLE `workflow_job` 
ADD COLUMN `input_data` VARCHAR(1000) NULL AFTER `execution_finished_on`,
ADD COLUMN `output_data` VARCHAR(1000) NULL AFTER `input_data`;


ALTER TABLE `workflow_job` 
DROP COLUMN `workflow_job_id`,
CHANGE COLUMN `job_id` `job_id` INT(11) NOT NULL AUTO_INCREMENT ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`job_id`);


CREATE TABLE `workflow_steps` (
  `step_instance_id` INT(11) NOT NULL AUTO_INCREMENT,
  `step_id` INT(11) NOT NULL,
  `job_id` INT(11) NOT NULL,
  `executed_on` DATETIME NULL,
  `executed_finished_on` DATETIME NULL,
  `input_data` VARCHAR(1000) NULL,
  `output_data` VARCHAR(1000) NULL,
  `completed` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`step_instance_id`));

CREATE TABLE `workflow_process` (
  `process_instance_id` INT(11) NOT NULL,
  `job_id` INT(11) NOT NULL,
  `process_id` INT(11) NOT NULL,
  `executed_on` DATETIME NULL,
  `executed_finished_on` DATETIME NULL,
  `input_data` VARCHAR(1000) NULL,
  `output_data` VARCHAR(1000) NULL,
  `completed` VARCHAR(45) NULL DEFAULT 0,
  PRIMARY KEY (`process_instance_id`));

ALTER TABLE `workflow_process` 
CHANGE COLUMN `process_instance_id` `process_instance_id` INT(11) NOT NULL AUTO_INCREMENT ;

