CREATE TABLE `webiface_sequencer_workflow` (
  `id_sequencer_workflow` INT(11) NOT NULL AUTO_INCREMENT,
  `sequencer_workflow_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_sequencer_workflow`));

CREATE TABLE `webiface_sequencer_test_workflow_mapping` (
  `id_test_workflow_mapping` INT(11) NOT NULL AUTO_INCREMENT,
  `test_id` INT(11) NULL,
  `workflow_id` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_test_workflow_mapping`));

CREATE TABLE `webiface_sequencer_test_master` (
  `id_master_test` INT(11) NOT NULL AUTO_INCREMENT,
  `test_name` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_master_test`));

ALTER TABLE `webiface_sequencer_test_master` 
CHANGE COLUMN `test_name` `test_name` VARCHAR(255) NULL DEFAULT NULL ;


INSERT INTO `webiface_sequencer_test_master` (`id_master_test`, `test_name`, `added_on`, `status`) VALUES ('1', 'MGCD265-101 TUMOR v2.1 NGS', '2016-07-04 15:03:07', '1');
INSERT INTO `webiface_sequencer_workflow` (`id_sequencer_workflow`, `sequencer_workflow_name`, `added_on`, `status`) VALUES ('1', 'Mirati MGCDv2.1 Tumor Panel v2.2', '2016-07-04 15:03:07', '1');
INSERT INTO `webiface_sequencer_test_workflow_mapping` (`id_test_workflow_mapping`, `test_id`, `workflow_id`, `added_on`, `status`) VALUES ('1', '1', '1', '2016-07-04 15:03:07', '1');

CREATE TABLE `webiface_sequencer_fastq_master` (
  `id_fastq_master` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_output_path` VARCHAR(255) NULL,
  `seq_output_filename` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_fastq_master`));
