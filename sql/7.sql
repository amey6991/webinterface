CREATE TABLE `webiface_metrics_details` (
  `output_Id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_id` INT(11) NULL,
  `model` VARCHAR(255) NULL,
  `cycle_config_read1` INT(11) NULL,
  `cycle_config_read2_index` INT(11) NULL,
  `cycle_config_read3` INT(11) NULL,
  `mean_cluster_density` BIGINT(11) NULL,
  `total_clusters` BIGINT(11) NULL,
  `total_pf_clusters` BIGINT(11) NULL,
  `percentage_pf_clusters` FLOAT(11) NULL,
  `aligned_phix` FLOAT(11) NULL,
  `read_1` FLOAT(11) NULL,
  `read_2` FLOAT(11) NULL,
  `read_3` FLOAT(11) NULL,
  `extra` VARCHAR(255) NULL,
  PRIMARY KEY (`output_Id`));

CREATE TABLE `webiface_metrics_sample_output` (
  `Sample_output_id` INT(11) NOT NULL AUTO_INCREMENT,
  `output_Id` INT(11) NULL,
  `Barcode_name` VARCHAR(255) NULL,
  `Cycle_no` INT(11) NULL,
  `Number_closed_to_sample` BIGINT(11) NULL,
  `extra` VARCHAR(255) NULL,
  PRIMARY KEY (`Sample_output_id`));

ALTER TABLE `webiface_metrics_details` 
ADD COLUMN `status` INT(11) NULL DEFAULT 1 AFTER `extra`;
ALTER TABLE `webiface_metrics_sample_output` 
ADD COLUMN `status` INT(11) NULL DEFAULT 1 AFTER `extra`;

ALTER TABLE `webiface_metrics_details` 
ADD COLUMN `added_on` DATETIME NULL AFTER `status`;
ALTER TABLE `webiface_metrics_details` 
CHANGE COLUMN `added_on` `added_on` DATETIME NULL DEFAULT NULL AFTER `read_3`,
CHANGE COLUMN `status` `status` INT(11) NULL DEFAULT '1' AFTER `added_on`;

ALTER TABLE `webiface_metrics_sample_output` 
ADD COLUMN `added_on` VARCHAR(45) NULL AFTER `status`;
ALTER TABLE `webiface_metrics_sample_output` 
CHANGE COLUMN `added_on` `added_on` DATETIME NULL DEFAULT NULL AFTER `Number_closed_to_sample`,
CHANGE COLUMN `status` `status` INT(11) NULL DEFAULT '1' AFTER `added_on`;
