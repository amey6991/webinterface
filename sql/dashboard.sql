CREATE DATABASE  IF NOT EXISTS `app_webinterface` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `app_webinterface`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: app_webinterface
-- ------------------------------------------------------
-- Server version	5.6.24-2+deb.sury.org~trusty+2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `webiface_workflow`
--

DROP TABLE IF EXISTS `webiface_workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webiface_workflow` (
  `wf_id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_name` varchar(255) DEFAULT NULL,
  `added_on` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`wf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webiface_workflow`
--

LOCK TABLES `webiface_workflow` WRITE;
/*!40000 ALTER TABLE `webiface_workflow` DISABLE KEYS */;
INSERT INTO `webiface_workflow` VALUES (1,'GenerateFASTQ','2016-01-28 13:09:14',1),(2,'GenerateFASTQ Run','2016-01-28 13:09:14',1);
/*!40000 ALTER TABLE `webiface_workflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webiface_sequncer_folder`
--

DROP TABLE IF EXISTS `webiface_sequncer_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webiface_sequncer_folder` (
  `sequencer_output_folder_id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_name` varchar(255) NOT NULL,
  `folder_size` varchar(255) DEFAULT '0',
  `cycle_number` varchar(45) DEFAULT '0',
  `isCompleted` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL COMMENT '0= waiting\n1 = running (on sequncer)\n2= completed\n',
  PRIMARY KEY (`sequencer_output_folder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webiface_sequncer_folder`
--

LOCK TABLES `webiface_sequncer_folder` WRITE;
/*!40000 ALTER TABLE `webiface_sequncer_folder` DISABLE KEYS */;
INSERT INTO `webiface_sequncer_folder` VALUES (1,'160218_M01801_0126_000000000-AGU3J','163.1','12',0,'2016-03-04 17:13:46',1),(2,'160218_M01801_0126_000000000-AGU4J','52.3','24',1,'2016-02-04 07:23:26',1);
/*!40000 ALTER TABLE `webiface_sequncer_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webiface_test_workflow_mapping`
--

DROP TABLE IF EXISTS `webiface_test_workflow_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webiface_test_workflow_mapping` (
  `map_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `workflow_id` int(11) NOT NULL,
  `mapped_on` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`map_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webiface_test_workflow_mapping`
--

LOCK TABLES `webiface_test_workflow_mapping` WRITE;
/*!40000 ALTER TABLE `webiface_test_workflow_mapping` DISABLE KEYS */;
INSERT INTO `webiface_test_workflow_mapping` VALUES (1,1,1,'2016-03-07 16:37:53',0),(9,1,1,'2016-03-21 17:33:08',0),(10,1,2,'2016-03-21 19:16:04',1),(11,2,1,'2016-03-21 19:16:22',0),(12,2,1,'2016-03-21 19:17:14',1);
/*!40000 ALTER TABLE `webiface_test_workflow_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webiface_sample_test_mapping`
--

DROP TABLE IF EXISTS `webiface_sample_test_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webiface_sample_test_mapping` (
  `mapp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `mapped_on` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`mapp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webiface_sample_test_mapping`
--

LOCK TABLES `webiface_sample_test_mapping` WRITE;
/*!40000 ALTER TABLE `webiface_sample_test_mapping` DISABLE KEYS */;
INSERT INTO `webiface_sample_test_mapping` VALUES (1,1,1,'2016-03-21 19:00:58',0),(2,1,2,'2016-03-21 19:16:28',0),(3,1,1,'2016-03-21 19:26:57',0),(4,1,1,'2016-03-22 12:03:39',1),(5,2,1,'2016-03-22 12:03:41',1),(6,3,1,'2016-03-22 12:04:06',1),(7,4,1,'2016-03-22 12:04:09',0);
/*!40000 ALTER TABLE `webiface_sample_test_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webiface_sample_master`
--

DROP TABLE IF EXISTS `webiface_sample_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webiface_sample_master` (
  `sample_id` int(11) NOT NULL AUTO_INCREMENT,
  `sequncer_id` int(11) DEFAULT '0',
  `sample_name` varchar(500) DEFAULT NULL,
  `added_on` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webiface_sample_master`
--

LOCK TABLES `webiface_sample_master` WRITE;
/*!40000 ALTER TABLE `webiface_sample_master` DISABLE KEYS */;
INSERT INTO `webiface_sample_master` VALUES (1,1,'awio sample','2016-03-07 15:31:02',1),(2,1,'menitrio honga','2015-03-07 16:32:04',1),(3,1,'Sample awayo','2015-03-07 16:22:04',1),(4,2,'Sample awiy ca','2015-02-07 16:32:04',1);
/*!40000 ALTER TABLE `webiface_sample_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-27 11:07:54
