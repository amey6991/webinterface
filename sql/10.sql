CREATE TABLE `webiface_samplesheet_master` (
  `seq_samplesheet_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NOT NULL,
  `seq_server_path` VARCHAR(255) NOT NULL,
  `seq_local_path` VARCHAR(255) NOT NULL,
  `seq_newfile_name` VARCHAR(255) NOT NULL,
  `added_on` DATETIME NOT NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`seq_samplesheet_id`));


CREATE TABLE `webiface_sequencer_type` (
  `id` INT(11) NOT NULL,
  `seq_type` INT(11) NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `webiface_sequencer_type` 
CHANGE COLUMN `seq_type` `seq_type` VARCHAR(255) NULL DEFAULT NULL ;
INSERT INTO `webiface_sequencer_type` (`id`, `seq_type`, `isValid`) VALUES ('1', 'MiSeq', '1');
INSERT INTO `webiface_sequencer_type` (`id`, `seq_type`, `isValid`) VALUES ('2', 'NextSeq', '1');

CREATE TABLE `webiface_samplesheet_info` (
  `seq_samplesheet_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_type` INT(11) NULL,
  `seq_Server_path` VARCHAR(255) NULL,
  `local_server_path` VARCHAR(255) NULL,
  `experiment_name` VARCHAR(45) NULL,
  `workflow` VARCHAR(45) NULL,
  `sample_id` INT(11) NULL,
  `sample_name` VARCHAR(255) NULL,
  `barcode` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`seq_samplesheet_id`));


