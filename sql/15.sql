ALTER TABLE `webiface_sequencer_master` 
ADD COLUMN `seq_type` INT(11) NULL AFTER `seq_directory_id`,
ADD COLUMN `status` INT(11) NULL AFTER `seq_first_seen_on`;

CREATE TABLE `webiface_sequencer_xml_info_master` (
  `xml_info_master_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_file_type` INT(11) NULL,
  `seq_xml_file_info_object` VARCHAR(3000) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`xml_info_master_id`));
