CREATE TABLE `email_fqdn` (
  `email_id` INT NOT NULL AUTO_INCREMENT,
  `email_addr` VARCHAR(255) NOT NULL,
  `added_on` DATETIME NOT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`email_id`));

CREATE TABLE `email_feature` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email_feature_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `activate` INT(11) NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `email_feature` (`id`, `email_feature_name`, `added_on`, `activate`, `status`) VALUES ('1', 'Email on sequencer error', '2016-02-22 12:45:23', '1', '1');
INSERT INTO `email_feature` (`id`, `email_feature_name`, `added_on`, `activate`, `status`) VALUES ('2', 'Email on QC pipeline failure', '2016-02-22 12:45:23', '1', '1');
INSERT INTO `email_feature` (`id`, `email_feature_name`, `added_on`, `activate`, `status`) VALUES ('3', 'Email on analysis pipeline failure', '2016-02-22 12:45:23', '1', '1');
INSERT INTO `email_feature` (`id`, `email_feature_name`, `added_on`, `activate`, `status`) VALUES ('4', 'Email on sequencer Q30 Failure', '2016-02-22 12:45:23', '1', '1');
INSERT INTO `email_feature` (`id`, `email_feature_name`, `added_on`, `activate`, `status`) VALUES ('5', 'Email on sequencer Passing filter failure', '2016-02-22 12:45:23', '1', '1');

CREATE TABLE `webiface_test_master` (
  `test_id` INT(11) NOT NULL,
  `test_name` VARCHAR(500) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`test_id`));

ALTER TABLE `webiface_test_master` 
CHANGE COLUMN `test_id` `test_id` INT(11) NULL AUTO_INCREMENT ;

CREATE TABLE `webiface_sample_master` (
  `sample_id` INT(11) NULL AUTO_INCREMENT,
  `sample_name` VARCHAR(500) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`sample_id`));

CREATE TABLE `webiface_test_workflow_mapping` (
  `map_id` INT(11) NULL AUTO_INCREMENT,
  `test_id` INT(11) NOT NULL,
  `workflow_id` INT(11) NOT NULL,
  `mapped_on` DATETIME NOT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`map_id`));


