CREATE TABLE `webiface_sequencer_fastq_status_master` (
  `fastq_status_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `scan_status` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`fastq_status_id`));
