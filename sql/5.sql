CREATE TABLE `webiface_sequncer_folder_output` (
  `sequencer_output_folder_id` INT(11) NOT NULL AUTO_INCREMENT,
  `folder_name` VARCHAR(255) NOT NULL,
  `folder_size` VARCHAR(255) NULL,
  `created_date` DATETIME NOT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`sequencer_output_folder_id`));

ALTER TABLE `webiface_sequncer_folder_output` 
CHANGE COLUMN `folder_size` `folder_size` VARCHAR(255) NULL DEFAULT 0 ,
CHANGE COLUMN `status` `status` INT(11) NOT NULL COMMENT '0= waiting\n1 = running (on sequncer)\n2= completed\n' ,
ADD COLUMN `cycle_number` VARCHAR(45) NULL DEFAULT 0 AFTER `folder_size`, RENAME TO  `webiface_sequncer_folder` ;

  CREATE TABLE `webiface_sequncer_sample_output` (
  `sample_output_file_Id` INT(11) NOT NULL AUTO_INCREMENT,
  `sample_id` INT(11) NULL,
  `output_file_name` VARCHAR(255) NULL,
  `output_file_size` VARCHAR(255) NULL,
  `output_date_time` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`sample_output_file_Id`));

  CREATE TABLE `webiface_sample_triggered` (
  `trigger_id` INT(11) NOT NULL AUTO_INCREMENT,
  `sample_output_file_Id` INT(11) NOT NULL,
  `sample_id` INT(11) NOT NULL,
  `workflow_id` INT(11) NULL,
  `trigger_date_time` DATETIME NULL,
  `is_complete` INT(11) NULL,
  `completed_date_time` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`trigger_id`));

CREATE TABLE `webiface_sample_pipeline_output` (
  `pipeline_output_id` INT(11) NOT NULL AUTO_INCREMENT,
  `trigger_id` INT(11) NULL,
  `output_filename` VARCHAR(255) NULL,
  `output_date_time` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`pipeline_output_id`));

CREATE TABLE `webiface_sample_test_mapping` (
  `mapp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `sample_id` INT(11) NULL,
  `test_id` INT(11) NULL,
  `mapped_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`mapp_id`));

ALTER TABLE `webiface_sample_master` 
ADD COLUMN `sequncer_id` INT(11) NULL AFTER `sample_id`;
ALTER TABLE `webiface_sample_master` 
CHANGE COLUMN `sequncer_id` `sequncer_id` INT(11) NULL DEFAULT 0 ;

ALTER TABLE `webiface_sequncer_folder` 
ADD COLUMN `isCompleted` INT(11) NOT NULL DEFAULT 0 AFTER `cycle_number`;
