CREATE TABLE `webiface_state_master` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(500) NULL,
  `extra` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('1', 'Alabama', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('2', 'Alaska', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('3', 'Arkansas', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('4', 'Arizona', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('5', 'Arkansas', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('6', 'California', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('7', 'Colorado', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('8', 'Connecticut', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('9', 'Delaware', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('10', 'Florida', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('11', 'Georgia', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('12', 'Hawaii', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('13', 'Idaho', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('14', 'Illinois', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('15', 'Indiana', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('16', 'Iowa', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('17', 'Kansas', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('18', 'Kentucky', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('19', 'Louisiana', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('20', 'Maine', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('21', 'Maryland', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('22', 'Massachusetts', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('23', 'Michigan', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('24', 'Minnesota', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('25', 'Mississippi', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('26', 'Missouri', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('27', 'Montana', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('28', 'Nebraska', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('29', 'Nevada', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('30', 'New Hampshire', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('31', 'New Jersey', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('32', 'New Mexico', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('33', 'New York', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('34', 'North Carolina', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('35', 'North Dakota', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('36', 'Ohio', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('37', 'Oklahoma', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('38', 'Oregon', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('39', 'Pennsylvania', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('40', 'Rhode Island', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('41', 'South Carolina', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('42', 'South Dakota', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('43', 'Tennessee', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('44', 'Texas', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('45', 'Utah', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('46', 'Vermont', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('47', 'Virginia', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('48', 'Washington', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('49', 'West Virginia', '');
INSERT INTO `webiface_state_master` (`id`, `name`, `extra`) VALUES ('50', 'Wisconsin', '');
INSERT INTO `webiface_state_master` (`id`, `name`) VALUES ('51', 'Wyoming');
