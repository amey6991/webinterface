CREATE TABLE `webiface_sequencer_read_info` (
  `id_read_info` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_file_type` INT(11) NULL,
  `cycle_number` VARCHAR(255) NULL,
  `number` VARCHAR(255) NULL,
  `isIndex` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_read_info`));

CREATE TABLE `webiface_sequencer_xmlread_master_info` (
  `id_master` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_file_type` INT(11) NULL,
  `xml_tag_label` VARCHAR(255) NULL,
  `xml_tag_value` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_master`));

CREATE TABLE `webiface_sequencer_xml_samplesummary_info` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_file_type` INT(11) NULL,
  `sample_id` VARCHAR(255) NULL,
  `sample_name` VARCHAR(255) NULL,
  `sample_number` VARCHAR(255) NULL,
  `sample_numberofclusterspf` VARCHAR(255) NULL,
  `sample_numberofclustersraw` VARCHAR(255) NULL,
  `sample_percentQ30` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id`));
