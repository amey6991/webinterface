ALTER TABLE `webiface_sequencer_test_master` 
CHANGE COLUMN `id_master_test` `test_id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `webiface_sequencer_test_workflow_mapping` 
CHANGE COLUMN `id_test_workflow_mapping` `map_id` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `added_on` `mapped_on` DATETIME NULL DEFAULT NULL ;

ALTER TABLE `webiface_sequencer_workflow` 
CHANGE COLUMN `sequencer_workflow_name` `galaxy_workflow_name` VARCHAR(255) NULL DEFAULT NULL ,
ADD COLUMN `galaxy_workflow_id` VARCHAR(255) NULL AFTER `id_sequencer_workflow`;
