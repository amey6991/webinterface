CREATE TABLE `webiface_sequencer_files_master` (
  `seq_file_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_file_type` INT(11) NULL,
  `seq_server_path` VARCHAR(255) NULL,
  `seq_local_path` VARCHAR(255) NULL,
  `seq_newfile_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`seq_file_id`));