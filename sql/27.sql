CREATE TABLE `webiface_sequencer_galaxy_library_master` (
  `id_galaxy_library` INT(11) NOT NULL,
  `galaxy_library_id` VARCHAR(255) NULL,
  `galaxy_library_name` VARCHAR(255) NULL,
  `galaxy_library_desc` VARCHAR(1000) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_galaxy_library`));

ALTER TABLE `webiface_sequencer_galaxy_library_master` 
CHANGE COLUMN `id_galaxy_library` `id_galaxy_library` INT(11) NOT NULL AUTO_INCREMENT ;


CREATE TABLE `webiface_sequencer_galaxy_library_folder_master` (
  `id_galaxy_folder` INT(11) NOT NULL AUTO_INCREMENT,
  `galaxy_library_id` VARCHAR(255) NULL,
  `galaxy_folder_id` VARCHAR(255) NULL,
  `galaxy_folder_name` VARCHAR(255) NULL,
  `galaxy_folder_desc` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_galaxy_folder`));

CREATE TABLE `webiface_sequencer_galaxy_library_folder_dataset_master` (
  `id_galaxy_dataset` INT(11) NOT NULL AUTO_INCREMENT,
  `galaxy_dataset_type` INT(11) NULL,
  `galaxy_library_id` VARCHAR(255) NULL,
  `galaxy_folder_id` VARCHAR(255) NULL,
  `galaxy_fastq_path` VARCHAR(500) NULL,
  `galaxy_dataset_url` VARCHAR(255) NULL,
  `galaxy_dataset_name` VARCHAR(255) NULL,
  `galaxy_dataset_id` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id_galaxy_dataset`));

CREATE TABLE `webiface_sequencer_galaxy_history_master` (
  `id_galaxy_history` INT(11) NOT NULL,
  `galaxy_history_id` VARCHAR(255) NULL,
  `galaxy_history_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_galaxy_history`));

CREATE TABLE `webiface_sequencer_galaxy_history_dataset_master` (
  `id_history_dataset` INT(11) NOT NULL,
  `galaxy_history_table_id` VARCHAR(255) NULL,
  `galaxy_dataset_id` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_history_dataset`));

ALTER TABLE `webiface_sequencer_galaxy_history_master` 
CHANGE COLUMN `id_galaxy_history` `id_galaxy_history` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `webiface_sequencer_galaxy_library_folder_dataset_master` 
DROP COLUMN `galaxy_dataset_name`,
DROP COLUMN `galaxy_dataset_url`;

CREATE TABLE `webiface_sequencer_sample_test_mapping` (
  `mapp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `sample_info_table_id` INT(11) NULL,
  `test_id` INT(11) NULL,
  `mapped_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`mapp_id`));
