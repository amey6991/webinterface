CREATE TABLE `webiface_sequencer_master_info` (
  `id_master_info` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_directory_size` VARCHAR(255) NULL,
  `seq_cycle_number` VARCHAR(255) NULL,
  `seq_run_id` VARCHAR(255) NULL,
  `seq_scanner_id` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_master_info`));
