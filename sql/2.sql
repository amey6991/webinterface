CREATE TABLE `webiface_users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NULL,
  `full_name` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `mobile_no` VARCHAR(50) NULL,
  `email` VARCHAR(100) NULL,
  `status` INT(11) NULL DEFAULT 1,
  `deleted` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`user_id`));

ALTER TABLE `webiface_users` 
ADD COLUMN `added_on` DATETIME NULL AFTER `email`,
ADD COLUMN `added_by` INT(11) NULL AFTER `added_on`;
ALTER TABLE `webiface_users` 
ADD COLUMN `user_type_id` INT(11) NULL AFTER `full_name`;
ALTER TABLE `webiface_users` 
CHANGE COLUMN `user_type_id` `client_id` INT(11) NULL DEFAULT NULL ;


CREATE TABLE `webiface_client` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client_name` VARCHAR(500) NULL,
  `client_email` VARCHAR(500) NULL,
  `client_mobile` VARCHAR(500) NULL,
  `client_country` VARCHAR(500) NULL,
  `client_state` VARCHAR(500) NULL,
  `client_city` VARCHAR(500) NULL,
  `client_url` VARCHAR(500) NULL,
  `client_address` VARCHAR(500) NULL,
  `client_added_by` VARCHAR(500) NULL,
  `client_added_on` VARCHAR(500) NULL,
  `status` VARCHAR(500) NULL DEFAULT '1',
  `deleted` VARCHAR(500) NULL DEFAULT 0,
  PRIMARY KEY (`id`));

ALTER TABLE `webiface_client` 
CHANGE COLUMN `id` `client_id` INT(11) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `webiface_users` 
ADD COLUMN `user_type_id` INT(11) NULL AFTER `full_name`;

ALTER TABLE `webiface_client` 
ADD COLUMN `person_name` VARCHAR(500) NULL AFTER `client_address`,
ADD COLUMN `person_email` VARCHAR(100) NULL AFTER `person_name`,
ADD COLUMN `person_mobile` VARCHAR(100) NULL AFTER `person_email`;

CREATE TABLE `webiface_user_files` (
  `map_id` INT(11) NOT NULL,
  `user_id` INT(11) NULL,
  `file_id` INT(11) NULL,
  `deleted` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`map_id`));

ALTER TABLE `webiface_user_files` 
CHANGE COLUMN `map_id` `map_id` INT(11) NULL AUTO_INCREMENT ;



