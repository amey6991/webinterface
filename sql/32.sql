CREATE TABLE `webiface_sequencer_galaxy_history_output` (
  `id_history_output` INT(11) NOT NULL,
  `galaxy_history_table_id` INT(11) NULL,
  `galaxy_history_output_index` INT(11) NULL,
  `galaxy_history_output_id` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_history_output`));

ALTER TABLE `webiface_sequencer_galaxy_history_output` 
CHANGE COLUMN `id_history_output` `id_history_output` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `webiface_sequencer_galaxy_history_output` 
CHANGE COLUMN `galaxy_history_output_id` `galaxy_history_output_id` VARCHAR(255) NULL DEFAULT NULL ;
