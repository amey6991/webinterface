CREATE TABLE `webiface_sequencer_xml_master` (
  `seq_xml_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_xml_type` INT(11) NULL COMMENT '1-runparameter,2-runInfo,3-completedJobInfo',
  `seq_new_filename` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`seq_xml_id`));
ALTER TABLE `webiface_sequencer_xml_master` 
ADD COLUMN `seq_server_path` VARCHAR(255) NULL AFTER `seq_xml_type`,
ADD COLUMN `seq_local_path` VARCHAR(255) NULL AFTER `seq_server_path`;
