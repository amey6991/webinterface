ALTER TABLE `webiface_sequncer_folder` 
ADD COLUMN `started_on` DATETIME NULL AFTER `isCompleted`,
ADD COLUMN `completed_on` DATETIME NULL AFTER `started_on`;

ALTER TABLE `webiface_sequncer_folder` 
CHANGE COLUMN `started_on` `started_on` DATE NULL DEFAULT NULL ,
ADD COLUMN `run_id` VARCHAR(255) NULL AFTER `completed_on`,
ADD COLUMN `scanner_id` VARCHAR(255) NULL AFTER `run_id`,
ADD COLUMN `barcode` VARCHAR(255) NULL AFTER `scanner_id`,
ADD COLUMN `experiment_name` VARCHAR(255) NULL AFTER `barcode`,
ADD COLUMN `run_number` INT(11) NULL AFTER `experiment_name`,
ADD COLUMN `fpgaversion` VARCHAR(255) NULL AFTER `run_number`,
ADD COLUMN `mcsversion` VARCHAR(255) NULL AFTER `fpgaversion`,
ADD COLUMN `rtaversion` VARCHAR(255) NULL AFTER `mcsversion`,
ADD COLUMN `pr2bottlebarcode` VARCHAR(255) NULL AFTER `rtaversion`,
ADD COLUMN `reagentkitpartnumberentered` VARCHAR(255) NULL AFTER `pr2bottlebarcode`,
ADD COLUMN `reagentkitversion` VARCHAR(255) NULL AFTER `reagentkitpartnumberentered`,
ADD COLUMN `reagentkitbarcode` VARCHAR(255) NULL AFTER `reagentkitversion`;
