-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2015 at 11:16 AM
-- Server version: 5.6.21-1+deb.sury.org~precise+1
-- PHP Version: 5.5.21-1+deb.sury.org~precise+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webiface_pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `webiface_login_instance`
--

CREATE TABLE `webiface_login_instance` (
  `instance_id` bigint(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_date` date NOT NULL,
  `login_time` time NOT NULL,
  `logout_time` time DEFAULT NULL,
  `browser` varchar(300) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `device_type` varchar(150) DEFAULT NULL,
  `user_session_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `webiface_password_reset`
--

CREATE TABLE `webiface_password_reset` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  `req_date` datetime NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `webiface_user_logins`
--

CREATE TABLE `webiface_user_logins` (
  `login_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `webiface_user_logins`
--

INSERT INTO `webiface_user_logins` (`login_id`, `username`, `password`, `user_id`) VALUES
(3, 'admin', 'bd143ac9ca29ea42dcf54cafa4b3a65900ee92aea646f5f521a640db68174666dae34aa1330e5a7ee76802a0a3f119e8', 3);

-- --------------------------------------------------------

--
-- Table structure for table `webiface_user_profiles`
--

CREATE TABLE `webiface_user_profiles` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `login_id` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `user_type_id` int(1) NOT NULL,
  `createdOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `webiface_user_profiles`
--

INSERT INTO `webiface_user_profiles` (`user_id`, `login_id`, `name`, `email`, `staff_id`, `user_type_id`, `createdOn`) VALUES
(3, '3', 'Amey Damle', 'amey.damle@plus91.in', '', 1, '2015-12-12 17:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `webiface_user_type`
--

CREATE TABLE `webiface_user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `webiface_user_type`
--

INSERT INTO `webiface_user_type` (`user_type_id`, `user_type`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Pharmacist'),
(4, 'Sales Person'),
(5, 'Stockist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `webiface_login_instance`
--
ALTER TABLE `webiface_login_instance`
  ADD PRIMARY KEY (`instance_id`),
  ADD KEY `login_date` (`login_date`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `webiface_password_reset`
--
ALTER TABLE `webiface_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `token_2` (`token`),
  ADD KEY `used` (`used`);

--
-- Indexes for table `webiface_user_logins`
--
ALTER TABLE `webiface_user_logins`
  ADD PRIMARY KEY (`login_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `profile_id` (`user_id`),
  ADD KEY `profile_id_2` (`user_id`),
  ADD KEY `username_2` (`username`);

--
-- Indexes for table `webiface_user_profiles`
--
ALTER TABLE `webiface_user_profiles`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `login_id` (`login_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `webiface_user_type`
--
ALTER TABLE `webiface_user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `webiface_login_instance`
--
ALTER TABLE `webiface_login_instance`
  MODIFY `instance_id` bigint(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `webiface_password_reset`
--
ALTER TABLE `webiface_password_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `webiface_user_logins`
--
ALTER TABLE `webiface_user_logins`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `webiface_user_profiles`
--
ALTER TABLE `webiface_user_profiles`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `webiface_user_type`
--
ALTER TABLE `webiface_user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;