CREATE TABLE `webiface_sequencer_error_master` (
  `id_error_master` INT(11) NOT NULL AUTO_INCREMENT,
  `sequencer_id` INT(11) NULL,
  `seq_type` INT(11) NULL,
  `error_msg` VARCHAR(2000) NULL,
  `added_on` INT(11) NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_error_master`));
ALTER TABLE `webiface_sequencer_error_master` 
CHANGE COLUMN `added_on` `added_on` DATETIME NULL DEFAULT NULL ;
