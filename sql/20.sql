ALTER TABLE `webiface_sequencer_summary_json` 
ADD COLUMN `seq_interop_type` INT(11) NULL AFTER `seq_directory_id`, RENAME TO  `webiface_sequencer_interop_csv_json` ;

CREATE TABLE `webiface_sequencer_other_info` (
  `id_master_other` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `cycle_number` INT(11) NULL,
  `q30` VARCHAR(255) NULL,
  `passingfilter_clusterpf` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_master_other`));

ALTER TABLE `webiface_sequencer_parse_interop_master` 
ADD COLUMN `seq_local_filepath` VARCHAR(255) NULL DEFAULT '' AFTER `seq_parsed_filename`;
