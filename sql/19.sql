CREATE TABLE `webiface_sequencer_parse_interop_status` (
  `id_master_interop` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_parse_status` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_master_interop`));

CREATE TABLE `webiface_sequencer_parse_interop_master` (
  `id_info_interop` INT NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_parse_file_path` VARCHAR(255) NULL,
  `seq_parsed_filename` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_info_interop`));

CREATE TABLE `webiface_sequencer_summary_json` (
  `seq_json_master_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_summary_json` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`seq_json_master_id`));

ALTER TABLE `webiface_sequencer_summary_json` 
CHANGE COLUMN `seq_summary_json` `seq_summary_json` LONGTEXT NULL DEFAULT NULL ;

CREATE TABLE `webiface_sequencer_extraction_status` (
  `id_extraction_master` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_extraction_status` INT(11) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_extraction_master`));

ALTER TABLE `webiface_sequencer_parse_interop_master` 
ADD COLUMN `seq_parse_type` INT(11) NULL AFTER `seq_directory_id`;
