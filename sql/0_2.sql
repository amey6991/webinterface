
CREATE TABLE `email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `e_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `added_on` datetime DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `e_id` (`e_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;




CREATE TABLE `email_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `app` int(11) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_email` varchar(100) DEFAULT NULL,
  `reply_to` varchar(100) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `plain_text` mediumtext,
  `html_text` mediumtext,
  `sent` varchar(100) DEFAULT '',
  `to_send` int(100) DEFAULT NULL,
  `to_send_lists` text,
  `recipients` int(100) DEFAULT '0',
  `timeout_check` varchar(100) DEFAULT NULL,
  `opens` longtext,
  `wysiwyg` int(11) DEFAULT '0',
  `send_date` varchar(100) DEFAULT NULL,
  `lists` text,
  `timezone` varchar(100) DEFAULT NULL,
  `errors` longtext,
  `bounce_setup` int(11) DEFAULT '0',
  `complaint_setup` int(11) DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `email_attachements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `e_id` int(11) DEFAULT NULL,
  `real_filename` varchar(150) DEFAULT NULL,
  `generated_filename` varchar(150) DEFAULT NULL,
  `file_path` varchar(150) DEFAULT NULL,
  `content_type` varchar(200) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `file_extension` varchar(100) DEFAULT NULL,
  `CID` varchar(100) NOT NULL COMMENT 'Embedded Image cid',
  `status` tinyint(1) NOT NULL COMMENT '1 for simple attachment and 2 for Embedded attachement ',
  `valid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `e_id` (`e_id`),
  KEY `valid` (`valid`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `email_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `e_id` int(11) DEFAULT NULL,
  `exception_id` int(11) DEFAULT NULL,
  `exception_msg` varchar(150) DEFAULT NULL,
  `attempt_on` datetime DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `e_id` (`e_id`),
  KEY `attempt_on` (`attempt_on`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;



CREATE TABLE `email_exception_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `extra` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

