CREATE DATABASE  IF NOT EXISTS `app_webinterface` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `app_webinterface`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: app_webinterface
-- ------------------------------------------------------
-- Server version	5.6.24-2+deb.sury.org~trusty+2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_perm_matrix`
--

DROP TABLE IF EXISTS `auth_perm_matrix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_perm_matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_entity_type` tinyint(1) NOT NULL COMMENT 'Type of user_entity_id, 0=user type id, 1=staff type id, 2=user_id',
  `user_entity_id` int(11) NOT NULL COMMENT 'Value interpretation depends on user_entity_type , eg. 0=user_type_id, 1=staff_type_id, 2=user_id',
  `perm_entity_type` tinyint(1) NOT NULL COMMENT 'Type of user_entity_id, 0=page_id, 1=feature_id, 2=action_id',
  `perm_entity_id` int(11) NOT NULL,
  `perm_level` tinyint(1) NOT NULL,
  `added_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_entity_type` (`user_entity_type`,`user_entity_id`,`perm_entity_type`,`perm_entity_id`,`perm_level`),
  KEY `user_entity_type_3` (`user_entity_type`,`user_entity_id`),
  KEY `perm_entity_type` (`perm_entity_type`,`perm_entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_perm_matrix`
--

LOCK TABLES `auth_perm_matrix` WRITE;
/*!40000 ALTER TABLE `auth_perm_matrix` DISABLE KEYS */;
INSERT INTO `auth_perm_matrix` VALUES (1,1,1,0,1,2,'2016-01-11 17:07:56','2016-01-11 17:07:56',0),(2,1,1,0,2,2,'2016-01-11 17:08:16','2016-01-11 17:08:16',0),(3,1,1,0,3,2,'2016-01-11 17:08:33','2016-01-11 17:08:33',0),(4,1,1,0,4,2,'2016-01-11 17:08:45','2016-01-11 17:08:45',0),(5,1,1,1,1,2,'2016-01-11 17:30:13','2016-01-11 17:30:13',0),(6,1,2,0,1,1,'2016-01-11 17:49:00','2016-01-11 17:49:00',0),(7,1,2,1,6,2,'2016-01-11 17:54:10','2016-01-11 17:54:10',0),(8,1,1,1,2,2,'2016-01-11 18:00:04','2016-01-11 18:00:04',0),(9,1,1,1,3,2,'2016-01-11 18:02:41','2016-01-11 18:02:41',0),(10,1,1,1,4,2,'2016-01-11 18:02:57','2016-01-11 18:02:57',0),(11,1,1,1,5,2,'2016-01-11 18:03:15','2016-01-11 18:03:15',0),(12,1,2,1,3,2,'2016-01-11 18:03:33','2016-01-11 18:03:33',0),(13,1,2,1,4,2,'2016-01-11 18:03:43','2016-01-11 18:03:43',0),(14,1,2,1,5,2,'2016-01-11 18:03:56','2016-01-11 18:03:56',0),(15,1,3,0,1,1,'2016-01-12 10:49:10','2016-01-12 10:49:10',0),(16,1,3,0,5,2,'2016-01-15 12:23:07','2016-01-15 12:23:07',0),(17,1,3,0,6,2,'2016-01-15 12:23:23','2016-01-15 12:23:23',0),(18,1,1,1,6,2,'2016-01-19 12:01:39','2016-01-19 12:01:39',0),(19,1,2,0,6,2,'2016-02-11 11:42:33','2016-02-11 11:42:33',0),(20,1,2,0,7,2,'2016-02-15 17:02:44','2016-02-15 17:02:44',0),(21,1,2,1,7,2,'2016-02-15 17:08:42','2016-02-15 17:08:42',0),(22,1,2,0,8,2,'2016-02-15 17:34:16','2016-02-15 17:34:16',0),(23,1,2,1,8,2,'2016-02-16 11:43:40','2016-02-16 11:43:40',0),(24,1,2,1,9,2,'2016-02-16 11:43:59','2016-02-16 11:43:59',0),(25,1,2,1,10,2,'2016-02-16 11:44:16','2016-02-16 11:44:16',0),(26,1,2,1,11,2,'2016-02-16 19:05:06','2016-02-16 19:05:06',0),(27,1,3,1,11,2,'2016-02-19 16:13:41','2016-02-19 16:13:41',0),(28,1,2,0,9,2,'2016-03-04 13:24:03','2016-03-04 13:24:03',0),(30,1,2,0,10,2,'2016-03-07 12:34:12','2016-03-07 12:34:12',0),(33,1,2,0,11,2,'2016-03-07 13:51:50','2016-03-07 13:51:50',0),(34,1,2,0,12,2,'2016-03-07 13:52:06','2016-03-07 13:52:06',0),(35,1,2,0,13,2,'2016-03-07 13:52:20','2016-03-07 13:52:20',0),(36,1,2,0,14,2,'2016-03-07 13:52:32','2016-03-07 13:52:32',0),(37,1,2,0,15,2,'2016-03-21 19:12:55','2016-03-21 19:12:55',0),(38,1,1,1,13,2,'2016-04-29 19:49:28','2016-04-29 19:49:28',0),(39,1,1,1,14,2,'2016-04-29 19:49:46','2016-04-29 19:49:46',0),(40,1,1,1,12,2,'2016-04-29 19:50:03','2016-04-29 19:50:03',0),(41,1,1,0,9,2,'2016-04-29 19:53:31','2016-04-29 19:53:31',0),(42,1,1,0,10,2,'2016-04-29 19:53:54','2016-04-29 19:53:54',0),(43,1,1,0,11,2,'2016-04-29 19:54:11','2016-04-29 19:54:11',0),(44,1,1,0,12,2,'2016-04-29 19:54:31','2016-04-29 19:54:31',0),(45,1,1,0,13,2,'2016-04-29 19:54:48','2016-04-29 19:54:48',0),(46,1,1,0,14,2,'2016-04-29 19:55:01','2016-04-29 19:55:01',0),(47,1,1,0,15,2,'2016-04-29 19:56:14','2016-04-29 19:56:14',0);
/*!40000 ALTER TABLE `auth_perm_matrix` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-03 10:59:17