-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2015 at 12:22 PM
-- Server version: 5.6.21-1+deb.sury.org~precise+1
-- PHP Version: 5.5.18-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_webinterface`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_action_master`
--

CREATE TABLE IF NOT EXISTS `auth_action_master` (
`action_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `action_code` varchar(100) NOT NULL COMMENT 'a unique action code',
  `action_name` varchar(250) NOT NULL,
  `notes` varchar(2500) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_feature_master`
--

CREATE TABLE IF NOT EXISTS `auth_feature_master` (
`feature_id` int(11) NOT NULL,
  `feature_code` varchar(100) NOT NULL COMMENT 'a unique feature code',
  `feature_name` varchar(250) NOT NULL,
  `notes` varchar(2500) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_page_master`
--

CREATE TABLE IF NOT EXISTS `auth_page_master` (
`page_id` int(11) NOT NULL,
  `page_code` varchar(100) NOT NULL COMMENT 'a unique page code',
  `page_name` varchar(250) NOT NULL,
  `notes` varchar(2500) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_perm_matrix`
--

CREATE TABLE IF NOT EXISTS `auth_perm_matrix` (
`id` int(11) NOT NULL,
  `user_entity_type` tinyint(1) NOT NULL COMMENT 'Type of user_entity_id, 0=user type id, 1=staff type id, 2=user_id',
  `user_entity_id` int(11) NOT NULL COMMENT 'Value interpretation depends on user_entity_type , eg. 0=user_type_id, 1=staff_type_id, 2=user_id',
  `perm_entity_type` tinyint(1) NOT NULL COMMENT 'Type of user_entity_id, 0=page_id, 1=feature_id, 2=action_id',
  `perm_entity_id` int(11) NOT NULL,
  `perm_level` tinyint(1) NOT NULL,
  `added_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_action_master`
--
ALTER TABLE `auth_action_master`
 ADD PRIMARY KEY (`action_id`), ADD UNIQUE KEY `feature_code` (`action_code`), ADD KEY `feature_id` (`feature_id`);

--
-- Indexes for table `auth_feature_master`
--
ALTER TABLE `auth_feature_master`
 ADD PRIMARY KEY (`feature_id`), ADD UNIQUE KEY `feature_code` (`feature_code`);

--
-- Indexes for table `auth_page_master`
--
ALTER TABLE `auth_page_master`
 ADD PRIMARY KEY (`page_id`), ADD UNIQUE KEY `page_code` (`page_code`);

--
-- Indexes for table `auth_perm_matrix`
--
ALTER TABLE `auth_perm_matrix`
 ADD PRIMARY KEY (`id`), ADD KEY `user_entity_type` (`user_entity_type`,`user_entity_id`,`perm_entity_type`,`perm_entity_id`,`perm_level`), ADD KEY `user_entity_type_3` (`user_entity_type`,`user_entity_id`), ADD KEY `perm_entity_type` (`perm_entity_type`,`perm_entity_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_action_master`
--
ALTER TABLE `auth_action_master`
MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_feature_master`
--
ALTER TABLE `auth_feature_master`
MODIFY `feature_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_page_master`
--
ALTER TABLE `auth_page_master`
MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_perm_matrix`
--
ALTER TABLE `auth_perm_matrix`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
