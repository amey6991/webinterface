ALTER TABLE `webiface_sequencer_master_info` 
ADD COLUMN `seq_density` VARCHAR(255) NULL AFTER `seq_PF`;

ALTER TABLE `webiface_sequencer_master_info` 
ADD COLUMN `seq_finished_on` DATETIME NULL AFTER `seq_scanner_id`;
