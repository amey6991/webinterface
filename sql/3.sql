CREATE TABLE `email_list` (
  `list_id` INT(11) NOT NULL AUTO_INCREMENT,
  `list_name` VARCHAR(255) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`list_id`));
ALTER TABLE `email_list` 
ADD COLUMN `is_active` INT(11) NOT NULL DEFAULT 0 AFTER `status`;

CREATE TABLE `email_subscriber` (
  `subs_id` INT(11) NOT NULL AUTO_INCREMENT,
  `list_id` VARCHAR(45) NOT NULL,
  `subs_name` VARCHAR(255) NULL,
  `subs_email` VARCHAR(255) NULL,
  `status` INT(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`subs_id`));

ALTER TABLE `mxcel_client` 
RENAME TO  `webiface_client` ;
ALTER TABLE `mxcel_files` 
RENAME TO  `webiface_files` ;
ALTER TABLE `mxcel_process` 
RENAME TO  `webiface_process` ;
ALTER TABLE `mxcel_process_log` 
RENAME TO  `webiface_process_log` ;
ALTER TABLE `mxcel_steps` 
RENAME TO  `webiface_steps` ;
ALTER TABLE `mxcel_workflow` 
RENAME TO  `mxcel_workflow` ;
ALTER TABLE `mxcel_user_type` 
RENAME TO  `webiface_user_type` ;
ALTER TABLE `mxcel_user_logins` 
RENAME TO  `webiface_user_logins` ;
ALTER TABLE `mxcel_user_files` 
RENAME TO  `webiface_user_files` ;
ALTER TABLE `mxcel_password_reset` 
RENAME TO  `webiface_password_reset` ;
ALTER TABLE `mxcel_login_instance` 
RENAME TO  `webiface_login_instance` ;
ALTER TABLE `mxcel_users` 
RENAME TO  `webiface_users` ;


