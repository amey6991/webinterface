ALTER TABLE `webiface_sequencer_master_info` 
ADD COLUMN `seq_q30` VARCHAR(255) NULL AFTER `seq_cycle_number`,
ADD COLUMN `seq_PF` VARCHAR(235) NULL AFTER `seq_q30`,
ADD COLUMN `updated_on` DATETIME NULL AFTER `added_on`;

CREATE TABLE `webiface_sequencer_bcl2log_log_msg` (
  `id_bcl2_master` INT NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_msg_type` INT(11) NULL,
  `seq_log_masg` VARCHAR(2000) NULL,
  `added_on` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`id_bcl2_master`));

CREATE TABLE `webiface_sequencer_nextseq_bcl2fastq_status` (
  `id_master_status` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_directory_id` INT(11) NULL,
  `seq_log_status` INT(11) NULL,
  `added_on` DATETIME NULL,
  `isValid` INT(11) NULL,
  PRIMARY KEY (`id_master_status`));
