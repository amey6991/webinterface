ALTER TABLE `webiface_sequencer_samplesheet_info_master` 
ADD COLUMN `assay` VARCHAR(255) NULL AFTER `cycle_no`,
ADD COLUMN `desc` VARCHAR(255) NULL AFTER `assay`,
ADD COLUMN `chemistry` VARCHAR(255) NULL AFTER `desc`;

ALTER TABLE `webiface_sequencer_sample_info_master` 
CHANGE COLUMN `index_barcode` `index` VARCHAR(255) NULL DEFAULT NULL ,
ADD COLUMN `sample_name_barcode` VARCHAR(255) NULL AFTER `sample_ID`,
ADD COLUMN `index_15_id` VARCHAR(255) NULL AFTER `index`,
ADD COLUMN `index2` VARCHAR(255) NULL AFTER `index_15_id`;

CREATE TABLE `webiface_sequencer_samplesheet_object_info` (
  `seq_samplesheet_object_master_id` INT(11) NOT NULL,
  `seq_directory_id` INT(11) NULL,
  `seq_samplesheet_json` VARCHAR(2000) NULL,
  `added_date` DATETIME NULL,
  `status` INT(11) NULL,
  PRIMARY KEY (`seq_samplesheet_object_master_id`));

ALTER TABLE `webiface_sequencer_samplesheet_info_master` 
ADD COLUMN `seq_samplesheet_object_master_id` INT(11) NULL AFTER `seq_samplesheet_master_id`;

ALTER TABLE `webiface_sequencer_samplesheet_object_info` 
CHANGE COLUMN `seq_samplesheet_object_master_id` `seq_samplesheet_object_master_id` INT(11) NOT NULL AUTO_INCREMENT ;

