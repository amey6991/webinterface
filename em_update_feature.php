<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$iFeatureId = $_POST['feature_id'];
	$sFeatureCode = $_POST['feature_code'];
	$sFeatureName = $_POST['feature_name'];
	$sFeatureNote = $_POST['feature_notes'];
	
	$bResult = fUpdateFeature($iFeatureId,$sFeatureCode,$sFeatureName,$sFeatureNote);
	
	if($bResult)
	{
		$sMsg = array();
		$sMsg[] = "S7";
	    redirectWithAlert("viewFeatures.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E7";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("updateFeature.php?iFeatureID=".$iFeatureId, $sMsg);
		}
?>