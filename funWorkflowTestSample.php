<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";

    // Manage Workflow

    function addWorkflow($sWrokflowName){
	    $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sWorkflowTable = DATABASE_TABLE_PREFIX.'_workflow';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sWorkflowTable}`(`wf_id`,`workflow_name`,`added_on`,`status`)
                    VALUES (NULL,'{$sWrokflowName}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;
    }

    function getWorkflowList(){
    	$aWorkflowData = array();
        $sWorkflowTable = DATABASE_TABLE_PREFIX.'_sequencer_workflow';
		// $sWorkflowTable = DATABASE_TABLE_PREFIX.'_workflow';
        
		$sSQuery = "SELECT * FROM `{$sWorkflowTable}` WHERE `status`=1 ";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aWorkflowData[]=$aRow;
                }
            }
    	}
    	return $aWorkflowData;
    }

    function updateWorkflow($iWorkflowID,$sWrokflowName){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sWorkflowTable = DATABASE_TABLE_PREFIX.'_workflow';
		$sUQuery = "UPDATE `{$sWorkflowTable}` SET `workflow_name`='{$sWrokflowName}' WHERE `wf_id`='{$iWorkflowID}' AND `status`=1";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }


//!! Add the TestName in Master Entry
    function addTestMaster($sTestName){
	    $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTestMasterTable = DATABASE_TABLE_PREFIX.'_test_master';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sTestMasterTable}`(`test_id`,`test_name`,`added_on`,`status`)
                    VALUES (NULL,'{$sTestName}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;
    }


    function getTestList(){
    	$aTestList = array();
		$sTestMasterTable = DATABASE_TABLE_PREFIX.'_test_master';
        
		$sSQuery = "SELECT * FROM `{$sTestMasterTable}`";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aTestList[]=$aRow;
                }
            }
    	}
    	return $aTestList;
    }

    function freezTest($iTestID){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sTestMasterTable = DATABASE_TABLE_PREFIX.'_test_master';
		$sUQuery = "UPDATE `{$sTestMasterTable}` SET `status`='0' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
    function unFreezTest($iTestID){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sTestMasterTable = DATABASE_TABLE_PREFIX.'_test_master';
		$sUQuery = "UPDATE `{$sTestMasterTable}` SET `status`='1' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

    // Manage Samples
    // Sample with SequncerID
    function addSampleMaster($sSampleName,$iSampleSequncerID=0){
	    $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSampleMasterTable = DATABASE_TABLE_PREFIX.'_sample_master';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        if($iSampleSequncerID>0){
            $sIQuery = "INSERT INTO `{$sSampleMasterTable}`(`sample_id`,`sequncer_id`,`sample_name`,`added_on`,`status`)
                    VALUES (NULL,'{$iSampleSequncerID}','{$sSampleName}','{$dAdedOn}','1')";    
        }
        $sIQuery = "INSERT INTO `{$sSampleMasterTable}`(`sample_id`,`sample_name`,`added_on`,`status`)
                    VALUES (NULL,'{$sSampleName}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;
    }

    function getSampleList(){
    	$aSampleData = array();
		$sTestMasterTable = DATABASE_TABLE_PREFIX.'_sample_master';
        $sSQuery = "SELECT * FROM `{$sTestMasterTable}`";
		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aSampleData[]=$aRow;
                }
            }
    	}
    	return $aSampleData;
    }

    function updateSample($iSampleID,$sSampleName){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sSampleTable = DATABASE_TABLE_PREFIX.'_sample_master';
		$sUQuery = "UPDATE `{$sSampleTable}` SET `sample_name`='{$sSampleName}' WHERE `sample_id`='{$iSampleID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

    function freezSample($iSampleID){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sSampleMasterTable = DATABASE_TABLE_PREFIX.'_sample_master';
		$sUQuery = "UPDATE `{$sSampleMasterTable}` SET `status`='0' WHERE `sample_id`='{$iSampleID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
    function unFreezSample($iSampleID){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sSampleMasterTable = DATABASE_TABLE_PREFIX.'_sample_master';
		$sUQuery = "UPDATE `{$sSampleMasterTable}` SET `status`='1' WHERE `sample_id`='{$iSampleID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

    function mapWorkflowWithTest($iTestID,$iWorkflowID){
    	$bResult = unmapWorkflowForTest($iTestID);
    	$iInsertID=0;
    	if($bResult){
    		$DBMan = new DBConnManager();
	        $conn =  $DBMan->getConnInstance();
	        $sMapTable = DATABASE_TABLE_PREFIX.'_test_workflow_mapping';
	        $dAdedOn=date(DB_DATETIME_FORMATE);
	        $sIQuery = "INSERT INTO `{$sMapTable}`(`map_id`,`test_id`,`workflow_id`,`mapped_on`,`status`)
	                    VALUES (NULL,'{$iTestID}','{$iWorkflowID}','{$dAdedOn}','1')";
	        
	        $sResult = $conn->query($sIQuery);        
	        if($sResult){
	            $iInsertID = $conn->insert_id;
	        }
	        return $iInsertID;	
    	}else{
    		return $iInsertID;
    	}
    	
    }
    function unmapWorkflowForTest($iTestID){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
    	$sMappedTable = DATABASE_TABLE_PREFIX.'_test_workflow_mapping';
		$sUQuery = "UPDATE `{$sMappedTable}` SET `status`='0' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

    function getMappedWorkflowIDForTestID($iTestID){
    	$iWorkflowID=0;
    	$sTestSampleMapTable = DATABASE_TABLE_PREFIX.'_test_workflow_mapping';
        $sSQuery = "SELECT `workflow_id` FROM `{$sTestSampleMapTable}` WHERE `test_id`='{$iTestID}' AND `status`=1";
		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iWorkflowID=$aRow['workflow_id'];
            }
    	}
    	return $iWorkflowID;
    }

    // mapp and unmapp the sample and Test
    function mapSampleWithTest($iTestID,$iSampleID){
        $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sMapTable = DATABASE_TABLE_PREFIX.'_sample_test_mapping';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sMapTable}`(`mapp_id`,`sample_id`,`test_id`,`mapped_on`,`status`)
                    VALUES (NULL,'{$iSampleID}','{$iTestID}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;  
    }
    function unMapSampleForTest($iTestID,$iSampleID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sMappedTable = DATABASE_TABLE_PREFIX.'_sample_test_mapping';
        $sUQuery = "UPDATE `{$sMappedTable}` SET `status`='0' WHERE `test_id`='{$iTestID}' AND `sample_id`='{$iSampleID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
    function getMappedSampleIDForTestID($iTestID){
        $aSampleID=array();
        $sTestSampleMapTable = DATABASE_TABLE_PREFIX.'_sample_test_mapping';
        $sSQuery = "SELECT `sample_id` FROM `{$sTestSampleMapTable}` WHERE `test_id`='{$iTestID}' AND `status`=1";
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSampleID[]=$aRow['sample_id'];
                }
            }
        }
        return $aSampleID;
    }
?>