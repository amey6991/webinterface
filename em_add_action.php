<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$ifeatureID = $_POST['feature_id'];
	$sfeatureName = $_POST['feature_name'];
	$sActionCode = $_POST['action_code'];
	$sActionName = $_POST['action_name'];
	$sActionNote = $_POST['action_notes'];
	
	$iInsertID = fAddAction($ifeatureID,$sActionCode,$sActionName,$sActionNote);
	
	if($iInsertID>0)
	{
		$sMsg = array();
		$sMsg[] = "S10";
	    redirectWithAlert("viewActions.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E10";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("addAction.php", $sMsg);
		}
?>