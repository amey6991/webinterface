<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
$sessionManager = new SessionManager();
//! Logout the user
$sessionManager->logout();
//! bring him to login page
header('Location: login.php');

?>