<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$sPageTitle = "View Clients";
$aClients = fGetAllClients();
$iCountClient = count($aClients);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Client</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Client Name</th>
                                                <th>Client Email</th>
                                                <th>Client Contact</th>
                                                <th>Client Country</th>
                                                <th>Client City</th>
                                                <th>Client URL</th>
                                                <th>Update</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountClient;$iii++){
                                                    $iClientID = $aClients[$iii]['client_id'];
                                                    $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iClientID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td><?php echo $aClients[$iii]['client_name'];?></td>
                                                <td><?php echo $aClients[$iii]['client_email'];?></td>
                                                <td><?php echo $aClients[$iii]['client_mobile'];?></td>
                                                <td><?php echo $aClients[$iii]['client_country'];?></td>
                                                <td><?php echo $aClients[$iii]['client_city'];?></td>
                                                <td><?php echo $aClients[$iii]['client_url'];?></td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".classUpdateButton").click(function(){
            var iClientID = this.id;
            window.location="updateClient.php?iClientID="+iClientID;
        });
    });

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>