<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "addTest.php";
$iPageID=11;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "Add Test Master";

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"Add Test","link"=>"addTest.php","isActive"=>true)
            );
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <ol class="breadcrumb navbar-breadcrumb">
                                            <?php echo parseBreadcrumb($aBreadcrumb); ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormTest" action="#">
                                        <div class="form-group">
                                            <label for="id_test_name" class="col-sm-2 control-label">Test Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="test_name" class="form-control" id="id_test_name" placeholder="Enter Test Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idAddTest" class="btn btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idAddTest").click(function(){
                var sTestName = $("#id_test_name").val();
                if(sTestName ==''){
                    alert('Workflow Name Must to enter.');
                }else{
                    $.ajax({
                        url:"ajaxRequest.php",
                        data:{sFlag:'addTestMaster',sTestName:sTestName},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                            
                            if(iResult > 0){
                                displayAlert('Test Added Successfully','success');
                                $("#id_test_name").val('');
                            }else{
                                displayAlert('Some Error Occured','warning');
                                $("#id_test_name").val('');
                            }
                        }
                    });
                }
         });
    });
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>