<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";

//! Session Validator
$oSessionManager = new SessionManager();


$aFunctionMap = array(
	'getPatient'   =>  "ajax_getPatient",
);

$iFunc = $_REQUEST['func'];

if( isset($aFunctionMap[$iFunc]) ) {
   $method = $aFunctionMap[$iFunc];

   $aResponse = $method($_GET, $_POST);
   header("Content-Type: application/json");
   echo json_encode($aResponse);
}
else {
   header("HTTP/1.1 401 Unauthorized");
   exit;
}

function ajax_getPatient() {


}

?>