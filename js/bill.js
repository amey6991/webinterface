function POS(iPatientID,dBillDate,iBillID) {
	this.oBill 			   = new BillViewMaster(iBillID,dBillDate,iPatientID);
	this.aPrescriptionList = ko.observableArray([]);
	this.oPatient 		   = new Patient(iPatientID);
}

function Patient(iPatientID) {
	var self = this;
	this.isPatientExternal = ko.observable(true);
	this.iPatientID = ko.observable(iPatientID);
    this.sPatientNo = ko.observable();
    this.sPatientName = ko.observable();
    this.sAddress = ko.observable();
    this.sCity = ko.observable();
    this.sState = ko.observable();
    this.sCountry = ko.observable();
    this.iPincode = ko.observable();
    this.sMobile = ko.observable();
    this.sEmail = ko.observable();
    this.dDOB = ko.observable();
    this.iAge = ko.observable();
    this.sGender = ko.observable();
    this.sBloodGroup = ko.observable();
    this.sEmergencyContactName = ko.observable();
    this.iEmergencyContactNo = ko.observable();
    this.sEmergencyContactRelation = ko.observable();
    this.sProfileImage = ko.observable("https://placeholdit.imgix.net/~text?&w=250&h=250");
    this.iIsVIP = ko.observable();
    this.sVIPRemarks = ko.observable();
    this.sMembershipType = ko.observable();
    this.iMedixcelPatientID = ko.observable(0);

    this.iPatientID.subscribe(function(iPatientID) {
    	if(iPatientID){
	    	$.get("ajaxHandler.php", {iPatientID: iPatientID, func: 'getPatientByPatientID'}, function(data) {
	    		self.isPatientExternal(data.isPatientExternal);
				self.iPatientID(data.iPatientID);
			    self.sPatientNo(data.sPatientNo);
			    self.sPatientName(data.sPatientName);
			    self.sAddress(data.sAddress);
			    self.sCity(data.sCity);
			    self.sState(data.sState);
			    self.sCountry(data.sCountry);
			    self.iPincode(data.iPincode);
			    self.sMobile(data.sMobile);
			    self.sEmail(data.sEmail);
			    self.dDOB(data.dDOB);
			    self.iAge(data.iAge);
			    self.sGender(data.sGender);
			    self.sBloodGroup(data.sBloodGroup);
			    self.sEmergencyContactName(data.sEmergencyContactName);
			    self.iEmergencyContactNo(data.iEmergencyContactNo);
			    self.sEmergencyContactRelation(data.sEmergencyContactRelation);
			    if(data.sProfileImage){
			    	self.sProfileImage(data.sProfileImage);
			    }
			    self.iIsVIP(data.iIsVIP);
			    self.sVIPRemarks(data.sVIPRemarks);
			    self.sMembershipType(data.sMembershipType);
			    self.iMedixcelPatientID(data.iMedixcelPatientID);
	    	});
    	} else {
    		self.isPatientExternal(false);
			self.iPatientID(0);
		    self.sPatientNo(0);
		    self.sPatientName("");
		    self.sAddress("");
		    self.sCity("");
		    self.sState("");
		    self.sCountry("");
		    self.iPincode("");
		    self.sMobile("");
		    self.sEmail("");
		    self.dDOB("");
		    self.iAge("");
		    self.sGender("");
		    self.sBloodGroup("");
		    self.sEmergencyContactName("");
		    self.iEmergencyContactNo("");
		    self.sEmergencyContactRelation("");
		    self.sProfileImage("");
		    self.iIsVIP("");
		    self.sVIPRemarks("");
		    self.sMembershipType("");
		    self.iMedixcelPatientID(0);
    	}
	});
}

// function / contructor for Master Bill
function BillViewMaster(iBillID,dBillDate,sBillTo){
	this.iBillID=iBillID;
	this.dBillDate=dBillDate;
	this.sBillTo=sBillTo;
	this.aItemParticulars = ko.observableArray([]);
	this.aItemTotal = [];
	this.aPaymentPerticular = ko.observableArray([]);
	this.iPaidAmount = ko.observable(0);
	this.iNetAmount = ko.observable();
	this.iSuggestionItemID = ko.observable(0);
	var iCount=0;

	this.addItem = function() {
		iCount++;
		this.aItemParticulars.push(new ItemParticulars(new Item('ABC-001','ABC-001','Pen',100),4,iCount,2,3));
	}


	this.iGrandTotal = ko.pureComputed(function(){
		    var iTotal = 0;
			for (var iLoop in this.aItemParticulars()) {
				var oItem = this.aItemParticulars()[iLoop];
				var	iSum = oItem['iTotal'];
				iTotal = iTotal + iSum;
			 }
			return iTotal;
	},this);

	this.iTaxAmount = ko.pureComputed(function(){
			var iTotalTaxAmount = 0;
			for (var iLoop in this.aItemParticulars()) {
				var oItem = this.aItemParticulars()[iLoop];
				var	iSumTax = oItem['iTaxAmount'];
				iTotalTaxAmount = iTotalTaxAmount + iSumTax;
			 }
			return iTotalTaxAmount;
	},this);

	this.iDiscountAmount = ko.pureComputed(function(){
			var iTotalDisAmount = 0;
			for (var iLoop in this.aItemParticulars()) {
				var oItem = this.aItemParticulars()[iLoop];
				var	iSumDisc = oItem['iDiscountAmount'];
				iTotalDisAmount = iTotalDisAmount + iSumDisc;
			 }
			return iTotalDisAmount;
	},this);

	this.iPendingAmount = ko.pureComputed(function(){
			var iPendingAmount=0;
			for (var iLoop in this.aPaymentPerticular()) {
				var oItem = this.aPaymentPerticular()[iLoop];
				var	oPayment = oItem['oPayment'];
				var iSumPending = oPayment['iPending'];
				iPendingAmount =  this.iNetAmount() - iSumPending;
			 }
			 return iPendingAmount;
	},this);

	this.iPaidAmount = ko.pureComputed(function(){
			var iPaidAmount=0;
			for (var iLoop in this.aPaymentPerticular()) {
				var oItem = this.aPaymentPerticular()[iLoop];
				var	oPayment = oItem['oPayment'];
				var iSumPaid = oPayment['iPaidAmount'];
				iPaidAmount =  iPaidAmount + iSumPaid;
			 }
			 return iPaidAmount;
	},this);

	this.iNetAmount = ko.pureComputed(function(){
			var iNetAmount = this.iGrandTotal() + this.iTaxAmount() - this.iDiscountAmount() - this.iPaidAmount();
			return iNetAmount;
	},this);

	// this.addItem = function(oItem,iQty) {
	// 	iCount++;
	// 	this.aItemParticulars.push(new ItemParticulars(oItem,iQty,iCount,iTax,iDiscount));
	// 	this.setNetAmount();
	// }


	// this.addPayment = function(iAmount,sPaymentMode,dPaymentDate){
	// 	this.iPendingAmount = this.iNetAmount - iAmount;
	// 	this.aPayment.push(new Payment(iAmount,sPaymentMode,dPaymentDate,this.iPendingAmount));
	// }

	this.addPayment = function(){
		if(this.iNetAmount() == 225){
			var iPendingAmount = this.iNetAmount() - 225;
			this.aPaymentPerticular.push(new PaymentPerticulars(new Payment(225,'Cash','2015-12-10',iPendingAmount)));
		}
		if(this.iNetAmount() < 225){
			console.log('You can not pay More then Net Amount.');
		}
		if(this.iNetAmount() > 225){
			var iPendingAmount = this.iNetAmount() - 225;
			this.aPaymentPerticular.push(new PaymentPerticulars(new Payment(225,'Cash','2015-12-10',iPendingAmount)));			
		}
		
	}


	this.removeItem = function(iSrNo){
		if(iSrNo>0){
			var iFlag= -1;
			for (var iLoop in this.aItemParticulars()) {
			 	var oItemData = this.aItemParticulars()[iLoop];
			 	var iItemSrNo = oItemData['iSrNo'];
			 	if(iSrNo == iItemSrNo){
			 		iFlag=iLoop;
			 		break;
			 	}
			}
			if(iFlag == 0){
				this.aItemParticulars.shift(); 
			}else if(iFlag>0){
				this.aItemParticulars.splice(iFlag,iFlag); 
			}
			iSrNoCount = 0;
			for (var iLoop in this.aItemParticulars()) {
				iSrNoCount++;
			 	var oItemData = this.aItemParticulars()[iLoop];
			 	oItemData['iSrNo']=iSrNoCount;
			}
		}
	}

	//! return the array of objects 
	this.getItemDetailForMoreThanItemQty = function(iQtty){
		aItemObj = [];
		for (var iLoop in this.aItemParticulars()) {
			var oItem = this.aItemParticulars()[iLoop];
			var	iCTotal = oItem['iTotal'];
			var	iCSrNo = oItem['iSrNo'];
			var	iCQty = oItem['iQty'];
			var oObjItem = oItem['oItem'];
			if(iCQty >= iQtty){
				var iItemNo = oObjItem['iItemNo'];
				var iItemRate = oObjItem['iItemRate'];
				var sItemName = oObjItem['sItemName'];
				var sItemCode = oObjItem['sItemCode'];
				aItemObj.push(new ItemMoreThenQty(iCSrNo,iCQty,iItemNo,iItemRate,sItemName,sItemCode,iCTotal));
			}
		}
		return aItemObj;
	}

	this.getAllItemDetail = function(){
		aItemObj = [];
		for (var iLoop in this.aItemParticulars()) {
			var oItem = this.aItemParticulars()[iLoop];
			var	iCTotal = oItem['iTotal'];
			var	iCSrNo = oItem['iSrNo'];
			var	iCQty = oItem['iQty'];
			var oObjItem = oItem['oItem'];
			var iItemNo = oObjItem['iItemNo'];
			var iItemRate = oObjItem['iItemRate'];
			var sItemName = oObjItem['sItemName'];
			var sItemCode = oObjItem['sItemCode'];
			aItemObj.push(new Itemss(iCSrNo,iCQty,iItemNo,iItemRate,sItemName,sItemCode,iCTotal));
			
		}
		return aItemObj;
	}
}

function ItemParticulars(oItem,iQty,iCount,iItemTax,iItemDiscount) {
	this.oItem = oItem;
	this.iQty = ko.observable(iQty);
	this.iSrNo= iCount;
	this.iItemTax= ko.observable(iItemTax);
	this.iItemDiscount=ko.observable(iItemDiscount);
	var iItemPrice = this.oItem.iItemRate;
	this.iTotalAmount = ko.observable((iItemPrice * iQty));

	this.iTaxAmount = parseFloat((this.iTotalAmount() * this.iItemTax())/ 100);
	this.iDiscountAmount = parseFloat((this.iTotalAmount() * this.iItemDiscount())/ 100);
	this.iTotal = (this.iTotalAmount()+this.iTaxAmount)-this.iDiscountAmount;
}
function PaymentPerticulars(oPayment){
	this.oPayment = oPayment;
}

// creating Item Object
function Item(iItemNo,sItemCode,sItemName,iItemRate){
	this.iItemNo = iItemNo;
	this.sItemCode =sItemCode;
	this.sItemName = sItemName;
	this.iItemRate = iItemRate;
}

function Payment(iPaidAmount,sPaymentMode,dPaymentDate,iPending){
	this.iPaidAmount = iPaidAmount;
	this.sPaymentMode = sPaymentMode;
	this.dPaymentDate = dPaymentDate;
	this.iPending = iPending;
}

function Itemss(iCSrNo,iCQty,iItemNo,iItemRate,sItemName,sItemCode,iCTotal){
	this.iCSrNo = iCSrNo;
	this.iCQty = iCQty;
	this.iItemNo =iItemNo ;
	this.iItemRate = iItemRate;
	this.sItemName = sItemName;
	this.sItemCode= sItemCode;
	this.iCTotal= iCTotal;
}

function ItemMoreThenQty(iCSrNo,iCQty,iItemNo,iItemRate,sItemName,sItemCode,iCTotal){
	this.iCSrNo = iCSrNo;
	this.iCQty = iCQty;
	this.iItemNo =iItemNo ;
	this.iItemRate = iItemRate;
	this.sItemName = sItemName;
	this.sItemCode= sItemCode;
	this.iCTotal= iCTotal;
}

	// this.updateItem = function (iSrNo,iItemNo,sItemCode,sItemName,iItemRate,iItemQty){
		
	// 	for (var iLoop in this.aItemParticulars) {
	// 	 	var oItemData = this.aItemParticulars[iLoop];
		 	
	// 		var	iItemSrNo = oItemData['iSrNo'];
	// 		var oObjItem = oItemData['oItem'];
	// 		var iCQty = oItemData['iQty'];
	// 		oItemData['iTotal'];
	// 		oObjItem['iItemNo'];
	// 		oItemData['iTotal'];
	// 		oObjItem['sItemCode'];
	// 		oObjItem['sItemName'];
	// 	 	if(iSrNo == iItemSrNo){
	// 	 		if(iItemQty!=undefined && iItemRate!=undefined){
	// 	 			oItemData['iQty']=iItemQty;
	// 	 			oObjItem['iItemRate']=iItemRate;
	// 	 			oItemData['iTotal']= iItemQty * iItemRate;
	// 	 		}
	// 	 		if(iItemNo!=undefined){
	// 	 			oObjItem['iItemNo']=iItemNo;
	// 	 		}
	// 	 		if(sItemName!=undefined){
	// 	 			oObjItem['sItemName']=sItemName;
	// 	 		}
	// 	 		if(sItemCode!=undefined){
	// 	 			oObjItem['sItemCode']=sItemCode;
	// 	 		}
		 	
	// 			break;
	// 	 	}
	// 	}
	// 	this.setNetAmount();
	// }