
function DashboardDataViewModal(){
	var self = this;
	// var oDate = new Date();
	// var sDate = oDate.getDate()+'-'+(oDate.getMonth()+1)+'-'+oDate.getFullYear();
	
	// var dStartDate = oDate.getDate() - oDate.getDay();

	//var lastday = new Date(oDate.setDate(oDate.getDate() - oDate.getDay()+6));
	// dStartDate = '2016-07-13';
	// dEndDate = '2016-07-22';

	
	// process table
	
	
	//this.processDataTable = ko.observableArray([]);

	this.dStartDate =ko.observable(0);
	this.dEndDate =ko.observable(0);

	this.applyDateFilter = function(dStartDate,dEndDate){
		aStartDate = dStartDate.split('-');
		dStartDate = aStartDate[2]+'-'+aStartDate[0]+'-'+aStartDate[1];
		aEndDate = dEndDate.split('-');
		dEndDate = aEndDate[2]+'-'+aEndDate[0]+'-'+aEndDate[1];
		this.dStartDate(dStartDate);
		this.dEndDate(dEndDate);
	}


	this.aSequencerProcessData = ko.pureComputed(function(){
		var aData = [];
		$.ajax({
			url:'ajaxRequest.php?sFlag=fetchSequenceData',
			async:false,
			data:{dStartDate:this.dStartDate(),dEndDate:this.dEndDate()},
	        method:"GET",
	        success:function(sData){
		    	sData = $.parseJSON(sData);
		    	if(sData.length > 0){
		    		for(var iii=0;iii< sData.length;iii++){
			    		var iSeqID = sData[iii].seq_scanner_id;
			    		var iSeqTableID = sData[iii].seq_directory_id;
			    		var iSeqType = sData[iii].seq_type;
			    		var sErrorStatus = sData[iii].error_status;
			    		var sRunID = sData[iii].seq_dir_name;
			    		var iSize = sData[iii].seq_directory_size;
			    		var iCycNum = sData[iii].seq_cycle_number;
			    		var iStatus = sData[iii].seq_status;
			    		var sQ30 = sData[iii].seq_q30;
			    		var sPassFilter = sData[iii].seq_PF;
			    		var dStartDate = sData[iii].start_date;
			    		if(sData[iii].seq_finished_on==null){
			    			var dFinishDate = '';	
			    		}else{
			    			var dFinishDate = sData[iii].seq_finished_on;
			    		}
			    		var oObject = new sequenceData(iSeqTableID,iSeqID,sRunID,iSeqType,iStatus,iCycNum,iSize,sQ30,sPassFilter,dStartDate,dFinishDate,sErrorStatus);
			    		aData.push(oObject);		    		
			    	}
		    	}
		    	//return self.processDataTable();
			}
		});
		return aData;
	},this);
	

	this.aFilerSeqStatus = ko.observableArray([]);
	this.aSequencerObservable = ko.pureComputed(function(){
									
									var aSequencerData = [];
									var aFilerSeqStatus = self.aFilerSeqStatus();
									var sSequencerData = self.aSequencerProcessData(); // process array of ajax above
									var aRunningSeq = ['2','5','3','6','4','7','8','9','12','13'];
									$.each(sSequencerData, function(index, aObject) {
										var bInclude = true;
										var bRunInclude = true;
										var bError = true;
										var sError ='0';
										var sStatus =50;
										var sErrStatus ='60';
										
										if(aFilerSeqStatus.length){
											var tempStatus = aObject.cstatus;
											sError = aObject.sErrorStatus;
											var sTempStatus = tempStatus.toString();
											sStatus = sStatus.toString();
											console.log(aFilerSeqStatus.indexOf(sTempStatus));
											if(aFilerSeqStatus.indexOf(sErrStatus) == -1){
												bError = false;
											}
											if(aFilerSeqStatus.indexOf(sTempStatus) == -1){
												bInclude = false;
											}
											if(aFilerSeqStatus.indexOf(sStatus) == -1){
												bRunInclude = false;
											}	
										}

										if(bInclude==true){
											aSequencerData.push(aObject);
										}
										if(bRunInclude==true){
											if(aRunningSeq.indexOf(sTempStatus) != -1){
												aSequencerData.push(aObject);
											}
										}
										if(bError==true){
											if(sError == '111'){
												aSequencerData.push(aObject);
											}
										}
									});
									return aSequencerData;
								},this);


	this.iTotalProcessDataRows = ko.computed(function() { 
		return self.aSequencerObservable().length;
	}, this);
	
	
	this.iCurrentPage =ko.observable(1);
	
	this.iMaxPages =ko.observable(5);

	this.iRowPerProcessPage =ko.observable(10);

	
	this.iTotalPagestoDisplay = ko.computed(function() { 
						 return Math.ceil((this.iTotalProcessDataRows()) / (this.iRowPerProcessPage()));
					}, this);
	
	this.aViewableProcessRows = ko.pureComputed(function(){
							if(this.iCurrentPage() == 1){
								var istartIndex = 0;
							}else{
								var istartIndex = this.iRowPerProcessPage()* ( this.iCurrentPage()-1 );
							}
							var iEndIndex = ( this.iRowPerProcessPage()*this.iCurrentPage() );

							return this.aSequencerObservable().slice(istartIndex,iEndIndex);

						},this);
	
	
	this.iPageDisplayOnPagination = ko.observable(3);
	this.iPageDisplayFrom = ko.observable(1);

	this.setCurrentPage = function(iPageNo){
		this.iCurrentPage(iPageNo);
	}

	this.nextProcessPage = function(){
		console.log(this.iPageDisplayFrom());
		this.iCurrentPage(this.iPageDisplayFrom()+this.iMaxPages());
		this.iPageDisplayFrom(this.iCurrentPage());
	}
	this.previousProcessPage = function(){
		console.log(this.iPageDisplayFrom());
		this.iCurrentPage(this.iPageDisplayFrom()-this.iMaxPages());
		this.iPageDisplayFrom(this.iCurrentPage());
	}
	
	
    this.sProcessPagination = ko.pureComputed(function(){
        	
        	var iTotal = self.iTotalPagestoDisplay();
        	var iMaxPage = self.iMaxPages();
        	// sPaginationArray =[];
        	sPaginationArray ='';
        	var iRemainder = iTotal % iMaxPage;
        	if(iRemainder == 0){
        		var iDisable = iTotal - iMaxPage;
        		iDisable = iDisable+1;
        	}else{
        		var iDisable = iTotal - iRemainder;
        		iDisable = iDisable+1;
        	}
        	
            if(this.iCurrentPage() <= this.iMaxPages()){
	
			}else{
				// sPaginationArray.push("<li><a href='#'' aria-label='Previous' onClick='oViewModelDashboard.previousProcessPage()'><span aria-hidden='true'>&laquo;</span></a></li>");
				sPaginationArray +="<li><a href='#'' aria-label='Previous' onClick='oViewModelDashboard.previousProcessPage()'><span aria-hidden='true'>&laquo;</span></a></li>";
			}

			var iCount=1;
			for(var iii= self.iPageDisplayFrom();iii <= iTotal; iii++){
				 if(iCount <= self.iMaxPages()){
				 	iCount++;
					// sPaginationArray.push("<li><a href='#' id='id_page_"+iii+" class='classPagination' onClick='oViewModelDashboard.setCurrentPage("+iii+")' >"+iii+"</a></li>");
					sPaginationArray +="<li><a href='#' id='id_process_page_"+iii+" class='classPagination' onClick='oViewModelDashboard.setCurrentPage("+iii+")' >"+iii+"</a></li>";
				 }
			}
			if(this.iCurrentPage() == self.iTotalPagestoDisplay()){
				// sPaginationArray.push("<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
				sPaginationArray +="<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
			}else if((iCount-1) == self.iTotalPagestoDisplay()){
				//sPaginationArray.push("<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>"); Not in Use
			}else if(this.iCurrentPage() >= iDisable){
					// sPaginationArray.push("<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
					sPaginationArray +="<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
			}else{
				// sPaginationArray.push("<li><a href='#' aria-label='Next' onClick='oViewModelDashboard.nextProcessPage()'><span aria-hidden='true'>&raquo;</span></a></li>");
				sPaginationArray +="<li><a href='#' aria-label='Next' onClick='oViewModelDashboard.nextProcessPage()'><span aria-hidden='true'>&raquo;</span></a></li>";
			}
        
    		return sPaginationArray;    
        
    },this);

	
// Sequencer Sample Table #2

// Sample Data through Ajax
	this.availablePipeline = ko.observableArray([]);
	this.iCurrentSamplePage =ko.observable(1);
	this.iRowPerSamplePage =ko.observable(10);
	this.iMaxSamplePages =ko.observable(6);
	this.iPageDisplayOnPaginationSample = ko.observable(3);
	this.iSamplePageDisplayFrom = ko.observable(1);

	// this.sampleDataQCAnalysis = ko.observableArray([]); No Use 
	this.sSamplePaginationArray = ko.observableArray([]);
		// Filter applied in Sample Sequecne Table

	this.iFilterSampleDataStatus = ko.observable(0);
	this.iFilterSampleDataPipeline = ko.observable(0);
	
	this.aFilerStatus = ko.observableArray([]);
	this.aFilerPipeline = ko.observableArray([]);
	this.iLength = ko.computed(function(){
		return self.aFilerPipeline().length;
	});
	this.sPipelineText = ko.observable('');

	this.aFilterPipelineAvail = ko.observableArray([]);

	// Dashboard counts
	this.iActiveRuns = ko.observable(0);
	this.iRunningSequencer = ko.observable(0);
	this.iSequencerCompletedWeek = ko.observable(0);
	this.iSequencerCompletedMonth = ko.observable(0);
	this.iSequencerWarning = ko.observable(0);

	$.ajax({
		url:'ajaxRequest.php?sFlag=running_sequencer',
		data:{dStartDate:this.dStartDate(),dEndDate:this.dEndDate()},
		async:false,
        method:"GET",
        success:function(sData){
        	sData = $.parseJSON(sData);
        	self.iActiveRuns(sData.iTotalRun);
	    	self.iRunningSequencer(sData.iRunningTotal);
	    	self.iSequencerCompletedWeek(sData.iCompletedSequencerWeek);
	    	self.iSequencerCompletedMonth(sData.iCompletedSequencerMonth);
	    	self.iSequencerWarning(sData.iWarningSequencer);
		}
	});

// Sample Information by Date Filter

	this.dSampleStartDate =ko.observable(0);
	this.dSampleEndDate =ko.observable(0);

	this.applySampleDateFilter = function(dStartDate,dEndDate){
		aStartDate = dStartDate.split('-');
		dStartDate = aStartDate[2]+'-'+aStartDate[0]+'-'+aStartDate[1];
		aEndDate = dEndDate.split('-');
		dEndDate = aEndDate[2]+'-'+aEndDate[0]+'-'+aEndDate[1];
		this.dSampleStartDate(dStartDate);
		this.dSampleEndDate(dEndDate);
	}

	this.aSequencerSampleProcessData = ko.pureComputed(function(){
		var aData = [];
		$.ajax({
			url:'ajaxRequest.php?sFlag=fetchSampleSequenceData',
			data:{dStartDate:this.dSampleStartDate(),dEndDate:this.dSampleEndDate()},
			async:false,
	        method:"GET",
	        success:function(sData){
		    	sData = $.parseJSON(sData);
		    	aStatus = [1,2,3,4,5,2,4,1,5,3,1];
		    	
		    	if(sData.length > 0){
		    		var aPipeline =[];
		    		for(var iii=0;iii< sData.length;iii++){
			    		var iSeqTableID = sData[iii].seq_directory_id;
			    		var iSeqType = sData[iii].seq_type;
			    		var iSeqID = sData[iii].seq_scanner_id;
			    		var sRunID = sData[iii].run_id;
			    		var sSampleName = sData[iii].sample_name_barcode;
			    		var sPipeLineName = sData[iii].pipeline;
			    		var sPipeLineID = 1;//sData[iii].workflow_id;
			    		sPipeLineID = sPipeLineID.toString();
			    		var iStatus = sData[iii].seq_sample_status; 
			    		//fGetStatusforSequencerID(iSeqTableID);
			    		
						aPipeline.push({text: sPipeLineName , iPipelineID: sPipeLineID});
		    		
			    		var oObject = new sampleSequnceData(iSeqTableID,iSeqType,sSampleName,iSeqID,sRunID,sPipeLineName,sPipeLineID,iStatus);
			    		aData.push(oObject);
			    		//self.sampleDataQCAnalysis.push(oObject);
			    	}
			    	
			    	// My worst code for finding out the duplicate Need to Correct
			    	// console.log(aPipeline);
			    	var aAvailPipe = [];
			    	var aAvailPipeLine = [];
			    	for (var i = 0; i < aPipeline.length; i++) {
			    		var iPipelineID = aPipeline[i].iPipelineID;
			    		var iNextID =0;
			    		for(var j=i+1;j < aPipeline.length;j++){
			    			var iNextPipelineID = aPipeline[j].iPipelineID;
			    			if(iPipelineID == iNextPipelineID){
			    				aAvailPipe.push(i);
			    			}
			    		}
			    	}
			    	//console.log(aAvailPipe);
			    	for (var i = 0; i < aPipeline.length; i++) {
			    		if(aAvailPipe.indexOf(i) == -1){
			    			aAvailPipeLine.push(aPipeline[i]);
			    		}
			    	}
			    	// console.log(aAvailPipeLine);
			    	self.availablePipeline(aAvailPipeLine);
			    }
			}
		});
		return aData;
	},this);
	var iCount =0;
	
	this.setPipelineText = function(sText){
		if(sText != ''){
			this.sPipelineText(sText);
		}else{
			this.sPipelineText('');
		}
	}
	this.aFilterPipelineAvail = ko.pureComputed(function(){
		var aFilerPipeline = self.aFilerPipeline();
		//var sText = self.sPipelineText();
		// if(sText == ''){
			if(aFilerPipeline.length){
				var aTempAvailData = [];
				var aTempNotAvailData = [];
				$.each(self.availablePipeline(),function(iKey,aObject){
					var iPipelineID = aObject.iPipelineID;
					iPipelineID = iPipelineID.toString();
					var bInclude = true;
					if(aFilerPipeline.indexOf(iPipelineID) >= 0){
						aTempAvailData.push(aObject);
					}
					// else{
					// 	aTempNotAvailData.push(aObject);
					// }
				});
				self.iCurrentSamplePage(1);
				return aTempAvailData;
			}
		//}
	});

	this.aPipelineAvailable = ko.pureComputed(function(){
		var aFilerPipeline = self.aFilerPipeline();
		var sText = self.sPipelineText();
		if(sText == ''){
			if(aFilerPipeline.length){
				var aTempAvailData = [];
				var aTempNotAvailData = [];
				$.each(self.availablePipeline(),function(iKey,aObject){
					var iPipelineID = aObject.iPipelineID;
					iPipelineID = iPipelineID.toString();
					var bInclude = true;
					if(aFilerPipeline.indexOf(iPipelineID) >= 0){
						aTempAvailData.push(aObject);
					}else{
						aTempNotAvailData.push(aObject);
					}
				});
				
				self.iCurrentSamplePage(1);
				return aTempNotAvailData;
				//return aTempAvailData;
			}else{
				return self.availablePipeline();
			}
		}else{
			
			if(aFilerPipeline.length){
				var aTempAvailData = [];
				var aTempNotAvailData = [];

				$.each(self.availablePipeline(),function(iKey,aObject){
					var iPipelineID = aObject.iPipelineID;
					iPipelineID = iPipelineID.toString();
					var bInclude = true;
					if(aFilerPipeline.indexOf(iPipelineID) >= 0){
						aTempAvailData.push(aObject);
					}else{
						aTempNotAvailData.push(aObject);
					}
				});
				
				//var aTempAvailData = aTempAvailData.concat(aTempNotAvailData);
				var aTempPipeline = [];
				$.each(aTempNotAvailData,function(iKey,aObject){
					var sPipelineText = aObject.text;
					var bInclude = true;
					if(sPipelineText.indexOf(sText) == -1){
						bInclude = false;
					}
					if(bInclude){
						aTempPipeline.push(aObject);
					}
				});
				return aTempPipeline;
			}else{
				var aTempPipeline = [];
				$.each(self.availablePipeline(),function(iKey,aObject){
					var sPipelineText = aObject.text;
					var bInclude = true;
					if(sPipelineText.indexOf(sText) == -1){
						bInclude = false;
					}
					if(bInclude){
						aTempPipeline.push(aObject);
					}
				});
				return aTempPipeline;
			}
		}
	},this);

	this.aSampleDataQCObservable = ko.pureComputed(function(){
									
									var aSampleData = [];
									var aFilerStatus = self.aFilerStatus();
									var aFilerPipeline = self.aFilerPipeline();
									var sSampleQCData = self.aSequencerSampleProcessData(); //self.sampleDataQCAnalysis(); replace by new computed observable array

									$.each(sSampleQCData, function(index, aObject) {
										var bInclude = true;

										if(aFilerPipeline.length){
											var tempPipeline = aObject.sPipeLineID;
											var sTempPipeline = tempPipeline.toString();
											if(aFilerPipeline.indexOf(sTempPipeline) == -1){
												bInclude = false;
											}
										}
										
										if(aFilerStatus.length){
											var tempStatus = aObject.iStatus;
											var sTempStatus = tempStatus.toString();
											console.log(sTempStatus);
											console.log(aFilerStatus.indexOf(sTempStatus));
											if(aFilerStatus.indexOf(sTempStatus) == -1){
												bInclude = false;
											}	
										}

										if(bInclude){
											aSampleData.push(aObject);
										}
									});
									return aSampleData;
								},this);

	this.availableStatus = ko.observableArray([{ text: 'On Sequncer', iStatus: 1},{ text: 'Analysis Complete', iStatus: 5} ,{ text: 'Sample Analsyis', iStatus: 3},{ text: 'QC Analysis', iStatus: 4},{ text: 'Waiting', iStatus: 2}])
	

	this.iTotalSampleDataRows = ko.computed(function() { 
		return self.aSampleDataQCObservable().length;
	}, this);

	this.iTotalSamplePagesToDisplay = ko.computed(function() { 
				 return Math.ceil((this.iTotalSampleDataRows()) / (this.iRowPerSamplePage()));
				}, this);
	

	this.availableSampleDataQCAnalysis = ko.pureComputed(function(){
							// return this.sampleDataQCAnalysis();
							if(this.iCurrentSamplePage() == 1){
								var istartIndex = 0;
							}else{
								var istartIndex = this.iRowPerSamplePage()* ( this.iCurrentSamplePage()-1 );
							}
							var iEndIndex = ( this.iRowPerSamplePage()*this.iCurrentSamplePage() );

							return this.aSampleDataQCObservable().slice(istartIndex,iEndIndex);
					},this);

	this.setSampleCurrentPage = function(iPageNo){
		this.iCurrentSamplePage(iPageNo);
	}

	this.nextSamplePage = function(){
		this.iCurrentSamplePage(this.iSamplePageDisplayFrom()+this.iMaxSamplePages());
		this.iSamplePageDisplayFrom(this.iCurrentSamplePage());
	}
	this.previousSamplePage = function(){
		this.iCurrentSamplePage(this.iSamplePageDisplayFrom()-this.iMaxSamplePages());
		this.iSamplePageDisplayFrom(this.iCurrentSamplePage());
	}

	this.sSampleProcessPagination = ko.pureComputed(function(){
			// total no of pages
			var iTotal = self.iTotalSamplePagesToDisplay();
			//sSamplePaginationArray =[];
			sSamplePaginationArray ='';
			var iMaxPage = self.iMaxSamplePages();
        	var iRemainder = iTotal % iMaxPage;
        	if(iRemainder == 0){
        		var iDisable = iTotal - iMaxPage;
        		iDisable = iDisable+1;
        	}else{
        		var iDisable = iTotal - iRemainder;
        		iDisable = iDisable+1;
        	}
        	//if(this.iCurrentSamplePage() == 1 || this.iCurrentSamplePage() <= this.iPageDisplayOnPaginationSample()){
            if(this.iCurrentSamplePage() <= iMaxPage){
				//sSamplePaginationArray.push("<li class='disabled'><a href='#'' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>");
			}else{
				//sSamplePaginationArray.push("<li><a href='#'' aria-label='Previous' onClick='oViewModelDashboard.previousSamplePage()'><span aria-hidden='true'>&laquo;</span></a></li>");
				sSamplePaginationArray +="<li><a href='#'' aria-label='Previous' onClick='oViewModelDashboard.previousSamplePage()'><span aria-hidden='true'>&laquo;</span></a></li>";
			}

			var iSampleCount=1;
			for(var iii= self.iSamplePageDisplayFrom();iii <= iTotal; iii++){
				 if(iSampleCount <= iMaxPage){
				 	iSampleCount++;
					//sSamplePaginationArray.push("<li><a href='#' id='id_page_"+iii+" class='classPagination' onClick='oViewModelDashboard.setSampleCurrentPage("+iii+")' >"+iii+"</a></li>");
					sSamplePaginationArray +="<li><a href='#' id='id_sample_process_page_"+iii+" class='classPagination' onClick='oViewModelDashboard.setSampleCurrentPage("+iii+")' >"+iii+"</a></li>";
				 }
			}
			
			if(this.iCurrentSamplePage() == self.iTotalSamplePagesToDisplay()){
				// sSamplePaginationArray.push("<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
				sSamplePaginationArray +="<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
			}else if(this.iCurrentSamplePage() >= iDisable && (iDisable > 0)){
				// sSamplePaginationArray.push("<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
				sSamplePaginationArray +="<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
			}else if(self.iTotalSampleDataRows() == 0){

			}else{
				// sSamplePaginationArray.push("<li><a href='#' aria-label='Next' onClick='oViewModelDashboard.nextSamplePage()'><span aria-hidden='true'>&raquo;</span></a></li>");
				sSamplePaginationArray +="<li><a href='#' aria-label='Next' onClick='oViewModelDashboard.nextSamplePage()'><span aria-hidden='true'>&raquo;</span></a></li>";
			}
        	return sSamplePaginationArray;    
        
    },this);				
	
	this.onSequncerMouseOver = function(data,event){
		var $el = $(event.target);
		var sID = $el.context.id;
		var iID = sID.split('_')[1];
   		
   		$.ajax({
   			url:'ajaxRequest.php',
   			data:{iSeqTableID:iID,sFlag:'getSequencerDetail'},
   			async:true,
   			method:'GET',
   			success:function(sData){
   				var aData = $.parseJSON(sData);
   				var iSeqType = aData.seq_type;
   				var iStatus = aData.seq_status;
   				if(iSeqType==1){
   					var sSeqType = 'MiSeq';
   					if(iStatus=='100' || iStatus=='101'){
   						var sCompleted = "MiSeq Run Completed.";
   					}
   				}else{
   					var sSeqType = 'NextSeq';
   					if(iStatus=='10' || iStatus=='11'){
   						var sCompleted = "NextSeq Run Completed.";
   					}
   				}
   				if(aData.seq_scanner_id==undefined){
   					var sScannerID = 'Awaited';
   				}else{
   					var sScannerID = aData.seq_scanner_id;
   				}
   				var sInfo = 'Sequencer ID :'+sScannerID+'<br/> Run ID :'+aData.run_id+'<br/> Sequencer Type :'+sSeqType+'<br/> Started Date :'+aData.started_on+'<br/> Status :'+sCompleted+'<br/>';
   				PNotify.removeAll();
   				new PNotify({
				    title: 'Sequencer Info',
				    text: sInfo,
				    type: 'info',
				    stack:false,
				    hide:true,
				    nonblock:false
				});
			}
   		});

	}
	this.onSequncerMouseLeave = function(data,event){
		var $el = $(event.target);
		var sID = $el.context.id;
		var iID = sID.split('_')[1];
	}

	this.searchKey = function(nameKey, myArray){
		if(myArray.length >0 ){
		    for (var i=0; i < myArray.length; i++) {
		        if (myArray[i].iPipelineID == nameKey) {
		            return myArray[i];
		        }else{
		        	return -1;
		        }
		    }			
		}else{
			return -1;
		}

		
	}
}



function sequenceData(iSeqTableID,iSequenceID,sRunID,iSeqType,iStatus,iCycleNumber,iDataSize,sQ30,sPassingFliter,dStartDate,dFinishDate,sErrorStatus){
	this.iSeqTableID = iSeqTableID;
	this.sequenceID = iSequenceID;
	this.sRunID = sRunID;
	this.iSeqType = iSeqType;
	this.cstatus = iStatus;
	this.cyclenumber = iCycleNumber;
	this.datasize = iDataSize;
	this.q30 = sQ30;
	this.passingfilter = sPassingFliter;
	this.dStartDate = dStartDate;
	this.dFinishDate = dFinishDate;
	this.sErrorStatus= sErrorStatus
}
function sampleSequnceData(iSeqTableID,iSeqType,sSampleName,iSequenceID,iRunID,sPipeLineName,sPipeLineID,iStatus){
	this.iSeqTableID = iSeqTableID;
	this.iSeqType = iSeqType;
	this.sSampleName = sSampleName;
	this.iSequenceID = iSequenceID;
	this.iRunID = iRunID;
	this.sPipeLineName = sPipeLineName;
	this.sPipeLineID = sPipeLineID;
	this.iStatus = iStatus;
}
function fGetStatusforSequencerID(iSequencerID){
	   	var iStatus=0;
	   	$.ajax({
   			url:'ajaxRequest.php',
   			data:{iSequencerID:iSequencerID,sFlag:'getSequencerStatus'},
   			async:false,
   			method:'GET',
   			success:function(sData){
   				iStatus=sData;
   			}
   		});
   		return iStatus;
}