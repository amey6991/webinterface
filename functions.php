<?php
require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funFileManager.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funWebinterfaceWorkflow.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";

//! function returns the base64_encode of the @a $string that can be passed in URL
//! @return string encoded string
function urlsafe_b64encode($string)
{
  $data = base64_encode($string);
  $data = str_replace(array('+','/','='),array('-','_','.'),$data);
  return $data;
}

//! function returns the base64_decode of the @a $string that was encoded with urlsafe_b64encode
//! @return string decoded string
function urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

//! fetches the encoded alert constants 
function displayAlerts() {
    //! if no alerts, do nothing.
    if(!isset($_GET['alerts'])){
        return;
    }

    $alerts = explode('::x::',urlsafe_b64decode($_GET['alerts']));

    //! for each alert, display a messaage
    foreach ($alerts as $key => $value) {

        //Check if constant is defined?
        if(defined($value)){
            
            //! get the type of alert
            $type = substr($value, 0,1);

            //! Check if alert is a success message
            if($type == 'S') {
                $sNoteType = "success";
            }
            //! Check if alert is a warning message 
            else if($type == 'W'){
                $sNoteType = "warning";
            } 
            //! Check if alert is a error message
            else if($type == 'E'){
                $sNoteType = "error";
            }

            ?>
            <script type="text/javascript">
                noty({
                    text        : "<strong>Alert!</strong><br /><?php echo constant($value); ?>",
                    type        : "<?php echo $sNoteType; ?>",
                    theme       : 'relax',
                    dismissQueue: true,
                    timeout: false,
                    layout      : "topRight",
                    closeWith: ['click']
                });
            </script>
            <?php
        }
    }

}

//! rediects user to specified url with specified error/warning/success messages.
//! @param $url string URL to which user has to be redirected.
//! @param $msgCodes array array of messages.
function redirectWithAlert($url, $msgCodes){
    $sMsg = implode("::x::",$msgCodes);
    $sCode = urlsafe_b64encode($sMsg);
    if(strstr($url,'?')!==FALSE){
        $fURL =  $url."&alerts={$sCode}";
    }
    else {
        $fURL =  $url."?alerts={$sCode}";
    }
    
    header("Location: {$fURL}");
    
}

//! Encodes array into string to be used in URL
//! @param $aArray array Array of values to be encoded in a string
//! @return string Encoded String for given array
function encode_array($aArray){
    $newPath = implode("::x::", $aArray);
    $newChkSum = substr(md5($newPath),0,4);
    $newPath = $newPath . "::y::" . $newChkSum;
    $encodedNewPath = urlsafe_b64encode($newPath);
    return $encodedNewPath;
}

//! Decode string into array
//! @param $sString string encoded string
//! @return array decoded array
function decode_array($sString){
    $b64decoded = urlsafe_b64decode($sString);
    $aExploded = explode("::y::", $b64decoded);
    $sImplodedString = $aExploded[0];
    $checkSum = $aExploded[1];

    //! if checksum is not matching, mark it as an invalid decoded string
    if(substr(md5($sImplodedString),0,4) != $checkSum){
        return FALSE;
    }

    $aArray = explode("::x::",$sImplodedString);

    return $aArray;
}

//! Gives the name of the state from status integer
function getStateFromStatusInt($int){
    if($int == -1){
        return "Draft";
    }
    if($int == 0){
        return "Unapproved";
    }
    else if($int == 1){
        return "Approved";
    }
    else if($int == 2){
        return "Rejected";
    }
    else if($int == 3){
        return "Retired";
    }
}

//! Converts the size into appropriate unit from bytes
function getSizeFromBytes($iBytes) {
    $iSize = $iBytes;
    $sUnit = "bytes";
    if($iSize > 1024){
        $iSize = $iSize / 1024;
        $sUnit = "KB";
    }
    if($iSize > 1024){
        $iSize = $iSize / 1024;
        $sUnit = "MB";
    }
    if($iSize > 1024){
        $iSize = $iSize / 1024;
        $sUnit = "GB";
    }
    return round($iSize,2).' '.$sUnit;
}


function putMomentTimestamp($sTime) {
    ?>
    <span class="classMomentTimestamp" data-timestamp="<?php echo $sTime; ?>" title="<?php echo $sTime; ?>"><?php echo $sTime; ?></span>
    <?php
}

function parseBreadcrumb($aBreadcrumb) {

    if(empty($aBreadcrumb)) {
        $aBreadcrumb = array(array(
                "title"=>"ResearchDx",
                "link"=>"dashboard.php",
                "isActive"=>"true"
            ));
    }else{
        $iCount = count($aBreadcrumb);
        foreach ($aBreadcrumb as $key => $aBreadcrumbItem) {
            $sClass = "";
            if($aBreadcrumbItem['isActive']) {
                $sClass .= "active";
            }
            $sTitle = $aBreadcrumbItem['title'];
            $sLink = $aBreadcrumbItem['link'];
            if($key == ($iCount-1)){
            ?>
                <li class="<?php echo $sClass; ?>"><strong><?php echo $sTitle; ?></strong> </li>
            <?php
            }else{
            ?>
                <li class="<?php echo $sClass; ?>"><a href="<?php echo $sLink; ?>"><?php echo $sTitle; ?></a></li>
            <?php
            }
             ?>
             <?php
         }
    }
}

function getDashboardStats() {
    $oSession = new SessionManager();
    $iUserType = $oSession->iType;
    $iUserClientID= $oSession->iUserClientID;
    if($iUserType==1){
       $iTotalUsers =fGetCountAllUsers();
       $iTotalClients =fGetCountAllClients();
       $iTotalFile = getTotalFiles($iUserType,$iUserClientID);
    }
    if($iUserType==2){
       $iTotalUsers =fGetCountAllUsers($iUserClientID);
       $iTotalFile = getTotalFiles($iUserType,$iUserClientID);
    }
    
    // $iRunningSequencer = fGetRunningSequencer();
    // $iCompletedSequencerWeek = fGetSequencerCompletedThisWeek();
    // $iCompletedSequencerMonth = fGetSequencerCompletedThisMonth();

    $aArray = array(
            "aTotalClient" => $iTotalClients,
            "aTotalUsers" => $iTotalUsers,
            "aTotalFilePending" => $iTotalFile,
            "aTotalFileProcessed" => 133,
            // "running_sequencer" => $iRunningSequencer,
            // "completed_sequencer_week" => $iCompletedSequencerWeek,
            // "completed_sequencer_month" => $iCompletedSequencerMonth
        );

    return $aArray;
}
function getAllStates(){
    $aState = array();
    $stable = DATABASE_TABLE_PREFIX."_state_master";
    $sQuery = "SELECT * FROM `{$stable}`";
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $sResult = $conn->query($sQuery);
    if($conn != false){
        if($sResult != FALSE){
            while($aRow = $sResult->fetch_assoc()){
                $aState[]=$aRow;
            }
        }
    }
    return $aState;
}
?>