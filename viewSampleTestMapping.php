<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "viewSampleTestMapping.php";
$iPageID=15;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
if(isset($_GET['iTestID'])){
	$iTestID = $_GET['iTestID'];
}else{
	header('location:dashboard.php');
}

$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "View List";
$aSampleData = getSampleList();

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Test List</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                	 <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable" style="margin-left:40px">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Sample Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<count($aSampleData);$iii++){
                                                	$aMapSampleID=getMappedSampleIDForTestID($iTestID);
                                                    $iSampleID = $aSampleData[$iii]['sample_id'];
                                                	$sSampleName = $aSampleData[$iii]['sample_name'];
                                                    $iStatus = $aSampleData[$iii]['status'];
                                                	if($iStatus ==1){
                                                		if(in_array($iSampleID,$aMapSampleID)){
                                                			$sMappButton = "<button type='button' class='btn btn-success classUnMappButton' id='{$iSampleID}_{$iTestID}'>UnMap</button>";
                                                		}else{
                                                			$sMappButton = "<button type='button' class='btn btn-info classMappButton' id='{$iSampleID}_{$iTestID}'>Map</button>";
                                                		}
                                                    }else{
                                                        $sMappButton = "Sample is Freezed , Unfreeze First.";
                                                    }
                                                    
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td>
                                               		<?php echo $sSampleName;?>
                                                </td>
                                                <td><?php echo $sMappButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        
        $(".classMappButton").click(function(){
            var iTestID = this.id;
            aID = iTestID.split('_');
            var iSampleID = aID[0];
            var iTestID = aID[1];
            if(confirm('Are you sure want to Mapp this Sample ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'MappSample',iTestID:iTestID,iSampleID:iSampleID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
								window.location='viewSampleTestMapping.php?iTestID='+iTestID;   
                            }
                });
            }
        });

        $(".classUnMappButton").click(function(){
            var iTestID = this.id;
            aID = iTestID.split('_');
            var iSampleID = aID[0];
            var iTestID = aID[1];
            if(confirm('Are you sure want to UnMapp this Sample ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'UnMappSample',iTestID:iTestID,iSampleID:iSampleID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
								window.location='viewSampleTestMapping.php?iTestID='+iTestID;   
                            }
                });
            }
        });

    	$(".classFreezeButton").click(function(){
    		var iTestID = this.id;
    		if(confirm('Are you sure want to Freeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'FreezeTest',iTestID:iTestID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                                window.location='viewTestList.php';    
                            }
                });
            }
        });

        $(".classUnFreezeButton").click(function(){
            var iTestID = this.id;
            if(confirm('Are you sure want to UnFreeze the Test ?')){
                $.ajax({
                    url:"ajaxRequest.php",
                        data:{sFlag:'UnFreezeTest',iTestID:iTestID},
                        async:true,
                        method:"GET",
                        success:function(iResult){
                           window.location='viewTestList.php';
                        }
                });
            }
        });
    });
    function setSample(iWorkflowID,iTestID){
        $.ajax({
            url:"ajaxRequest.php",
            data:{sFlag:'MapTestWorkflow',iTestID:iTestID,iWorkflowID:iWorkflowID},
            async:true,
            method:"GET",
            success:function(iResult){
               window.location='viewTestList.php';
            } 
        });
    }
    </script>
        <div class="modal fade" id="idMapSampleModal" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update File Name</h4>
              </div>
              <div class="modal-body">
                    <input type="text" name="new_workflow_name" id="idNewWorkflowName" class="form-control">
                    <input type="hidden" name="file_id" id="idWorkflowID">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="idRenameWorkflow">Update</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>