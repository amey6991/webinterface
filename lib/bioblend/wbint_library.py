"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 4:
    print("Usage: python wbint_library.py <galaxy-url> <galaxy-API-key> <galaxy-Data-Library-Name>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sLibraryName = sys.argv[3]
sLibraryDesc = 'Library for DataSets'



gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)


oLibrary = gi.libraries.create_library(sLibraryName,sLibraryDesc) 

print (oLibrary['id'])
