"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python wbint_add_dataset.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys
import simplejson as json

from bioblend.galaxy import GalaxyInstance


if len(sys.argv) != 7:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key> <galaxy-Workflow-ID> <galaxy-datasetmap-dict> <galaxy-parametermap-dict> <galaxy-history-id>")
    exit(1)

galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sWorkFlowID = sys.argv[3]
dataset_map = sys.argv[4]
parameters_map = sys.argv[5]
sHistoryID = sys.argv[6]


dataset_map = json.loads(dataset_map)

parameters_map = json.loads(parameters_map)
# print (parameters_map)

# # print (parameters_map)
#exit(1)

bImportInputsToHistory = True

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

# creating History for workflow
aResult = gi.workflows.run_workflow(sWorkFlowID, dataset_map, parameters_map,sHistoryID,
                                    import_inputs_to_history=True)
print (aResult)
# print ('History id is :'+aResult['history'])
print ('Output ID is :'+aResult['outputs'])