"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python wbint_add_dataset.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 8:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key> <galaxy-Library-ID> <galaxy-FastFilePath> <galaxy-folder-id> <galaxy-file-type> <galaxy-db-key>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sLibraryID = sys.argv[3]
sFilePath = sys.argv[4]
sFolderID = sys.argv[5]
file_type = sys.argv[6]
sDBKey = sys.argv[7]
#print("Initiating Galaxy connection")

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

# creating History for workflow
oResult = gi.libraries.upload_from_galaxy_filesystem(sLibraryID, sFilePath, sFolderID, file_type, sDBKey)
print (oResult[0]['id'])