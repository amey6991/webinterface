"""
This SCRIPT retrieves details of history Outputs.

Usage: python wbint_history_output_status.py <galaxy-url> <galaxy-API-key> <history-id>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 4:
    print("Usage: python wbint_history_output_status.py <galaxy-url> <galaxy-API-key> <galaxy-History-ID>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sHistoryID = sys.argv[3]

# get the History status
gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)
oHist = gi.histories.show_history(sHistoryID, contents=False, deleted=None, visible=None, details=None, types=None)
print (oHist['state_ids']['ok'])
