"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 5:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key> <galaxy-Library-ID> <galaxy-Folder-Name> <galaxy-Folder-Desc>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sLibraryID = sys.argv[3]
sFolderName = sys.argv[4]
sFolderDesc = 'Folder Created to store all Datasets'

#print("Initiating Galaxy connection")

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

# creating folder inside the Library
oFolder = gi.libraries.create_folder(sLibraryID,sFolderName,sFolderDesc)

oList = gi.libraries.get_folders(sLibraryID)
for aList in oList:
	sFolderTestName = aList['name'][1:]
	if sFolderTestName==sFolderName:
		folder_id = aList['id']
		print (folder_id)
