"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys
import datetime
from bioblend.galaxy import GalaxyInstance

run_date = '2016-02-11'
run_name = '160211_M01801_0125_000000000-AGTRV'
sdbkey = 'hg19'

if len(sys.argv) != 3:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
gi = GalaxyInstance(url=galaxy_url, key=galaxy_key) 
sHistory_ID ='132016f833b57406' # 132016f833b57406 a0c15f4d91084599
oHist = gi.histories.show_history(sHistory_ID, contents=False, deleted=None, visible=None, details=None, types=None)
print (oHist)
exit(1)
historyid = 'fa6833a4eadf9064'
bGzip=True
bWait=True
gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)
oExport = gi.histories.get_status(historyid)
# print (oExport['state_details']['discarded'])
# print (oExport['state_details']['ok'])
# print (oExport['state_details']['failed_metadata'])
# print (oExport['state_details']['upload'])
# print (oExport['state_details']['paused'])
# print (oExport['state_details']['running'])
# print (oExport['state_details']['setting_metadata'])
# print (oExport['state_details']['error'])
# print (oExport['state_details']['new'])
# print (oExport['state_details']['empty'])
# print (oExport['state'])
# print (oExport['percent_complete'])

# {
# 	'state_details': {
# 		u'discarded': 0, 
# 		u'ok': 32, 
# 		u'failed_metadata': 0, 
# 		u'upload': 0, 
# 		u'paused': 0, 
# 		u'running': 1, 
# 		u'setting_metadata': 0, 
# 		u'error': 0, 
# 		u'new': 0, 
# 		u'queued': 0, 
# 		u'empty': 0
# 	}, 
# 	'state': u'running', 
# 	'percent_complete': 96
# }
#oExport = gi.histories.export_history(historyid, bGzip, False, False, bWait)
# jeha_id = 'f597429621d6eb2b'
# outf = dict()
# oRes = gi.histories.download_history(historyid, jeha_id, outf, chunk_size=4096)
# print (oRes)
# print (outf)
# [
# 	u'48e973444aea3119', 
# 	u'7e9b613206eaaf1e', 
# 	u'2b5a42e310d9ecec', 
# 	u'458643c1fd1ad258', 
# 	u'5c9b009d026b263d', 
# 	u'0bc4510535113457', 
# 	u'943acefe3401b23f', 
# 	u'5bf48ae97a5468bd', 
# 	u'6cbd9dc0b2c641eb', 
# 	u'c902c2778094018d', 
# 	u'55c65f5d4d60c2ed', 
# 	u'620acfcaf710e190', 
# 	u'18eded0954840f15', 
# 	u'65ee55df090144ef', 
# 	u'f1865dc0d4c73197', 
# 	u'930aa2ab09bd6a8c', 
# 	u'd9947e1e5a292284', 
# 	u'b974e36bb6f3a591', 
# 	u'fbe6007e912a4844', 
# 	u'f8951a3b75ecb8c9', 
# 	u'658e80466b680543', 
# 	u'cc8a3628a74b8797', 
# 	u'9813f61fb27a58e8', 
# 	u'0d31124a89b31301', 
# 	u'66959b3071fe278f', 
# 	u'e4fe0382bcdd86a5', 
# 	u'51905535ae9e3a01',
# 	u'05cf8fc5764686de'
# ]

# exit(1)
print("Initiating Galaxy connection")

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

print("Retrieving Workflows list")

workflows = gi.workflows.get_workflows()

if len(workflows) == 0:
    print("There are no Workflows in your account.")
else:
    print("\nWorkflows:")
    for wf_dict in workflows:
        print("{0} : {1}".format(wf_dict['name'], wf_dict['id']))


print("Retrieving Workflow Items")

workflowdetail = gi.workflows.show_workflow('1cd8e2f6b131e891')

# print ("workflow name :"+workflowdetail['name'])
# print ("workflow ID :"+workflowdetail['id'])
# print ("workflow URL :"+workflowdetail['url'])

sReferenceLib = '5a1cff6882ddb5b2'
sReferenceFolderID = 'F2fdbd5c5858e78fb'

sLibraryName = "Test Library Reference"
sLibraryDesc = "Added By using API Code for Reference of Mirati Workflow"

# # print("Adding Data Library")

#oLibrary = gi.libraries.create_library(sLibraryName,sLibraryDesc) 

# # print("Retrieving added Library Information: ")

# print ("library ID :"+oLibrary['id'])
# exit(1)
sLibraryID = '2d9035b3fc152403'

# print ("library Name :"+oLibrary['name'])

# print ("library URL :"+oLibrary['url']) // given the ERROR

# creating folder inside the Library
sFolderName = 'Reference'
sFolderDesc = 'A Reference File directory for Mirati workflow'

#oFolder = gi.libraries.create_folder(sReferenceLib,sFolderName,sFolderDesc)

oList = gi.libraries.get_folders(sReferenceLib)
for aList in oList:
	sFolderTestName = aList['name'][1:]
	if sFolderTestName==sFolderName:
		folder_id = aList['id']
		#print ('folder id: '+folder_id)


sFolderID = 'F964b37715ec9bd22'


# Json List of all the folder in the library

sPileUpFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/NA19204.copynormal.pileup'
sDataSetIDPile = 'd7ee944608c6557c'
sBaitFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/mirati_version_2_bait_region_for_picard.bed'
sDataSetIDBait = 'a48a0c0293ce7a88'
sTargetFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/mirati_version_2_target_region_for_picard.bed'
sDataSetIDTarget = '6da2a95430bdafdf'
#sReferenceFolderID = 'Fba751ee0539fff04'
reference_file_type = 'auto'

# oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sPileUpFile, sReferenceFolderID, reference_file_type,dbkey=sdbkey)
# print (oResult)
# oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sBaitFile, sReferenceFolderID, reference_file_type,dbkey=sdbkey)
# print (oResult)
# oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sTargetFile, sReferenceFolderID, reference_file_type,dbkey=sdbkey)
# print (oResult)


#exit(1)
server_dir = '/home/plus91/data/MiSeqRuns/160211_M01801_0125_000000000-AGTRV/Data/Intensities/BaseCalls/'
folder_id ='Fb472e2eb553fa0d1' 
# # #Fdf7a1f0c02a5b08e

# oFileResult = gi.libraries.upload_file_from_server('a799d38679e985db', server_dir, folder_id,'fastq',dbkey=sdbkey)
# print (oFileResult)
# exit(1)
# for aFile in oFileResult:
# 	sFileID = aFile['id']
# 	sFileName = aFile['name']
# 	print (sFileID+' :'+sFileName)
# 	sFileUrl = aFile['url']


file_url = '/home/plus91/data/MiSeqRuns/160211_M01801_0125_000000000-AGTRV/Data/Intensities/BaseCalls/Spikein1_S5_L001_R2_001.fastq.gz';
file_type='fastq'; #
# folder_id ='F5969b1f7201f12ae'

# [{u'url': u'/api/libraries/a799d38679e985db/contents/59ace41fc068d3ad', u'name': u'Pasted Entry', u'id': u'59ace41fc068d3ad'}]
# oResult = gi.libraries.upload_file_from_url(sLibraryID, file_url, folder_id, file_type)
# print (oResult)

# No such file or Directory
# oResult = gi.libraries.upload_file_from_local_path(sLibraryID, file_url, folder_id, file_type,dbkey=sdbkey)
# print (oResult)

# It is workiing ---- IMP

# oResult = gi.libraries.upload_from_galaxy_filesystem(sLibraryID, file_url, sFolderID, file_type,dbkey=sdbkey)
# print (oResult)
# exit(1)
# 25d23a09c720d5c6
# [{u'url': u'/api/libraries/a799d38679e985db/contents/339ac952dd54f240', u'name': u'Spikein1_S5_L001_R1_001.fastq', u'id': u'339ac952dd54f240'}]
# [{u'url': u'/api/libraries/a799d38679e985db/contents/d448712f90897b61', u'name': u'Spikein1_S5_L001_R2_001.fastq', u'id': u'd448712f90897b61'}]

# history_name = 'Test History'
# oHistory = gi.histories.create_history(history_name)
# print (oHistory['id'])
# exit(1)
# sHisitoryID = oHistory['id']
# gi.histories.delete_history('5a1cff6882ddb5b2')
# oHistory = gi.histories.get_histories()
# print (oHistory)
# # # 911dde3ddb677bcd
# exit(1)



sHisitoryID = 'bf60fd5f5f7f44bf' #For 'Test History'
#sHisitoryID = '1fad1eaf5f4f1766' # For 'Test History New 1'
#sHisitoryID = '2fdbd5c5858e78fb' # For 'Test History New 2'

# oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, sDataSetIDTarget)
# oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, sDataSetIDPile)
# oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, sDataSetIDBait)
# oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, 'a89326c04f05a73f')
# oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, 'c4fa4dcd0ef9867a')
# exit(1)
sWorkFlowID = '1cd8e2f6b131e891'; #  1cd8e2f6b131e891  (Mirati)

# aResult = gi.workflows.show_workflow(sWorkFlowID)
# print (aResult)
#exit(1)
bImportInputsToHistory = True;
dataset_map = dict()
dataset_map['0'] = { 'src':'ld', 'id': '9ec39b9650fa3424' }
dataset_map['1'] = { 'src':'ld', 'id': 'e4ce61a6ecb936ab' }
dataset_map['2'] = { 'src':'ld', 'id': 'd7ee944608c6557c' }
dataset_map['3'] = { 'src':'ld', 'id': 'a48a0c0293ce7a88' }
dataset_map['4'] = { 'src':'ld', 'id': '6da2a95430bdafdf' }

#print (dataset_map)
run_name = 'MGCDv2.1'
sample_name = 'Spikein1'
run_date = '2016-07-12'
rglb = 'Clinical'

parameters_map = {
    'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0': {
        'rgid': run_name,
        'rgsm': sample_name,
        'rglb': rglb,
        'rgdt' : run_date
    },
    'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65': {
        'contaminants': None,
        'limits': None
    }
}
#dataset_map = {"1":{"src":"ld","id":"c4ce012fb71d3955"},"0":{"src":"ld","id":"d4d1d55a9a009572"},"3":{"src":"ld","id":"ea8cd5be190fc233"},"2":{"src":"ld","id":"d881ac1fa95a104e"},"4":{"src":"ld","id":"6a65222fe2393f8f"}}
# parameters_map = {"toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0":{"rgid":"MGCDv2.1","rgsm":"Spikein1","rglb":"2016-07-12","rgdt":"Clinical"},"toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65":{"contaminants":"None","limits":"None"}}
# sHistoryID = '6505e875ddb66fd2'
aResult = gi.workflows.run_workflow(sWorkFlowID, dataset_map, parameters_map,sHisitoryID,
                                    bImportInputsToHistory)
print (aResult)
exit(1)

#history_id = '0c5ffef6d88a1e97' # 

# jeha_id = 'f2db41e1fa331b3e'
# iChunkSize=4096

# STATUS of HISTORY

# oStatus = gi.histories.get_status('911dde3ddb677bcd')
# print (oStatus)
# aState = oStatus['state']
# aState = oStatus['percent_complete']
# aState = oStatus['state_details']
# aState['discarded ok failed_metadata upload paused running setting_metadata error new queued empty']

# outf = dict()
# print (gi.histories.download_history(history_id, jeha_id, outf, iChunkSize))
# exit(1)

# print ('Output is Here')
# print (aResult['history'])
# print (aResult['outputs'])



	# u'name': u'Mirati MGCDv2.1 Tumor Panel v2.2', 
	# u'tags': [], 
	# u'deleted': False, 
	# u'latest_workflow_uuid': u'762db124-81b2-45ad-86f3-35ca3908c618', 
	# u'id': u'1cd8e2f6b131e891', 
	# u'url': u'/api/workflows/1cd8e2f6b131e891', 
	# u'steps': 
	# 	{
	# 		u'20': 
	# 			{
	# 				u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/samtools_mpileup/samtools_mpileup/2.0', 
	# 				u'tool_version': u'2.0', 
	# 				u'annotation': None, 
	# 				u'input_steps': 
	# 					{
	# 						u'reference_source|input_bams_0|input_bam': 
	# 							{
	# 								u'step_output': u'output', 
	# 								u'source_step': 17
	# 							}
	# 					}, 
	# 				u'tool_inputs': 
	# 					{
	# 						u'advanced_options': u'{"advanced_options_selector": "basic", "__current_case__": 1}', 
	# 						u'genotype_likelihood_computation_type': u'{"genotype_likelihood_computation_type_selector": "perform_genotype_likelihood_computation", "output_format": "--VCF", "perform_indel_calling": {"__current_case__": 0, "perform_indel_calling_selector": "perform_indel_calling_def"}, "compressed": "false", "__current_case__": 0, "output_tags": null}', 
	# 						u'reference_source': u'{"ref_file": "hg19full", "reference_source_selector": "cached", "input_bams": [{"__index__": 0, "input_bam": {"__class__": "RuntimeValue"}}], "__current_case__": 0}'}, u'type': u'tool', u'id': 20}, u'21': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/freebayes/freebayes/0.4.1', u'tool_version': u'0.4.1', u'annotation': None, u'input_steps': {u'reference_source|input_bams_0|input_bam': {u'step_output': u'output', u'source_step': 17}}, u'tool_inputs': {u'reference_source': u'{"ref_file": "hg19full", "reference_source_selector": "cached", "input_bams": [{"__index__": 0, "input_bam": {"__class__": "RuntimeValue"}}], "__current_case__": 0}', u'options_type': u'{"options_type_selector": "full", "allele_scope": {"__current_case__": 1, "allele_scope_selector": "false"}, "reporting": {"reporting_selector": "false", "__current_case__": 1}, "genotype_likelihoods": {"__current_case__": 1, "genotype_likelihoods_selector": "false"}, "O": "false", "population_model": {"population_model_selector": "false", "__current_case__": 1}, "reference_allele": {"reference_allele_selector": "false", "__current_case__": 1}, "optional_inputs": {"optional_inputs_selector": "false", "__current_case__": 1}, "__current_case__": 0, "population_mappability_priors": {"__current_case__": 1, "population_mappability_priors_selector": "false"}, "input_filters": {"min_coverage": "50", "C": "2", "e": "1000", "__current_case__": 0, "G": "1", "F": "0.02", "standard_filters": "false", "m": "20", "q": "20", "mismatch_filters": {"mismatch_filters_selector": "false", "__current_case__": 1}, "R": "25", "use_duplicate_reads": "false", "Y": "25", "input_filters_selector": "true", "min_alternate_qsum": "0"}, "algorithmic_features": {"algorithmic_features_selector": "false", "__current_case__": 1}}', u'target_limit_type': u'{"target_limit_type_selector": "do_not_limit", "__current_case__": 0}'}, u'type': u'tool', u'id': 21}, u'22': {u'tool_id': u'192.168.35.58:9009/repos/plus91/pindel/pindel/0.1.2', u'tool_version': u'0.1.2', u'annotation': None, u'input_steps': {u'input_bam': {u'step_output': u'output', u'source_step': 17}}, u'tool_inputs': {u'genomeSource': u'{"indices": "hg19full", "refGenomeSource": "indexed", "__current_case__": 0}', u'input_bam': u'{"__class__": "RuntimeValue"}', u'insert_size': u'"100"'}, u'type': u'tool', u'id': 22}, u'23': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/iuc/snpeff/snpEff/4.0.0', u'tool_version': u'4.0.0', u'annotation': None, u'input_steps': {u'input': {u'step_output': u'output_vcf', u'source_step': 21}}, u'tool_inputs': {u'spliceSiteSize': u'"2"', u'noLog': u'"true"', u'udLength': u'"5000"', u'inputFormat': u'"vcf"', u'outputConditional': u'{"outputFormat": "vcf", "__current_case__": 0}', u'filterOut': u'null', u'generate_stats': u'"true"', u'filter': u'{"specificEffects": "no", "__current_case__": 0}', u'chr': u'""', u'intervals': u'{"__class__": "RuntimeValue"}', u'snpDb': u'{"genome_version": "GRCh37.74", "__current_case__": 2, "genomeSrc": "named"}', u'offset': u'"default"', u'input': u'{"__class__": "RuntimeValue"}', u'transcripts': u'{"__class__": "RuntimeValue"}', u'annotations': u'null'}, u'type': u'tool', u'id': 23}, u'1': {u'tool_id': None, u'tool_version': None, u'annotation': None, u'input_steps': {}, u'tool_inputs': {u'name': u'Reverse Reads'}, u'type': u'data_input', u'id': 1}, u'0': {u'tool_id': None, u'tool_version': None, u'annotation': None, u'input_steps': {}, u'tool_inputs': {u'name': u'Forward Reads'}, u'type': u'data_input', u'id': 0}, u'3': {u'tool_id': None, u'tool_version': None, u'annotation': None, u'input_steps': {}, u'tool_inputs': {u'name': u'Target Region'}, u'type': u'data_input', u'id': 3}, u'2': {u'tool_id': None, u'tool_version': None, u'annotation': None, u'input_steps': {}, u'tool_inputs': {u'name': u'NA19204 pileup'}, u'type': u'data_input', u'id': 2}, u'5': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/kevyin/fastq_groomer_parallel/fastq_groomer_parallel/0.3.1', u'tool_version': u'0.3.1', u'annotation': None, u'input_steps': {u'input_file': {u'step_output': u'output', u'source_step': 0}}, u'tool_inputs': {u'input_type': u'"sanger"', u'options_type': u'{"__current_case__": 0, "options_type_selector": "basic"}', u'input_file': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 5}, u'4': {u'tool_id': None, u'tool_version': None, u'annotation': None, u'input_steps': {}, u'tool_inputs': {u'name': u'Bait Region'}, u'type': u'data_input', u'id': 4}, u'7': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/pjbriggs/trimmomatic/trimmomatic/0.32.3', u'tool_version': u'0.32.3', u'annotation': None, u'input_steps': {u'paired_end|paired_input_type_conditional|fastq_r1_in': {u'step_output': u'output_file', u'source_step': 5}, u'paired_end|paired_input_type_conditional|fastq_r2_in': {u'step_output': u'output_file', u'source_step': 6}}, u'tool_inputs': {u'operations': u'[{"__index__": 0, "operation": {"window_size": "4", "name": "SLIDINGWINDOW", "__current_case__": 0, "required_quality": "20"}}]', u'paired_end': u'{"is_paired_end": "true", "paired_input_type_conditional": {"paired_input_type": "pair_of_files", "fastq_r1_in": {"__class__": "RuntimeValue"}, "__current_case__": 0, "fastq_r2_in": {"__class__": "RuntimeValue"}}, "__current_case__": 1}', u'illuminaclip': u'{"do_illuminaclip": "false", "__current_case__": 1}'}, u'type': u'tool', u'id': 7}, u'6': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/kevyin/fastq_groomer_parallel/fastq_groomer_parallel/0.3.1', u'tool_version': u'0.3.1', u'annotation': None, u'input_steps': {u'input_file': {u'step_output': u'output', u'source_step': 1}}, u'tool_inputs': {u'input_type': u'"sanger"', u'options_type': u'{"__current_case__": 0, "options_type_selector": "basic"}', u'input_file': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 6}, u'9': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/fastqc/0.65', u'tool_version': u'0.65', u'annotation': None, u'input_steps': {u'input_file': {u'step_output': u'fastq_out_r1_paired', u'source_step': 7}}, u'tool_inputs': {u'contaminants': u'{"__class__": "RuntimeValue"}', u'limits': u'{"__class__": "RuntimeValue"}', u'input_file': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 9}, u'8': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/bwa/bwa_mem/0.7.12.1', u'tool_version': u'0.7.12.1', u'annotation': None, u'input_steps': {u'fastq_input|fastq_input1': {u'step_output': u'fastq_out_r1_paired', u'source_step': 7}, u'fastq_input|fastq_input2': {u'step_output': u'fastq_out_r2_paired', u'source_step': 7}}, u'tool_inputs': {u'analysis_type': u'{"analysis_type_selector": "full", "algorithmic_options": {"algorithmic_options_selector": "do_not_set", "__current_case__": 1}, "io_options": {"a": "false", "C": "false", "io_options_selector": "set", "h": "5", "M": "true", "T": "30", "__current_case__": 0, "V": "false", "Y": "false"}, "__current_case__": 2, "scoring_options": {"scoring_options_selector": "do_not_set", "__current_case__": 1}}', u'reference_source': u'{"ref_file": "hg19full", "reference_source_selector": "cached", "__current_case__": 0}', u'rg': u'{"rg_selector": "do_not_set", "__current_case__": 3}', u'fastq_input': u'{"iset_stats": "", "fastq_input2": {"__class__": "RuntimeValue"}, "__current_case__": 0, "fastq_input_selector": "paired", "fastq_input1": {"__class__": "RuntimeValue"}}'}, u'type': u'tool', u'id': 8}, u'11': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.136.0', u'tool_version': u'1.136.0', u'annotation': None, u'input_steps': {u'inputFile': {u'step_output': u'bam_output', u'source_step': 8}}, u'tool_inputs': {u'read_group_sm_conditional': u'{"do_auto_name": "false", "SM": {"__class__": "RuntimeValue"}, "__current_case__": 1}', u'CN': u'""', u'PU': u'"Lane-1"', u'read_group_lb_conditional': u'{"do_auto_name": "false", "LB": {"__class__": "RuntimeValue"}, "__current_case__": 1}', u'read_group_id_conditional': u'{"do_auto_name": "false", "ID": {"__class__": "RuntimeValue"}, "__current_case__": 1}', u'validation_stringency': u'"LENIENT"', u'DT': u'{"__class__": "RuntimeValue"}', u'PI': u'""', u'DS': u'""', u'PL': u'"ILLUMINA"', u'inputFile': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 11}, u'10': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/fastqc/0.65', u'tool_version': u'0.65', u'annotation': None, u'input_steps': {u'input_file': {u'step_output': u'fastq_out_r2_paired', u'source_step': 7}}, u'tool_inputs': {u'contaminants': u'{"__class__": "RuntimeValue"}', u'limits': u'{"__class__": "RuntimeValue"}', u'input_file': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 10}, u'13': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_MarkDuplicates/1.136.0', u'tool_version': u'1.136.0', u'annotation': None, u'input_steps': {u'inputFile': {u'step_output': u'output', u'source_step': 12}}, u'tool_inputs': {u'duplicate_scoring_strategy': u'"SUM_OF_BASE_QUALITIES"', u'remove_duplicates': u'"true"', u'read_name_regex': u'"[a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*."', u'validation_stringency': u'"LENIENT"', u'comments': u'[]', u'assume_sorted': u'"true"', u'optical_duplicate_pixel_distance': u'"100"', u'inputFile': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 13}, u'12': {u'tool_id': u'192.168.35.58:9009/repos/plus91/samtools_filter/samtools_filter_v_0_1_0/0.1.0', u'tool_version': u'0.1.0', u'annotation': None, u'input_steps': {u'input_bam': {u'step_output': u'outFile', u'source_step': 11}}, u'tool_inputs': {u'input_bam': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 12}, u'15': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_CASM/1.136.1', u'tool_version': u'1.136.1', u'annotation': None, u'input_steps': {u'inputFile': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'bisulphite': u'"false"', u'adapters': u'[]', u'assume_sorted': u'"true"', u'metric_accumulation_level': u'"ALL_READS"', u'validation_stringency': u'"LENIENT"', u'reference_source': u'{"ref_file": "hg19full", "reference_source_selector": "cached", "__current_case__": 0}', u'maxinsert': u'"100000"', u'inputFile': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 15}, u'14': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_CollectInsertSizeMetrics/1.136.0', u'tool_version': u'1.136.0', u'annotation': None, u'input_steps': {u'inputFile': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'deviations': u'"10.0"', u'hist_width': u'""', u'assume_sorted': u'"true"', u'metric_accumulation_level': u'"ALL_READS"', u'validation_stringency': u'"LENIENT"', u'reference_source': u'{"ref_file": "hg19full", "reference_source_selector": "cached", "__current_case__": 0}', u'min_pct': u'"0.05"', u'inputFile': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 14}, u'17': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/aaronquinlan/bedtools/bedtools_intersectbed_bam/0.1.0', u'tool_version': u'0.1.0', u'annotation': None, u'input_steps': {u'inputB': {u'step_output': u'output', u'source_step': 3}, u'inputA': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'reciprocal': u'"false"', u'invert': u'"false"', u'inputB': u'{"__class__": "RuntimeValue"}', u'inputA': u'{"__class__": "RuntimeValue"}', u'split': u'"true"', u'fraction': u'""', u'strand': u'""'}, u'type': u'tool', u'id': 17}, u'16': {u'tool_id': u'192.168.35.58:9009/repos/plus91/picard_researchdx/picard_HsMetrics/1.126.0', u'tool_version': u'1.126.0', u'annotation': None, u'input_steps': {u'target_bed': {u'step_output': u'output', u'source_step': 3}, u'bait_bed': {u'step_output': u'output', u'source_step': 4}, u'input_file': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'target_bed': u'{"__class__": "RuntimeValue"}', u'bait_bed': u'{"__class__": "RuntimeValue"}', u'out_prefix': u'"Picard HS Metrics"', u'input_file': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 16}, u'19': {u'tool_id': u'toolshed.g2.bx.psu.edu/repos/aaronquinlan/bedtools/bedtools_genomecoveragebed_bedgraph/0.1.0', u'tool_version': u'0.1.0', u'annotation': None, u'input_steps': {u'input': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'zero_regions': u'"false"', u'input': u'{"__class__": "RuntimeValue"}', u'scale': u'""', u'split': u'"false"', u'strand': u'""'}, u'type': u'tool', u'id': 19}, u'18': {u'tool_id': u'192.168.35.58:9009/repos/plus91/hardsearch/hardsearch_v_0_1_2/0.1.2', u'tool_version': u'0.1.2', u'annotation': None, u'input_steps': {u'input_bed': {u'step_output': u'output', u'source_step': 3}, u'input_bam': {u'step_output': u'outFile', u'source_step': 13}}, u'tool_inputs': {u'input_bed': u'{"__class__": "RuntimeValue"}', u'read_count': u'"20"', u'input_bam': u'{"__class__": "RuntimeValue"}'}, u'type': u'tool', u'id': 18}},
	# 						u'published': False, 
	# 						u'owner': u'plus91', 
	# 						u'model_class': u'StoredWorkflow', 
	# 						u'annotation': u''
	# 	}

#Inputs
# {
# 	u'1': 
# 		{
# 			u'uuid': u'e3bbadad-aea0-4e9b-8784-feaf8139802e', 
# 			u'value': u'', 
# 			u'label': u'Reverse Reads'
# 		}, 
# 	u'0': 
# 		{
# 			u'uuid': u'25e5e172-4892-4c5d-8c32-8b0b6083312f', 
# 			u'value': u'', 
# 			u'label': u'Forward Reads'
# 		}, 
# 	u'3': 
# 		{
# 			u'uuid': u'a895492d-5259-4a92-88d1-8aff2fbd7b9d', 
# 			u'value': u'', 
# 			u'label': u'Target Region'
# 		}, 
# 	u'2': 
# 		{
# 			u'uuid': u'8565349d-4f3e-418c-87f6-dd6df19c1150', 
# 			u'value': u'', 
# 			u'label': u'NA19204 pileup'
# 		}, 
# 	u'4': 
# 		{
# 			u'uuid': u'e24b54a5-6858-40be-98d6-b1a4af08ee27', 
# 			u'value': u'', 
# 			u'label': u'Bait Region'
# 		}
# 	}

# First run workflow Output

# {
# 	u'inputs': {}, 
# 	u'update_time': u'2016-07-12T06:53:36.541693', 
# 	u'uuid': u'5b4c00bc-47fd-11e6-86a9-127b317b56fd', 
# 	u'outputs': [], 
# 	u'history_id': u'4b187121143038ff', 
# 	u'workflow_id': u'2f94e8ae9edff68a', 
# 	u'state': u'scheduled', 
# 	u'steps': [
# 				{
# 					u'workflow_step_label': None, 
# 					u'update_time': u'2016-07-12T06:53:36.543470', 
# 					u'job_id': None, 
# 					u'state': None, 
# 					u'workflow_step_uuid': u'a42ce1db-77f5-4faa-a597-4a720ac5881e', 
# 					u'order_index': 0, 
# 					u'action': None, 
# 					u'model_class': u'WorkflowInvocationStep', 
# 					u'workflow_step_id': u'5935f39e477d88b2', 
# 					u'id': u'bf60fd5f5f7f44bf'
# 				}, 
# 				{
# 					u'workflow_step_label': None, 
# 					u'update_time': u'2016-07-12T06:53:36.544375', 
# 					u'job_id': None, 
# 					u'state': None, 
# 					u'workflow_step_uuid': u'3df6eff8-4c5d-4d65-bfe6-3f665553e766', 
# 					u'order_index': 1, 
# 					u'action': None, 
# 					u'model_class': u'WorkflowInvocationStep', 
# 					u'workflow_step_id': u'a73186a6d4577bbf', 
# 					u'id': u'90240358ebde1489'
# 				}
# 			], 
# 	u'model_class': u'WorkflowInvocation', 
# 	u'id': u'33b43b4e7093c91f', 
# 	u'history': u'4b187121143038ff'
# }



# Other Info
# 55504e7a2466a2e3 e38f593eae81d119 c333314861e68c5c ba1915f3923e3bf1 bd0beca16aa307c6 dfd15528ee538abe 7ef8021ae23ac2fc 
# [{u'url': u'/api/libraries/a799d38679e985db/contents/4eb81b04b33684fd', u'name': u'16151000_S2_L001_R1_001.fastq', u'id': u'4eb81b04b33684fd'}, {u'url': u'/api/libraries/a799d38679e985db/contents/5761546ab79a71f2', u'name': u'16151000_S2_L001_R2_001.fastq', u'id': u'5761546ab79a71f2'}, {u'url': u'/api/libraries/a799d38679e985db/contents/68013dab1c13fb37', u'name': u'22036000_S1_L001_R1_001.fastq', u'id': u'68013dab1c13fb37'}, {u'url': u'/api/libraries/a799d38679e985db/contents/60e680a037f41974', u'name': u'22036000_S1_L001_R2_001.fastq', u'id': u'60e680a037f41974'}, {u'url': u'/api/libraries/a799d38679e985db/contents/6d9affd96770ffb9', u'name': u'33414000_S4_L001_R1_001.fastq', u'id': u'6d9affd96770ffb9'}, {u'url': u'/api/libraries/a799d38679e985db/contents/55504e7a2466a2e3', u'name': u'33414000_S4_L001_R2_001.fastq', u'id': u'55504e7a2466a2e3'}, {u'url': u'/api/libraries/a799d38679e985db/contents/e38f593eae81d119', u'name': u'99957000_S3_L001_R1_001.fastq', u'id': u'e38f593eae81d119'}, {u'url': u'/api/libraries/a799d38679e985db/contents/c333314861e68c5c', u'name': u'99957000_S3_L001_R2_001.fastq', u'id': u'c333314861e68c5c'}, {u'url': u'/api/libraries/a799d38679e985db/contents/ba1915f3923e3bf1', u'name': u'Spikein1_S5_L001_R1_001.fastq', u'id': u'ba1915f3923e3bf1'}, {u'url': u'/api/libraries/a799d38679e985db/contents/bd0beca16aa307c6', u'name': u'Spikein1_S5_L001_R2_001.fastq', u'id': u'bd0beca16aa307c6'}, {u'url': u'/api/libraries/a799d38679e985db/contents/dfd15528ee538abe', u'name': u'Spikein2_S6_L001_R1_001.fastq', u'id': u'dfd15528ee538abe'}, {u'url': u'/api/libraries/a799d38679e985db/contents/7ef8021ae23ac2fc', u'name': u'Spikein2_S6_L001_R2_001.fastq', u'id': u'7ef8021ae23ac2fc'}]


# oDataset = gi.datasets.show_dataset(sLibraryID,'25d23a09c720d5c6')
# print (oDataset)
# exit(1)

# {
# 	u'accessible': True, 
# 	u'type_id': u'dataset-a799d38679e985db', 
# 	u'file_name': u'/home/plus91/galaxy/database/files/000/dataset_6.dat', 
# 	u'resubmitted': False, 
# 	u'create_time': u'2016-06-01T23:23:01.280428', 
# 	u'creating_job': u'a799d38679e985db', 
# 	u'file_size': 337088060, 
# 	u'dataset_id': u'a799d38679e985db', 
# 	u'id': u'a799d38679e985db', 
# 	u'misc_info': u"Groomed 956720 sanger reads into sanger reads.\ntotal_valid_formats= set(['sanger'])\nBased upon quality and sequence, the input data is valid for: sanger\nInput ASCII range: '#'(35) - 'I'(73)\nInput decimal range: 2 - 40\nGroomer took: 291020.749 ms using 1",
# 	u'hda_ldda': u'hda', 
# 	u'download_url': u'/api/histories/f597429621d6eb2b/contents/a799d38679e985db/display', 
# 	u'state': u'ok', 
# 	u'display_types': [], 
# 	u'display_apps': [], 
# 	u'permissions': {
# 				u'access': [], 
# 				u'manage': [u'f2db41e1fa331b3e']
# 				},
# 	u'type': u'file', 
# 	u'misc_blurb': u'321.5 MB', 
# 	u'peek': u'<table cellspacing="0" cellpadding="3"><tr><td>@M01801:123:000000000-AGVJK:1:1101:15130:1535 1:N:0:3</td></tr><tr><td>TACAGACATTCTAATGAAACTTTAAAGAGAGAAGCAAAAACAAACAAACAAACAAACAAAAAACAAAACTTTGGAGTTCAAGTTACAGGTCCATGTCTGATATATGTGTTCAGAAAGCAACAAAATGTAGAATATTTAAAACCATATTGCT</td></tr><tr><td>+</td></tr><tr><td>1&gt;&gt;AA1CCFFFFGFGCCBGGGGHD31DBFFEGCFGFFBBGEHCHHHGHHGFFGFAFHHGHFGGGHGFHGHHHHHFGHHHFFFHHGHHGFHGHHHHHHHHFHEGHHHHHHHHHHHEFHGHGHHHAFHHHEHFHHHHHHGFFHHGGHHHHHHH</td></tr><tr><td>@M01801:123:000000000-AGVJK:1:1101:17173:1536 1:N:0:3</td></tr><tr><td>TTTTTCTCTTGTCTAGGAGAATCATATTTTGAACTGGACTAAAAATTTGCTCTGATTAATAATTTTTACAATTCTGATCCCCCAATGCATCAACCTGCAGGAAACCTTACTATGTTGGGCTTCATACTGGGATCCATGATGTTGTAGCTGC</td></tr></table>', 
# 	u'update_time': u'2016-06-01T23:28:06.961303', 
# 	u'data_type': u'galaxy.datatypes.sequence.FastqSanger', 
# 	u'tags': [], 
# 	u'deleted': False, 
# 	u'history_id': u'f597429621d6eb2b', 
# 	u'meta_files': [], 
# 	u'genome_build': u'hg19', 
# 	u'metadata_sequences': None, 
# 	u'hid': 6, 
# 	u'visualizations': [
# 			{
# 				u'href': u'/plugins/visualizations/graphviz/show?dataset_id=a799d38679e985db', 
# 				u'target': u'galaxy_main', 
# 				u'html': u'Graph Visualization', 
# 				u'embeddable': False
# 			}], 
# 	u'metadata_data_lines': None, 
# 	u'file_ext': u'fastqsanger', 
# 	u'annotation': None, 
# 	u'metadata_dbkey': u'hg19', 
# 	u'history_content_type': u'dataset', 
# 	u'name': u'FASTQ Parallel Groomer on data 4', 
# 	u'extension': u'fastqsanger', 
# 	u'visible': True, 
# 	u'url': u'/api/histories/f597429621d6eb2b/contents/a799d38679e985db', 
# 	u'uuid': u'934777e8-6df2-48ce-8dd3-8e09c8171a96', 
# 	u'model_class': u'HistoryDatasetAssociation', 
# 	u'rerunnable': True, 
# 	u'purged': False, 
# 	u'api_type': u'file'
# }


# {u'importable': False, u'create_time': u'2016-07-21T17:05:57.249845', u'contents_url': u'/api/histories/132016f833b57406/contents', u'id': u'132016f833b57406', u'size': 86098439, u'user_id': u'f2db41e1fa331b3e', u'username_and_slug': None, u'annotation': None, u'state_details': {u'discarded': 0, u'ok': 33, u'failed_metadata': 0, u'upload': 0, u'paused': 0, u'running': 0, u'setting_metadata': 0, u'error': 0, u'new': 0, u'queued': 0, u'empty': 0}, u'state': u'ok', u'empty': False, u'update_time': u'2016-07-21T17:06:05.188555', u'tags': [], u'deleted': False, u'genome_build': None, u'slug': None, u'name': u'Test History New 4', u'url': u'/api/histories/132016f833b57406', u'state_ids': {u'discarded': [], 
# u'ok': [
# 	u'556237b7328da3af', 
# 	u'dcf349068765fa56', 
# 	u'f7bbb5e66d40415a', 
# 	u'd216b0eb84f63cf4', 
# 	u'be439a3589617e67', 
# Output 
# u'6e01c0e0a145bde3', 
# u'1fe8025a3eb5c482', 
# u'a87795113868c92b', 
# u'39ac0e5728f31c3f', 
# u'3a437f8167ca4c87', 
# u'a83adc160e0a77a0', 
# u'b1da20f285d33353', 
# u'0375199fa16cf1e6', 
# u'8be3d18182a9e60b', 
# u'1bca6b54cf56afed', 
# u'871ce98d8c078309', 
# u'4969f1ebf8379add', 
# u'e373ca7c82675833', 
# u'303d4722059b63c2', 
# u'dc72786b0d4875fb', 
# u'26d704460e4a4985', 
# u'5786ce0546cbc07f', 
# u'80c7a7e8240dd658', 
# u'0ff195fdd529b966', 
# u'4a86c8de6f7e9376', 
# u'13c07ce0ed2cea74', 
# u'07630b166a59bdad', 
# u'fae76ced2b7e97d7', 
# u'18c1ddee9d6509e0',
# u'fb1af791c00dd0e4', 
# u'7e5a6282b7150cce', 
# u'78176dfc865c8302', 
# u'43c04a4df15a1f7a'], 
