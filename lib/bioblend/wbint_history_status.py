"""
This SCRIPT retrieves details of history status.

Usage: python wbint_history_status.py <galaxy-url> <galaxy-API-key> <history-id>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 5:
    print("Usage: python wbint_history_status.py <galaxy-url> <galaxy-API-key> <galaxy-History-ID>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sHistoryID = sys.argv[3]
sCondition = sys.argv[4]

# get the History status
gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)
oExport = gi.histories.get_status(sHistoryID)
# it return three values
if sCondition=='1':
	print (oExport['percent_complete'])
if sCondition=='2':
	print (oExport['state'])