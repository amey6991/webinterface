from __future__ import print_function
import sys
import datetime
from bioblend.galaxy import GalaxyInstance

run_name = '160211_M01801_0125_000000000-AGTRV'

if len(sys.argv) != 3:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]

print("Initiating Galaxy connection")

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

sWorkFlowID = '1cd8e2f6b131e891'; # (Mirati)

oDownload = gi.histories.download_dataset('a0c15f4d91084599', '0ff195fdd529b966', '/var/www/', use_default_filename=True)
print (oDownload)
exit(1)

sLibraryName = "API library Reference"
sLibraryDesc = 'DataSet Library'
oLibrary = gi.libraries.create_library(sLibraryName,sLibraryDesc) 
sLibraryID = oLibrary['id']

sFolderName = 'Test folder Dataset'
sFolderDesc = 'Test Description'
oFolder = gi.libraries.create_folder(sLibraryID,sFolderName,sFolderDesc)

oList = gi.libraries.get_folders(sLibraryID)
for aList in oList:
    sFolderTestName = aList['name'][1:]
    if sFolderTestName==sFolderName:
        sFolderID = aList['id']

sFolderID = sFolderID
sdbkey = 'hg19'
# upload R1 fastq file

file_url = '/home/plus91/data/MiSeqRuns/160211_M01801_0125_000000000-AGTRV/Data/Intensities/BaseCalls/Spikein1_S5_L001_R1_001.fastq.gz'; # throug this i Add both files for input dataset
file_type='fastq';
oResult = gi.libraries.upload_from_galaxy_filesystem(sLibraryID, file_url, sFolderID, file_type, dbkey=sdbkey)
print (oResult) # i get the dataset ID from this object
sDataSetIDForward = oResult[0]['id']
# upload R2 fastq file

file_url = '/home/plus91/data/MiSeqRuns/160211_M01801_0125_000000000-AGTRV/Data/Intensities/BaseCalls/Spikein1_S5_L001_R2_001.fastq.gz'; # throug this i Add both files for input dataset
oResult = gi.libraries.upload_from_galaxy_filesystem(sLibraryID, file_url, sFolderID, file_type, dbkey=sdbkey)
print (oResult) # i get the dataset ID from this object
sDataSetIDReverse = oResult[0]['id']


sReferenceLibName = 'API library Reference'
sLibraryDesc = 'Refrence Library 1'
oLibrary = gi.libraries.create_library(sReferenceLibName,sLibraryDesc) 
sReferenceLib = oLibrary['id']

sReferenceFolderName = 'Reference'
sFolderDesc = 'Test folder for reference'
oFolder = gi.libraries.create_folder(sReferenceLib,sReferenceFolderName,sFolderDesc)

oList = gi.libraries.get_folders(sReferenceLib)
for aList in oList:
    sFolderTestName = aList['name'][1:]
    if sFolderTestName==sReferenceFolderName:
        sReferenceFolderID = aList['id']
        
sReferenceFolderID = sReferenceFolderID
reference_file_type = 'auto'

# Upload the reference Dataset files

sPileUpFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/NA19204.copynormal.pileup'
oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sPileUpFile, sReferenceFolderID, reference_file_type, dbkey=sdbkey)
sDataSetIDPile = oResult[0]['id']
#sDataSetIDPile = '84f63edc852e8a15' # ID got from above object
sBaitFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/mirati_version_2_bait_region_for_picard.bed'
oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sBaitFile, sReferenceFolderID, reference_file_type, dbkey=sdbkey)
sDataSetIDBait = oResult[0]['id']
#sDataSetIDBait = '1efe9eb0bff40152' # ID got from above object
sTargetFile = '/home/plus91/data/UploadDatasets/plus91@researchdx.com/TMP/mirati_version_2_target_region_for_picard.bed'
oResult = gi.libraries.upload_from_galaxy_filesystem(sReferenceLib, sTargetFile, sReferenceFolderID, reference_file_type, dbkey=sdbkey)
sDataSetIDTarget = oResult[0]['id']
#sDataSetIDTarget = 'a3ff968b01c9046c' # ID got from above object

# Create history 

history_name = 'Test History New 5'
oHistory = gi.histories.create_history(history_name)
print (oHistory['id'])

sHistoryID = oHistory['id']
#sHistoryID = '964b37715ec9bd22'
#sHistoryID = '1fad1eaf5f4f1766'
#sHistoryID = '2fdbd5c5858e78fb'

'''
## Mapping the dataset with History for two Input fastq dataset and 3 reference dataset
oResHis = gi.histories.upload_dataset_from_library(sHistoryID, sDataSetIDTarget)
oResHis = gi.histories.upload_dataset_from_library(sHistoryID, sDataSetIDPile)
oResHis = gi.histories.upload_dataset_from_library(sHistoryID, sDataSetIDBait)
oResHis = gi.histories.upload_dataset_from_library(sHistoryID, 'a89326c04f05a73f') # this is input fastq dataset
oResHis = gi.histories.upload_dataset_from_library(sHistoryID, 'c4fa4dcd0ef9867a') # this is input fastq dataset
'''

## Inputs to run the workflow

bImportInputsToHistory = True;
dataset_map = dict()
dataset_map['0'] = { 'src':'ld', 'id': sDataSetIDForward }
dataset_map['1'] = { 'src':'ld', 'id': sDataSetIDReverse }
dataset_map['2'] = { 'src':'ld', 'id': sDataSetIDPile }
dataset_map['3'] = { 'src':'ld', 'id': sDataSetIDTarget }
dataset_map['4'] = { 'src':'ld', 'id': sDataSetIDBait }

run_name = 'MGCDv2.1'
sample_name = 'Spikein1'
run_date = '2016-07-12'
rglb = 'Clinical'

parameters_map = {
    'toolshed.g2.bx.psu.edu/repos/devteam/picard/picard_AddOrReplaceReadGroups/1.126.0': {
        'rgid': run_name,
        'rgsm': sample_name,
        'rglb': rglb,
        'rgdt' : run_date
    },
    'toolshed.g2.bx.psu.edu/repos/devteam/fastqc/rgFastQC/0.65': {
        'contaminants': None,
        'limits': None
    }
}
print (dataset_map)
print (parameters_map)

# datetime.datetime.strptime(run_date,"%Y-%m-%d") , This is goving error for so i am using datetime.datetime(2016,02,11).strftime("%Y-%m-%d") to  RUn

# Run the workflow
exit(1)
aResult = gi.workflows.run_workflow(sWorkFlowID, dataset_map, parameters_map,sHistoryID,
                                    import_inputs_to_history=True)
print (aResult)
