"""
This SCRIPT retrieves details of all the Workflows in our Galaxy account and lists information on them.

Usage: python list_workflows.py <galaxy-url> <galaxy-API-key>
"""
from __future__ import print_function
import sys

from bioblend.galaxy import GalaxyInstance

if len(sys.argv) != 5:
    print("Usage: python list_workflows.py <galaxy-url> <galaxy-API-key> <galaxy-History-ID> <galaxy-Dataset-ID>")
    sys.exit(1)
galaxy_url = sys.argv[1]
galaxy_key = sys.argv[2]
sHisitoryID = sys.argv[3]
sDataSetID = sys.argv[4]

#print("Initiating Galaxy connection")

gi = GalaxyInstance(url=galaxy_url, key=galaxy_key)

# creating History for workflow
oResHis = gi.histories.upload_dataset_from_library(sHisitoryID, sDataSetID)