<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";

    function addSequnceDirectory($sDIrName , $sDirSize ,$dtStart,$iRUnID,$iScannerID,$sBarCode,$sExperimentName,$iRunNumber,$fpgaversion,$mcsversion,$rtaversion,$pr2bottlebarcode,$reagentkitpartnumberentered,$reagentkitversion,$reagentkitbarcode,$sDirPath=''){
    	$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();

    	$sTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
    	
    	$dAddedOn = date('Y-m-d');

    	$sIQuery = "INSERT INTO {$sTable} (`sequencer_output_folder_id`,`folder_name`,`folder_size`,`started_on`,`run_id`,`scanner_id`,`barcode`,`experiment_name`,`run_number`,`fpgaversion`,`mcsversion`,`rtaversion`,`pr2bottlebarcode`,`reagentkitpartnumberentered`,`reagentkitversion`,`reagentkitbarcode`,`created_date`,`status`) 
    				VALUES(NULL,'{$sDIrName}','{$sDirSize}','{$dtStart}','{$iRUnID}','{$iScannerID}','{$sBarCode}','{$sExperimentName}','{$iRunNumber}','{$fpgaversion}','{$mcsversion}','{$rtaversion}','{$pr2bottlebarcode}','{$reagentkitpartnumberentered}','{$reagentkitversion}','{$reagentkitbarcode}','{$dAddedOn}','1')";
    	$sResult = $conn->query($sIQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
    	}
    	return $iInsertID;
    }
    
    function updateSequencerCompleteDateTime($iSequncerID , $dtCompleted){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
        
        $sUQuery ="UPDATE `{$sTable}` SET `completed_on`='{$dtCompleted}' WHERE `sequencer_output_folder_id`='{$iSequncerID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }      
    }

    // function to update the  sequncer cycle no
    function updateSequncerCycleNumber($iSequncerID,$iCycleNumber){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
        
        $sUQuery ="UPDATE `{$sTable}` SET `cycle_number`='{$iCycleNumber}' WHERE `sequencer_output_folder_id`='{$iSequncerID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
    // function to update the  sequncer status of completed
    function updateSequncerStatus($iSequncerID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
        
        $sUQuery ="UPDATE `{$sTable}` SET `isCompleted`='{$iStatus}' WHERE `sequencer_output_folder_id`='{$iSequncerID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
        // function to update the  sequncer status of completed
    function updateSequncerAsOver($iSequncerID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
        
        $sUQuery ="UPDATE `{$sTable}` SET `status`='0' WHERE `sequencer_output_folder_id`='{$iSequncerID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
    function fGetSequencerDetailForSequencerID($iSeqID){
        $aSequnceData = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sSequnceMasterInfoTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        // scanner id use if uniqie
        $sQuery ="SELECT a.`seq_dir_name` as `run_id`,a.`seq_first_seen_on` as `started_on`,
                    a.`seq_type`,b.`seq_directory_size`,
                    b.`seq_cycle_number`,b.`seq_scanner_id`,c.`seq_status`
                    FROM `{$sSequnceMasterTable}` as a
                    LEFT JOIN `{$sSequnceMasterInfoTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    LEFT JOIN `{$sSequnceStatusTable}` AS c ON c.`seq_directory_id`=a.`seq_directory_id` AND c.`isValid`=1
                    WHERE a.`seq_directory_id` = '{$iSeqID}'";

        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $aSequnceData = $aRow;
        }
        return $aSequnceData;
    }
    
    function fGetSequencerDetailForRunID($sRunID){
        $aSequnceData = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sSequnceMasterInfoTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        // scanner id use if uniqie
        $sQuery ="SELECT a.`seq_directory_id`,a.`seq_dir_name` as `run_id`,a.`seq_first_seen_on` as `started_on`,
                    a.`seq_type`,b.`seq_directory_size`,
                    b.`seq_cycle_number`,b.`seq_scanner_id`,c.`seq_status`
                    FROM `{$sSequnceMasterTable}` as a
                    LEFT JOIN `{$sSequnceMasterInfoTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    LEFT JOIN `{$sSequnceStatusTable}` AS c ON c.`seq_directory_id`=a.`seq_directory_id` AND c.`isValid`=1
                    WHERE a.`seq_dir_name` = '{$sRunID}'";

        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $aSequnceData = $aRow;
        }
        return $aSequnceData;
    }
    // function to add sample output files
    function addSequnceSampleOutput($iSampleID , $sSampleOPFileName , $sSampleOPFileSize){
    	$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();

    	$sTable = DATABASE_TABLE_PREFIX.'_sequncer_sample_output';
    	
    	$dAddedOn = date('Y-m-d');

    	$sIQuery = "INSERT INTO {$sTable} (`sample_output_file_Id`,`sample_id`,`output_file_name`,`output_file_size`,`output_date_time`,`status`) 
    				VALUES(NULL,'{$iSampleID}','{$sSampleOPFileName}','{$sSampleOPFileSize}','{$dAddedOn}','1')";
    	$sResult = $conn->query($sIQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
    	}
    	// its iSampleOutputFileId
    	return $iInsertID;
    }
    // function to add trigger for workflow
    function addTriggeredSampleToPipeline($iSampleOutputFileId , $iSampleID , $iWorkflowId , $iIsComplete , $dtCompleted){
    	$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();

    	$sTable = DATABASE_TABLE_PREFIX.'_sample_triggered';
    	
    	$dTriggeredOn = date('Y-m-d');

    	$sIQuery = "INSERT INTO {$sTable} (`trigger_id`,`sample_output_file_Id`,`sample_id`,`workflow_id`,`trigger_date_time`,`is_complete`,`completed_date_time`,`status`) 
    				VALUES(NULL,'{$iSampleOutputFileId}','{$iSampleID}','{$iWorkflowId}','{$dTriggeredOn}','0','{$dTriggeredOn}','1')";
    	$sResult = $conn->query($sIQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
    	}
    	return $iInsertID;
    }
    // function to update the triggered sample as completed
    function updateTriggeredSampleAsCompleted($iTriggerId){
    	$DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX.'_sample_triggered';
        $dtTriggeredCompletedOn = date('Y-m-d H:i:s');

        $sUQuery ="UPDATE `{$sTable}` SET `is_complete`='1',`completed_date_time`='{$dtTriggeredCompletedOn}' WHERE `trigger_id`='{$iTriggerId}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
    // function to add the output details of pipeline of samples
    function addOutputSampleOfPipeline($iTriggerId , $sOutputFilename , $sOutputDateTime){
    	$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();

    	$sTable = DATABASE_TABLE_PREFIX.'_sample_pipeline_output';
    	
    	$dTriggeredOn = date('Y-m-d');

    	$sIQuery = "INSERT INTO {$sTable} (`pipeline_output_id`,`trigger_id`,`output_filename`,`output_date_time`,`status`) 
    				VALUES(NULL,'{$iTriggerId}','{$sOutputFilename}','{$sOutputDateTime}','1')";
    	$sResult = $conn->query($sIQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
    	}
    	return $iInsertID;
    }

    function getAllSequncerDetail($dStartDate,$dEndDate){
    	$aSequnceData = array();
    	$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();

    	//$sSequnceTable = DATABASE_TABLE_PREFIX.'_sequncer_folder';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sMasterStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sMasterInfoTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $sErrorMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_error_master';
        
        $sQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,DATE_FORMAT(a.`seq_first_seen_on`,'%m-%d-%Y %H:%i:%s') AS start_date,b.`seq_status`,b.`isValid`,
            c.`seq_directory_size`,c.`seq_cycle_number`,c.`seq_PF`,c.`seq_q30`,c.`seq_density`,
            DATE_FORMAT(c.`seq_finished_on`,'%m-%d-%Y %H:%i:%s') AS `seq_finished_on`,c.`seq_scanner_id`,CASE d.`error_msg` WHEN d.`error_msg` IS NULL THEN '111' ELSE '222'END AS `error_status`
            FROM `{$sMasterTable}` AS a
            LEFT JOIN `{$sMasterStatusTable}` AS b ON b.`seq_directory_id` =a.`seq_directory_id`
            LEFT JOIN `{$sMasterInfoTable}` AS c ON c.`seq_directory_id` =a.`seq_directory_id`
            LEFT JOIN `{$sErrorMasterTable}` AS d ON d.`sequencer_id` = a.`seq_directory_id`
            WHERE a.`status`=1 and b.`isValid`=1 AND a.`seq_first_seen_on` BETWEEN '{$dStartDate}' AND '{$dEndDate}'";

    	$sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aSequnceData[] = $aRow;
            }
        }
        return $aSequnceData;
    }

    function getAllSequncerSampleData($dStartDate,$dEndDate){
        $aSampleSequnceData = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequencerTable = DATABASE_TABLE_PREFIX."_sequencer_master";
        $sSequencerSampleInfoTable = DATABASE_TABLE_PREFIX."_sequencer_samplesheet_info_master";
        $sSequencerSampleInfoMasterTable = DATABASE_TABLE_PREFIX."_sequencer_sample_info_master";
        $sSequencerSampleInfoObjectMasterTable = DATABASE_TABLE_PREFIX."_sequencer_samplesheet_object_info";
        $sSequencerMasterTable = DATABASE_TABLE_PREFIX."_sequencer_master_info";
        $sSequencerSampleStatusTable = DATABASE_TABLE_PREFIX."_sequencer_sample_status";

        $sQuery ="SELECT d.`seq_directory_id`,d.`seq_type`,d.`seq_dir_name` AS `run_id`,a.`seq_samplesheet_master_id`,a.`sample_ID`,
            a.`sample_name_barcode`,f.`seq_sample_status`,e.`seq_scanner_id`,
            'Mirati MGCDv2.1 Tumor Panel v2.2' AS pipeline
            FROM `{$sSequencerSampleInfoMasterTable}` AS a
            LEFT JOIN `{$sSequencerSampleStatusTable}` as f on f.`seq_sample_info_id` = a.`seq_sample_info_id`
            LEFT JOIN `{$sSequencerSampleInfoTable}` AS b ON b.`seq_samplesheet_master_id`=a.`seq_samplesheet_master_id`
            LEFT JOIN `{$sSequencerSampleInfoObjectMasterTable}` AS c ON c.`seq_samplesheet_object_master_id`=b.`seq_samplesheet_object_master_id`
            LEFT JOIN `{$sSequencerTable}` AS d on d.`seq_directory_id`=c.`seq_directory_id`
            LEFT JOIN `{$sSequencerMasterTable}` AS e ON e.`seq_directory_id`=d.`seq_directory_id` 
            WHERE a.`isValid`=1 AND d.`seq_first_seen_on` BETWEEN '{$dStartDate}' AND '{$dEndDate}'";   
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aSampleSequnceData[] = $aRow;
            }
        }
        return $aSampleSequnceData;
    }


    // Function to save the metrics Output

    function fSaveMetricsOutput($iSeqID , $sMOdel , $iCycleRead1 , $iCycleReadIndex2 , $iCycleRead3 , $fMeanCluster,$fTotalCluster,$fTotalPfCluster,$fPercentPfCluster,$iAlignedFix , $fRead1,$fRead2,$fRead3 ){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_metrics_details';
        
        $dAddedOn = date('Y-m-d');

        $sIQuery = "INSERT INTO {$sTable} (`output_Id`,`seq_id`,`model`,`cycle_config_read1`,`cycle_config_read2_index`,`cycle_config_read3`,`mean_cluster_density`,`total_clusters`,`total_pf_clusters`,`percentage_pf_clusters`,`aligned_phix`,`read_1`,`read_2`,`read_3`,`added_on`,`status`) 
                    VALUES(NULL,{'{$iSeqID}'},{'$sMOdel'},{'$iCycleRead1'}, {'$iCycleReadIndex2'},{'$iCycleRead3'}, {'$fMeanCluster'},{'$fTotalCluster'},{'$fTotalPfCluster'},{'$fPercentPfCluster'},{'$iAlignedFix'} , {'$fRead1'},{'$fRead2'},{'$fRead3'} ,'{$dAddedOn}','1')";
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }

    function fSaveMetricsSampleRecord($iOutputSeqID , $sBarcodeName , $iCycleNumber , $iSampleNumber){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_metrics_sample_output';
        
        $dAddedOn = date('Y-m-d');

        $sIQuery = "INSERT INTO {$sTable} (`Sample_output_id`,`output_Id`,`Barcode_name`,`Cycle_no`,`Number_closed_to_sample`,`added_on`,`status`) 
                    VALUES(NULL,{'{$iOutputSeqID}'},{'$sBarcodeName'},{'$iCycleNumber'}, {'$iSampleNumber'},{'$dAddedOn'},'1')";
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }

/*
    Function to Display Counts of Sequencer in Dashboard
*/
    
    function fGetRunningSequencer(){
        $iSequencer = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';

        $sQuery ="SELECT count(DISTINCT `seq_directory_id`) as `seqeuncer_running` FROM `{$sSequnceStatusTable}` AS a
            LEFT JOIN webiface_sequencer_error_master AS b ON b.`sequencer_id`=a.`seq_directory_id`
            WHERE a.`seq_status` not IN (101,11) AND a.`isValid`=1 AND b.`sequencer_id` IS NULL";
               
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $iSequencer = $aRow['seqeuncer_running'];
            
        }
        return $iSequencer;       
    }
// SELECT * FROM mxcel_pharmacy.webiface_sequencer_master_status where seq_status in (101,11) and isValid=1 and seq_timestamp between '2016-07-02' and '2016-07-06';

    function fGetSequencerCompletedThisWeek(){
        $iDayOfWeek = date('w');
        $dStartDate = date('Y-m-d', strtotime('-'.$iDayOfWeek.' days'));
        $dEndDate = date('Y-m-d', strtotime('+'.(7-$iDayOfWeek).' days'));
        
        $iSequencer = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        

        $sQuery ="SELECT count(DISTINCT `seq_directory_id`) as `seqeuncer_completed` FROM `{$sSequnceStatusTable}` WHERE seq_status in (101,11) AND isValid=1 AND `seq_timestamp` between '{$dStartDate}' AND '{$dEndDate}' ";
        
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $iSequencer = $aRow['seqeuncer_completed'];
            
        }
        return $iSequencer;
    }
    
    function fGetSequencerCompletedThisMonth(){
        $dStartDate = date('Y-m-d',strtotime('first day of this month'));
        $dLastDate = date('Y-m-d',strtotime('last day of this month'));

        $iSequencer = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        

        $sQuery ="SELECT count(DISTINCT `seq_directory_id`) as `seqeuncer_completed` FROM `{$sSequnceStatusTable}` WHERE seq_status in (101,11) AND isValid=1 AND `seq_timestamp` between '{$dStartDate}' AND '{$dLastDate}' ";
               
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $iSequencer = $aRow['seqeuncer_completed'];
            
        }
        return $iSequencer;
    }
//!! Function to get the sequencer which are in Warning stage
    function fGetSequencerInError(){
        $iSequencer = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sSequnceStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sSequnceErrorTable = DATABASE_TABLE_PREFIX.'_sequencer_error_master';

        $sQuery ="SELECT count(DISTINCT b.`sequencer_id`) as `seqeuncer_warning` FROM `{$sSequnceStatusTable}` AS a
            LEFT JOIN `{$sSequnceErrorTable}` AS b ON b.`sequencer_id`=a.`seq_directory_id`
            WHERE a.`seq_status` not IN (101,11) AND a.`isValid`=1";
               
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            $aRow = $sQueryR->fetch_assoc();
            $iSequencer = $aRow['seqeuncer_warning'];
            
        }
        return $iSequencer;
    }

?>