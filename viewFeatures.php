<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";


$sPageTitle = "View Feature Listing";

$aFeatureData = fGetAllFeatures();
$iCountfeature = count($aFeatureData);

$oSession = new SessionManager();
$iType = $oSession->iType;

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Feature </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Feature Code</th>
                                                <th>Feature Name</th>
                                                <th>Feature Notes</th>
                                                <th>Feature Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountfeature;$iii++){
                                                   $iFeatureID = $aFeatureData[$iii]['feature_id'];
                                                   $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iFeatureID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td><?php echo $aFeatureData[$iii]['feature_code'];?></td>
                                                <td><?php echo $aFeatureData[$iii]['feature_name'];?></td>
                                                <td><?php echo $aFeatureData[$iii]['notes'];?></td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".classUpdateButton").click(function(){
            var iFeatureID = this.id;
            window.location="updateFeature.php?iFeatureID="+iFeatureID;
        });
        $('#idDataTable').DataTable();
    });

    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>