<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "dashboard.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$iFeatureID = $_GET['iFeatureID'];
$oSession = new SessionManager();
$iType = $oSession->iType;
$sFeatureTitle = "Update Feature";
$aFeatureData = fGetFeatureDetail($iFeatureID);
$iCountfeature = count($aFeatureData);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Update Feature</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" id="idFormUpdateFeature" action="em_update_feature.php">
                                     <?php
                                                for($iii=0;$iii<$iCountfeature;$iii++){
                                    ?>
                                        <div class="form-group">
                                            <label for="id_feature_code" class="col-sm-2 control-label">Feature Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="feature_code" class="form-control" id="id_feature_code" placeholder="Enter Unique feature Code" value="<?php echo $aFeatureData[$iii]['feature_code']?>">
                                                <input type="hidden" name="feature_id" value="<?php echo $aFeatureData[$iii]['feature_id']?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_feature_name" class="col-sm-2 control-label">Feature Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="feature_name" class="form-control" id="id_feature_name" placeholder="Enter feature Name" value="<?php echo $aFeatureData[$iii]['feature_name']?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_feature_notes" class="col-sm-2 control-label">Feature Note</label>
                                            <div class="col-md-4">
                                                <textarea type="text" name="feature_notes" class="form-control" id="id_feature_notes"><?php echo $aFeatureData[$iii]['notes']?></textarea>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-md-4">
                                                <button type="button" id="idUpdateFeature" class="btn btn-success">Update</button>
                                                <a href="viewFeatures.php" class="btn btn-info">Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

         $("#idUpdateFeature").click(function(){
                var sFeatureCode = $("#id_feature_code").val();
                var sFeatureName = $("#id_feature_name").val();
                
                
                
                if(sFeatureCode ==''){
                    alert('Feature Code Must to enter.');
                }else if(sFeatureName==''){
                    alert('Feature Name Must to enter.');
                }else{
                    $("#idFormUpdateFeature").submit();
                }
         });
    });
    </script>
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>