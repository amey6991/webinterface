<?php
$sessionManager = new SessionManager();
//! Page requires authenticated user. Redirect to login page if not logged in

if(!isset($sScreenURL) && $sScreenURL!="") {
    $sScreenURL = false;
}

$sessionManager->requireAuth($sScreenURL);
?>