<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$sPageCode = $_POST['page_code'];
	$sPageName = $_POST['page_name'];
	$sPageNote = $_POST['page_notes'];
	
	$iInsertID = fAddPage($sPageCode,$sPageName,$sPageNote);
	
	if($iInsertID>0)
	{
		$sMsg = array();
		$sMsg[] = "S6";
	    redirectWithAlert("viewPages.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E6";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("addPage.php", $sMsg);
		}
?>