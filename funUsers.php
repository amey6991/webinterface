<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";

	//! function to get user types
	function fGetUserTypes(){
		$aUserType = array();
		$sUserTypeTable = DATABASE_TABLE_PREFIX.'_user_type';
		$sSQuery = "SELECT * FROM `{$sUserTypeTable}`";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aUserType[]=$aRow;
                }
            }
    	}
    	return $aUserType;
	}
	//! function to fetch all users
	function fGetAllUsers($iUserClientID=0){
		$aUsers = array();
		$sUserTable = DATABASE_TABLE_PREFIX.'_users';
        $sWhere="";
        if($iUserClientID>0){
            $sWhere = " AND `client_id`={$iUserClientID} AND `user_type_id` != 2";
        }
		$sSQuery = "SELECT * FROM `{$sUserTable}` WHERE `status`=1 AND `deleted`=0 {$sWhere}";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                	$aUsers[]=$aRow;
                }
            }
    	}
    	return $aUsers;
	}

    function fGetCountAllUsers($iUserClientID=0){
        $iTotal = 0;
        $sUserTable = DATABASE_TABLE_PREFIX.'_users';
        $sWhere="";
        if($iUserClientID>0){
            $sWhere = " AND `client_id`={$iUserClientID} AND `user_type_id` != 2";
        }
        $sSQuery = "SELECT count(*) as `iTotal` FROM `{$sUserTable}` WHERE `status`=1 AND `deleted`=0 {$sWhere}";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iTotal =$aRow['iTotal'];
                
            }
        }
        return $iTotal;
    }
    // get count of all client
    function fGetCountAllClients(){
        $iTotal = 0;
        $sClientTable = DATABASE_TABLE_PREFIX.'_client';
        $sWhere="";
        $sSQuery = "SELECT count(*) as `iTotal` FROM `{$sClientTable}` WHERE `status`=1 AND `deleted`=0";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iTotal =$aRow['iTotal'];
                
            }
        }
        return $iTotal;
    }
	//! function to get user type for User type ID
	function fGetUserTypeLabel($iUserTYpeID){
		$aUserTypeLabel = array();
		$sUserTypeTable = DATABASE_TABLE_PREFIX.'_user_type';
		$sSQuery = "SELECT `user_type` FROM `{$sUserTypeTable}` WHERE `user_type_id`='{$iUserTYpeID}'";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $aUserTypeLabel = $sResult->fetch_assoc();
            }
    	}
    	return $aUserTypeLabel;
	}
	//! function to check username availibility
	function fGetCheckUserName($sUserName){
        $aUsers = array();
        $sUserLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sSQuery = "SELECT * FROM `{$sUserLoginTable}` WHERE `username`='{$sUserName}'";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aUsers=$aRow;
                }
            }
        }
        if(!empty($aUsers)){
            return  true;
        }else{
            return false;    
        }
        
	}

    //! function to get all clients
    function fGetAllClients(){
        $aClients = array();
        $sClientTable = DATABASE_TABLE_PREFIX.'_client';
        $sSQuery = "SELECT * FROM `{$sClientTable}` WHERE `status`=1 AND `deleted`=0";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aClients[]=$aRow;
                }
            }
        }
        return $aClients;
    }
        //! function to get all client detail by client ID
    function fGetAllClientDetail($iClientID){
        $aClient = array();
        $sClientTable = DATABASE_TABLE_PREFIX.'_client';
        $sSQuery = "SELECT * FROM `{$sClientTable}` WHERE `client_id`='{$iClientID}' AND `status`=1 AND `deleted`=0";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aClient=$aRow;
                }
            }
        }
        return $aClient;
    }
    //! function to get usernames autocomplete
    function fGetUserIDAndUserNameByUserName($sUserName){
        $aUserData = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sUserTable = DATABASE_TABLE_PREFIX.'_users';
        $sSQuery = "SELECT `user_id`,`full_name` FROM {$sUserTable} where `full_name` like '%{$sUserName}%' AND `status`='1'";

        if($conn != false){

         $sSQueryR = $conn->query($sSQuery);

             if($sSQueryR!==FALSE){
                while($aRow = $sSQueryR->fetch_row()){
                        $aUserData[]= $aRow;
                    }
                }
                $DBMan = null;
            }
            return $aUserData;
        }

    //! function to fetch users detail
    function fGetUserDetail($iUserID){
        $aUsers = array();
        $sUserTable = DATABASE_TABLE_PREFIX.'_users';
        
        $sSQuery = "SELECT * FROM `{$sUserTable}` WHERE `status`=1 AND `deleted`=0 AND `user_id`='{$iUserID}'";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aUsers=$aRow;
                }
            }
        }
        return $aUsers;
    }
?>