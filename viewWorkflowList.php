<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";

$sScreenURL = "viewWorkflow.php";
$iPageID=10;

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}
$oSession = new SessionManager();
$iType = $oSession->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: dashboard.php');
    }
}
$sPageTitle = "View Workflow";

$aWorkflowData = getWorkflowList();

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">View Workflow</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                	 <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable" style="margin-left:40px">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Workflow Name</th>
                                                <th>Update</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<count($aWorkflowData);$iii++){
                                                	$iWorkflowID = $aWorkflowData[$iii]['wf_id'];
                                                	$sWorkflowName = $aWorkflowData[$iii]['workflow_name'];
                                                	$sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iWorkflowID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td>
                                               		<?php echo $sWorkflowName;?>
                                                </td>
                                                
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
    	$(".classUpdateButton").click(function(){
    		iID = this.id;
    		$("#idWorkflowID").val(iID);
    		$("#idUpdateWorkflowModal").modal('show');
    	});

    	$("#idRenameWorkflow").click(function(){
    		var iWorkflowID = $("#idWorkflowID").val();
    		var sWorkflowName = $("#idNewWorkflowName").val();
    		
    		if(sWorkflowName!=''){
                if(confirm('Are Your Sure to Update Workflow Name ?')){
                    $.ajax({
                        url:"ajaxRequest.php",
                        data:{sFlag:'UpdateWorkflow',iWorkflowID:iWorkflowID,sWorkflowName:sWorkflowName},
                        async:true,
                        method:"GET",
                        success:function(bResult){
                            if(bResult){
                                window.location="viewWorkflowList.php";    
                            }
                        }
                    });
                }else{
                    $("#idWorkflowID").val('');
                    $("#idUpdateWorkflowModal").modal('hide');
                }
            }else{
                $("#idWorkflowID").val('');
                $("#idUpdateWorkflowModal").modal('hide');
            }
    	});
    });
    </script>
        <div class="modal fade" id="idUpdateWorkflowModal" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update File Name</h4>
              </div>
              <div class="modal-body">
                    <input type="text" name="new_workflow_name" id="idNewWorkflowName" class="form-control">
                    <input type="hidden" name="file_id" id="idWorkflowID">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="idRenameWorkflow">Update</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</body>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>