<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."config/config.app.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.PermissionHandler.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.EmailList.php";

//! Specified the Page ID for the current page
$iPageID  = 8;

$sScreenURL = "emailSetup.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$sPageTitle = "Email Setup";

$iType = $sessionManager->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: logout.php');
    }
}

$oEmailList = new EmailList();
$aEmailList = $oEmailList->getAllEmailList();

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
?>
<header>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#idAddEmailList").click(function(){
            $("#idModalAddEmailList").modal('show');
        });
        $("#idAddNameEmailList").click(function(){
            var sNameEmailList= $("#idNameEmailList").val();
            if(sNameEmailList !=''){
                //! Add the Name
                $.ajax({
                    url: "ajaxRequest.php?sFlag=addEmailListName",
                    data:{ sNameEmailList: sNameEmailList},
                    asyn: false,
                    success: function(data){
                        if($.trim(data) != false){
                            displayAlert('Email List is Added.','success');
                            $("#idNameEmailList").val('');
                            $("#idModalAddEmailList").modal('hide');
                        }
                    }
                });
            }else{
                displayAlert('Name Can not be Empty.','warning');
            }
        });

        $.ajax({
            url: "ajaxRequest.php?sFlag=getAllEmailList",
            asyn: false,
            success: function(data){
                if($.trim(data) != false){
                    data = $.parseJSON(data);
                    var iLength = data.length;
                    if(iLength>0){
                        $("#idSelectEmailList").append('<option value=0>Select</option>');
                        $.each(data , function(id,value){
                            var iListID = value['list_id'];
                            var sListName = value['list_name'];
                            var iIsActive = value['is_active'];
                            $("#idSelectEmailList").append('<option value='+iListID+'>'+sListName+'</option>');
                            
                            //$("#idDivEmailFeature").append(sListName);
                            // if(iIsActive>0){
                            //     $("#idFeatureCheckbox #idLabelFeature").append('Enabled');
                            // }else{
                            //     $("#idFeatureCheckbox #idLabelFeature").append('Disabled');
                            // }
                        });
                    }
                }
            }
        });

        //$('#idDataTable').DataTable();

        $('#idDivEmailListSubscriber').on('click', '#idAddSubscriber',function(){
            var iListID = $("#idAddSubscriber").data('listid');
            $("#idListID").val(iListID);
            $("#idModalAddEmailSubscriber").modal('show');
        });

        $(".classFeature").click(function(){
            $("#idLabelFeature").empty();
            if(this.checked){
                var iListID= $("#idListIDValue").val();
                $("#idLabelFeature").append('Enabled');
                $.ajax({
                    url: "ajaxRequest.php?sFlag=updateFeatureEmail",
                    data:{ iListID: iListID,iFlag:1},
                    asyn: false,
                    success: function(data){
                        if($.trim(data) != false){
                            data = $.parseJSON(data);
                            if(data>0){
                                displayAlert('Email Feature Updated','success');
                                getLoadSubscriber(iListID);
                            }else{
                                displayAlert('Email Feature Not Updated','danger');
                            }
                            
                        }
                    }
                });

            }else{
                var iListID= $("#idListIDValue").val();
                $("#idLabelFeature").append('Disabled');
                $.ajax({
                    url: "ajaxRequest.php?sFlag=updateFeatureEmail",
                    data:{ iListID: iListID,iFlag:0},
                    asyn: false,
                    success: function(data){
                        if($.trim(data) != false){
                            data = $.parseJSON(data);
                            if(data>0){
                                displayAlert('Email Feature Updated','success');
                                getLoadSubscriber(iListID);
                            }else{
                                displayAlert('Email Feature Not Updated','danger');
                            }
                            
                        }
                    }
                });
            }
        });
        //$("[name='my-checkbox']").bootstrapSwitch();

        $("#idAddEmailListSubs").click(function(){
            var iListID= $("#idListID").val();
            var sSubsEmail = $("#idSubsEmail").val();
            var sSubsName = $("#idSubsName").val();
            if(sSubsName =='' || sSubsEmail ==''){
                displayAlert('Name Or Email can not be empty.','warning');
            }else{
                $.ajax({
                    url: "ajaxRequest.php?sFlag=addEmailListSubscriber",
                    data:{ iListID: iListID,sSubsEmail:sSubsEmail,sSubsName:sSubsName},
                    asyn: false,
                    success: function(data){
                        if($.trim(data) != false){
                            data = $.parseJSON(data);
                            if(data>0){
                                displayAlert('A Subscriber is Added to Email List.','success');
                                $("#idListID").val('');
                                $("#idModalAddEmailSubscriber").modal('hide');
                                getLoadSubscriber(iListID);   
                            }else{
                                displayAlert('A Subscriber is NOt Added to Email List.','danger');
                            }
                            
                        }
                    }
                });
            }
        });
    });
    function getLoadSubscriber(iEmailListID){
        if(iEmailListID>0){
            $.ajax({
                url: "ajaxRequest.php?sFlag=getEmailListSubscriber",
                data:{ iEmailListID: iEmailListID},
                asyn: false,
                success: function(data){
                    if($.trim(data) != false){
                        data = $.parseJSON(data);
                        var iLength = data.length;
                        $("#idDivEmailListSubscriber").text('');
                        if(iLength>0){
                            $("#idDivDataTable table tbody").empty();
                            $("#idTableMessage").empty();
                            $.each(data , function(id,value){
                                var iSubID = value['subs_id'];
                                var iListID = value['list_id'];
                                var sSubsName = value['subs_name'];
                                var sSubsEmail = value['subs_email'];
                                var sNewRow='<tr><td>'+(id+1)+'</td><td>'+sSubsName+'</td><td>'+sSubsEmail+'</td></tr>';
                                $("#idDivDataTable table tbody").append(sNewRow);

                            });
                            $("#idDivDataTable").removeClass('hide');
                            $("#idDivEmailListSubscriber").append('<input type="button" class="btn btn-info" value="Add Subscribers" id="idAddSubscriber" data-listid='+iEmailListID+'>');
                            // get the feature section is enable or disable
                            $.ajax({
                                url: "ajaxRequest.php?sFlag=getListDetail",
                                data:{ iEmailListID: iEmailListID},
                                asyn: false,
                                success: function(aData){
                                    aData = $.parseJSON(aData);
                                    var sLabel ="<span>"+aData.list_name+"</span>";
                                    $("#idDivEmailFeature").empty();
                                    $("#idLabelFeature").empty();
                                    $("#idListIDValue").val('');
                                    $("#idDivFeatureCheckbox").removeClass('hide');
                                    $("#idMainDivEmailListFeature").removeClass('hide');
                                    $("#idDivEmailFeature").append(sLabel);
                                    if(aData.is_active == 1){
                                        $("#idLabelFeature").append('Enabled');
                                        $("#idDivFeatureCheckbox").find("#idCheckBox").prop('checked',true);
                                        $("#idListIDValue").val(iEmailListID);
                                    }else{
                                        $("#idLabelFeature").append('Disabled');
                                        $("#idCheckBox").attr('checked',false);
                                        $("#idListIDValue").val(iEmailListID);   
                                    }
                                    
                                    }
                            });
                        }else{
                            $("#idDivDataTable table tbody").empty();
                            $("#idTableMessage").append('No Data Found.Please Add Subscribers.');
                            $("#idDivDataTable").show();
                            $("#idDivEmailListSubscriber").append('<input type="button" class="btn btn-info" value="Add Subscribers" id="idAddSubscriber" data-listid='+iEmailListID+'>');
                            $("#idListIDValue").val('');
                            $("#idLabelFeature").empty();
                            $("#idDivFeatureCheckbox").addClass('hide');
                            
                        }
                    }else{
                        
                    }
                }
            });
        }else{
            $("#idDivDataTable").addClass('hide');
            $("#idDivEmailListSubscriber").empty();
            $("#idListIDValue").val('');
            $("#idLabelFeature").empty();
            $("#idMainDivEmailListFeature").addClass('hide');
            $("#idDivFeatureCheckbox").addClass('hide');
        }
    }
    </script>
</header>
<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Email Setup</span>
            <div class="description"></div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Email Setup Management</div>
                            <div class="card-subtitle">
                            Manage the Subscriber and the list.     
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <select class="form-control" id="idSelectEmailList" onchange="getLoadSubscriber(this.value)">
                                        
                                    </select>    
                                </div>
                                <div class="col-md-9">
                                    <input type="button" class="btn btn-success pull-right" id="idAddEmailList" value="Add EmailList">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-7 hide" id="idDivDataTable">
                                <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Recipient Name</th>
                                            <th>Recipint Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            
                                    </tbody>
                                </table>
                                <span id="idTableMessage" style="font-weight:800;padding-left:27%;"></span>
                            </div>
                            <div class="col-md-5" id="idDivEmailListSubscriber">
                               <!--  <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-small bootstrap-switch-animate bootstrap-switch-off" style="width: 90px;">
                                    <div class="bootstrap-switch-container" style="width: 132px; margin-left: -44px;">
                                        <span class="bootstrap-switch-handle-on bootstrap-switch-primary" style="width: 44px;">ON</span>
                                        <span class="bootstrap-switch-label" style="width: 44px;">&nbsp;</span>
                                        <span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 44px;">OFF</span>
                                        <input type="checkbox" class="toggle-checkbox" name="my-checkbox" checked="">
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-md-12" id="idMainDivEmailListFeature">
                                
                                <div class="col-md-5" id="idDivEmailFeature">
                                    
                                </div>

                                <div class="col-md-6 hide" id="idDivFeatureCheckbox">
                                    <input type="checkbox" class="classFeature" id="idCheckBox" >
                                    <label id="idLabelFeature" style="font-weight:800">
                                    </label>
                                    <input type="hidden" id="idListIDValue" name="ilist_id">
                                </div>
                                         

                            </div>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal to add the email list for -->
<div class="modal fade" id="idModalAddEmailList" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Email List Name</h4>
      </div>
      <div class="modal-body">
            <input type="text" class="form-control" id="idNameEmailList" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="idAddNameEmailList">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal-view -->
<div class="modal fade" id="idModalAddEmailSubscriber" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Email List Subscriber</h4>
      </div>
      <div class="modal-body">
            <input type="hidden" id="idListID" name="list_id">
            Email :<input type="email" class="form-control" id="idSubsEmail" >
            Name : <input type="email" class="form-control" id="idSubsName" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="idAddEmailListSubs">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>
