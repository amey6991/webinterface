<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funFileManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.FileManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
	
	$iError=0;

	$oFileManager = new FileManager();
	
	$aFileName = $_FILES['file_detail']['name'];
	$aFileType = $_FILES['file_detail']['type'];
	$aFileSize = $_FILES['file_detail']['size'];
	$aFileTempName = $_FILES['file_detail']['tmp_name'];
	$iUserID = $_POST['user_id'];

	//echo "<pre>";print_r($_FILES['file_detail']);
	// allowing file type array is set as per extension
	$aFIleType = array('png','jpg','jpeg','xls','xlsx','zip','csv','sql','docx','txt','pdf');
	$oFileManager->setFileTypeToAllow($aFIleType);
	
	$oFileManager->setMinimumFileSize(0);
	$oFileManager->setMaximumFileSize(931072);
	$oFileManager->setOverWrittenFile(false);
	$aFile = array();
	$aFileID = array();
	for($iii=0;$iii<count($aFileName);$iii++){
		$sFileName = $aFileName[$iii];
		$sFileType = $aFileType[$iii];
		$iFileSize = $aFileSize[$iii];
		$sFileTempName = $aFileTempName[$iii];
		
		$sTempPath = __DIR__.'/temp/'.$sFileName;
		$bTempMove = move_uploaded_file($sFileTempName,$sTempPath);
		
		if($bTempMove){
			$sSourcePath=$sTempPath;
			$iInsertID = $oFileManager->uploadFile($sSourcePath);
			unlink($sSourcePath);
		}
		
		if($iInsertID== -1){
			$iError =1;
		}else if($iInsertID== -2){
			$iError =2;
		}
		if($iInsertID>0){
			$aFileID[]= $iInsertID;	
		}
	}
	
	if(!empty($aFileID)){
		if(count($aFileName) != count($aFileID)){
			for($iii=0;$iii<count($aFileID);$iii++){
				$oFileManager->deleteFileForFileID($aFileID[$iii]);	
			}
			$sMsg = array();
			$sMsg[] = "E14";
		    redirectWithAlert("uploadFile.php", $sMsg);
		}else{
			for($iii=0;$iii<count($aFileID);$iii++){
				mapUserWithFIle($iUserID,$aFileID[$iii]);
			}

			$sMsg = array();
			$sMsg[] = "S14";
		    redirectWithAlert("viewUploadedFile.php", $sMsg);
		}
		
	}else{
		if($iError==1){
			$sMsg = array();
			$sMsg[] = "S15";
	    	redirectWithAlert("viewUploadedFile.php", $sMsg);
		}else if($iError==2){
			$sMsg = array();
			$sMsg[] = "E15";
	    	redirectWithAlert("viewUploadedFile.php", $sMsg);
		}else{
			$sMsg = array();
			$sMsg[] = "E14";
	    	redirectWithAlert("uploadFile.php", $sMsg);	
		}
	}
?>