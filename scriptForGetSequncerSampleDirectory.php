<?php
  
  require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
  include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
  include_once __DIR__.DIRECTORY_SEPARATOR."funWebinterfaceWorkflow.php";

  $aDirectory = dirToArray(__DIR__.'/data/');

  $sPath = __DIR__.'/data/SequencingRuns/';
  $sXmlFilePath = __DIR__.'/data/SequencingRuns/160218_M01801_0126_000000000-AGU3J/';
  $sFullePathRunInfofFile = $sXmlFilePath.'RunInfo.xml';
  $sFullPathCompleteJobRunInfofFile = $sXmlFilePath.'CompletedJobInfo.xml';
  $sPathToGenerateFASTQRunStatistics = $sXmlFilePath.'GenerateFASTQRunStatistics.xml';

  if(!empty($aDirectory)){
    foreach ($aDirectory as $key => $aValue) {
      foreach ($aValue as $key1 => $aValue1) {
          $sDIrName=$key1;
          $iDirSize = filesize($sPath.$sDIrName);
          addSequnceDirectory($sDIrName , $iDirSize);
      }
      
    }
  }

  echo "<pre>";
  print_r($aDirectory);
  // readRunInfoXml($sFullePathRunInfofFile);
  // readCompleteJobInfoXml($sFullPathCompleteJobRunInfofFile);
  //readGenerateFASTQRunStatistics($sPathToGenerateFASTQRunStatistics);

  function dirToArray($dir) { 
     
     $result = array(); 

     $cdir = scandir($dir); 
     foreach ($cdir as $key => $value) 
     { 
        if (!in_array($value,array(".",".."))) 
        { 
           if($value=='160218_M01801_0126_000000000-AGU3J_OLD' || $value=='L001'){
              continue;
           }
           if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
           { 
              echo 'DIR name is : '$key.'->'.$value.'<br>';
              $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
           } 
           else 
           { 
              $result[] = $value; 
           } 
        } 
     } 
     
     return $result; 
  } 

  // read and get data from Info.xml
  function readRunInfoXml($sFullePathRunInfofFile){
    if (file_exists($sFullePathRunInfofFile)) {
       $oXml = simplexml_load_file($sFullePathRunInfofFile);
       $sRunID = $oXml->Run['Id'];
       $iRunNumber = $oXml->Run['Number'];
       print_r($oXml->Run);
       
    } else {
        exit('Failed to open RunInfo.xml.');
    } 
  }
  // read and get data from completeJobInfo.xml
  function readCompleteJobInfoXml($sFullPathCompleteJobRunInfofFile){
    if (file_exists($sFullPathCompleteJobRunInfofFile)) {
      $oXml = simplexml_load_file($sFullPathCompleteJobRunInfofFile);
      $dtCompleteTime = $oXml->CompletionTime;
      $dtStartTime = $oXml->StartTime;
      $sSheetType = $oXml->Sheet->Type;
      $sWorkflow = $oXml->Sheet->WorkflowType;

      $sRTAOutputFolder = $oXml->RTAOutputFolder;
       
      print_r($oXml);
       
    } else {
        exit('Failed to open CompletedJobInfo.xml.');
    } 
  }
  // function to read and get the GenerateFASTQRunStatistics
  function readGenerateFASTQRunStatistics($sPathToGenerateFASTQRunStatistics){
    if (file_exists($sPathToGenerateFASTQRunStatistics)) {
      $oXml = simplexml_load_file($sPathToGenerateFASTQRunStatistics);
      
      $aOverallSampleStatics = $oXml->OverallSamples;
      $iTotalSampleCount = count($aOverallSampleStatics->SummarizedSampleStatistics);
      for ($iii=0; $iii < $iTotalSampleCount; $iii++) { 
        $aSampleStatic = $aOverallSampleStatics->SummarizedSampleStatistics[$iii];
        $iSampleStaticID = $aSampleStatic['SampleID'];
        $sSampleStaticName = $aSampleStatic['SampleName'];
        $iSampleStaticNumber = $aSampleStatic['SampleNumber'];
        $iSampleStaticQ30 = $aSampleStatic['PercentQ30'];

        //$aSampleStatic['PercentQ30R1'];$aSampleStatic['PercentQ30R2'];
        print_r($aOverallSampleStatics->SummarizedSampleStatistics[$iii]); 
      }
      
      // print_r($oXml->OverallSamples);
       
    } else {
        exit('Failed to open GenerateFASTQRunStatistics.xml.');
    } 
  }

  function xml_attribute($object, $attribute)
  {
      if(isset($object[$attribute]))
          return (string) $object[$attribute];
  }

  // read the csv file..


  // $row = 1;
  // if (($handle = fopen("SampleSheet.csv", "r")) !== FALSE) {
  //     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
  //         // $num is no of field saprated by comma
  //         $num = count($data);
  //         echo "<p> $num fields in line $row: <br /></p>\n";
  //         $row++;
  //         for ($c=0; $c < $num; $c++) {
  //         	// these are single single files for the field
  //             echo $data[$c] . "<br />\n";
  //         }
  //     }
  //     fclose($handle);
  // }
?>