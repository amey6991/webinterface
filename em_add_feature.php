<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$sFeatureCode = $_POST['feature_code'];
	$sFeatureName = $_POST['feature_name'];
	$sFeatureNote = $_POST['feature_notes'];
	
	$iInsertID = fAddFeature($sFeatureCode,$sFeatureName,$sFeatureNote);
	
	if($iInsertID>0)
	{
		$sMsg = array();
		$sMsg[] = "S8";
	    redirectWithAlert("viewFeatures.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E8";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("addFeature.php", $sMsg);
		}
?>