<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.User.php";

	//! function to add page
	function fAddPage($sPageCode,$sPageName,$sPageNote){
		$iInsertID=0;
		$sPageTable = 'auth_page_master';
		$sSQuery = "INSERT INTO `{$sPageTable}` (`page_id`,`page_code`,`page_name`,`notes`,`deleted`) 
            VALUES (NULL,'{$sPageCode}', '{$sPageName}', '{$sPageNote}','0')";

		$DBMan = new DBConnManager();
    	$conn =  $DBMan->getConnInstance();
    	$sResult = $conn->query($sSQuery);
    	if($conn != false){
    		if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
    	}
    	return $iInsertID;
	}
    //! function get all pages
    function fGetAllPages(){
        $aPageData = array();
        $sPageTable = 'auth_page_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sPageTable}` WHERE `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aPageData[] = $aRow;
            }
        }
        return $aPageData;
    }
    //! funtion to get page detail for page id
    function fGetPageDetail($iPageID){
        $aPageData = array();
        $sPageTable = 'auth_page_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sPageTable}` WHERE `page_id`='{$iPageID}' AND `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aPageData[] = $aRow;
            }
        }
        return $aPageData; 
    }
    //! function to update pages
    function fUpdatePage($iPageId,$sPageCode,$sPageName,$sPageNote){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sPageTable = 'auth_page_master';
        $sUQuery ="UPDATE `{$sPageTable}` SET `page_code`='{$sPageCode}',`page_name`='{$sPageName}',`notes`='{$sPageNote}' WHERE `page_id`='{$iPageId}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
    //! function to add feature
    function fAddFeature($sFeatureCode,$sFeatureName,$sFeatureNote){
        $iInsertID=0;
        $sFeatureTable = 'auth_feature_master';
        $sSQuery = "INSERT INTO `{$sFeatureTable}` (`feature_id`,`feature_code`,`feature_name`,`notes`,`deleted`) 
            VALUES (NULL,'{$sFeatureCode}', '{$sFeatureName}', '{$sFeatureNote}','0')";

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
        //! function get all features
    function fGetAllFeatures(){
        $aFeatureData = array();
        $sFeatureTable = 'auth_feature_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sFeatureTable}` WHERE `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aFeatureData[] = $aRow;
            }
        }
        return $aFeatureData;
    }
    //! funtion to get page detail for page id
    function fGetFeatureDetail($iFeatureID){
        $aFeatureData = array();
        $sFeatureTable = 'auth_feature_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sFeatureTable}` WHERE `feature_id`='{$iFeatureID}' AND `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aFeatureData[] = $aRow;
            }
        }
        return $aFeatureData; 
    }
    //! function to update pages
    function fUpdateFeature($iFeatureId,$sFeatureCode,$sFeatureName,$sFeatureNote){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sFeatureTable = 'auth_feature_master';
        $sUQuery ="UPDATE `{$sFeatureTable}` SET `feature_code`='{$sFeatureCode}',`feature_name`='{$sFeatureName}',`notes`='{$sFeatureNote}' WHERE `feature_id`='{$iFeatureId}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
        //! function to add action
    function fAddAction($ifeatureID,$sActionCode,$sActionName,$sActionNote){
        $iInsertID=0;
        $sActionTable = 'auth_action_master';
        $sSQuery = "INSERT INTO `{$sActionTable}` (`action_id`,`feature_id`,`action_code`,`action_name`,`notes`,`deleted`) 
            VALUES (NULL,'{$ifeatureID}','{$sActionCode}', '{$sActionName}', '{$sActionNote}','0')";
        
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
    //! function get all pages
    function fGetAllActions(){
        $aPageData = array();
        $sActionTable = 'auth_action_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sActionTable}` WHERE `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aPageData[] = $aRow;
            }
        }
        return $aPageData;
    }
    //! funtion to get page detail for page id
    function fGetActionDetail($iActionID){
        $aPageData = array();
        $sActionTable = 'auth_action_master';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sActionTable}` WHERE `action_id`='{$iActionID}' AND `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aPageData[] = $aRow;
            }
        }
        return $aPageData; 
    }
    //! function to update pages
    function fUpdateAction($ifeatureID,$iActionId,$sActionCode,$sActionName,$sActionNote){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sActionTable = 'auth_action_master';
        $sUQuery ="UPDATE `{$sActionTable}` SET `feature_id`='{$ifeatureID}',`action_code`='{$sActionCode}',`action_name`='{$sActionName}',`notes`='{$sActionNote}' WHERE `action_id`='{$iActionId}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        } 
    }
    //! function to add permission
    function fAddPermission($iUserEntityType,$iUserEntityTypeID,$iPermissionEntityType,$iPermissionEntityTypeID,$iPermissionLevel){
        $iInsertID=0;
        $sPermissionTable = 'auth_perm_matrix';
        $dAddedOn = date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sPermissionTable}` (`id`,`user_entity_type`,`user_entity_id`,`perm_entity_type`,`perm_entity_id`,`perm_level`,`added_on`,`modified_on`,`deleted`) 
            VALUES (NULL,'{$iUserEntityType}','{$iUserEntityTypeID}', '{$iPermissionEntityType}', '{$iPermissionEntityTypeID}','{$iPermissionLevel}','{$dAddedOn}','{$dAddedOn}','0')";
        
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID; 
    }
    //! function to get all permission data
    function fGetAllPermissionData(){
        $aPermissionData = array();
        $sPermissionTable = 'auth_perm_matrix';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sSQuery = "SELECT * FROM `{$sPermissionTable}` WHERE `deleted`=0";
        $sQueryR = $conn->query($sSQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aPermissionData[] = $aRow;
            }
        }
        return $aPermissionData;
    }
?>