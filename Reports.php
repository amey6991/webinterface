<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."config/config.app.php";
include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.PermissionHandler.php";
include_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";

//! Specified the Page ID for the current page
$iPageID  = 16;

$sScreenURL = "Reports.php";

if(isset($_GET['redirect'])) {
    $sRedirect = "?redirect={$_GET['redirect']}";
}
else {
    $sRedirect= "";
}

$aWorkflows = getWorkflowList();
$sOption ="<option value=''>Select</option>";
foreach ($aWorkflows as $key => $value) {
    $iWorkflowID = $value['id_sequencer_workflow'];
    $iGalaxyWorkflowID = $value['galaxy_workflow_id'];
    $sWorkflowName = $value['galaxy_workflow_name'];
    $sOption.="<option value='$iWorkflowID'>$sWorkflowName</option>";
}

// echo $sOption;
// exit();
$sPageTitle = "View Reports";

$iType = $sessionManager->iType;
if(isset($iPageID)) {
    $_iPagePermLevel = PermissionHandler::getPagePermissionLevel($sessionManager, $iPageID);
    
    //! If it doesn't have permission for the page, take him away!
    if($_iPagePermLevel==0) {
        header('Location: logout.php');
    }
}
include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";

$aDashboardStats = getDashboardStats();
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>true),
                array("title"=>"View Reports","link"=>"dashboard.php","isActive"=>true),
            );
?>
<div class="container-fluid">
    <div class="side-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <ol class="breadcrumb navbar-breadcrumb">
                                <?php echo parseBreadcrumb($aBreadcrumb); ?>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                       <div class="card-title">
                            <div class="title">Reports</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-1">
                                   <label class="pull-left">Select Pipeline</label>
                                </div>
                                <div class="col-md-3">
                                   <select id="idPipelineSelect" name="idPipelineSelect" class="form-control">
                                    <?php echo $sOption; ?>
                                   </select>
                                </div>
                                <div class="col-md-1">
                                    <label class="pull-left">Start Date</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group date" data-provide="datepicker">
                                      <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idStartDate" name="start_date">
                                        <div class="input-group-addon">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-1">
                                    <label class="pull-left">Finish Date</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group date" data-provide="datepicker">
                                      <input type="text" class="form-control" readonly aria-describedby="basic-addon2" id="idFinishDate" name="finish_date">
                                      <span class="input-group-addon" id="basic-addon2">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                      </span>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <input type="button" class="btn btn-primary" value="Apply Filter" id="idApplyFilter">
                                </div>
                            </div>
                            <div class="col-md-12" id="idResultContainer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4" id="">
                                              Total Samples Processed in this Pipeline
                                        </div>
                                        <div class="col-md-2" id="">
                                             15 Samples
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4" id="">
                                              How many Samples are in Sequencer
                                        </div>
                                        <div class="col-md-2" id="">
                                              12 Samples
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4" id="">
                                              How many Samples are completed
                                        </div>
                                        <div class="col-md-2" id="">
                                             3 Samples
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4" id="">
                                              Average Time Taken by Pipeline  
                                        </div>
                                        <div class="col-md-2" id="">
                                              2 hours
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4" id="">
                                              Total Test mapped with Pipeline
                                        </div>
                                        <div class="col-md-2" id="">
                                              17 Test <a href="#" title="view list" style="margin-left:3px"><i class="fa fa-external-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="idContainerTable">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td>Feature</td>
                                            <td>P1</td>
                                            <td>P2</td>
                                            <td>P3</td>
                                            <td>P4</td>
                                            <td>P5</td>
                                            <td>P6</td>
                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total Samples Processed</td>
                                            <td>12</td>
                                            <td>10</td>
                                            <td>9</td>
                                            <td>5</td>
                                            <td>22</td>
                                            <td>3</td>
                                        </tr>
                                        <tr>
                                            <td>How many Samples are in Sequencer</td>
                                            <td>09</td>
                                            <td>10</td>
                                            <td>19</td>
                                            <td>15</td>
                                            <td>02</td>
                                            <td>13</td>
                                        </tr>
                                        <tr>
                                            <td>How many Samples are completed</td>
                                            <td>03</td>
                                            <td>11</td>
                                            <td>19</td>
                                            <td>2</td>
                                            <td>08</td>
                                            <td>03</td>
                                        </tr>
                                        <tr>
                                            <td>Average Time Taken by Pipeline</td>
                                            <td>12 hours</td>
                                            <td>10 hours</td>
                                            <td>09 hours</td>
                                            <td>05 hours</td>
                                            <td>22 hours</td>
                                            <td>03 hours</td>
                                        </tr>
                                        <tr>
                                            <td>Total Test mapped</td>
                                            <td>12</td>
                                            <td>10</td>
                                            <td>9</td>
                                            <td>5</td>
                                            <td>22</td>
                                            <td>3</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.fn.datepicker.defaults.format = "dd/mm/yyyy";
        $.fn.datepicker.defaults.clearBtn = "true";
    });
</script>
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>