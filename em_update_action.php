<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$ifeatureID = $_POST['feature_id'];
	$sfeatureName = $_POST['feature_name'];
	$iActionId = $_POST['action_id'];
	$sActionCode = $_POST['action_code'];
	$sActionName = $_POST['action_name'];
	$sActionNote = $_POST['action_notes'];
	
	$bResult =fUpdateAction($ifeatureID,$iActionId,$sActionCode,$sActionName,$sActionNote);
	
	if($bResult)
	{
		$sMsg = array();
		$sMsg[] = "S7";
	    redirectWithAlert("viewActions.php", $sMsg);
	}else {
		    $sMsg = array();
		    $sMsg[] = "E7";
		    //! Redirect User with appropriate alert message
		    redirectWithAlert("updateAction.php?iActionID=".$iActionId, $sMsg);
		}
?>