<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";


$sPageTitle = "View Users";
$aUserTypes = fGetUserTypes();

$oSession = new SessionManager();
$iType = $oSession->iType;
$iUserClientID = $oSession->iUserClientID;

if($iType==2){
    $aUsers = fGetAllUsers($iUserClientID);    
}else{
    $aUsers = fGetAllUsers();
}


$iCountUsers = count($aUsers);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"Dashboard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"View Users","link"=>"viewUsers.php","isActive"=>true)
            );
?>
<!-- <body class="flat-blue">
    <div class="app-container">
        <div class="row content-container"> -->
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <ol class="breadcrumb navbar-breadcrumb">
                                            <?php echo parseBreadcrumb($aBreadcrumb); ?>
                                        </ol>
                                    </div>
                                    <a href="addUser.php" class="btn btn-primary pull-right classTopMargin">Add user</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                   <div class="card-body">
                                    <table class="datatable table table-striped" cellspacing="0" width="100%" id="idDataTable">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>User Name</th>
                                                <th>User Type</th>
                                                <!-- <th>Client Name</th> -->
                                                <th>City</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Update</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                for($iii=0;$iii<$iCountUsers;$iii++){
                                                    $iUserID = $aUsers[$iii]['user_id'];
                                                    $iType = $aUsers[$iii]['user_type_id'];
                                                    $aUserType = fGetUserTypeLabel($iType);
                                                    $sUserType=$aUserType['user_type'];
                                                    $aClientData=fGetAllClientDetail($aUsers[$iii]['client_id']);
                                                    $sClientName=$aClientData['client_name'];
                                                    if($sClientName==""){
                                                        $sClientName="All Clients";
                                                    }
                                                    $sUpdateButton = "<button type='button' class='btn btn-info classUpdateButton' id='{$iUserID}'>Update</button>";
                                            ?>
                                            <tr>
                                                <td><?php echo $iii+1;?></td>
                                                <td><?php echo $aUsers[$iii]['full_name'];?></td>
                                                <td><?php echo $sUserType;?></td>
                                                <!-- <td><?php //echo $sClientName;?></td> -->
                                                <td><?php echo $aUsers[$iii]['city'];?></td>
                                                <td><?php echo $aUsers[$iii]['email'];?></td>
                                                <td><?php echo $aUsers[$iii]['mobile_no'];?></td>
                                                <td><?php echo $sUpdateButton;?></td>
                                            </tr>
                                            <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    
<!--             </div>
        </div>
    </div> -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".classUpdateButton").click(function(){
            var iUserID = this.id;
            window.location="updateUser.php?iUserID="+iUserID;
        });
        $('#idDataTable').DataTable({
            "columnDefs": [ { "targets": 6, "orderable": false },{ "targets": 5, "orderable": false },{ "targets": 4, "orderable": false },{ "targets": 3, "orderable": false },{ "targets": 2, "orderable": false } ]
        });
    });

    </script>
<!-- </body> -->
<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>