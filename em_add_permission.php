<?php
	include_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";

	$iUserEntityType = $_POST['user_permission'];
	// if $iUserPermission=0
	$sUsername = $_POST['user_name'];
	if($iUserEntityType==0){
		// if $iUserPermission=0
		$iUserEntityTypeID = $_POST['user_id'];	
	}else{
		// if $iUserPermission=1
		$iUserEntityTypeID = $_POST['user_type'];	
	}
	
	// page=0 , feature=1 , action =2
	$iPermissionEntityType = $_POST['permission_entity_type'];
	if($iPermissionEntityType==0){
		// if $iPermissionEntityType = 0
		$_POST['permission_page_entity']; 
		$iPermissionEntityTypeID = $_POST['page_id'];
		// if $iPermissionEntityType = 0 OR 1 (page & feature only)
		$iPermissionLevel = $_POST['permission_level'];
	}else if($iPermissionEntityType==1){
		// if $iPermissionEntityType = 1
		$_POST['permission_feature_entity'];
		$iPermissionEntityTypeID = $_POST['feature_id'];
		// if $iPermissionEntityType = 0 OR 1 (page & feature only)
		$iPermissionLevel = $_POST['permission_level'];
	}else if($iPermissionEntityType==2){
		// if $iPermissionEntityType = 2
		$_POST['permission_action_entity'];
		$iPermissionEntityTypeID = $_POST['action_id'];
		// if $iPermissionEntityType = 2 for Action only
		$iPermissionLevel = $_POST['permission_level_action'];
	}
	$iInsertID = fAddPermission($iUserEntityType,$iUserEntityTypeID,$iPermissionEntityType,$iPermissionEntityTypeID,$iPermissionLevel);
	if($iInsertID>0){
		$sMsg = array();
		$sMsg[] = "S12";
	    redirectWithAlert("viewPermissions.php", $sMsg);
	}else{
		$sMsg = array();
		$sMsg[] = "E12";
	    redirectWithAlert("addPermission.php", $sMsg);
	}
?>