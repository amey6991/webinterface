<?php
	require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funEmails.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funPermissions.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funWorkflowTestSample.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funWebinterfaceWorkflow.php";
	require_once __DIR__.DIRECTORY_SEPARATOR."funSequencers.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.SessionManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.FileManager.php";
	include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.EmailList.php";
	

	$sFlag = $_GET['sFlag'];

	// check for username availablity
	if($sFlag=="GetuserNameAvailablility")
	{
		$sUserName = $_GET['sUserName'];
		$bUserAvailable = fGetCheckUserName($sUserName);
		echo $bUserAvailable;
	}
	// get feature name for featureID
	if($sFlag=="GetFeatureName"){
		$sFeatureName="";
		$iFeatureID = $_GET['iFeatureID'];
		$aFeatureData = fGetFeatureDetail($iFeatureID);
		if(!empty($aFeatureData)){
			$sFeatureName = $aFeatureData[0]['feature_name'];	
		}
		echo $sFeatureName;
	}
	// get all user names in autocomplete
	if($sFlag=="GetAllUserNames"){
		$sUserName = $_GET['sUserName'];
		$aUserData = fGetUserIDAndUserNameByUserName($sUserName);
		if(!empty($aUserData)){
			echo json_encode($aUserData);
		}else{
			echo false;
		}
	}

	if($sFlag=="GetFilePath"){
		$iFileID = $_GET['iFileID'];
		$oFileManager = new FileManager();
		$sFilePath = $oFileManager->getFilePath($iFileID);
		$sFilePath = str_replace("/var/www/WebInterface/",'',$sFilePath);
		$sFileName = $sFilePath;
		echo $sFilePath;
	}
	if($sFlag=="DeleteFile"){
		$iFileID = $_GET['iFileID'];
		$oFileManager = new FileManager();
		$bDeleteFile = $oFileManager->deleteFile($iFileID);
		echo json_encode($bDeleteFile);
	}
	if($sFlag=="RenameFile"){
		$iFileID = $_GET['iFileID'];
		$sFileName = $_GET['sFileName'];
		$oFileManager = new FileManager();
		$bRenameFile = $oFileManager->renameFile($iFileID,$sFileName);
		echo json_encode($bRenameFile);
	}
	//! add the 
	if($sFlag=="addEmailListName"){
		$sEmailListName = $_GET['sNameEmailList'];
		$oEmailList = new EmailList();
		$iListID= $oEmailList->addEmailListName($sEmailListName);
		echo json_encode($iListID);
	}
	
	if($sFlag=="getAllEmailList"){
		$aList = array();
		$oEmailList = new EmailList();
		$aList= $oEmailList->getAllEmailList();
		echo json_encode($aList);
	}
	// add subscriber for Email List
	if($sFlag=="addEmailListSubscriber"){
		$iListID = $_GET['iListID'];
		$sSubEmail = $_GET['sSubsEmail'];
		$sSubName = $_GET['sSubsName'];

		$oEmailList = new EmailList();
		$iInsertID= $oEmailList->addEmailListSubscriber($iListID,$sSubName,$sSubEmail);
		echo json_encode($iInsertID);
	}
	//! get all the subscriber for select email list
	if($sFlag=="getEmailListSubscriber"){
		$aSubList = array();
		$iEmailListID = $_GET['iEmailListID'];
		$oEmailList = new EmailList();
		$aSubList= $oEmailList->getEmailListSubscriber($iEmailListID);
		echo json_encode($aSubList);
	}
	
	if($sFlag=="getListDetail"){
		$aSubList = array();
		$iEmailListID = $_GET['iEmailListID'];
		$oEmailList = new EmailList();
		$aListData = $oEmailList->getEmailListDetail($iEmailListID);
		echo json_encode($aListData);
	}
	
	if($sFlag=="updateFeatureEmail"){
		$iListID = $_GET['iListID'];
		$iFlag = $_GET['iFlag'];

		$oEmailList = new EmailList();
		$bResult = $oEmailList->updateFeatureEmail($iListID,$iFlag);
		echo json_encode($bResult);
	}
	if($sFlag=="addEmailFqdn"){
		$sEmail = $_GET['sEmail'];
		$bResult = addEmailFQDN($sEmail);
		echo json_encode($bResult);
	}
	if($sFlag=="addEmailFqdnSetting"){
		$sFQDNSMTPSecure = $_GET['sFQDNSMTPSecure'];
		$sFQDNHost = $_GET['sFQDNHost'];
		$sFQDNPort = $_GET['sFQDNPort'];
		$sFQDNUserName = $_GET['sFQDNUserName'];
		$sFQDNPassword = $_GET['sFQDNPassword'];
		$iResult = fAddFQDNConfiguration($sFQDNSMTPSecure,$sFQDNHost,$sFQDNPort,$sFQDNUserName,$sFQDNPassword);
		echo $iResult;
	}
	if($sFlag=="updateEmailFqdnSetting"){
		$sFQDNSMTPSecure = $_GET['sFQDNSMTPSecure'];
		$sFQDNHost = $_GET['sFQDNHost'];
		$sFQDNPort = $_GET['sFQDNPort'];
		$sFQDNUserName = $_GET['sFQDNUserName'];
		$sFQDNPassword = $_GET['sFQDNPassword'];
		$iResult = fUpdateFQDNConfiguration($sFQDNSMTPSecure,$sFQDNHost,$sFQDNPort,$sFQDNUserName,$sFQDNPassword);
		echo $iResult;
	}
	
	if($sFlag == 'EmailStateChange'){
		$bChecked = $_GET['bChecked'];
		$iFeatureID = $_GET['iFeatureID'];
		if($bChecked === 'true'){
			$bResult = fEnableEmailFeature($iFeatureID);
		}
		if($bChecked === 'false'){
			$bResult = fDisableEmailFeature($iFeatureID);
		}
		echo $bResult;
	}
	// workflow flags

	if($sFlag=="addWorkflow"){
		$sWrokflowName = $_GET['sWrokflowName'];
		$iResult = addWorkflow($sWrokflowName);
		echo $iResult;
	}

	if($sFlag=="UpdateWorkflow"){
		$iWorkflowID = $_GET['iWorkflowID'];
		$sWorkflowName = $_GET['sWorkflowName'];
		$bResult = updateWorkflow($iWorkflowID,$sWorkflowName);
		echo $bResult;
	}

	// Manage  Test
	if($sFlag=="addTestMaster"){
		$sTestName = $_GET['sTestName'];
		$iResult = addTestMaster($sTestName);
		echo $iResult;
	}
	if($sFlag=="FreezeTest"){
		$iTestID = $_GET['iTestID'];
		$bResult = freezTest($iTestID);
		echo $bResult;
	}
	if($sFlag=="UnFreezeTest"){
		$iTestID = $_GET['iTestID'];
		$bResult = unFreezTest($iTestID);
		echo $bResult;
	}

	// Manage Samples

	if($sFlag=="addSampleMaster"){
		$sSampleName = $_GET['sSampleName'];
		$iResult = addSampleMaster($sSampleName);
		echo $iResult;
	}
	if($sFlag=="UpdateSample"){
		$iSampleID = $_GET['iSampleID'];
		$sSampleName = $_GET['sSampleName'];
		$bResult = updateSample($iSampleID,$sSampleName);
		echo $bResult;
	}
	
	if($sFlag=="FreezeSample"){
		$iSampleID = $_GET['iSampleID'];
		$bResult = freezSample($iSampleID);
		echo $bResult;
	}
	if($sFlag=="UnFreezeSample"){
		$iSampleID = $_GET['iSampleID'];
		$bResult = unFreezSample($iSampleID);
		echo $bResult;
	}

	// Manage Mapping (Test samples Workflow)
	if($sFlag=="MapTestWorkflow"){
		$iTestID = $_GET['iTestID'];
		$iWorkflowID = $_GET['iWorkflowID'];
		$iResult = mapWorkflowWithTest($iTestID,$iWorkflowID);
		echo $iResult;
	}
	if($sFlag=="MappSample"){
		$iTestID = $_GET['iTestID'];
		$iSampleID = $_GET['iSampleID'];
		$iResult = mapSampleWithTest($iTestID,$iSampleID);
		echo $iResult;
	}
	if($sFlag=="UnMappSample"){
		$iTestID = $_GET['iTestID'];
		$iSampleID = $_GET['iSampleID'];
		$iResult = unMapSampleForTest($iTestID,$iSampleID);
		echo $iResult;
	}
	// fetch the sequce data for UI dashboard
	if($sFlag == "fetchSequenceData"){
		$aSequenceData = array();
		$dStartDate = $_GET['dStartDate'];
		$dEndDate = $_GET['dEndDate'];
		$aSequenceData = getAllSequncerDetail($dStartDate,$dEndDate);	
		if(!empty($aSequenceData)){
			echo (json_encode($aSequenceData));
		}
	}
	if($sFlag == "fetchSampleSequenceData"){
		$dStartDate = $_GET['dStartDate'];
		$dEndDate = $_GET['dEndDate'];
		
		$aSampleSequenceData = array();
		$aSampleSequenceData = getAllSequncerSampleData($dStartDate,$dEndDate);
		if(!empty($aSequenceData)){
			echo (json_encode($aSampleSequenceData));
		}else{
			echo (json_encode($aSampleSequenceData));
		}
	}

	if($sFlag == 'getSequencerDetail'){
		$aSequencer = array();
		$iSeqTableID = $_GET['iSeqTableID'];
		$aSequencer =fGetSequencerDetailForSequencerID($iSeqTableID);
		if(!empty($aSequencer)){
			echo (json_encode($aSequencer));
		}else{
			echo false;
		}
	}
	if($sFlag == 'getSequencerStatus'){
		$iStatus = 0;
		$iSequencerID = $_GET['iSequencerID'];
		$iStatus = fGetStatusForSequencerID($iSequencerID);

		if($iStatus > 0){
			echo $iStatus;
		}else{
			echo false;
		}		
	}
	if($sFlag == 'running_sequencer'){
		$dStartDate = $_GET['dStartDate'];
		$dEndDate = $_GET['dEndDate'];

		$aTotalRun = getAllSequencerActive();
		$iTotalRun = count($aTotalRun);
		$iRunningTotal = fGetRunningSequencer();
		$iCompletedSequencerWeek = fGetSequencerCompletedThisWeek();
    	$iCompletedSequencerMonth = fGetSequencerCompletedThisMonth();
    	$iWarningSequencer = fGetSequencerInError();
		$aSequencer = array(
				'iRunningTotal'=>$iRunningTotal,
				'iCompletedSequencerWeek'=>$iCompletedSequencerWeek,
				'iCompletedSequencerMonth'=>$iCompletedSequencerMonth,
				'iWarningSequencer' => $iWarningSequencer,
				'iTotalRun' => $iTotalRun
			);

		if(!empty($aSequencer)){
			echo (json_encode($aSequencer));
		}else{
			echo false;
		}		
	}


	if($sFlag == 'DefaultEmailList'){
		$sName = $_GET['sName'];
		$sEmail = $_GET['sEmail'];
		
		$iResult = fAddDefualtEmailList($sName,$sEmail);

		echo $iResult;
	}
	if($sFlag=='DisableDefaultEmail'){
		$iID = $_GET['sID'];
		$bResult = fDisableDefaultEmail($iID);
		echo $bResult;
	}
?>