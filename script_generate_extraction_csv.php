<!-- 
  Source Code : PHP
  Author Name : Amey Damle
  Location    : WebInterface::middleware
  FileName    : wbint_script_scan_interop_metrics.php
  Description : This script will scan all the directories for ExtractionMetricsOut.bin in InterOp directory and execute the command so that all binary file will parse in CSV. that csv will store in a directory. and its information into database.
-->
<?php
/*
  Include the function pages , any classes if required for the script
*/

require_once 'funSequencers.php';

// Log with a message
$dtCurrentDateTime = date('Y-m-d H:i:s');
$sLogMsg = "\r\n"."---STT---".date('Y-m-d H:i:s')."---------Script To get the csv file of ExtractionMetricsOut.bin file by parsing through command for each sequencer Runs at ".$dtCurrentDateTime."\r\n";
fLogger(99,$sLogMsg);

/*
  Define the config values , default values here 
*/
$sMiSeqDirectoryPath = "/home/plus91/data/MiSeqRuns/";
$sNextSeqDirectoryPath = "/home/plus91/data/NextSeqRuns/";

$sExtractionProgrammPath = "/home/plus91/data/SetupInterop/setup/bin/interop2csv";
$sRemoteCopyPathForCSV = "/home/plus91/data/sequencer_progress/";

$sExtractionProgressDirectory = "/home/plus91/data/MiSeq_Inprogress/";

$aDirectories = array(
              'Cycle_11','Cycle_Start','Run_Start'
              );


$sIP = '68.106.75.78';
$iPort = 35022;
$sUserName = 'plus91';
$sPassword = 'Plus91.13912';


$aParsedInterOpInfo = array();
foreach ($aDirectories as $key => $sValue) {


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
      so , here we are going to connect the remote server by given port no ,username and password.
      and also we are going to run the command to generate the CSV file of summary into a directory.
      we renaming the csv file with , directoryName_parsesummary.csv
    */

    $connection = ssh2_connect($sIP, $iPort);
    if($connection){
      $autherised = ssh2_auth_password($connection, $sUserName, $sPassword);
      if(!$autherised){
          $sLogMsg = "---XXX---".date('Y-m-d H:i:s')."---------CONNECTION Done but Authentication Refused FOR Script for Parse the InterOp Summary and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(99,$sLogMsg);
          exit();
      }else{
          $sLogMsg = "---LOG---".date('Y-m-d H:i:s')."---------CONNECTION & Authentication Passed FOR Script for Parse the ExtractionMetricsOut of InterOp and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
          fLogger(99,$sLogMsg);
      }
    }else{
      $sLogMsg = "---X-X-X-".date('Y-m-d H:i:s')."---------CONNECTION refused FOR Parse the ExtractionMetricsOut InterOp and get the CSV file in different Directory at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(99,$sLogMsg);
      exit();
    }

    $sftp = ssh2_sftp($connection);
    
    // Log with a message
    $sLogMsg = "---------".date('Y-m-d H:i:s')."---------SSH connection stablished at ".date('Y-m-d H:i:s')."\r\n";
    fLogger(99,$sLogMsg);

    $SeqDirName = $sValue;
    // $SeqServerPath = $aValue['seq_server_path'];

    // if($SeqType==1){
    //   $sRemoteDirectoryPath = $sMiSeqDirectoryPath.$SeqDirName.'/';
    // }elseif($SeqType==2){
    //   $sRemoteDirectoryPath = $sNextSeqDirectoryPath.$SeqDirName.'/';
    // }
    $sTimeStamp = date('Y-m-d-H:i:s');
    $sCSVFileName = $SeqDirName.'_'.$sTimeStamp.'.csv';
    $sRemoteCopyFullPathForCSV = $sRemoteCopyPathForCSV.$sCSVFileName;

    $sExtractionProgressDirectory = $sExtractionProgressDirectory.$SeqDirName.'/InterOp/ExtractionMetricsOut.bin';
    //!! command is written below here
    $sCommand = "{$sExtractionProgrammPath} {$sExtractionProgressDirectory} > {$sRemoteCopyFullPathForCSV}";
    //!! Execute the command
    $stream = ssh2_exec($connection, $sCommand);
    
    //sleep(10);
    //!! if command Execution is TRUE
    if($stream != FALSE){
      $aParsedInterOpInfo[] = array(
                          'directory_name' => $SeqDirName,
                          'seq_parsed_summary_path'=>$sRemoteCopyFullPathForCSV,
                          'parsed_csv_filename'=>$sCSVFileName,
                            ); 
      $sLogMsg = "--------".date('Y-m-d H:i:s')."--------- Command Execution Succeeded , CSV file is generated ( {$sRemoteCopyFullPathForCSV} ) at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(99,$sLogMsg); 
    }else{
      $sLogMsg = "--X-XX--".date('Y-m-d H:i:s')."--------- Command Execution is FAILD No file found for {$SeqDirName} for generate Sequencer's summary of InterOP Extraction at ".date('Y-m-d H:i:s')."\r\n";
      fLogger(99,$sLogMsg);      
    }
}
//echo "<pre>";print_r($aParsedInterOpInfo);
//!! Loop the array to store the information
// if(!empty($aParsedInterOpInfo)){
//     foreach ($aParsedInterOpInfo as $key => $aValue) {
//         //!! Extract The Array information 
//         $SeqDirectroryID = $aValue['seq_directory_id'];
//         $sRemoteCopyFullPathForCSV = $aValue['seq_parsed_summary_path'];
//         $sCSVFileName = $aValue['parsed_csv_filename'];

//         $iResult = fAddSequencerParseInterOpMasterInfo($SeqDirectroryID,$sRemoteCopyFullPathForCSV,$sCSVFileName);
//         // Add status as 2 for the sequencer that summary CSV get generated
//         fDisableInterOpExtractionStatus($SeqDirectroryID);
//         fAddSequencerInterOpExtractionStatus($SeqDirectroryID,2);

//         if($iResult > 0){
//           $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
//          fLogger(99,$sLogMsg);
//         }else{
//           $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed But the Information NOT Saved for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
//          fLogger(99,$sLogMsg);
//         }
//     }
// }else{
//       $sLogMsg = "--------".date('Y-m-d H:i:s')."---------Parsing of InterOp Summary and get the CSV file in different Directory is Succeed But the Information NOT Saved for Sequencer ( $sRemoteDirectoryPath ) at ".date('Y-m-d H:i:s')."\r\n";
//      fLogger(99,$sLogMsg);
// }
?>