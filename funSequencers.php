<?php

    require_once __DIR__.DIRECTORY_SEPARATOR."config/config.php";
    include_once __DIR__.DIRECTORY_SEPARATOR."classes/class.DBConnManager.php";
    ini_set('auto_detect_line_endings', true);
/*
    functions for sequencer master script
    add  info in database
    check already added or not
*/
    function addSequncerMaster($sDIrName,$iSequencerType,$sDirServerPath,$dtFirstSeenOn){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_directory_id`,`seq_type`,`seq_dir_name`,`seq_server_path`,`seq_first_seen_on`,`status`) 
                    VALUES(NULL,'{$iSequencerType}','{$sDIrName}','{$sDirServerPath}','{$dtFirstSeenOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! Function to get all the sequencer
    function getAllSequencerActive(){
        $aSequencers = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `status`=1";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[] = $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
//!! Function to add the status for the sequencer
    function addSequncerMasterStatus($iSequecerMasterID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`status_id`,`seq_directory_id`,`seq_status`,`seq_timestamp`,`isValid`) 
                    VALUES(NULL,'{$iSequecerMasterID}','{$iStatus}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! Function to disable all the old status of the sequencer
    function disableSequencerOldStatus($iSeqDirectoryid,$iScanStatus=0){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        if($iScanStatus > 0){
            $sUQuery = "UPDATE {$sTable} SET `isValid`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}' AND `seq_status`='{$iScanStatus}'";
        }else{
            $sUQuery = "UPDATE {$sTable} SET `isValid`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}'";
        }
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
/* 
    FUnction to check the sequencer is already added to our database or NOT
    This will return TRUE if Not Added , FALSE on Added.
*/

    function bCheckSequencerInMaster($sDirName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `seq_dir_name`= '{$sDirName}' ";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(empty($aRow)){
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }
/*
    Function to check the Entry of status and the sequencers
    Return True when There is no ENTRY
    Return False when there is any entry
*/
    function fCheckStatusForSequencer($iSeqDirectoryid,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `seq_directory_id`= '{$iSeqDirectoryid}' AND `seq_status`='{$iStatus}'";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(empty($aRow)){
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }        
    }

/*
    Function to get the sequencer status
*/
    function fGetStatusForSequencerID($iSeqDirectoryid){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

       $sSQuery = "SELECT * FROM `{$sTable}` WHERE `seq_directory_id`= '{$iSeqDirectoryid}' AND `isValid`=1";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return $aRow['seq_status'];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }        
    }
/*
    function to add master info for the files of sequencer
*/
    function addSequencerFileInfoMaster($iSeqDirectoryid,$iSeqFileType,$sSeqRemoteDirectoryPath,$sSeqLocalDirectoryPath,$SNewFileName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_files_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_file_id`,`seq_directory_id`,`seq_file_type`,`seq_server_path`,`seq_local_path`,`seq_newfile_name`,`added_on`,`isValid`) VALUES(NULL,'{$iSeqDirectoryid}','{$iSeqFileType}','{$sSeqRemoteDirectoryPath}','{$sSeqLocalDirectoryPath}','{$SNewFileName}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }

/*
    Function add the Sequencer Run & Directory Info for Dashboard Uses
*/
    function addSequencerMasterInfo($iSeqDirectoryid,$sSeqDirectorySize,$iSeqCycleNumber,$sSesQ30,$sSeqPF,$sDensity,$sSeqRunID,$sSeqScannerID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_master_info`,`seq_directory_id`,`seq_directory_size`,`seq_cycle_number`,`seq_q30`,`seq_PF`,`seq_density`,`seq_run_id`,`seq_scanner_id`,`added_on`,`updated_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$sSeqDirectorySize}','{$iSeqCycleNumber}','{$sSesQ30}','{$sSeqPF}','{$sDensity}','{$sSeqRunID}','{$sSeqScannerID}','{$dAddedOn}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;        
    }

/*
    Function to add the OtherInfo for Sequencer Remove this Later
*/
    function fAddSequencerExtraInfo($iSeqDirectoryid,$iCycleNumber=0,$iQ30=0,$iPassingFilter=0){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX."_sequencer_other_info";
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_master_other`,`seq_directory_id`,`cycle_number`,`q30`,`passingfilter_clusterpf`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$iCycleNumber}','{$iQ30}','{$iPassingFilter}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;  
    }

/*
    Function to update the Q30 and ClusterPassingFilter for Sequencer
*/
    function fUpdateSequencerMasterInfo($iSeqDirectoryid,$sQ30,$sPassingFilter,$sDensity){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $dUpdatedOn = date('Y-m-d H:i:s');

        $sUQuery = "UPDATE {$sTable} SET `seq_q30` = '{$sQ30}',`seq_PF` = '{$sPassingFilter}',`updated_on` = '{$dUpdatedOn}',`seq_density`='{$sDensity}' WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Update the scannerID for sequencer
    function fUpdateScannerID($iSequencerDirID,$iScannerID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $dUpdatedOn = date('Y-m-d H:i:s');

        $sUQuery = "UPDATE {$sTable} SET `seq_scanner_id` = '{$iScannerID}',`updated_on` = '{$dUpdatedOn}' WHERE `seq_directory_id` = '{$iSequencerDirID}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Update the Finish time of sequencer
    function fUpdateFinishTimeSequencerMasterInfo($iSeqDirectoryid){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $dUpdatedOn = date('Y-m-d H:i:s');

        $sUQuery = "UPDATE {$sTable} SET `seq_finished_on` = '{$dUpdatedOn}',`updated_on` = '{$dUpdatedOn}' WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!!    Function to update the cycleNumber  for Sequencer

    function fUpdateSequencerCycleNumberMasterInfo($iSeqDirectoryid,$iCycleNumber){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $dUpdatedOn = date('Y-m-d H:i:s');

        $sUQuery = "UPDATE {$sTable} SET `seq_cycle_number` = '{$iCycleNumber}',`updated_on` = '{$dUpdatedOn}' WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!!    Function to update the Directory Size  for Sequencer

    function fUpdateSequencerSizeMasterInfo($iSeqDirectoryid,$sSize){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master_info';
        $dUpdatedOn = date('Y-m-d H:i:s');

        $sUQuery = "UPDATE {$sTable} SET `seq_directory_size` = '{$sSize}',`updated_on` = '{$dUpdatedOn}' WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
/*
    Functions to get Sequencer for The CSV and XML

    1. Functions to get Sequencer for the scan the samplesheet.csv in the DIR
    2. Functions to get Sequencer for the scan the RunParameter.xml in the DIR
    3. Functions to get Sequencer for the scan the GenerateFASTQRunStatistics.xml in the DIR
    4. Functions to get Sequencer for the scan the CompletedJobInfo.xml in the DIR
    5. Functions to get Sequencer for the scan the RunInfo.xml in the DIR
    6. Functions to get Sequencer for the scan the Demultiplexing.xml in the DIR
    7. Functions to get Sequencer for the scan the RunCompletionJObStatus.xml in the DIR
    8. Functions to get Sequencer for the galaxy workflow once the fastq file read by middileware
*/
    function fGetSequencerForSamplesheet(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        
        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 2 and b.`isValid`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }       
    }

/*
    function to get sequencer for scan the runParameter.xml in the directory
*/
    function fGetSequencerForRunParameter(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

         $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 3 and b.`isValid`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }   
    }
/*
    function to get sequencer for scan the GenerateFASTQRunStatistics.xml in the directory
*/
    function fGetSequencerForFASTQRunStatistics(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 4 AND b.`isValid`=1 AND a.`seq_type`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }   
    }

/*
    Function to get sequencer for scan the CompletedJobInfo.xml in the directory
    This will use for only MiSeq Sequencer 
*/
    function fGetSequencerForCompletedJobInfo(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 100 and b.`isValid`=1 AND a.`seq_type`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }   
    }
//!! Function to return the array of sequencer Info on which we will scan the RunInfo.xml
    function fGetSequencerForRunInfo(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 8 and b.`isValid`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

//!! Function to return the array of sequencer Info on which we will scan the Demultiplexing.xml
    function fGetSequencerForDemultiplexingStats(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` WHERE b.`seq_status`!= 12 AND b.`isValid`=1 AND a.`seq_type`=2";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

//!! Function to return the array of sequencer Info on which we will scan the RunCompletionJObStatus.xml
    function fGetSequencerForRunCompletionJObStatus(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';


        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `$sTable` as a
                    LEFT JOIN `$sStatusTable` AS b ON b.`seq_directory_id`= a.`seq_directory_id` 
                    WHERE b.`seq_status`!= 10 and b.`isValid`=1 AND a.`seq_type`=2";

        $sResult = $conn->query($sSQuery);

        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    
                    $aSequencers[]= $aRow;
                }
                
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
//!! Function to get the sequencer information which are completed and need to look for fastq output files
    function fGetSequencerToScanFastqOutputFiles(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';


       $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `$sTable` as a
                    LEFT JOIN `$sStatusTable` AS b ON b.`seq_directory_id`= a.`seq_directory_id` 
                    WHERE b.`seq_status` IN (11,101) and b.`isValid`=1 AND a.`seq_type` IN (1,2) 
                    AND a.`seq_directory_id` NOT IN 
                    (
                        SELECT `seq_directory_id` FROM webiface_sequencer_fastq_status_master WHERE `status`=1
                    )";

        $sResult = $conn->query($sSQuery);

        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }        
    }
//!! Function to add the sequencer status for fastq scan
    function fAddFastQScanStatus($iSeqDirectoryID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_fastq_status_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`fastq_status_id`,`seq_directory_id`,`scan_status`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryID}',1,'{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;        
    }
//!! Function to check the sequencer status for fastq scan
    function fCheckSequencerFastQScanStatus($iSeqDirectoryID){
        $aScanInfo = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_fastq_status_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sSQuery = "SELECT * FROM {$sTable} WHERE `seq_directory_id`='{$iSeqDirectoryID}' AND `status`=1";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $aScanInfo =$aRow;
            }
        }
        if(!empty($aScanInfo)){
            return true;
        }else{
            return false;
        }
    }
//!! Function to get Sequencer for the galaxy workflow once the fastq file read by middileware
    function fGetSequencerForgalaxyWorkflow(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFastqTable = DATABASE_TABLE_PREFIX.'_sequencer_fastq_master';

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_output_path`,c.`seq_output_filename` FROM `$sTable` as a
            LEFT JOIN `$sFastqTable` AS c ON c.`seq_directory_id`= a.`seq_directory_id` ";

        $sResult = $conn->query($sSQuery);

        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }         
    }
    // CSV section
/*
    Functions for Whole cycle of SampleSheet.csv
    1. Function to get Sequencer information for Reading SampleSheet.csv 
    2. Function to add Samplesheet's all data in the form of Json
    3. Function to add few important information of samplesheet
    4. Function to add Sample information of SampleSHeet
    5. Function to get all information in form of ARRAY of SampleSheet.csv
*/

    function fetSequencerToReadSamplesheet(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid`,c.`seq_local_path` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=1
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=5)
                    AND b.`seq_status`=2";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        } 
    }

    /*
        Function to add Object of whole samplesheet.csv file as json
    */
    function addMasterSampleSheetData($iSequencerDirID,$sSampleSheetInfo){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX."_sequencer_samplesheet_object_info";

        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_samplesheet_object_master_id`,`seq_directory_id`,`seq_samplesheet_json`,`added_date`,`status`) 
                    VALUES(NULL,'{$iSequencerDirID}','{$sSampleSheetInfo}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;       
    }
    
    function addSampleSheetInfoMaster($iMasterEntrySampleSheetData,$iSequencerDirID,$sWorkflow,$sApplication,$sExperimentName,$sInvestigatorName,$sIEMFileVersion,$iCycleNo,$dDate,$sAssay,$sDescription,$sChemistry){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX."_sequencer_samplesheet_info_master";

        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_samplesheet_master_id`,`seq_samplesheet_object_master_id`,`seq_directory_id`,`workflow`,`application`,`experiment_name`,`investigator_name`,`IEMFileVersion`,`cycle_no`,`assay`,`desc`,`chemistry`,`created_date`,`status`) 
                    VALUES(NULL,'{$iMasterEntrySampleSheetData}','{$iSequencerDirID}','{$sWorkflow}','{$sApplication}','{$sExperimentName}','{$sInvestigatorName}','{$sIEMFileVersion}','{$iCycleNo}','{$sAssay}','{$sDescription}','{$sChemistry}','{$dDate}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! Function to get the Sample_info_id from Database for Valid SampleName (barcode)
    function fGetSampleInfoIDForValidSampleNameBarcode($sSampleNameBarcode){
        $iSampleInfoID = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTable = DATABASE_TABLE_PREFIX."_sequencer_sample_info_master";
        $sQuery = "SELECT `seq_sample_info_id` FROM {$sTable} WHERE `isValid`=1 AND `sample_name_barcode`='{$sSampleNameBarcode}' AND `status`=1";
        $sResult = $conn->query($sQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iSampleInfoID= $aRow['seq_sample_info_id'];
            }
            return $iSampleInfoID;
        }else{
            return $iSampleInfoID;
        } 
    }
//!! Function to add The Sample Info in Master table
    function addSampleShhetSampleInfo($iSampleSheetMasterID,$iSampleID,$sSampleName,$s17IndexID,$sIndex,$s15IndexID,$sIndex2,$iIsValid){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX."_sequencer_sample_info_master";

        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_sample_info_id`,`seq_samplesheet_master_id`,`sample_ID`,`sample_name_barcode`,`index_17_id`,`index`,`index_15_id`,`index2`,`isValid`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSampleSheetMasterID}','{$iSampleID}','{$sSampleName}','{$s17IndexID}','{$sIndex}','{$s15IndexID}','{$sIndex2}','{$iIsValid}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! Function to get SampleName Valid for LimsDx Or Not , so that we can proceed it to Galaxy
    function fCheckSampleNameValidForLimsDx($sSampleName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = DATABASE_TABLE_PREFIX."_sequencer_sample_info_master";
        
        $sSQuery = "SELECT `isValid` FROM `{$sTable}` WHERE `sample_name_barcode`='{$sSampleName}'";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iIsValid= $aRow['isValid'];
            }
            return $iIsValid;
        }else{
            return $iIsValid;
        } 
    }
//!! Function to add samples status
    function fAddSampleStatus($iSampleInfoID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX."_sequencer_sample_status";

        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_sample_status`,`seq_sample_info_id`,`seq_sample_status`,`added_on`,`isValid`) 
                    VALUES(NULL,'{$iSampleInfoID}','{$iStatus}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;        
    }
//!! Function to update the status of Sample
    function fUpdateSampleStatus($iSampleInfoID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_sample_status';
        if($iStatus > 0){
            $sUQuery = "UPDATE {$sTable} SET `seq_sample_status`= '{$iStatus}' WHERE `seq_sample_info_id`='{$iSampleInfoID}' ";

        }
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Function to Disable the sample
    function fDisableSampleStatus($iSampleInfoID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_sample_status';
        if($iScanStatus > 0){
            $sUQuery = "UPDATE {$sTable} SET `isValid`= 0 WHERE `seq_sample_info_id`='{$iSampleInfoID}' ";
        }
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
    /*
        The function is to read the CSV file for given path and the sequencer
        Function will read the whole CSV file and make its associative array for each heading and sub values.
        Function will return the associative array.
    */    
    function getReadSampleSheetInfo($sSampleSheetPath,$iSequencerType){
        //!! Define the arry for below solution
        $aSampleSheetMasterInfo = array();
        $aSampleSheetValueInfo = array();
        $aSampleSheetHeaderInfo = array();
        $aSampleSheetReadsInfo = array();
        $aSampleSheetAdapterInfo = array();

        $aFileInfo = file("$sSampleSheetPath",FILE_SKIP_EMPTY_LINES);

        //!! check if sequencer type are one of two current sequencer using
        if($iSequencerType==1 || $iSequencerType==2)
        {

            ///!! Define the variables
            $iRead=0;
            $iReadCount=0;
            $iCount=0;
            $bSampleRowFound = false;
            $bReadCount = false;
            $bHeader=false;
            $bReads =false;
            $bSettings=false;
            
            $fIEMFileVersion='';
            $sInvestigatorName='';
            $sExperimentName='';
            $dCreatedDate='';
            $sWorkflow='';
            $sApplication='';
            $sAssay='';
            $sDescription='';
            $sChemistry='';

            //!! Loop on the arry of Fileinfo got from samplesheet file
            foreach ($aFileInfo as $iRow => $sValue) {
                  
                $aValueInfo = explode(',', $sValue);
              
                $aValueInfo[0]=trim($aValueInfo[0]);
                //!!! When the CSV line is empty , continue the loop
                if($aValueInfo[0]==''){
                    continue;
                }

                //!!! Check the CSV's different parts
                if($aValueInfo[0]=='[Header]'){
                    $bHeader = true;
                }elseif($aValueInfo[0]=='[Reads]'){
                    $bReads = true;
                    $bHeader = false;
                    continue;
                }elseif($aValueInfo[0]=='[Settings]'){
                    $bSettings=true;
                    $bReads = false;
                    $bHeader = false;
                    continue;
                }elseif($aValueInfo[0]=='[Data]'){
                    $bSampleRowFound = true;
                    $bSettings= false;
                    $bReads = false;
                    $bHeader = false;
                    continue;
                }

                //!!! When Header Part is there , get the Values
                if($bHeader==true){
                     if($aValueInfo[0]=='IEMFileVersion'){
                            $fIEMFileVersion = $aValueInfo[1];
                            $aSampleSheetHeaderInfo['fIEMFileVersion']=$fIEMFileVersion;
                      }elseif($aValueInfo[0]=='Investigator Name'){
                            $sInvestigatorName = $aValueInfo[1];
                            $aSampleSheetHeaderInfo['investigator_name']=$sInvestigatorName;
                      }elseif($aValueInfo[0]=='Experiment Name'){
                            $sExperimentName = $aValueInfo[1];
                            $aSampleSheetHeaderInfo['experiment_name']=$sExperimentName;
                      }elseif($aValueInfo[0]=='Date'){
                            $dCreatedDate = $aValueInfo[1];
                            $aSampleSheetHeaderInfo['date']=$dCreatedDate;
                      }elseif($aValueInfo[0]=='Workflow'){
                             $sWorkflow = $aValueInfo[1];
                             $aSampleSheetHeaderInfo['workflow']=$sWorkflow;
                      }elseif($aValueInfo[0]=='Application'){
                             $sApplication = $aValueInfo[1];
                             $aSampleSheetHeaderInfo['application']=$sApplication;
                      }elseif($aValueInfo[0]=='Assay'){
                             $sAssay = $aValueInfo[1];
                             $aSampleSheetHeaderInfo['sAssay']=$sAssay;
                      }elseif($aValueInfo[0]=='Description'){
                             $sDescription = $aValueInfo[1];
                             $aSampleSheetHeaderInfo['sDescription']=$sDescription;
                      }elseif($aValueInfo[0]=='Chemistry'){
                            $sChemistry = $aValueInfo[1];
                             $aSampleSheetHeaderInfo['sChemistry']=$sChemistry;
                      }
                }
                //!!! When Reads Part is there , get the Values
                if($bReads==true){
                    
                       $aSampleSheetReadsInfo[$iRead] = $aValueInfo[0];
                       $iRead++;
                    
                }
                //!!! When Setting Part is there , get the Values
                if($bSettings==true){
                    if($aValueInfo[0]=='Adapter'){
                            $sAdapter = $aValueInfo[1];
                            $aSampleSheetAdapterInfo['Adapter']=$sAdapter;
                      }elseif($aValueInfo[0]=='AdapterRead2'){
                            $sAdapterRead2 = $aValueInfo[1];
                            $aSampleSheetAdapterInfo['AdapterRead2']=$sAdapterRead2;
                      }
                }

                //!!! When SampleData Part is there , get the Values
                if($bSampleRowFound==true){
                    
                    if($aValueInfo[0]=='Sample_ID'){
                        continue;
                    }else{
                        $iTotalColumn = count($aValueInfo);
                        for ($iii=0; $iii < count($aValueInfo); $iii++) { 
                            if($iii==0){
                                $aTemp['sample_id']=$aValueInfo[$iii];
                            }elseif($iii==1){
                                $aTemp['sample_name']=$aValueInfo[$iii];
                            }elseif($iii==2){
                                $aTemp['Sample_Plate']=$aValueInfo[$iii];
                            }elseif($iii==3){
                                $aTemp['Sample_Well']=$aValueInfo[$iii];
                            }elseif($iii==4){
                                $aTemp['17_index_id']=$aValueInfo[$iii];
                            }elseif($iii==5){
                                $aTemp['index']=$aValueInfo[$iii];
                            }elseif($iTotalColumn>8 && $iii==6){
                                $aTemp['I5_Index_ID']=$aValueInfo[$iii];
                            }elseif($iii==6){
                                $aTemp['I5_Index_ID']='';
                                $aTemp['Sample_Project']=$aValueInfo[$iii];
                            }elseif($iTotalColumn > 8 && $iii==7){
                                $aTemp['index2']=$aValueInfo[$iii];
                            }elseif($iii==7){
                                $aTemp['index2']= '';
                                $aTemp['Description']=$aValueInfo[$iii];
                            }
                        }
                        $aSampleSheetValueInfo[]=$aTemp;
                    }
                }

            }
            
            //!! When the Header values are not in CSV make it empty
            if($fIEMFileVersion != ''){
                $aSampleSheetHeaderInfo['fIEMFileVersion']=$fIEMFileVersion;
            }elseif($sInvestigatorName !=''){
                $aSampleSheetHeaderInfo['investigator_name']=$sInvestigatorName;
            }elseif($sExperimentName !=''){
                $aSampleSheetHeaderInfo['experiment_name']=$sExperimentName;
            }elseif($dCreatedDate !=''){
                $aSampleSheetHeaderInfo['date']=$dCreatedDate;
            }elseif($sWorkflow !=''){
                $aSampleSheetHeaderInfo['workflow']=$sWorkflow;
            }elseif($sApplication !=''){
                $aSampleSheetHeaderInfo['application']=$sApplication;
            }elseif($sAssay !=''){
                $aSampleSheetHeaderInfo['sAssay']=$sAssay;
            }elseif($sDescription !=''){
                $aSampleSheetHeaderInfo['sDescription']=$sDescription;
            }elseif($sChemistry !=''){
                $aSampleSheetHeaderInfo['sChemistry']=$sChemistry;
            }
            
            //!! Make the Array of different parts with associated Key
            $aSampleSheetHeader=array('Header'=>$aSampleSheetHeaderInfo);
            $aSampleSheetReads=array('Reads'=>$aSampleSheetReadsInfo);
            $aSampleSheetAdapter=array('Setting'=>$aSampleSheetAdapterInfo);
            $aSampleSheetValue = array('sampleinfo'=>$aSampleSheetValueInfo);

            //!! Merge the All Different Array to MasterInfo Array
            $aSampleSheetMasterInfo = array_merge($aSampleSheetHeader,$aSampleSheetReads,$aSampleSheetAdapter,$aSampleSheetValue);
          
        }
        
        return $aSampleSheetMasterInfo;
    }
//!! Function to read the CSV file of Summary of InterOp
    function fReadSummaryCSVInfo($sLocalPath,$iSequencerType){
        $aFileInfo = file("$sLocalPath",FILE_SKIP_EMPTY_LINES);
        
        $aHeader = array();
        $aReadInfo = array();
        $aExtraInfo = array();
        if($iSequencerType==1){
            foreach ($aFileInfo as $iRow => $sValue) {
                  
                $aValueInfo = explode(',', $sValue);
                $aValueInfo[0]=trim($aValueInfo[0]);
                if($aValueInfo[0]==''){
                    continue;
                }
                //print_r($aValueInfo);
                if($iRow==0){
                    $sVersionName = $aValueInfo[0];
                }else{
                    $sValue = preg_replace('/\s\s+/', ' ', $aValueInfo[0]);
                    $aValue = explode(' ', $sValue);
                    // print_r($aValue);
                    // echo count($aValue);
                    if($aValue[0]=='Level'){
                        $aHeader[$aValue[0]]= array();
                        $aHeader[$aValue[1]]= array();
                        $aHeader[$aValue[2].' '.$aValue[3]]= array();
                        $aHeader[$aValue[4]]= array();
                        $aHeader[$aValue[5].' '.$aValue[6]]= array();
                        $aHeader[$aValue[7].' '.$aValue[8]]= array();
                        $aHeader[$aValue[9]]= array();
                    }elseif(count($aValue)==8){
                            $aHeader['Level'][]=array($aValue[0].' '.$aValue[1]);
                            $aHeader['Yield'][]=array($aValue[2]); 
                            $aHeader['Projected Yield'][]=array($aValue[3]); 
                            $aHeader['Aligned'][]=array($aValue[4]); 
                            $aHeader['Error Rate'][]=array($aValue[5]); 
                            $aHeader['Intensity C1'][]=array($aValue[6]); 
                            $aHeader['%>=Q30'][]=array($aValue[7]); 
                    }elseif(count($aValue)==7){
                            $aHeader['Level'][]=array($aValue[0]);
                            $aHeader['Yield'][]=array($aValue[1]); 
                            $aHeader['Projected Yield'][]=array($aValue[2]); 
                            $aHeader['Aligned'][]=array($aValue[3]); 
                            $aHeader['Error Rate'][]=array($aValue[4]); 
                            $aHeader['Intensity C1'][]=array($aValue[5]); 
                            $aHeader['%>=Q30'][]=array($aValue[6]);
                    }elseif(count($aValue)==9){
                            $aHeader['Level'][]=array($aValue[0].' '.$aValue[1].' '.$aValue[2]);
                            $aHeader['Yield'][]=array($aValue[3]); 
                            $aHeader['Projected Yield'][]=array($aValue[4]); 
                            $aHeader['Aligned'][]=array($aValue[5]); 
                            $aHeader['Error Rate'][]=array($aValue[6]); 
                            $aHeader['Intensity C1'][]=array($aValue[7]); 
                            $aHeader['%>=Q30'][]=array($aValue[8]);
                    }elseif(count($aValue)==2){
                            $aReadInfo[$aValue[0].' '.$aValue[1]] = array();
                            $sRead = $aValue[0].' '.$aValue[1];
                    }elseif(count($aValue)==3){
                            $aReadInfo[$aValue[0].' '.$aValue[1].$aValue[2]] = array();
                            $sRead = $aValue[0].' '.$aValue[1].$aValue[2];
                    }elseif(count($aValue)==23){
                            $aReadInfo[$sRead][$aValue[0]] = array();
                            $aReadInfo[$sRead][$aValue[1]] = array();
                            $aReadInfo[$sRead][$aValue[2]] = array();
                            $aReadInfo[$sRead][$aValue[3].' '.$aValue[4]] = array();
                            $aReadInfo[$sRead][$aValue[5]] = array();
                            $aReadInfo[$sRead][$aValue[6]] = array();
                            $aReadInfo[$sRead][$aValue[7].' '.$aValue[8]] = array();
                            $aReadInfo[$sRead][$aValue[9]] = array();
                            $aReadInfo[$sRead][$aValue[10]] = array();
                            $aReadInfo[$sRead][$aValue[11].' '.$aValue[12]] = array();
                            $aReadInfo[$sRead][$aValue[13]] = array();
                            $aReadInfo[$sRead][$aValue[14]] = array();
                            $aReadInfo[$sRead][$aValue[15].' '.$aValue[16]] = array();
                            $aReadInfo[$sRead][$aValue[17].' '.$aValue[18]] = array();
                            $aReadInfo[$sRead][$aValue[19].' '.$aValue[20]] = array();
                            $aReadInfo[$sRead][$aValue[21].' '.$aValue[22]] = array();
                    }elseif(count($aValue) >= 34){
                            if(count($aValue) == 34){
                                $aReadInfo[$sRead]['Lane'][] = array($aValue[0]);
                                $aReadInfo[$sRead]['Tiles'][] = array($aValue[1]);
                                $aReadInfo[$sRead]['Density'][] = array($aValue[2].$aValue[3].$aValue[4]);
                                $aReadInfo[$sRead]['Cluster PF'][] = array($aValue[5].$aValue[6].$aValue[7]);
                                $aReadInfo[$sRead]['Phas/Prephas'][] = array($aValue[8].$aValue[9].$aValue[10]); 
                                $aReadInfo[$sRead]['Reads'][] = array($aValue[11]);
                                $aReadInfo[$sRead]['Reads PF'][] = array($aValue[12]);
                                $aReadInfo[$sRead]['%>=Q30'][] = array($aValue[13]);
                                $aReadInfo[$sRead]['Yield'][] = array($aValue[14]);
                                $aReadInfo[$sRead]['Cycles Error'][] = array($aValue[15]);
                                $aReadInfo[$sRead]['Aligned'][] = array($aValue[16].$aValue[17].$aValue[18]);
                                $aReadInfo[$sRead]['Error'][] = array($aValue[19].$aValue[20].$aValue[21]);
                                $aReadInfo[$sRead]['Error (35)'][] = array($aValue[22].$aValue[23].$aValue[24]);
                                $aReadInfo[$sRead]['Error (75)'][] = array($aValue[25].$aValue[26].$aValue[27]); 
                                $aReadInfo[$sRead]['Error (100)'][] = array($aValue[28].$aValue[29].$aValue[30]);
                                $aReadInfo[$sRead]['Intensity C1'][] = array($aValue[31].$aValue[32].$aValue[33]);
                            }elseif(count($aValue) == 36){
                                $aReadInfo[$sRead]['Lane'][] = array($aValue[0]);
                                $aReadInfo[$sRead]['Tiles'][] = array($aValue[1]);
                                $aReadInfo[$sRead]['Density'][] = array($aValue[2].$aValue[3].$aValue[4]);
                                $aReadInfo[$sRead]['Cluster PF'][] = array($aValue[5].$aValue[6].$aValue[7]);
                                $aReadInfo[$sRead]['Phas/Prephas'][] = array($aValue[8].$aValue[9].$aValue[10]); 
                                $aReadInfo[$sRead]['Reads'][] = array($aValue[11]);
                                $aReadInfo[$sRead]['Reads PF'][] = array($aValue[12]);
                                $aReadInfo[$sRead]['%>=Q30'][] = array($aValue[13]);
                                $aReadInfo[$sRead]['Yield'][] = array($aValue[14]);
                                $aReadInfo[$sRead]['Cycles Error'][] = array($aValue[15].$aValue[16].$aValue[17]);
                                $aReadInfo[$sRead]['Aligned'][] = array($aValue[18].$aValue[19].$aValue[20]);
                                $aReadInfo[$sRead]['Error'][] = array($aValue[21].$aValue[22].$aValue[23]);
                                $aReadInfo[$sRead]['Error (35)'][] = array($aValue[24].$aValue[25].$aValue[26]);
                                $aReadInfo[$sRead]['Error (75)'][] = array($aValue[27].$aValue[28].$aValue[29]); 
                                $aReadInfo[$sRead]['Error (100)'][] = array($aValue[30].$aValue[31].$aValue[32]);
                                $aReadInfo[$sRead]['Intensity C1'][] = array($aValue[33].$aValue[34].$aValue[35]);                                
                            }
                    }elseif(count($aValue)==4){
                        if($aValue[0]=='Extracted:'){
                            $aExtraInfo['Extracted'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        );
                        }elseif($aValue[0]=='Called:'){
                            $aExtraInfo['Called'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        ); 
                        }elseif($aValue[0]=='Scored:'){
                            $aExtraInfo['Scored'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        );
                        }
                    }else{
                        continue;
                    }
                    
                    
                }
            }
            $aClusterPF = array();
            $aDensity = array();
            if(!empty($aReadInfo)){
                foreach ($aReadInfo as $key => $aRead) {
                    foreach ($aRead as $key1 => $aValue) {
                        if($key1=='Cluster PF'){
                            $aClusterPF[] = $aValue[0][0];
                        }
                        if($key1=="Density"){
                            $aDensity[] = $aValue[0][0];   
                        }
                    }
                }
            }
            $iRowCount = count($aHeader['%>=Q30']); 

            $aMasterInfo = array_merge($aHeader,$aReadInfo,$aExtraInfo);
            $aMasterInfo['avg_q30'] = $aHeader['%>=Q30'][$iRowCount-1][0];
            $aMasterInfo['clusterPF'] = $aClusterPF;
            $aMasterInfo['aDensity'] = $aDensity;
            return $aMasterInfo;
        }elseif($iSequencerType==2){
            foreach ($aFileInfo as $iRow => $sValue) {
                  
                $aValueInfo = explode(',', $sValue);
                $aValueInfo[0]=trim($aValueInfo[0]);
                if($aValueInfo[0]==''){
                    continue;
                }
                //print_r($aValueInfo);
                if($iRow==0){
                    $sVersionName = $aValueInfo[0];
                }else{
                    $sValue = preg_replace('/\s\s+/', ' ', $aValueInfo[0]);
                    $aValue = explode(' ', $sValue);
                    // print_r($aValue);
                    // echo count($aValue);
                    if($aValue[0]=='Level'){
                        $aHeader[$aValue[0]]= array();
                        $aHeader[$aValue[1]]= array();
                        $aHeader[$aValue[2].' '.$aValue[3]]= array();
                        $aHeader[$aValue[4]]= array();
                        $aHeader[$aValue[5].' '.$aValue[6]]= array();
                        $aHeader[$aValue[7].' '.$aValue[8]]= array();
                        $aHeader[$aValue[9]]= array();
                    }elseif(count($aValue)==8){
                            $aHeader['Level'][]=array($aValue[0].' '.$aValue[1]);
                            $aHeader['Yield'][]=array($aValue[2]); 
                            $aHeader['Projected Yield'][]=array($aValue[3]); 
                            $aHeader['Aligned'][]=array($aValue[4]); 
                            $aHeader['Error Rate'][]=array($aValue[5]); 
                            $aHeader['Intensity C1'][]=array($aValue[6]); 
                            $aHeader['%>=Q30'][]=array($aValue[7]); 
                    }elseif(count($aValue)==7){
                            $aHeader['Level'][]=array($aValue[0]);
                            $aHeader['Yield'][]=array($aValue[1]); 
                            $aHeader['Projected Yield'][]=array($aValue[2]); 
                            $aHeader['Aligned'][]=array($aValue[3]); 
                            $aHeader['Error Rate'][]=array($aValue[4]); 
                            $aHeader['Intensity C1'][]=array($aValue[5]); 
                            $aHeader['%>=Q30'][]=array($aValue[6]);
                    }elseif(count($aValue)==9){
                            $aHeader['Level'][]=array($aValue[0].' '.$aValue[1].' '.$aValue[2]);
                            $aHeader['Yield'][]=array($aValue[3]); 
                            $aHeader['Projected Yield'][]=array($aValue[4]); 
                            $aHeader['Aligned'][]=array($aValue[5]); 
                            $aHeader['Error Rate'][]=array($aValue[6]); 
                            $aHeader['Intensity C1'][]=array($aValue[7]); 
                            $aHeader['%>=Q30'][]=array($aValue[8]);
                    }elseif(count($aValue)==2){
                            $aReadInfo[$aValue[0].' '.$aValue[1]] = array();
                            $sRead = $aValue[0].' '.$aValue[1];
                    }elseif(count($aValue)==3){
                            $aReadInfo[$aValue[0].' '.$aValue[1].$aValue[2]] = array();
                            $sRead = $aValue[0].' '.$aValue[1].$aValue[2];
                    }elseif(count($aValue)==23){
                            $aReadInfo[$sRead][$aValue[0]] = array();
                            $aReadInfo[$sRead][$aValue[1]] = array();
                            $aReadInfo[$sRead][$aValue[2]] = array();
                            $aReadInfo[$sRead][$aValue[3].' '.$aValue[4]] = array();
                            $aReadInfo[$sRead][$aValue[5]] = array();
                            $aReadInfo[$sRead][$aValue[6]] = array();
                            $aReadInfo[$sRead][$aValue[7].' '.$aValue[8]] = array();
                            $aReadInfo[$sRead][$aValue[9]] = array();
                            $aReadInfo[$sRead][$aValue[10]] = array();
                            $aReadInfo[$sRead][$aValue[11].' '.$aValue[12]] = array();
                            $aReadInfo[$sRead][$aValue[13]] = array();
                            $aReadInfo[$sRead][$aValue[14]] = array();
                            $aReadInfo[$sRead][$aValue[15].' '.$aValue[16]] = array();
                            $aReadInfo[$sRead][$aValue[17].' '.$aValue[18]] = array();
                            $aReadInfo[$sRead][$aValue[19].' '.$aValue[20]] = array();
                            $aReadInfo[$sRead][$aValue[21].' '.$aValue[22]] = array();
                    }elseif(count($aValue) >= 34){
                            if(count($aValue) == 34){
                                $aReadInfo[$sRead]['Lane'][] = array($aValue[0]);
                                $aReadInfo[$sRead]['Tiles'][] = array($aValue[1]);
                                $aReadInfo[$sRead]['Density'][] = array($aValue[2].$aValue[3].$aValue[4]);
                                $aReadInfo[$sRead]['Cluster PF'][] = array($aValue[5].$aValue[6].$aValue[7]);
                                $aReadInfo[$sRead]['Phas/Prephas'][] = array($aValue[8].$aValue[9].$aValue[10]); 
                                $aReadInfo[$sRead]['Reads'][] = array($aValue[11]);
                                $aReadInfo[$sRead]['Reads PF'][] = array($aValue[12]);
                                $aReadInfo[$sRead]['%>=Q30'][] = array($aValue[13]);
                                $aReadInfo[$sRead]['Yield'][] = array($aValue[14]);
                                $aReadInfo[$sRead]['Cycles Error'][] = array($aValue[15]);
                                $aReadInfo[$sRead]['Aligned'][] = array($aValue[16].$aValue[17].$aValue[18]);
                                $aReadInfo[$sRead]['Error'][] = array($aValue[19].$aValue[20].$aValue[21]);
                                $aReadInfo[$sRead]['Error (35)'][] = array($aValue[22].$aValue[23].$aValue[24]);
                                $aReadInfo[$sRead]['Error (75)'][] = array($aValue[25].$aValue[26].$aValue[27]); 
                                $aReadInfo[$sRead]['Error (100)'][] = array($aValue[28].$aValue[29].$aValue[30]);
                                $aReadInfo[$sRead]['Intensity C1'][] = array($aValue[31].$aValue[32].$aValue[33]);
                            }elseif(count($aValue) == 36){
                                $aReadInfo[$sRead]['Lane'][] = array($aValue[0]);
                                $aReadInfo[$sRead]['Tiles'][] = array($aValue[1]);
                                $aReadInfo[$sRead]['Density'][] = array($aValue[2].$aValue[3].$aValue[4]);
                                $aReadInfo[$sRead]['Cluster PF'][] = array($aValue[5].$aValue[6].$aValue[7]);
                                $aReadInfo[$sRead]['Phas/Prephas'][] = array($aValue[8].$aValue[9].$aValue[10]); 
                                $aReadInfo[$sRead]['Reads'][] = array($aValue[11]);
                                $aReadInfo[$sRead]['Reads PF'][] = array($aValue[12]);
                                $aReadInfo[$sRead]['%>=Q30'][] = array($aValue[13]);
                                $aReadInfo[$sRead]['Yield'][] = array($aValue[14]);
                                $aReadInfo[$sRead]['Cycles Error'][] = array($aValue[15].$aValue[16].$aValue[17]);
                                $aReadInfo[$sRead]['Aligned'][] = array($aValue[18].$aValue[19].$aValue[20]);
                                $aReadInfo[$sRead]['Error'][] = array($aValue[21].$aValue[22].$aValue[23]);
                                $aReadInfo[$sRead]['Error (35)'][] = array($aValue[24].$aValue[25].$aValue[26]);
                                $aReadInfo[$sRead]['Error (75)'][] = array($aValue[27].$aValue[28].$aValue[29]); 
                                $aReadInfo[$sRead]['Error (100)'][] = array($aValue[30].$aValue[31].$aValue[32]);
                                $aReadInfo[$sRead]['Intensity C1'][] = array($aValue[33].$aValue[34].$aValue[35]);                                
                            }
                    }elseif(count($aValue)==4){
                        if($aValue[0]=='Extracted:'){
                            $aExtraInfo['Extracted'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        );
                        }elseif($aValue[0]=='Called:'){
                            $aExtraInfo['Called'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        ); 
                        }elseif($aValue[0]=='Scored:'){
                            $aExtraInfo['Scored'] = array(
                                        'Label'=>$aValue[0],
                                        'Value'=>$aValue[1].' '.$aValue[2].' '.$aValue[3],
                                        );
                        }
                    }else{
                        continue;
                    }
                    
                    
                }
            }
            $aClusterPF = array();
            $aDensity = array();
            if(!empty($aReadInfo)){
                foreach ($aReadInfo as $key => $aRead) {
                    foreach ($aRead as $key1 => $aValue) {
                        if($key1=='Cluster PF'){
                            $aClusterPF[] = $aValue[0][0];
                        }
                        if($key1=="Density"){
                            $aDensity[] = $aValue[0][0];   
                        }
                    }
                }
            }
            $aMasterInfo = array_merge($aHeader,$aReadInfo,$aExtraInfo);
            $iRowCount = count($aHeader['%>=Q30']);

            $aMasterInfo['avg_q30'] = $aHeader['%>=Q30'][$iRowCount-1][0];
            $aMasterInfo['clusterPF'] = $aClusterPF;
            $aMasterInfo['aDensity'] = $aDensity;
            return $aMasterInfo;
        }
    }
//!! Function to read the CSV of Extractionmetrcis.bin
    function fReadExtractionCSVInfo($sLocalPath){
        $aFileInfo = file("$sLocalPath",FILE_SKIP_EMPTY_LINES);
        
        
        $iTotalRow = (int)count($aFileInfo);
        $sSecondlastRow = $aFileInfo[$iTotalRow-2];
        
        $aValueInfo = explode(',', $sSecondlastRow);
        $iCycleNumber = (int)$aValueInfo[2];
        $sTimeStamp = $aValueInfo[3];
        
        $aMasterInfo = array(
                    'cycle_number'=>$iCycleNumber,
                    'dtDateTime'=>$sTimeStamp
                        );
            return $aMasterInfo;
    }
    //!! XML section
/*
    Functions for get the sequencers for all XML
    1. Function to get Sequencer information for Reading RunParameters.xml
    2. Function to get Sequencer information for Reading GeneratedFASTQRuns.xml
    3. Function to get Sequencer information for Reading RunInfo.xml
    4. Function to get Sequencer information for Reading RunCompletionJobStatus.xml
    5. Function to get Sequencer information for Reading CompletedJObInfo.xml
    
*/

//!! Function to get the sequencer for the runParameters
    function fetSequencerToReadRunParameters(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid`,c.`seq_local_path` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=6)
                    AND b.`seq_status`=3 AND c.`seq_file_type`=2";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        } 
    }

//!! Function to get the sequencer for Reading its FASTQRuns xml file

    function fetSequencerToReadFASTQRuns(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=3
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=7)
                    AND b.`seq_status`=4";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }         
    }

//!! Function to get the sequencer for Reading its RunInfo.xml file

    function fetSequencerToReadRunInfoXml(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid`,c.`seq_local_path` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=5
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=9)
                    AND b.`seq_status`=8";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

//!! This function is will get only NextSeq Directory by (seq_type=2)

    function fetSequencerToReadRunCompletionJobXml(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid`,c.`seq_local_path` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=6
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=11)
                    AND b.`seq_status`=10 AND a.`seq_type`=2";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

//!! Function to get sequencer information to read completedJObInfo.xml file

    function fGetSequencerToReadCompletedJobInfo(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

       $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid`,c.`seq_local_path` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=4
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=101)
                    AND b.`seq_status`=100 AND a.`seq_type`=1";
       
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }         
    }
//!! Function to get sequencer information to read Demultiplexing.xml file
    function fetSequencerToReadDemultiplexStatsXml(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sFileTable = DATABASE_TABLE_PREFIX."_sequencer_files_master";

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,c.`seq_local_path`,b.`seq_status`,b.`isValid` 
                    FROM `{$sTable}` as a 
                    LEFT JOIN `{$sStatusTable}` as b on a.`seq_directory_id` = b.`seq_directory_id`
                    LEFT JOIN `{$sFileTable}` as c on c.`seq_directory_id` = a.`seq_directory_id`
                    AND c.`seq_file_type`=7
                    WHERE 
                    b.`seq_directory_id`  NOT IN
                        (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE `seq_status`=13)
                    AND b.`seq_status`=12 AND a.`seq_type`=2";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }         
    }

/*
    Functions for get the XML Information in Array format
    1. Function to get Sequencer information for Reading RunParameters.xml
    2. Function to get Sequencer information for Reading GeneratedFASTQRuns.xml
    3. Function to get Sequencer information for Reading RunInfo.xml
    4. Function to get Sequencer information for Reading RunCompletionJobStatus.xml
    5. Function to get Sequencer information for Reading CompletedJObInfo.xml
    6. Function to get Sequencer information for Reading Demultiplexing.xml
    
*/


//!! Function to Read and get data from runParameter.xml
    function ReadAndGetRunParametersXmlInfo($sPathToRunParameters,$iSeqType){
        $aMasterInfoRunParameter = array();
        $oXml = simplexml_load_file($sPathToRunParameters);

        $aXML = xml2array($oXml);
        if($iSeqType==1){
            $iScannerID = (string)$oXml->ScannerID[0];
        }else{
            $iScannerID = (string)$oXml->InstrumentID[0];
        }

        $iRunID = (string)$oXml->RunID[0];
        $sBarcode = (string)$oXml->Barcode[0];
        $sExperimentName = (string)$oXml->ExperimentName[0];
         
        // date of sequencer starts

        $dRunStartDate = (string)$oXml->RunStartDate[0];
        $sYear = substr($dRunStartDate,0,2);
        $sMonth = substr($dRunStartDate,2,2);
        $sDate = substr($dRunStartDate,4,2);
        $dRunStartDate = $sYear.'-'.$sMonth.'-'.$sDate;
        $dRunStartDate = date('Y-m-d',strtotime($dRunStartDate));

        $iRunNumber = (string)$oXml->RunNumber[0];
         // Extra Information
        $sFPGAVersion = (string)$oXml->FPGAVersion[0];
        $sMCSVersion = (string)$oXml->MCSVersion[0];
        $sRTAVersion = (string)$oXml->RTAVersion[0];
        $sPR2BottleBarcode = (string)$oXml->PR2BottleBarcode[0];
        $iReagentKitPartNumberEntered = (string)$oXml->ReagentKitPartNumberEntered[0];
        $sReagentKitVersion = (string)$oXml->ReagentKitVersion[0];
        $sReagentKitBarcode = (string)$oXml->ReagentKitBarcode[0];

        $aMasterInfoRunParameter['iScannerID']=$iScannerID;
        $aMasterInfoRunParameter['iRunID']=$iRunID;
        $aMasterInfoRunParameter['sBarcode']=$sBarcode;
        $aMasterInfoRunParameter['sExperimentName']=$sExperimentName;

        $aMasterInfoRunParameter['dRunStartDate']=$dRunStartDate;
        $aMasterInfoRunParameter['iRunNumber']=$iRunNumber;
         
          
        $aMasterInfoRunParameter['sFPGAVersion']=$sFPGAVersion;
        $aMasterInfoRunParameter['sMCSVersion']=$sMCSVersion;
        $aMasterInfoRunParameter['sRTAVersion']=$sRTAVersion;
        $aMasterInfoRunParameter['sPR2BottleBarcode']=$sPR2BottleBarcode;
        $aMasterInfoRunParameter['iReagentKitPartNumberEntered']=$sPR2BottleBarcode;
        $aMasterInfoRunParameter['sReagentKitVersion']=$sPR2BottleBarcode;
        $aMasterInfoRunParameter['sReagentKitBarcode']=$sPR2BottleBarcode;
        
        if($iSeqType==1){
            $aReadInfo = array();
            foreach ($oXml->Reads->RunInfoRead as $key => $value) {
                $iNumber = (string)$value['Number'];
                $iNumCycle = (string)$value['NumCycles'];
                $sIndexed = (string)$value['IsIndexedRead'];
                $aTemp=array('run_number'=>$iNumber,'run_cycle'=>$iNumCycle,'run_indexed'=>$sIndexed);
                $aReadInfo[]=$aTemp;
            }
            $aMasterInfoRunParameter['aReadInfo']=$aReadInfo;
        }    

        $aMasterInfoRunParameter['allXML']=$aXML;
        return $aMasterInfoRunParameter;
    }

//!! Function to Read and Get the GenerateFASTQRunStatistics
    function ReadAndGetGenerateFASTQRunStatisticsXmlInfo($sPathToGenerateFASTQRunStatistics){
        $aMasterInfoFASTQRuns = array();
        $aSampleSummary = array();

        $oXml = simplexml_load_file($sPathToGenerateFASTQRunStatistics);
        $aXML = xml2array($oXml);
        $aRunStats = $oXml->RunStats;
        $sAnalysisSoftwareVersion = (string)$aRunStats->AnalysisSoftwareVersion[0];
        $aMasterInfoFASTQRuns['sAnalysisSoftwareVersion']=$sAnalysisSoftwareVersion;
        $iNumberOfClustersPF = (string)$aRunStats->NumberOfClustersPF[0];
        $aMasterInfoFASTQRuns['iNumberOfClustersPF']=$iNumberOfClustersPF;
        $iNumberOfClustersRaw = (string)$aRunStats->NumberOfClustersRaw[0];
        $aMasterInfoFASTQRuns['iNumberOfClustersRaw']=$iNumberOfClustersRaw;
        $iNumberOfDuplicateClusters = (string)$aRunStats->NumberOfDuplicateClusters[0];
        $aMasterInfoFASTQRuns['iNumberOfDuplicateClusters']=$iNumberOfDuplicateClusters;
        $iNumberOfUnalignedClusters = (string)$aRunStats->NumberOfUnalignedClusters[0];
        $aMasterInfoFASTQRuns['iNumberOfUnalignedClusters']=$iNumberOfUnalignedClusters;
        $iNumberOfUnalignedClustersPF = (string)$aRunStats->NumberOfUnalignedClustersPF[0];
        $aMasterInfoFASTQRuns['iNumberOfUnalignedClustersPF']=$iNumberOfUnalignedClustersPF;
        $iNumberOfUnindexedClustersPF = (string)$aRunStats->NumberOfUnindexedClustersPF[0];
        $aMasterInfoFASTQRuns['iNumberOfUnindexedClustersPF']=$iNumberOfUnindexedClustersPF;
        $sOverallCoverage = (string)$aRunStats->OverallCoverage[0];
        $aMasterInfoFASTQRuns['sOverallCoverage']=$iNumberOfUnindexedClustersPF;
        $sOverallErrorPlotPath = (string)$aRunStats->OverallErrorPlotPath[0];
        $aMasterInfoFASTQRuns['sOverallErrorPlotPath']=$sOverallErrorPlotPath;
        $iOverallNoCallPlotPath = (string)$aRunStats->OverallNoCallPlotPath[0];
        $aMasterInfoFASTQRuns['iOverallNoCallPlotPath']=$iOverallNoCallPlotPath;
        $iYieldInBasesPF = (string)$aRunStats->YieldInBasesPF[0];
        $aMasterInfoFASTQRuns['iYieldInBasesPF']=$iYieldInBasesPF;
        $iYieldInBasesRaw = (string)$aRunStats->YieldInBasesRaw[0];
        $aMasterInfoFASTQRuns['iYieldInBasesRaw']=$iYieldInBasesRaw;



        $aOverallSampleStatics = $oXml->OverallSamples;
        $iTotalSampleCount = count($aOverallSampleStatics->SummarizedSampleStatistics);
        for ($iii=0; $iii < $iTotalSampleCount; $iii++) { 
          $aSampleStatic = $aOverallSampleStatics->SummarizedSampleStatistics[$iii];
          
          $iSampleStaticID = (string)$aSampleStatic->SampleID;
          $sSampleStaticName = (string)$aSampleStatic->SampleName;
          $iSampleStaticNumber = (string)$aSampleStatic->SampleNumber;
          $iNumberOfClustersPF = (string)$aSampleStatic->NumberOfClustersPF;
          $iNumberOfClustersRaw = (string)$aSampleStatic->NumberOfClustersRaw;
          $iSampleStaticQ30 = (string)$aSampleStatic->PercentQ30;

          $aSampleSummary[]=array(
                               'SampleID'=> $iSampleStaticID,
                               'SampleName'=> $sSampleStaticName,
                               'SampleNumber'=> $iSampleStaticNumber,
                               'NumberOfClustersPF'=> $iNumberOfClustersPF,
                               'NumberOfClustersRaw'=> $iNumberOfClustersRaw,
                               'PercentQ30'=> $iSampleStaticQ30,
                              );
          
        }
        $aMasterInfoFASTQRuns['sample_summary']=$aSampleSummary;
        $aMasterInfoFASTQRuns['allXML']=$aXML;
        return $aMasterInfoFASTQRuns;
    }
        
//!! Function to read and return the array of RunInfo.xml
    function ReadAndGetRunInfoXmlInfo($sFileNameWithPath){
         $aRead = array();
         $aReadInfoMaster = array();

         $oXml = simplexml_load_file($sFileNameWithPath);
         $aXML = xml2array($oXml);

         $sRunID = (string)$oXml->Run['Id'];
         $iRunNumber = (string)$oXml->Run['Number'];
         
         $sInstrument = (string)$oXml->Run->Instrument;

         $aReadInfoMaster['runinfo']=array(
            'run_id'=>$sRunID,
            'run_number'=>$iRunNumber,
            'run_instrument'=>$sInstrument
          );

         
         $aReadInfo = $oXml->Run->Reads;
         for ($ii=0; $ii < count($aReadInfo->Read); $ii++) { 
            $iCycleNumber = (string)$aReadInfo->Read['NumCycles'];
            $iNumber = (string)$aReadInfo->Read['Number'];
            $sIsIndexedRead = (string)$aReadInfo->Read['IsIndexedRead'];
            if($sIsIndexedRead=='Y'){
              $bIsIndex = true;
            }else{
              $bIsIndex = false;
            }
            $aRead[]=array(
                'cycle_number'=>$iCycleNumber,
                'number'=>$iNumber,
                'bIsIndex'=>$bIsIndex
                );
         }

         $aReadInfoMaster['readinfo']=$aRead;
         $aReadInfoMaster['allXML']=$aXML;
         return $aReadInfoMaster;
    }

//!! Function to read and return the array of RunCompletionJobStatus.xml
    function ReadAndGetRunCompletionJobXmlInfo($sFileNameWithPath){
        $oXml = simplexml_load_file($sFileNameWithPath);
        
        $aXML = xml2array($oXml);
        
        $sVersion = (string)$oXml->Version;
        $sCompletionStatus = (string)$oXml->CompletionStatus;
        
        $sRunId = (string)$oXml->RunId;
        $sPlannedRead1Cycles = (string)$oXml->PlannedRead1Cycles;
        $sPlannedRead2Cycles = (string)$oXml->PlannedRead2Cycles;
        $sPlannedIndex1ReadCycles = (string)$oXml->PlannedIndex1ReadCycles;
        $sPlannedIndex2ReadCycles = (string)$oXml->PlannedIndex2ReadCycles;
        $sCompletedRead1Cycles = (string)$oXml->CompletedRead1Cycles;
        $sCompletedRead2Cycles = (string)$oXml->CompletedRead2Cycles;
        $sCompletedIndex1ReadCycles = (string)$oXml->CompletedIndex1ReadCycles;
        $sCompletedIndex2ReadCycles = (string)$oXml->CompletedIndex2ReadCycles;
        
        $sClusterDensity = (string)$oXml->ClusterDensity;
        $sClustersPassingFilter = (string)$oXml->ClustersPassingFilter;
        $sEstimatedYield = (string)$oXml->EstimatedYield;
        $sErrorDescription = (string)$oXml->ErrorDescription;

        $aReadInfoMaster['master_info']= array(
                    array('version',$sVersion),
                    array('CompletionStatus',$sCompletionStatus),
                    array('RunId',$sRunId),
                    array('PlannedRead1Cycles',$sPlannedRead1Cycles),
                    array('PlannedRead2Cycles',$sPlannedRead2Cycles),
                    array('PlannedIndex1ReadCycles',$PlannedIndex1ReadCycles),
                    array('PlannedIndex2ReadCycles',$PlannedIndex2ReadCycles),
                    array('CompletedRead1Cycles',$CompletedRead1Cycles),
                    array('CompletedRead2Cycles',$CompletedRead2Cycles),
                    array('CompletedIndex1ReadCycles',$CompletedIndex1ReadCycles),
                    array('CompletedIndex2ReadCycles',$CompletedIndex2ReadCycles),
                    array('ClusterDensity',$ClusterDensity),
                    array('ClustersPassingFilter',$ClustersPassingFilter),
                    array('EstimatedYield',$EstimatedYield),
                    array('ErrorDescription',$ErrorDescription)
                );
        $aReadInfoMaster['allXML']=$aXML;
        return $aReadInfoMaster;
    }

//!! Function to read and get data from completeJobInfo.xml
    function ReadAndGetCompleteJobXmlInfo($sFullPathCompleteJobRunInfofFile){
        $aMasterInfoCompleteJob = array();
        $oXml = simplexml_load_file($sFullPathCompleteJobRunInfofFile);

        $aXML = xml2array($oXml);
        // date time for completed sequencer
        $dtCompleteTime = (string)$oXml->CompletionTime;
        $aCompleteDateTime = explode('T', $dtCompleteTime);
        $dCompleteDate = $aCompleteDateTime[0];
        $tCompleteTime = substr($aCompleteDateTime[1],0,8);
        $dtCompleted = $dCompleteDate.' '.$tCompleteTime;
        $dtCompleted = date('Y-m-d H:i:s',strtotime($dtCompleted));

        
        $dtStartTime = (string)$oXml->StartTime;
        $aStartDateTime = explode('T', $dtStartTime);
        $dStartDate = $aStartDateTime[0];
        $tTime = substr($aStartDateTime[1],0,8);
        $dtStart = $dStartDate.' '.$tTime;
        $dtStart = date('Y-m-d H:i:s',strtotime($dtStart));

        $sSheetType = (string)$oXml->Sheet->Type;
        $sWorkflow = (string)$oXml->Sheet->WorkflowType;

        $sRTAOutputFolder = (string)$oXml->RTAOutputFolder;

        $aMasterInfoCompleteJob['dtStart']=$dtStart;
        $aMasterInfoCompleteJob['dtCompleted']=$dtCompleted;
        $aMasterInfoCompleteJob['sSheetType']=$sSheetType;
        $aMasterInfoCompleteJob['sWorkflow']=$sWorkflow;
        $aMasterInfoCompleteJob['sRTAOutputFolder']=$sRTAOutputFolder;
        
        $aMasterInfoCompleteJob['allXML']=$aXML;

       return $aMasterInfoCompleteJob;
    }
//!! Function to read and get data from DemultiplexStats.xml
    function ReadAndGetDemultiplexStatsXmlInfo($sFullPathDemultiplexingXmlInfofFile){
        $aMasterInfoDemultiplexStats = array();
        $oXml = simplexml_load_file($sFullPathDemultiplexingXmlInfofFile);
        $aXML = xml2array($oXml);
        //print_r($oXml);
        $aProArray = (array)$oXml->Flowcell;
       
        $aMasterInfo = array();
       
        $aProject = $aProArray['Project'];

        $iTotal = count($aProject); 
        $iTotalBarcodeCount = 0;
        foreach ($aProject as $key1 => $aValues) {
            
            $aValues = (array)$aValues;
            $aSample = $aValues['Sample'];
            $aSample =(array)$aSample;
            if(($iTotal-1)==$key1){
                   
                    $sSampleName = $aSample['@attributes']['name'];
                    $aBarcode = $aSample['Barcode'];
                    $aBarcode = (array)$aBarcode;
                    
                    $aBarcodeInfo = array();
                    

                        $aBarValue = (array)$aBarcode;
                       $sBarcodeName = $aBarValue['@attributes']['name'];
                        $aLane = $aBarValue['Lane'];
                        
                        $aLanInfo = array();
                        foreach ($aLane as $key3 => $aLanValue) {
                            $aLanValue = (array)$aLanValue;
                            $iLaneNumber = $aLanValue['@attributes']['number'];
                            $sBarcodeCount = $aLanValue['BarcodeCount'];
                            $sPerfectBarcodeCount = $aLanValue['PerfectBarcodeCount'];
                            $sOneMismatchBarcodeCount = $aLanValue['OneMismatchBarcodeCount'];
                            $aLanInfo[] = array(
                                            'iLaneNumber'=>$iLaneNumber,
                                            'sBarcodeCount'=>$sBarcodeCount,
                                            'sPerfectBarcodeCount'=>$sPerfectBarcodeCount,
                                            'sOneMismatchBarcodeCount'=>$sOneMismatchBarcodeCount,
                                        );
                            $iTotalBarcodeCount = $iTotalBarcodeCount + (int)$sBarcodeCount;
                        }
                        
                        $aBarcodeInfo[$sBarcodeName] = $aLanInfo;

                    $aMasterInfo[$sSampleName] = $aBarcodeInfo;   
            }else{
                foreach ($aSample as $key2 => $aValue) {
                    $aTemp = (array)$aValue;
                    $sSampleName = $aTemp['@attributes']['name'];
                    $aBarcode = $aTemp['Barcode'];
                    $aBarcode = (array)$aBarcode;
                    $aBarcodeInfo = array();
                    foreach ($aBarcode as $key3 => $aBarValue) {
                        $aBarValue = (array)$aBarValue;
                        $sBarcodeName = $aBarValue['@attributes']['name'];
                        $aLane = $aBarValue['Lane'];
                        $aLanInfo = array();
                        foreach ($aLane as $key4 => $aLanValue) {
                            $aLanValue = (array)$aLanValue;
                            $iLaneNumber = $aLanValue['@attributes']['number'];
                            $sBarcodeCount = $aLanValue['BarcodeCount'];
                            $sPerfectBarcodeCount = $aLanValue['PerfectBarcodeCount'];
                            $sOneMismatchBarcodeCount = $aLanValue['OneMismatchBarcodeCount'];
                            $aLanInfo[] = array(
                                            'iLaneNumber'=>$iLaneNumber,
                                            'sBarcodeCount'=>$sBarcodeCount,
                                            'sPerfectBarcodeCount'=>$sPerfectBarcodeCount,
                                            'sOneMismatchBarcodeCount'=>$sOneMismatchBarcodeCount,
                                        );
                        }
                        
                        $aBarcodeInfo[$sBarcodeName] = $aLanInfo;

                       // print_r($aBarValue['Lane']);
                    }
                    $aMasterInfo[$sSampleName] = $aBarcodeInfo;
                    //print_r($aTemp['Barcode']);
                }
            }
        }

        // print_r($aMasterInfo);
        // exit();
     
        $aMasterInfoDemultiplexStats['imp_info']=$aMasterInfo;
        $aMasterInfoDemultiplexStats['allXML']=$aXML;
        $aMasterInfoDemultiplexStats['iTotalBarcodeCount'] = $iTotalBarcodeCount;
       return $aMasterInfoDemultiplexStats;
    }


/*
    Function to add information of and XMl's
    1. Function to add Read Information of XML's
    2. Function to add Imporatnt Information of XML's like label-value
    3. Function to add Sample Summary Information from XML's
    4. Function to add all info of each XMl in JSON format
    5. Function to Logger
    6. Function to convert the xml object into array.
*/

//!! function to add reads info from thre XML
    function addReadsInfoFromXML($iSeqDirectoryid,$iSeqFileType,$aReadInfo){
        if(!empty($aReadInfo)){
                $DBMan = new DBConnManager();
                $conn =  $DBMan->getConnInstance();
                $sTable = DATABASE_TABLE_PREFIX."_sequencer_read_info";
                $dAddedOn = date('Y-m-d H:i:s');
                $sIQuery ='';
                foreach ($aReadInfo as $key => $aValue) {
                    $iCycleNumber = $aValue['cycle_number'];
                    $iNumber = $aValue['number'];
                    $bIndexed = $aValue['bIsIndex'];
                    if($bIndexed)
                       { $sIndex='Y';}
                    else
                        { $sIndex='N';}

                    $sIQuery .= "INSERT INTO {$sTable} (`id_read_info`,`seq_directory_id`,`seq_file_type`,`cycle_number`,`number`,`isIndex`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$iSeqFileType}','{$iCycleNumber}','{$iNumber}','{$sIndex}','{$dAddedOn}',1);";
                }
            
            $sResult = $conn->multi_query($sIQuery);
            if($sResult != FALSE){
                return true;
            }else{
                return false;
            }
        }
    }
//!! function to add imp info of Xml files
    function addImportantXMLInfo($iSeqDirectoryid,$iSeqFileType,$aRunMaster){
        if(!empty($aRunMaster)){
                $DBMan = new DBConnManager();
                $conn =  $DBMan->getConnInstance();
                $sTable = DATABASE_TABLE_PREFIX."_sequencer_xmlread_master_info";
                $dAddedOn = date('Y-m-d H:i:s');
                $sIQuery ='';
                foreach ($aRunMaster as $key => $aValue) {
                    $sXMLTagLabel = $aValue[0];
                    $sXMLTagValue = $aValue[1];
                    
                    $sIQuery .= "INSERT INTO {$sTable} (`id_master`,`seq_directory_id`,`seq_file_type`,`xml_tag_label`,`xml_tag_value`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$iSeqFileType}','{$sXMLTagLabel}','{$sXMLTagValue}','{$dAddedOn}',1);";
                }

            $sResult = $conn->multi_query($sIQuery);
            
            if($sResult != FALSE){
                return true;
            }else{
                return false;
            }           
        }  
    }
//!! Function will add the sample summary info got from the xml
    function addXMLSampleSummaryInfo($iSequencerDirID,$iSeqFileType,$aSampleInfo){
        if(!empty($aSampleInfo)){
                $DBMan = new DBConnManager();
                $conn =  $DBMan->getConnInstance();
                $sTable = DATABASE_TABLE_PREFIX."_sequencer_xml_samplesummary_info";
                $dAddedOn = date('Y-m-d H:i:s');
                $sIQuery ='';
                foreach ($aSampleInfo as $key => $aValue) {
                    $iSampleID = $aValue['SampleID'];
                    $sSampleName = $aValue['SampleName'];
                    $sSampleNumber = $aValue['SampleNumber'];
                    $sNumberOfClustersPF = $aValue['NumberOfClustersPF'];
                    $sNumberOfClustersRaw = $aValue['NumberOfClustersRaw'];
                    $sPercentQ30 = $aValue['PercentQ30'];
                    
                    
                    $sIQuery .= "INSERT INTO {$sTable} (`id`,`seq_directory_id`,`seq_file_type`,`sample_id`,`sample_name`,`sample_number`,`sample_numberofclusterspf`,`sample_numberofclustersraw`,`sample_percentQ30`,`added_on`,`status`) VALUES(NULL,'{$iSequencerDirID}','{$iSeqFileType}','{$iSampleID}','{$sSampleName}','{$sSampleNumber}','{$sNumberOfClustersPF}','{$sNumberOfClustersRaw}','{$sPercentQ30}','{$dAddedOn}',1);";
                }

            $sResult = $conn->multi_query($sIQuery);
            
            if($sResult != FALSE){
                return true;
            }else{
                return false;
            }           
        }
    }

//!!Function to add All XML file information as JSON Object into the database
    function addXMLFileInfoMaster($iSequencerDirID,$iSeqFileType,$sRunInfoJSON){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_xml_info_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`xml_info_master_id`,`seq_directory_id`,`seq_file_type`,`seq_xml_file_info_object`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSequencerDirID}','{$iSeqFileType}','{$sRunInfoJSON}','{$dAddedOn}',1)";
       
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }

    /// Cronjob log
    function fLogger($iFlag,$sLogMsg){
        // if($iFlag==99){
        //     $sLogFile = __DIR__.'/'.'sequencer_test_extraction.log';
        //     $fh = fopen($sLogFile, 'a');
        //     fwrite($fh, $sLogMsg);
        //     fclose($fh);    
        // }
        if($iFlag==1){
            $sLogFile = __DIR__.'/'.'sequencer_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==2){
            $sLogFile = __DIR__.'/'.'sequencer_samplesheet_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==3){
            $sLogFile = __DIR__.'/'.'sequencer_runparameter_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==4){
            $sLogFile = __DIR__.'/'.'sequencer_runfastqstatics_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==5){
            $sLogFile = __DIR__.'/'.'sequencer_completedjobinfo_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==6){
            $sLogFile = __DIR__.'/'.'sequencer_runInfo_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==7){
            $sLogFile = __DIR__.'/'.'sequencer_completionjobstatus_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==8){
            $sLogFile = __DIR__.'/'.'sequencer_demultiplex_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==10){
            $sLogFile = __DIR__.'/'.'sequencer_interact_limsdx_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==11){
            $sLogFile = __DIR__.'/'.'sequencer_galaxy_workflow.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==21){
            $sLogFile = __DIR__.'/'.'sequencer_read_completionjob_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==23){
            $sLogFile = __DIR__.'/'.'sequencer_read_samplesheet_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==22){
            $sLogFile = __DIR__.'/'.'sequencer_read_runfastqstatics_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==25){
            $sLogFile = __DIR__.'/'.'sequencer_read_completedjobinfo_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==24){
            $sLogFile = __DIR__.'/'.'sequencer_read_runparameter_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==26){
            $sLogFile = __DIR__.'/'.'sequencer_read_runinfo_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==27){
            $sLogFile = __DIR__.'/'.'sequencer_read_demultiplex_cron.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==15){
            $sLogFile = __DIR__.'/'.'sequencer_interop_summary.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==16){
            $sLogFile = __DIR__.'/'.'sequencer_summary_log.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==17){
            $sLogFile = __DIR__.'/'.'sequencer_bcl2fast_log.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==18){
            $sLogFile = __DIR__.'/'.'sequencer_read_summary.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }

        // Extraction Log
        
        if($iFlag==31){
            $sLogFile = __DIR__.'/'.'sequencer_extraction.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==32){
            $sLogFile = __DIR__.'/'.'sequencer_extraction_info.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
        if($iFlag==33){
            $sLogFile = __DIR__.'/'.'sequencer_extraction_readk.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }

        // scan the fastq files
        if($iFlag==19){
            $sLogFile = __DIR__.'/'.'sequencer_scan_fastq.log';
            $fh = fopen($sLogFile, 'a');
            fwrite($fh, $sLogMsg);
            fclose($fh);    
        }
    }

    //!! This function is going to return the array of the whole xml contents
    function xml2array ( $xmlObject, $aOutput = array () )
    {
        foreach ( (array) $xmlObject as $index => $aNode )
          $aOutput[$index] = ( is_object ( $aNode ) ) ? xml2array ( $aNode ) : $aNode;
          return $aOutput;
    }

/*
    Functions for InterOp Directory work
    1. Function will give the directories which not gone to parsing the binary files
*/
    function fAddSequencerParseInterOpStatus($iSequencerDirID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_master_interop`,`seq_directory_id`,`seq_parse_status`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSequencerDirID}','{$iStatus}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }

    function fGetSequencerForParseInterOp(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sSQuery = "SELECT * FROM `{$sTable}` AS a 
                    LEFT JOIN `{$sParseTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    WHERE a.`status`=1 AND b.`seq_parse_status`=1 AND b.`status`=1";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

    function fGetSequencerForParsedInterOpCSV(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_status';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';

        $dAddedOn = date('Y-m-d H:i:s');

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_parse_status`,c.`seq_parse_file_path`,c.`seq_parsed_filename` 
                    FROM `{$sTable}` AS a 
                    LEFT JOIN `{$sParseTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    LEFT JOIN `{$sMasterTable}` AS c ON c.`seq_directory_id`=a.`seq_directory_id`
                    WHERE a.`status`=1 AND b.`seq_parse_status`=2 AND b.`status`=1 AND c.`seq_parse_type`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

    function fDisableParseInterOpStatus($iSeqDirectoryid,$iStatus=0){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_status';
        
        if($iStatus > 0){
            $sUQuery = "UPDATE {$sTable} SET `status`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}' AND `seq_parse_status`='{$iStatus}' ";
        }else{
            $sUQuery = "UPDATE {$sTable} SET `status`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}'";
        }
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }

    function fAddInterOpCSVJSON($iSeqDirectoryID,$iSeqInterOpType,$sSummaryJSON){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_interop_csv_json';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`seq_json_master_id`,`seq_directory_id`,`seq_interop_type`,`seq_summary_json`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSeqDirectoryID}','{$iSeqInterOpType}','{$sSummaryJSON}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;       
    }

    function fGetSequencerToReadParsedInterOpSummaryCSV(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_status';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';

       $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_parse_file_path`,a.`seq_parse_type`,a.`seq_local_filepath`,a.`seq_parsed_filename`,b.`seq_parse_status`,c.`seq_dir_name`,c.`seq_type`  FROM `{$sTable}` AS a 
                    LEFT JOIN `{$sParseTable}` AS b ON a.`seq_directory_id`=b.`seq_directory_id` AND b.`seq_parse_status`=3
                    LEFT JOIN `{$sMasterTable}` AS c ON a.`seq_directory_id`= c.`seq_directory_id`
                    WHERE b.`status`=1 AND c.`status`=1 AND a.`seq_parse_type`=1";
        
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

    function fAddSequencerParseInterOpMasterInfo($iSequencerDirID,$iSeqParseType,$sRemoteCopiedPath,$sNewFileName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

       $sIQuery = "INSERT INTO {$sTable} (`id_info_interop`,`seq_directory_id`,`seq_parse_type`,`seq_parse_file_path`,`seq_parsed_filename`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSequencerDirID}','{$iSeqParseType}','{$sRemoteCopiedPath}','{$sNewFileName}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
    function fUpdateSequencerInterOpMasterInfo($iSequencerDirID,$iSeqParseType,$sLocalPath){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';
        
        
        $sUQuery = "UPDATE {$sTable} SET `seq_local_filepath`= '{$sLocalPath}' WHERE `seq_directory_id`='{$iSequencerDirID}' AND `seq_parse_type`='{$iSeqParseType}'";
        
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Function is here to check the entry of sequencer in parse master table
    function fCheckSequencerInInterOpMaster($iSequencerID , $iParseType){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';
        

        $sSQuery = "SELECT * FROM `{$sParseTable}` WHERE `seq_directory_id` = '{$iSequencerID}' AND `seq_parse_type` = '{$iParseType}'";
        
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return false;
                }else{
                    return true;
                }
                
            }
        }       
    }
//!! Function to check into file master
    function fCheckFileInfoExistInFileMaster($iSeqDirectoryid,$iSeqFileType){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sFileTable = DATABASE_TABLE_PREFIX.'_sequencer_files_master';
        

        $sSQuery = "SELECT * FROM `{$sParseTable}` WHERE `seq_directory_id` = '{$iSeqDirectoryid}' AND `seq_file_type` = '{$iSeqFileType}'";
        
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return false;
                }else{
                    return true;
                }
                
            }
        }         
    }

/*
    Functions to Deal with the ExtractionMetrics
*/
//!! Function to add status for the extraction workflow
    function fAddSequencerInterOpExtractionStatus($iSequencerDirID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_extraction_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_extraction_master`,`seq_directory_id`,`seq_extraction_status`,`added_on`,`status`) 
                    VALUES(NULL,'{$iSequencerDirID}','{$iStatus}','{$dAddedOn}',1)";

        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! function to get sequencer for parse the ExtractionMetrics
    function fGetSequencerForInterOpExtraction(){
        $aSequencers = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_extraction_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sSQuery = "SELECT * FROM `{$sTable}` AS a 
                    LEFT JOIN `{$sParseTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    WHERE a.`status`=1 AND b.`seq_extraction_status`=1 AND b.`status`=1";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
//!! Function to disable the status for sequencer
    function fDisableInterOpExtractionStatus($iSeqDirectoryid,$iStatus=0){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_extraction_status';
        
        if($iStatus==0){
            $sUQuery = "UPDATE {$sTable} SET `status`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}'";
        }else{
            $sUQuery = "UPDATE {$sTable} SET `status`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}' AND `seq_extraction_status`='{$iStatus}' ";
        }
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Function to get the sequencer info of which extraction csv is generated
    function fGetSequencerForParsedInterOpExtractionCSV(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_extraction_status';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';

        $dAddedOn = date('Y-m-d H:i:s');

        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_extraction_status`,c.`seq_parse_file_path`,c.`seq_parsed_filename` 
                    FROM `{$sTable}` AS a 
                    LEFT JOIN `{$sParseTable}` AS b ON b.`seq_directory_id`=a.`seq_directory_id`
                    LEFT JOIN `{$sMasterTable}` AS c ON c.`seq_directory_id`=a.`seq_directory_id`
                    WHERE a.`status`=1 AND b.`seq_extraction_status`=2 AND b.`status`=1 AND c.`seq_parse_type`=2";
        
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
//!! Function to get sequencer information for read the Extraction CSV
    function fGetSequencerToReadParsedInterOpExtractionCSV(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sParseTable = DATABASE_TABLE_PREFIX.'_sequencer_extraction_status';
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_parse_interop_master';
        $sMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        
        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_parse_file_path`,a.`seq_local_filepath`,a.`seq_parsed_filename`,b.`seq_extraction_status`,c.`seq_type` FROM `{$sTable}` AS a 
            LEFT JOIN `{$sParseTable}` AS b ON a.`seq_directory_id`=b.`seq_directory_id` AND b.`seq_extraction_status`=3
            LEFT JOIN `{$sMasterTable}` AS c ON a.`seq_directory_id`= c.`seq_directory_id` 
            WHERE b.`status`=1 AND c.`status`=1 AND a.`seq_parse_type`=2";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

/*
    Function to get all parameters of sequencer , which are stored through reading the xml
*/
    function fGetParametersForSequencer($iSeqDirectoryid){
        $aParameters = array();
        $aXMLInfo = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sXMLInfoTable = DATABASE_TABLE_PREFIX.'_sequencer_xmlread_master_info';
        
        $sXMLSQuery = "SELECT * FROM `{$sXMLInfoTable}` WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        $sResult = $conn->query($sXMLSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aXMLInfo[]= $aRow;
                }
            }
        }
        
        $aParameters['xml_info'] = $aXMLInfo;
        $aCSVInfo = fGetSampleSheetInfoForSequencer($iSeqDirectoryid);
        
        $aParameters['csv_info'] = $aCSVInfo;
        if(!empty($aCSVInfo)){
            $iSampleSheetInfoMasterID = $aCSVInfo['seq_samplesheet_master_id'];
            $aSampleInfo = fGetSampleInformationForSequencer($iSampleSheetInfoMasterID);
            $aParameters['csv_sample_info'] = $aSampleInfo;
        }
        return $aParameters;
    }
//!! Function to get sampleSheetInfo for the sequencer
    function fGetSampleSheetInfoForSequencer($iSeqDirectoryid){
        $aCSVInfo = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sCSVInfoTable = DATABASE_TABLE_PREFIX.'_sequencer_samplesheet_info_master';
        $sCSVSQuery = "SELECT * FROM `{$sCSVInfoTable}` WHERE `seq_directory_id` = '{$iSeqDirectoryid}' ";
        $sResult = $conn->query($sCSVSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $aCSVInfo= $aRow;
            }
        }
        return $aCSVInfo;
    }
//!! Function to get the sample information of the sequencer
    function fGetSampleInformationForSequencer($iSampleSheetInfoMasterID){
        $aSequencers = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_sample_info_master';
        
        if($iSampleSheetInfoMasterID > 0){
            $sQuery = "SELECT * FROM `{$sTable}` WHERE `seq_samplesheet_master_id` = {$iSampleSheetInfoMasterID}";
            $sResultMain = $conn->query($sQuery);
            if($conn != false){
                if($sResultMain != FALSE){
                    while($aRowMain = $sResultMain->fetch_assoc()){
                        $aSequencers[] = $aRowMain;
                    }
                    return $aSequencers;
                }
            }else{
                return $aSequencers;
            }
        }else{
            return $aSequencers;
        }        
    }

//!! Function to check the sequencer is completed or NOT
//!! If Completed return True Else False
    function fCheckSequencerCompleted($iSeqDirectoryID , $iSeqType){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';

        $sQuery = "SELECT * FROM `{$sStatusTable}` WHERE `seq_directory_id` = '{$iSeqDirectoryID}'";
        if($iSeqType==1){
            $sQuery.= " AND `seq_status`=101";
        }elseif($iSeqType==2){
            $sQuery.= " AND `seq_status`=11";
        }
        $sResult = $conn->query($sQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return true;
                }else{
                    return false;
                }
            }
            
        }
    }
/*
    The function is return the aray of sequencer info of NextSeq which alrdy got the RunCompletionStats.xml file of it 10.
*/
    function fGetSequencerForBCL2FASTQProgram(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        
        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` 
                WHERE 
                a.`seq_type`=2 
                AND b.`seq_directory_id`  NOT IN
                    (SELECT seq_directory_id FROM `{$sStatusTable}` WHERE seq_status=20)
                AND b.`seq_status`=10";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }

//!! Function to add status for sequencer of NextSeq for the bcl2fast program runs
    function fAddStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryid,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_nextseq_bcl2fastq_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_master_status`,`seq_directory_id`,`seq_log_status`,`added_on`,`isValid`) VALUES(NULL,'{$iSeqDirectoryid}','{$iStatus}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to disable the status for sequencer of bcl2fastQ
    function fDisableStatusForRunningSequencerBCL2FASTQProgram($iSeqDirectoryid){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_nextseq_bcl2fastq_status';
        
        $sUQuery = "UPDATE {$sTable} SET `isValid`= 0 WHERE `seq_directory_id`='{$iSeqDirectoryid}' ";
        
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
/*
    Function to get the running NextSeq sequencer for BCL2FASTQ program
*/
    function fGetSequencerForRunningBCL2FASTQProgram(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        $sStatusLogTable = DATABASE_TABLE_PREFIX.'_sequencer_nextseq_bcl2fastq_status';
        
        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid`,c.`seq_log_status` FROM `{$sTable}` as a 
                LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` 
                LEFT JOIN `{$sStatusLogTable}` AS c ON c.`seq_directory_id`= a.`seq_directory_id`
                WHERE 
                a.`seq_type`=2 
                AND c.`seq_log_status`=1 AND c.`isValid`=1";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
//!! Function to add the Log Msg of BCL@FAST PRogram
    function fAddLogStatusMessageForBCL2FASTQProgram($iSeqDirectoryid,$iLogType,$sLogMsg){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_bcl2log_log_msg';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_bcl2_master`,`seq_directory_id`,`seq_msg_type`,`seq_log_masg`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$iLogType}','{$sLogMsg}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;  
    }
//!! Function to check the count for Error Status of BCL2FASTQ
    function fCheckCountErrorStatusForSequencer($iSeqDirectoryid){
        $iTotal =0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_nextseq_bcl2fastq_status';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "SELECT count(*) as `total` FROM {$sTable} WHERE `seq_directory_id` = '{$iSeqDirectoryid}' AND `seq_log_status`=2 ";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iTotal = $aRow['total'];
            }
        }
        return $iTotal;        
    }

//!! Function to Store the fastq output information
    function addFastqFileMasterInfo($iSeqDirectoryid,$sSeqOutputPath,$sOutputFileName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_fastq_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_fastq_master`,`seq_directory_id`,`seq_output_path`,`seq_output_filename`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$sSeqOutputPath}','{$sOutputFileName}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to add the Error Occured sequencer information
    function fAddSequencerErrorOccured($iSeqDirectoryid,$iSeqType,$sErrorDesc){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_error_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_error_master`,`sequencer_id`,`seq_type`,`error_msg`,`added_on`,`status`) VALUES(NULL,'{$iSeqDirectoryid}','{$iSeqType}','{$sErrorDesc}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID; 
    }
//!! Function to check the sequencer is in Error Master
    function fCheckErrorMasterEntryForSequencer($iSeqDirectoryid){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_error_master';

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `sequencer_id`= '{$iSeqDirectoryid}' AND `status`=1";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(empty($aRow)){
                    return true;
                }else{
                    return false;
                }
            }
        }         
    }
//!! Function to get Sequencer which Samplesheet is Read by Middleware
    function fGetSequencerReadSampleSheet(){
        $aSequencers = array();

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_master';
        $sStatusTable = DATABASE_TABLE_PREFIX.'_sequencer_master_status';
        
        $sSQuery = "SELECT a.`seq_directory_id`,a.`seq_type`,a.`seq_dir_name`,a.`seq_server_path`,b.`seq_status`,b.`isValid` FROM `{$sTable}` as a 
                LEFT JOIN `{$sStatusTable}` AS b ON b.`seq_directory_id`= a.`seq_directory_id` AND b.`seq_status`=5";
                    
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSequencers[]= $aRow;
                }
            }
            return $aSequencers;
        }else{
            return $aSequencers;
        }
    }
/*
    Manage The Test and mapping with Samples
    1. function to add Test
    2. function to check the Test is alrdy added
    3. function to Freeze The Test
    4. function to Unfreeze The Test
    5. function to Map the Sample with Test
    6. function to get the mapped samples with Test
    7. function to map Test with Workflow
*/

//!! Function to add the TestName in Master
    function fAddTestMaster($sTestName){
        $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTestMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_test_master';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sTestMasterTable}`(`test_id`,`test_name`,`added_on`,`status`)
                    VALUES (NULL,'{$sTestName}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;
    }

//!! Check the test is added or not , IF ADDED RETURN True else false
    function fCheckTestAdded($sTestName){
        $iTestID = 0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTestMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_test_master';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sSQuery = "SELECT * FROM `{$sTestMasterTable}` WHERE `test_name` = '{$sTestName}' AND `status`=1";
        
        $sResult = $conn->query($sSQuery);  
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return $aRow['test_id'];
                }else{
                    return $iTestID;
                }
            }
        }
    }
//!! Freeze The Test
    function fFreezTest($iTestID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTestMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_test_master';
        $sUQuery = "UPDATE `{$sTestMasterTable}` SET `status`='0' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! UnFreeze The Test
    function fUnFreezTest($iTestID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sTestMasterTable = DATABASE_TABLE_PREFIX.'_sequencer_test_master';
        $sUQuery = "UPDATE `{$sTestMasterTable}` SET `status`='1' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
//!! Function to Map the Test and sample
    function fMapSampleWithTest($iTestID,$iSampleInfoID){
        $iInsertID=0;
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sMapTable = DATABASE_TABLE_PREFIX.'_sequencer_sample_test_mapping';
        $dAdedOn=date(DB_DATETIME_FORMATE);
        $sIQuery = "INSERT INTO `{$sMapTable}`(`mapp_id`,`sample_info_table_id`,`test_id`,`mapped_on`,`status`)
                    VALUES (NULL,'{$iSampleInfoID}','{$iTestID}','{$dAdedOn}','1')";
        
        $sResult = $conn->query($sIQuery);        
        if($sResult){
            $iInsertID = $conn->insert_id;
        }
        return $iInsertID;  
    }
//!! Function to get sampleinfoID of table by testID
    function fGetMappedSampleIDForTestID($iTestID){
        $aSampleID=array();
        $sTestSampleMapTable = DATABASE_TABLE_PREFIX.'_sequencer_sample_test_mapping';
        $sSQuery = "SELECT `sample_info_table_id` FROM `{$sTestSampleMapTable}` WHERE `test_id`='{$iTestID}' AND `status`=1";
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aSampleID[]=$aRow['sample_info_table_id'];
                }
            }
        }
        return $aSampleID;
    }
//!! Function to Map Test and Workflow
    function fMapWorkflowWithTest($iTestID,$iWorkflowID){
        $bResult = fUnMapWorkflowForTest($iTestID);
        $iInsertID=0;
        if($bResult){
            $DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sMapTable = DATABASE_TABLE_PREFIX.'_sequencer_test_workflow_mapping';
            $dAdedOn=date(DB_DATETIME_FORMATE);
            $sIQuery = "INSERT INTO `{$sMapTable}`(`map_id`,`test_id`,`workflow_id`,`mapped_on`,`status`)
                        VALUES (NULL,'{$iTestID}','{$iWorkflowID}','{$dAdedOn}','1')";
            
            $sResult = $conn->query($sIQuery);        
            if($sResult){
                $iInsertID = $conn->insert_id;
            }
            return $iInsertID;  
        }else{
            return $iInsertID;
        }
        
    } 
    
    function fUnMapWorkflowForTest($iTestID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sMappedTable = DATABASE_TABLE_PREFIX.'_sequencer_test_workflow_mapping';
        $sUQuery = "UPDATE `{$sMappedTable}` SET `status`='0' WHERE `test_id`='{$iTestID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }
    }
/*
    Function to maintain the Sequencer and Galaxy
    1. function to add the workflow
*/

//!! Function to ADD galaxy Workflow
    function fAddGalaxyWorkflow($sGalaxyWorkflowID , $sGalaxyWorkflowName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_workflow';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_sequencer_workflow`,`galaxy_workflow_id`,`galaxy_workflow_name`,`added_on`,`status`) VALUES(NULL,'{$sGalaxyWorkflowID}','{$sGalaxyWorkflowName}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to add Data-Library for workflow
    function fAddGalaxyDataLibrary($sLibraryID , $sLibraryName , $sLibraryDesc){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_galaxy_library`,`galaxy_library_id`,`galaxy_library_name`,`galaxy_library_desc`,`added_on`,`status`) VALUES(NULL,'{$sLibraryID}','{$sLibraryName}','{$sLibraryDesc}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }

//!! Function to add Folder inside Library for workflow
    function fAddGalaxyDataLibraryFolder($sLibraryID ,$sFOlderID,$sFolderName , $sFolderDesc){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_galaxy_folder`,`galaxy_library_id`,`galaxy_folder_id`,`galaxy_folder_name`,`galaxy_folder_desc`,`added_on`,`status`) VALUES(NULL,'{$sLibraryID}','{$sFOlderID}','{$sFolderName}','{$sFolderDesc}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to check the foldername is alrdy created or NOT , Pass decoded Library iD
    function fCheckFolderNameExist($sFolderName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_master';
        $sQuery = "SELECT * FROM `{$sTable}` WHERE `galaxy_folder_name` = '{$sFolderName}'";

        $sResult = $conn->query($sQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return true;
                }else{
                    return false;
                }
            }
            
        }
    }
//!! Function to Get the FolderID for FolderName
    function fGetFolderIDByFolderName($sFolderName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_master';
        $sQuery = "SELECT * FROM `{$sTable}` WHERE `galaxy_folder_name` = '{$sFolderName}'";

        $sResult = $conn->query($sQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                if(!empty($aRow)){
                    return $aRow['galaxy_folder_id'];
                }else{
                    return 0;
                }
            }
            
        }
    }
//!! Function to add information of uploaded fastq files ad DATASET
    // galaxy_dataset_type , 1 for Reference And 2 for Fastq
    function fAddGalaxyDataSet($sLibraryID ,$iDatasetType,$sFolderID , $sFastqFilePath ,$sDataSetID ){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_dataset_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_galaxy_dataset`,`galaxy_dataset_type`,`galaxy_library_id`,`galaxy_folder_id`,`galaxy_fastq_path`,`galaxy_dataset_id`,`added_on`,`status`) VALUES(NULL,'{$iDatasetType}','{$sLibraryID}','{$sFolderID}','{$sFastqFilePath}','{$sDataSetID}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;
    }
//!! Function to get DataSetID for Library & Folder (both ID are deocdes in Galaxy)
    function fGetDataSetIDForLibraryAndFolderID($sLibraryID,$sFolderID){
        $aDataSetID=array();
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_dataset_master';
        $sSQuery = "SELECT `id_galaxy_dataset`,`galaxy_dataset_type`,`galaxy_dataset_id` FROM `{$sTable}` WHERE `galaxy_library_id`='{$sLibraryID}' AND `galaxy_folder_id` = '{$sFolderID}' AND `status`=1";
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $iGalaxyDatasetTableID =$aRow['id_galaxy_dataset'];
                    $iGalaxyDatasetType =$aRow['galaxy_dataset_type'];
                    $iGalaxyDatasetID =$aRow['galaxy_dataset_id'];
                    $aDataSetID=array(
                        'iGalaxyDatasetTableID'=>$iGalaxyDatasetTableID,
                        'iGalaxyDatasetID'=>$iGalaxyDatasetID,
                        'iGalaxyDatasetType'=>$iGalaxyDatasetType
                        );
                }
            }
        }
        return $aSampleID;
    }
//!! Function to add History
    function fAddGalaxyHistory($sHistoryID,$sHistoryName){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

       $sIQuery = "INSERT INTO {$sTable} (`id_galaxy_history`,`galaxy_history_id`,`galaxy_history_name`,`added_on`,`status`) VALUES(NULL,'{$sHistoryID}','{$sHistoryName}','{$dAddedOn}',1)";
       
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to get Active History
    function fGetHistoryActive(){
        $aHistory = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_master';

        $sSQuery = "SELECT `id_galaxy_history`,`galaxy_history_id`,`galaxy_history_name` FROM `{$sTable}` WHERE `status`=1";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aHistory[]=$aRow;
                }
            }
        }
        return $aHistory;
    }
//!! Function to update the history status / while its completed or Deleted
    function fUpdateHistoryStatus($sHistoryID,$iStatus){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_master';

        $sUQuery = "UPDATE `{$sTable}` SET `status`='{$iStatus}' WHERE `galaxy_history_id`='{$sHistoryID}'";
        $sResult = $conn->query($sUQuery);
        if($sResult){
            return true;
        }else{
            return false;
        }

    }
//!! Function to add OutputID for History
    function fAddHistoryOutputID($iTableID,$iIndex,$sOutputID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_output';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_history_output`,`galaxy_history_table_id`,`galaxy_history_output_index`,`galaxy_history_output_id`,`added_on`,`status`) VALUES(NULL,'{$iTableID}','{$iIndex}','{$sOutputID}','{$dAddedOn}',1)";
       
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID; 
    }
//!! Function to chekc the output entry for history
    function fCheckHistoryOutputEntry($iHistoryTableID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_output';

        $sSQuery = "SELECT COUNT(*) as count FROM `{$sTable}` WHERE `galaxy_history_table_id`='{$iHistoryTableID}' ";
        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $iCount=$aRow['count'];
            }
        }
        if($iCount > 0){
            return true;
        }else{
            return false;
        }
    }
//!! Function to add history & dataset mapping
    function fAddHistoryDatasetMapping($iHistoryTableID ,$sDataSetID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_dataset_master';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_history_dataset`,`galaxy_history_table_id`,`galaxy_dataset_id`,`added_on`,`status`) VALUES(NULL,'{$iHistoryTableID}','{$sDataSetID}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;        
    }

//!! Function to get the ReferenceData 
    function fGetReferenceDataSetIDForLibraryAndFolder($sReferenceLibraryID,$sReferenceLibraryFolderID){
        $aDataSet = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_library_folder_dataset_master';
        $sSQuery = "SELECT * FROM {$sTable} WHERE `galaxy_dataset_type`=1 AND `galaxy_library_id`='{$sReferenceLibraryID}' AND `galaxy_folder_id`='{$sReferenceLibraryFolderID}'";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                while($aRow = $sResult->fetch_assoc()){
                    $aDataSet[] =$aRow['galaxy_dataset_id'];
                }
            }
        }
        return $aDataSet;
    }

//!! Add The Sample,fastq info for History
    function fAddGalaxyHistorySample($sHistoryID,$sSampleName,$sForwardDataSetID,$sReverseDataSetID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_info';
        
        $dAddedOn = date('Y-m-d H:i:s');

        $sIQuery = "INSERT INTO {$sTable} (`id_history_info`,`galaxy_history_id`,`galaxy_forward_dataset_id`,`galaxy_reverse_dataset_id`,`sample_name_barocde`,`added_on`,`status`) VALUES(NULL,'{$sHistoryID}','{$sForwardDataSetID}','{$sReverseDataSetID}','{$sSampleName}','{$dAddedOn}',1)";
        
        $sResult = $conn->query($sIQuery);
        if($conn != false){
            if($sResult != FALSE){
                $iInsertID = $conn->insert_id;
            }
        }
        return $iInsertID;         
    }
//!! Function to Get the SampleNameBarocode Used For History
    function fGetSampleNameForHistory($sGalaxyHistoryID){
        $sSampleNameBarCode = '';
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX.'_sequencer_galaxy_history_info';
        $sSQuery = "SELECT `sample_name_barocde` FROM {$sTable} WHERE `galaxy_history_id`='{$sGalaxyHistoryID}' AND `status`=1";

        $sResult = $conn->query($sSQuery);
        if($conn != false){
            if($sResult != FALSE){
                $aRow = $sResult->fetch_assoc();
                $sSampleNameBarCode =$aRow['sample_name_barocde'];
                return $sSampleNameBarCode;
                
            }
        }
        return $sSampleNameBarCode;        
    }
?>