<?php
//! Use autoLoading of classes incase required classes are not included on the page'
function __autoload($class_name) {
    include_once __DIR__.'/classes/class.' . $class_name . '.php';
}
spl_autoload_register('__autoload');
?>