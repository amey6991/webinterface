<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."custom_autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."sessChk.php";
require_once __DIR__.DIRECTORY_SEPARATOR."funUsers.php";
require_once __DIR__.DIRECTORY_SEPARATOR."functions.php";


$sPageTitle = "User Profile";
$iUserID = $_GET['iUserID'];

$aUserTypes = fGetUserTypes();

$oSession = new SessionManager();
$iUserID = $oSession->iUserID;
$iUserTypeID = $oSession->iUserTypeId;
$iUserClientID = $oSession->iUserClientID;

$aUsers =fGetUserDetail($iUserID);
//print_r($aUsers);exit();
// if($iUserTypeID==2){
//     $aUsers = fGetAllUsers($iUserClientID);    
// }else{
//     $aUsers = fGetAllUsers();
// }


$iCountUsers = count($aUsers);

include_once "mxcelHeaderBase.php";
include_once "mxcelHeaderApp.php";
$aBreadcrumb = array(
                array("title"=>"DashBoard","link"=>"dashboard.php","isActive"=>false),
                array("title"=>"View Profile","link"=>"viewProfile.php?iUserID={$iUserID}","isActive"=>true)
            );
?>

<!-- Main Content -->
<div class="container-fluid">
    <div class="side-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <ol class="breadcrumb navbar-breadcrumb">
                                <?php echo parseBreadcrumb($aBreadcrumb); ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                       <div class="card-body">
                            <form class="form-horizontal" method="POST" id="idFormAdduser" action="em_update_user.php">
                                <div class="form-group">
                                    <label for="idInputFirstName" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['first_name'];?></label>
                                        <input type="hidden" name="iuserID" value="<?php echo $aUsers['user_id'];?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="idInputLastName" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['last_name'];?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="idInputLastName" class="col-sm-2 control-label">User Type</label>
                                    <div class="col-md-4">
                                        <?php
                                            if($aUsers['user_type_id']==1){
                                        ?>
                                            <label>Super Admin</label>
                                        <?php
                                            }elseif($aUsers['user_type_id']==2){
                                        ?>
                                            <label>Admin</label>
                                        <?php
                                            }else{
                                        ?>
                                            <label>User</label>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputStateName" class="col-sm-2 control-label">State</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['state'];?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputCityName" class="col-sm-2 control-label">City</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['city'];?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['address'];?></label> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMobileNumber" class="col-sm-2 control-label">Mobile no</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['mobile_no'];?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-md-4">
                                        <label><?php echo $aUsers['email'];?></label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
<script type="text/javascript">
    $(document).ready(function(){
       
    });
</script>

<?php
//! Include footer files
include_once "mxcelFooterApp.php";
include_once "mxcelFooterBase.php";
?>